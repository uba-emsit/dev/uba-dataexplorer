Tips to improve your dev experience on UBA machines, applies to both regular old desktops and SINA laptops.

## Proxy

Make sure to properly configure the proxy settings, otherwise essential tools (such as git) will not function properly.
For example, your global `.gitconfig` in the root folder of your personal drive `P:` could look like this:

```ini
[user]
	name = <your name>
	email = <your mail address>
[http]
	proxy = http://proxyde.uba.de:8080
[safe]
	directory = C:/Program Files/<user-writable folder> (details below)
```

If any tool action fails or takes too long, a bad proxy setting is usually to blame. 

## Executables

All UBA machines prevent non-admin users to execute anything unless it is located under `C:\Program Files\*`. Thus, for proper development you will have to place your code in some folder under that root. Make sure that you have full access rights to that development folder and all its sub-folders.

When developing with Visual Studio and NuGet packages, you will encounter the problem that some of those also bring in executables. From the default NuGet package location in your personal folder those cannot run due to the restrictions explained above. To mitigate this problem, re-locate those packages to your folder under `C:\Program Files`.

```xml
<configuration>
  <config>
    <add key="globalPackagesFolder" value="C:\Program Files\<user-writeable folder>\NuGetPackages" />
    <add key="http_proxy" value="http://proxyde.uba.de:8080" />
  </config>
  <packageSources>
    ...
  </packageSources>  
</configuration>
```

### Happy coding!