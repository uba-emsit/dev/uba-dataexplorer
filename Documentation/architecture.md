# Architecture

The UBA Data Explorer is designed using the architecture patterns **MVVM** and **Clean Architecture**.

![DataExplorer architecture](Resources/dataexplorer-architecture.png)

The **MVVM pattern** helps to cleanly separate an application's business and presentation logic from its user interface (UI). Maintaining a clean separation between application logic and the UI helps address numerous development issues and makes an application easier to test, maintain, and evolve. It can also significantly improve code re-use opportunities and allows developers and UI designers to collaborate more easily when developing their respective parts of an app.

## MVVM Layers

At a high level, the view "knows about" the view model, and the view model "knows about" the model, but the model is unaware of the view model, and the view model is unaware of the view. Therefore, the view model isolates the view from the model, and allows the model to evolve independently of the view.

![CleanArchitecture-20240918-151808.jpg](https://external-content.duckduckgo.com/iu/?u=https%3A%2F%2Fpitchart.github.io%2Fddd-cqrs-mvc%2Fresources%2Fimg%2Fclean-architecture.png&f=1&nofb=1&ipt=5e321dcf116f406613b904b6b17657e95b66d9442513a3ce9fb445f0dfb7da9b&ipo=images)

**Clean Architecture** in the context of **MVVM** is about organizing code in a way that improves separation of concerns, testability, and maintainability. Clean Architecture focuses on the **independence** of different layers in an application, ensuring that the core business logic is not dependent on external frameworks or user interfaces. Compared to basic MVVM, it allows for a better separation of presentation based logic in the View Model and pure business logic in the Use Cases.

## Key Concepts:

1. **Entities**: These are the core business rules (e.g., domain models or objects) that are independent of anything else.
2. **Use Cases (or Interactors)**: These contain the application-specific business rules, managing the flow of data to and from entities as well as orchestrating actions between the ViewModel and the Model.
3. **ViewModel**: the ViewModel interacts with Use Cases to retrieve or process data. The ViewModel doesn't directly depend on the UI or specific framework elements. It transforms the data in a way that is suitable for the View (UI).
4. **UI (View)**: The View observes the ViewModel for updates but has no direct connection to business logic.

## How Clean Architecture works in MVVM:

- **Entities** and **Use Cases** are in the inner layers of the architecture, isolated from the framework or UI, ensuring they can be easily tested.
- The **ViewModel** interacts with Use Cases, following the principle of separation of concerns.
- The **UI (View)** depends on the ViewModel, making it easier to swap out or update UI components without impacting core business logic.

In summary, Clean Architecture in MVVM keeps the core logic and user interface independent, reducing coupling, improving testability, and maintaining flexibility for future changes.

Due to the requirement of providing multiple data storage backends, there is a need for another abstraction layer, i.e. a **model-entity map** between the main model section (repository) and the entities being used in the data storage backends such as Seven2one's Mesap Framework.

## Example: Read time series data

- User opens TimeseriesView "ABC" in "View" (Application)
- View notifies the ViewModel that user has opened the page with the TimeseriesView ABC
  - The View layer does not have any idea of the models or entities
  - The ViewModel contains the logic part of the UI
- The ViewModel requests data -> calls a Use Case, e.g. "loadTimeseriesDataByViewId" from the domain layer
- The Use Case calls TimeSeriesRepository and TimeseriesDataRepository and expects models (containing the requested data of the TimeseriesView ABC without knowing it is connected to the TimeSeriesView)
- The repository implementation (e.g. the Mesap Framework) retrieves the data and returns them as models to the Use Case
- The Use Case returns the models to the ViewModel as the result of the request
- ViewModel knows how to transform the models to the data which is to be presented in the View
- The View gets the data from the ViewModel via data binding (not the models themselves)
