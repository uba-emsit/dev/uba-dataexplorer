﻿namespace UBA.DataExplorer.UI;

public interface IDataGridHeader
{
    string DisplayText { get; set; }
}