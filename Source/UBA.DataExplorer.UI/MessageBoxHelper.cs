﻿using Avalonia.Controls;
using Avalonia.Threading;
using MsBox.Avalonia;
using MsBox.Avalonia.Enums;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UI;

public class MessageBoxHelper : IMessageBoxHelper
{
    // A static helper method for showing a custom error message box  

    public Task ShowErrorMsgBoxAsync(string title, string message) =>
        Dispatcher.UIThread.InvokeAsync(async () =>
        {
            await MessageBoxManager.GetMessageBoxCustom(
                new()
                {
                    ContentTitle = title,
                    ContentMessage = message,
                    ButtonDefinitions = [new() { Name = "OK" }],
                    Icon = Icon.Error,
                    WindowStartupLocation = WindowStartupLocation.CenterOwner,
                    CanResize = false,
                    MaxWidth = 500,
                    MaxHeight = 800
                })
                .ShowAsync();
        });

    /// <summary>
    /// Show a custom error message box for data loading errors
    /// </summary>
    /// <param name="entityType">Use Resources.EntityType.resx</param>
    /// <param name="error">The error from the load result (use Result.Error)</param>
    /// <returns></returns>
    public async Task ShowDataLoadErrorMsgBoxAsync(string entityType, Error error)
    {
        if (error == Error.Silent) return;
        await Dispatcher.UIThread.InvokeAsync(async () =>
        {
            await MessageBoxManager.GetMessageBoxCustom(
                    new()
                    {
                        ContentTitle = Resources.Resources.LoadData_Error_Title,
                        ContentMessage = string.Format(Resources.Resources.LoadData_Error_Description, error.Code,
                            error.Title, error.Description),
                        ButtonDefinitions = [new() { Name = "OK" }],
                        Icon = Icon.Error,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        CanResize = false,
                        MaxWidth = 500,
                        MaxHeight = 800
                    })
                .ShowAsync();
        });
    }

    /// <summary>
    /// Show a custom error message box for data loading errors
    /// </summary>
    /// <param name="entityType">Use Resources.EntityType.resx</param>
    /// <param name="count">The count of errors while loading the data</param>
    /// <returns></returns>
    public async Task ShowMultipleDataLoadErrorMsgBoxAsync(string entityType, int count)
    {
        await Dispatcher.UIThread.InvokeAsync(async () =>
        {
            var entityWord = count == 1 ? Resources.Resources.EntitySingular : Resources.Resources.EntityPlural;
            await MessageBoxManager.GetMessageBoxCustom(
                    new()
                    {
                        ContentTitle = Resources.Resources.LoadData_Error_Title,
                        ContentMessage = string.Format(Resources.Resources.LoadData_Error_MultipleErrors, count, entityWord),
                        ButtonDefinitions = [new() { Name = "OK" }],
                        Icon = Icon.Error,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        CanResize = false,
                        MaxWidth = 500,
                        MaxHeight = 800
                    })
                .ShowAsync();
        });
    }

    public async Task ShowDataWriteErrorMsgBoxAsync(string entityType, Error error)
    {
        if (error == Error.Silent) return;
        await Dispatcher.UIThread.InvokeAsync(async () =>
        {
            await MessageBoxManager.GetMessageBoxCustom(
                    new()
                    {
                        ContentTitle = Resources.Resources.WriteData_Error_Title,
                        ContentMessage = string.Format(Resources.Resources.LoadData_Error_Description, error.Code,
                            error.Title, error.Description),
                        ButtonDefinitions = [new() { Name = "OK" }],
                        Icon = Icon.Error,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        CanResize = false,
                        MaxWidth = 500,
                        MaxHeight = 800
                    })
                .ShowAsync();
        });
    }

    /// <summary>
    /// Show a custom error message box for data writing errors
    /// </summary>
    /// <param name="entityType">Use Resources.EntityType.resx</param>
    /// <param name="count">The count of errors while writing the data</param>
    /// <returns></returns>
    public async Task ShowMultipleDataWriteErrorMsgBoxAsync(string entityType, int count)
    {
        await Dispatcher.UIThread.InvokeAsync(async () =>
        {
            var entityWord = count == 1 ? Resources.Resources.EntitySingular : Resources.Resources.EntityPlural;
            await MessageBoxManager.GetMessageBoxCustom(
                    new()
                    {
                        ContentTitle = Resources.Resources.WriteData_Error_Title,
                        ContentMessage = string.Format(Resources.Resources.WriteData_Error_MultipleErrors, count, entityWord),
                        ButtonDefinitions = [new() { Name = "OK" }],
                        Icon = Icon.Error,
                        WindowStartupLocation = WindowStartupLocation.CenterOwner,
                        CanResize = false,
                        MaxWidth = 500,
                        MaxHeight = 800
                    })
                .ShowAsync();
        });
    }
}