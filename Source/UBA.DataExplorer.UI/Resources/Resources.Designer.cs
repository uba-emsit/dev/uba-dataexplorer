﻿//------------------------------------------------------------------------------
// <auto-generated>
//     This code was generated by a tool.
//     Runtime Version:4.0.30319.42000
//
//     Changes to this file may cause incorrect behavior and will be lost if
//     the code is regenerated.
// </auto-generated>
//------------------------------------------------------------------------------

namespace UBA.DataExplorer.UI.Resources {
    using System;
    
    
    /// <summary>
    ///   A strongly-typed resource class, for looking up localized strings, etc.
    /// </summary>
    // This class was auto-generated by the StronglyTypedResourceBuilder
    // class via a tool like ResGen or Visual Studio.
    // To add or remove a member, edit your .ResX file then rerun ResGen
    // with the /str option, or rebuild your VS project.
    [global::System.CodeDom.Compiler.GeneratedCodeAttribute("System.Resources.Tools.StronglyTypedResourceBuilder", "17.0.0.0")]
    [global::System.Diagnostics.DebuggerNonUserCodeAttribute()]
    [global::System.Runtime.CompilerServices.CompilerGeneratedAttribute()]
    public class Resources {
        
        private static global::System.Resources.ResourceManager resourceMan;
        
        private static global::System.Globalization.CultureInfo resourceCulture;
        
        [global::System.Diagnostics.CodeAnalysis.SuppressMessageAttribute("Microsoft.Performance", "CA1811:AvoidUncalledPrivateCode")]
        internal Resources() {
        }
        
        /// <summary>
        ///   Returns the cached ResourceManager instance used by this class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Resources.ResourceManager ResourceManager {
            get {
                if (object.ReferenceEquals(resourceMan, null)) {
                    global::System.Resources.ResourceManager temp = new global::System.Resources.ResourceManager("UBA.DataExplorer.UI.Resources.Resources", typeof(Resources).Assembly);
                    resourceMan = temp;
                }
                return resourceMan;
            }
        }
        
        /// <summary>
        ///   Overrides the current thread's CurrentUICulture property for all
        ///   resource lookups using this strongly typed resource class.
        /// </summary>
        [global::System.ComponentModel.EditorBrowsableAttribute(global::System.ComponentModel.EditorBrowsableState.Advanced)]
        public static global::System.Globalization.CultureInfo Culture {
            get {
                return resourceCulture;
            }
            set {
                resourceCulture = value;
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Value protection level.
        /// </summary>
        public static string AccessLevelMode {
            get {
                return ResourceManager.GetString("AccessLevelMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Confidential.
        /// </summary>
        public static string AccessLevelMode_Confidential {
            get {
                return ResourceManager.GetString("AccessLevelMode_Confidential", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Public.
        /// </summary>
        public static string AccessLevelMode_Public {
            get {
                return ResourceManager.GetString("AccessLevelMode_Public", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Analyst.
        /// </summary>
        public static string Analyst {
            get {
                return ResourceManager.GetString("Analyst", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Automatic.
        /// </summary>
        public static string Automatic {
            get {
                return ResourceManager.GetString("Automatic", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Create.
        /// </summary>
        public static string Create {
            get {
                return ResourceManager.GetString("Create", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created by.
        /// </summary>
        public static string CreatedBy {
            get {
                return ResourceManager.GetString("CreatedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Created on.
        /// </summary>
        public static string CreatedOn {
            get {
                return ResourceManager.GetString("CreatedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code: {0} {1}.
        ///
        ///Database: {3}
        ///
        ///Description: {2}.
        /// </summary>
        public static string DatabaseSelectorView_ConnectingError {
            get {
                return ResourceManager.GetString("DatabaseSelectorView_ConnectingError", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Error when opening the database.
        /// </summary>
        public static string DatabaseSelectorView_ConnectingErrorTitle {
            get {
                return ResourceManager.GetString("DatabaseSelectorView_ConnectingErrorTitle", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Additional databases:.
        /// </summary>
        public static string DatabaseSelectorView_MoreDatabases {
            get {
                return ResourceManager.GetString("DatabaseSelectorView_MoreDatabases", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No databases available..
        /// </summary>
        public static string DatabaseSelectorView_NoDatabases {
            get {
                return ResourceManager.GetString("DatabaseSelectorView_NoDatabases", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Delete.
        /// </summary>
        public static string Delete {
            get {
                return ResourceManager.GetString("Delete", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Deselect all.
        /// </summary>
        public static string DeselectAll {
            get {
                return ResourceManager.GetString("DeselectAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Display.
        /// </summary>
        public static string Display {
            get {
                return ResourceManager.GetString("Display", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edit.
        /// </summary>
        public static string Edit {
            get {
                return ResourceManager.GetString("Edit", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edited by.
        /// </summary>
        public static string EditedBy {
            get {
                return ResourceManager.GetString("EditedBy", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Edited on.
        /// </summary>
        public static string EditedOn {
            get {
                return ResourceManager.GetString("EditedOn", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entities.
        /// </summary>
        public static string EntityPlural {
            get {
                return ResourceManager.GetString("EntityPlural", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Entity.
        /// </summary>
        public static string EntitySingular {
            get {
                return ResourceManager.GetString("EntitySingular", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Filter active.
        /// </summary>
        public static string Filter_Active {
            get {
                return ResourceManager.GetString("Filter_Active", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reset filter.
        /// </summary>
        public static string Filter_Reset {
            get {
                return ResourceManager.GetString("Filter_Reset", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Smart filter.
        /// </summary>
        public static string Filter_Smart {
            get {
                return ResourceManager.GetString("Filter_Smart", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Columns.
        /// </summary>
        public static string Grid_Columns {
            get {
                return ResourceManager.GetString("Grid_Columns", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Rows.
        /// </summary>
        public static string Grid_Rows {
            get {
                return ResourceManager.GetString("Grid_Rows", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Importer.
        /// </summary>
        public static string Importer {
            get {
                return ResourceManager.GetString("Importer", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to List.
        /// </summary>
        public static string List {
            get {
                return ResourceManager.GetString("List", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Code: {0} {1}.
        ///
        ///Description: {2}.
        /// </summary>
        public static string LoadData_Error_Description {
            get {
                return ResourceManager.GetString("LoadData_Error_Description", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} {1} had errors while loading data, see log for details..
        /// </summary>
        public static string LoadData_Error_MultipleErrors {
            get {
                return ResourceManager.GetString("LoadData_Error_MultipleErrors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Load Error.
        /// </summary>
        public static string LoadData_Error_Title {
            get {
                return ResourceManager.GetString("LoadData_Error_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logging in with user {0}....
        /// </summary>
        public static string LoginView_LoggingInMsg {
            get {
                return ResourceManager.GetString("LoginView_LoggingInMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logging in with Windows account....
        /// </summary>
        public static string LoginView_WindowsLoggingInMsg {
            get {
                return ResourceManager.GetString("LoginView_WindowsLoggingInMsg", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Automation.
        /// </summary>
        public static string MainViewModel_Automation {
            get {
                return ResourceManager.GetString("MainViewModel_Automation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Calculations.
        /// </summary>
        public static string MainViewModel_CalculationMethods {
            get {
                return ResourceManager.GetString("MainViewModel_CalculationMethods", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Home.
        /// </summary>
        public static string MainViewModel_Home {
            get {
                return ResourceManager.GetString("MainViewModel_Home", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Reports.
        /// </summary>
        public static string MainViewModel_Reports {
            get {
                return ResourceManager.GetString("MainViewModel_Reports", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search.
        /// </summary>
        public static string MainViewModel_Search {
            get {
                return ResourceManager.GetString("MainViewModel_Search", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Settings.
        /// </summary>
        public static string MainViewModel_Settings {
            get {
                return ResourceManager.GetString("MainViewModel_Settings", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Switch....
        /// </summary>
        public static string MainViewModel_SwitchDatabase {
            get {
                return ResourceManager.GetString("MainViewModel_SwitchDatabase", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Time Series View.
        /// </summary>
        public static string MainViewModel_TimeSeriesTree {
            get {
                return ResourceManager.GetString("MainViewModel_TimeSeriesTree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Metadata.
        /// </summary>
        public static string Metadata {
            get {
                return ResourceManager.GetString("Metadata", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Name.
        /// </summary>
        public static string Name {
            get {
                return ResourceManager.GetString("Name", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to No data available.
        /// </summary>
        public static string NoData {
            get {
                return ResourceManager.GetString("NoData", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Node mode.
        /// </summary>
        public static string NodeMode {
            get {
                return ResourceManager.GetString("NodeMode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to None.
        /// </summary>
        public static string None {
            get {
                return ResourceManager.GetString("None", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Open.
        /// </summary>
        public static string Open {
            get {
                return ResourceManager.GetString("Open", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Origin.
        /// </summary>
        public static string Origin {
            get {
                return ResourceManager.GetString("Origin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Properties.
        /// </summary>
        public static string Properties {
            get {
                return ResourceManager.GetString("Properties", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Logging in....
        /// </summary>
        public static string ReLogin {
            get {
                return ResourceManager.GetString("ReLogin", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Representation.
        /// </summary>
        public static string Representation {
            get {
                return ResourceManager.GetString("Representation", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Search descriptor....
        /// </summary>
        public static string SearchDescriptor {
            get {
                return ResourceManager.GetString("SearchDescriptor", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select all.
        /// </summary>
        public static string SelectAll {
            get {
                return ResourceManager.GetString("SelectAll", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Select tree.
        /// </summary>
        public static string SelectTree {
            get {
                return ResourceManager.GetString("SelectTree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Sorting.
        /// </summary>
        public static string Sorting {
            get {
                return ResourceManager.GetString("Sorting", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tree.
        /// </summary>
        public static string Tree {
            get {
                return ResourceManager.GetString("Tree", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tree branch incl. nodes.
        /// </summary>
        public static string TreeFilterMode_BranchAndNode {
            get {
                return ResourceManager.GetString("TreeFilterMode_BranchAndNode", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Tree branch without nodes.
        /// </summary>
        public static string TreeFilterMode_BranchOnly {
            get {
                return ResourceManager.GetString("TreeFilterMode_BranchOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Nodes only.
        /// </summary>
        public static string TreeFilterMode_NodeOnly {
            get {
                return ResourceManager.GetString("TreeFilterMode_NodeOnly", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to User-defined.
        /// </summary>
        public static string UserDefined {
            get {
                return ResourceManager.GetString("UserDefined", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to View.
        /// </summary>
        public static string View {
            get {
                return ResourceManager.GetString("View", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to {0} {1} had errors while writing data, see log for details..
        /// </summary>
        public static string WriteData_Error_MultipleErrors {
            get {
                return ResourceManager.GetString("WriteData_Error_MultipleErrors", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Data Write Error.
        /// </summary>
        public static string WriteData_Error_Title {
            get {
                return ResourceManager.GetString("WriteData_Error_Title", resourceCulture);
            }
        }
        
        /// <summary>
        ///   Looks up a localized string similar to Write protection.
        /// </summary>
        public static string WriteProtection {
            get {
                return ResourceManager.GetString("WriteProtection", resourceCulture);
            }
        }
    }
}
