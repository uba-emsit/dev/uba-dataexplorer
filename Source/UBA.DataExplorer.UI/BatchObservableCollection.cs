﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;

namespace UBA.DataExplorer.UI;

public class BatchObservableCollection<T> : ObservableCollection<T>
{
    public void AddRange(IEnumerable<T> items)
    {
        if (items == null) return;

        foreach (var item in items)
        {
            Items.Add(item); // Directly add to the internal list
        }

        // Notify UI that the collection has been updated
        OnCollectionChanged(new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
    }
}