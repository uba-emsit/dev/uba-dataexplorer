﻿namespace UBA.DataExplorer.UI.Extensions;
public static class DictionaryExtension
{
    public static ListDictionary<TKey, TValue> CastToList<TKey, TValue>(this IDictionary<TKey, List<TValue>> source)
        where TKey : notnull
        => new(source);

    public class ListDictionary<TKey, TValue>(IDictionary<TKey, List<TValue>> dictionary)
        : Dictionary<TKey, List<TValue>>(dictionary)
        where TKey : notnull
    {
        public Dictionary<TKey, List<TListEntry>> Of<TListEntry>()
            => this.ToDictionary(kv => kv.Key, kv => kv.Value.OfType<TListEntry>().ToList());
    }
}