﻿using System.ComponentModel;
using System.Reactive;
using System.Reactive.Linq;

namespace UBA.DataExplorer.UI.Extensions;

static class NotifyPropertyChangedExtensions
{
    public static IObservable<EventPattern<PropertyChangedEventArgs>> WhenPropertyChanged(this INotifyPropertyChanged notifyPropertyChanged) =>
        Observable
            .FromEventPattern<PropertyChangedEventHandler, PropertyChangedEventArgs>(
                ev => notifyPropertyChanged.PropertyChanged += ev,
                ev => notifyPropertyChanged.PropertyChanged -= ev);
}
