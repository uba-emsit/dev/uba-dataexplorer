﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;

namespace UBA.DataExplorer.UI;

public static class DependencyInjection
{
    public static IServiceCollection AddSingletonServices(this IServiceCollection services)
    {
        return services
            .AddSingleton<CurrentContextService>()
            .AddSingleton<IDataService, DataService>()
            .AddSingleton<LoginViewModel>()
            .AddSingleton<DatabaseSelectorViewModel>()
            .AddSingleton<IMessageBoxHelper, MessageBoxHelper>()
            .AddSingleton<IReLoginHandler, ReLoginHandler>();
    }

    public static IServiceCollection AddViewModels(this IServiceCollection services)
    {
        return services
            .AddTransient<ContentViewModel>()
            .AddScoped<TimeSeriesMainViewModel>()
            .AddTransient<TimeSeriesTreeViewModel>();
    }

    /// <summary>
    /// Sets the static service provider used by the front end.
    /// </summary>
    /// <param name="provider"></param>
    public static void SetServiceProvider(IServiceProvider provider)
        => FrontEndServiceLocator.SetProvider(provider);
}