namespace UBA.DataExplorer.UI;

class LoadingIncrementer(Action<bool> setIsLoading)
{
    int _count;

    internal void Increment()
    {
        Interlocked.Increment(ref _count);
        setIsLoading(true);
    }

    internal void Decrement()
    {
        Interlocked.Decrement(ref _count);

        if (_count == 0)
            setIsLoading(false);
    }
}