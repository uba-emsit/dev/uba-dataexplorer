﻿using System.Globalization;
using Avalonia.Data.Converters;

namespace UBA.DataExplorer.UI.Converters;

// This converter is used to append a colon to a string (e.g. already existing resources), for example, to display a label.
public class AppendColonConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is string text)
        {
            return $"{text}:";
        }
        return value;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}