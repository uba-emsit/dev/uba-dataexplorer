﻿using System.Globalization;
using Avalonia.Data.Converters;

namespace UBA.DataExplorer.UI.Converters;

// A Converter that converts a double to a string in order to prevent Invalid cast exception when the value is null
public class DoubleNullableConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value == null || value is not double d || double.IsNaN(d))
            return string.Empty;

        return d;
    }

    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is string str && double.TryParse(str, out var result))
        {
            return result;
        }

        return null;
    }
}
