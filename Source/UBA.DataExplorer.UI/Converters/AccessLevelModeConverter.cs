﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models;

namespace UBA.DataExplorer.UI.Converters;

public class AccessLevelModeConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is AccessLevelMode mode)
        {
            return mode switch
            {
                AccessLevelMode.Public => Resources.Resources.AccessLevelMode_Public,
                AccessLevelMode.Confidential => Resources.Resources.AccessLevelMode_Confidential,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}