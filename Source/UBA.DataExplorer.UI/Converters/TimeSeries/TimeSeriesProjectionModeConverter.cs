﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class TimeSeriesProjectionModeConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is TimeSeriesProjectionMode format)
        {
            return format switch
            {
                TimeSeriesProjectionMode.TimeSeriesSetting => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesDisplay_TSSettings,
                TimeSeriesProjectionMode.Linear => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesProjection_Linear,
                _ => throw new ArgumentOutOfRangeException(),
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}