﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.Domain.Settings;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class TreeFilterModeConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is TreeFilterMode mode)
        {
            return mode switch
            {
                TreeFilterMode.NodeOnly => Resources.Resources.TreeFilterMode_NodeOnly,
                TreeFilterMode.BranchOnly => Resources.Resources.TreeFilterMode_BranchOnly,
                TreeFilterMode.BranchAndNode => Resources.Resources.TreeFilterMode_BranchAndNode,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is string str)
        {
            return str switch
            {
                var s when s == Resources.Resources.TreeFilterMode_NodeOnly => TreeFilterMode.NodeOnly,
                var s when s == Resources.Resources.TreeFilterMode_BranchOnly => TreeFilterMode.BranchOnly,
                var s when s == Resources.Resources.TreeFilterMode_BranchAndNode => TreeFilterMode.BranchAndNode,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return TreeFilterMode.NodeOnly;
    }
}