﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class TimeSeriesValuesDisplayFormatConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is TimeSeriesValuesDisplayFormat format)
        {
            return format switch
            {
                TimeSeriesValuesDisplayFormat.TimeSeriesSetting => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesDisplay_TSSettings,
                TimeSeriesValuesDisplayFormat.Automatic => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesDisplay_Format_Automatic,
                TimeSeriesValuesDisplayFormat.FixedPoint => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesDisplay_Format_FixedPoint,
                TimeSeriesValuesDisplayFormat.Exponential => Resources.TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesDisplay_Format_Exponential,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}