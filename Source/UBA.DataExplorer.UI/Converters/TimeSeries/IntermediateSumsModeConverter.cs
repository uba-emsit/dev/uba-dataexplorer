﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Resources;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class IntermediateSumsModeConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is IntermediateSumsMode mode)
        {
            return mode switch
            {
                IntermediateSumsMode.None => Resources.Resources.None,
                IntermediateSumsMode.SourceGroup => Descriptors.Descriptor_SourceGroup,
                IntermediateSumsMode.Region => Descriptors.Descriptor_Region,
                IntermediateSumsMode.ValueType => Descriptors.Descriptor_ValueType,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}