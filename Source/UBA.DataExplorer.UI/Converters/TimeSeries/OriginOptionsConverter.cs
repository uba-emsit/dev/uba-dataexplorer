﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Resources;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class OriginOptionsConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is OriginOptions mode)
        {
            return mode switch
            {
                OriginOptions.Analyst => Resources.Resources.Analyst,
                OriginOptions.ManualEntry => Resources.Resources.Importer,
                OriginOptions.Importer => TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_OriginOptions_Manual,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}