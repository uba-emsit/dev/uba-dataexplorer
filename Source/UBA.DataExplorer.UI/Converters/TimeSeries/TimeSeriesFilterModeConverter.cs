﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Resources;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class TimeSeriesFilterModeConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is TimeSeriesViewFilterMode mode)
        {
            return mode switch
            {
                TimeSeriesViewFilterMode.OnlyFilter => TimeSeriesResources
                    .TimeSeriesDataViewerView_Ribbon_FilterMode_OnlyFilter,
                TimeSeriesViewFilterMode.OnlyList => TimeSeriesResources
                    .TimeSeriesDataViewerView_Ribbon_FilterMode_OnlyList,
                TimeSeriesViewFilterMode.FilterList => TimeSeriesResources
                    .TimeSeriesDataViewerView_Ribbon_FilterMode_FilterList,
                TimeSeriesViewFilterMode.FilterOrList => TimeSeriesResources
                    .TimeSeriesDataViewerView_Ribbon_FilterMode_FilterOrList,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}