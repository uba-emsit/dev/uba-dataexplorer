﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Resources;

namespace UBA.DataExplorer.UI.Converters.TimeSeries;

public class TimeSeriesValueModeConverter : IValueConverter
{
    public object? Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is TimeSeriesValueMode mode)
        {
            return mode switch
            {
                TimeSeriesValueMode.Mapping => TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesMode_Mapping,
                TimeSeriesValueMode.Input => TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesMode_Recorded,
                TimeSeriesValueMode.Inherited => TimeSeriesResources.TimeSeriesDataViewerView_Ribbon_ValuesMode_Inherited,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object? ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotSupportedException();
    }
}