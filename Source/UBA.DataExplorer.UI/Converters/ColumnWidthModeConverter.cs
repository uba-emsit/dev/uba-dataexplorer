﻿using System.Globalization;
using Avalonia.Data.Converters;
using UBA.DataExplorer.UI.Models;

namespace UBA.DataExplorer.UI.Converters;

public class ColumnWidthModeConverter : IValueConverter
{
    public object Convert(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        if (value is ColumnWidthMode columnWidthMode)
        {
            return columnWidthMode switch
            {
                ColumnWidthMode.Automatic => Resources.Resources.Automatic,
                ColumnWidthMode.UserDefined => Resources.Resources.UserDefined,
                _ => throw new ArgumentOutOfRangeException()
            };
        }
        return string.Empty;
    }

    public object ConvertBack(object? value, Type targetType, object? parameter, CultureInfo culture)
    {
        throw new NotImplementedException();
    }
}
