﻿using System.Reactive.Subjects;

namespace UBA.DataExplorer.UI;

public enum IdLabel
{
    Id,
    Label,
    IdLabel,
    LabelId
}

public class ViewScope : IObservable<IdLabel>
{
    readonly ISubject<IdLabel> _subject = new Subject<IdLabel>();

    public IdLabel IdLabel { get; private set; } = IdLabel.Label;

    /// <summary>
    /// Sets IdLabel to the next state.
    /// </summary>
    public void NextIdLabel()
    {
        IdLabel = IdLabel switch
        {
            IdLabel.Id => IdLabel.Label,
            IdLabel.Label => IdLabel.IdLabel,
            IdLabel.IdLabel => IdLabel.LabelId,
            IdLabel.LabelId => IdLabel.Id,
            _ => IdLabel.Label
        };
        _subject.OnNext(IdLabel);
    }

    public void SetIdLabel(IdLabel idLabel)
    {
        IdLabel = idLabel;
        _subject.OnNext(IdLabel);
    }

    IDisposable IObservable<IdLabel>.Subscribe(IObserver<IdLabel> observer) => _subject.Subscribe(observer);
}
