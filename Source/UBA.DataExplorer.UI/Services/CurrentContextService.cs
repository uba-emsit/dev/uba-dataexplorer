﻿using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.UI.Services;

public class CurrentContextService
{
    public event EventHandler? ContextChanged;

    public LoginModel? Login { get; private set; }
    public bool CurrentlyDoingReLogin { get; private set; }
    public ConnectionModel? Connection { get; private set; }
    public ChangesLoader? ChangesLoader { get; private set; }
    public ObjectLocker? ObjectLocker { get; private set; }

    public void SetLogin(LoginModel login)
    {
        Login = login;
        CurrentlyDoingReLogin = false;
        OnContextChanged();
    }

    public void ClearLogin()
    {
        Login = null;
        CurrentlyDoingReLogin = false;
        OnContextChanged();
    }

    public void ClearLoginAndShowReLoginMessage()
    {
        Login = null;
        CurrentlyDoingReLogin = true;
        OnContextChanged();
    }

    public void SetDatabase(ConnectionModel connection, ChangesLoader changesLoader, ObjectLocker objectLocker)
    {
        Connection = connection;
        ChangesLoader = changesLoader;
        ObjectLocker = objectLocker;
        OnContextChanged();
    }

    public void SetMessageServer(IMessageServer messageServer)
    {
        ObjectLocker?.SetMessageServer(messageServer);
        ChangesLoader?.SetMessageServer(messageServer);
    }

    public void ClearDatabase()
    {
        Connection = null;
        ChangesLoader = null;
        ObjectLocker = null;
        OnContextChanged();
    }

    void OnContextChanged()
    {
        ContextChanged?.Invoke(this, EventArgs.Empty);
    }
}