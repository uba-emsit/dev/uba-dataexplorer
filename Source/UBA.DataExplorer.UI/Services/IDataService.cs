﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.UI.Services;

public interface IDataService
{
    IReadOnlyDictionary<int, List<IDescriptor>>? DescriptorsByDimension { get; }
    IReadOnlyDictionary<int, List<ITree>>? TreeNodesByDimension { get; }
    IObservable<IReadOnlyDictionary<int, List<IDescriptor>>?> DescriptorsByDimensionObservable { get; }
    IObservable<IReadOnlyDictionary<int, List<ITree>>?> TreeNodesByDimensionObservable { get; }
    Task LoadDataAsync();
}