﻿using System.Reactive.Linq;
using System.Reactive.Subjects;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;
using UBA.DataExplorer.UseCases.Descriptors;
using UBA.DataExplorer.UseCases.Dimensions;
using UBA.DataExplorer.UseCases.MessageServer;
using UBA.DataExplorer.UseCases.Trees;

namespace UBA.DataExplorer.UI.Services;

public class DataService(CurrentContextService currentContextService, ISender sender)
    : IDataService, IDisposable
{
    private Dictionary<int, List<IDescriptor>> _descriptorsByDimension = new();
    private Dictionary<int, List<ITree>> _treeNodesByDimension = new();

    private readonly Subject<IReadOnlyDictionary<int, List<IDescriptor>>?> _descriptorsByDimensionSubject = new();
    private readonly Subject<IReadOnlyDictionary<int, List<ITree>>?> _treeNodesByDimensionSubject = new();

    public IObservable<IReadOnlyDictionary<int, List<IDescriptor>>?> DescriptorsByDimensionObservable => _descriptorsByDimensionSubject.AsObservable();
    public IObservable<IReadOnlyDictionary<int, List<ITree>>?> TreeNodesByDimensionObservable => _treeNodesByDimensionSubject.AsObservable();

    public IReadOnlyDictionary<int, List<IDescriptor>>? DescriptorsByDimension => _descriptorsByDimension;
    public IReadOnlyDictionary<int, List<ITree>>? TreeNodesByDimension => _treeNodesByDimension;

    ChangesLoader.DescriptorSubscription? _descriptorsSubscription;
    ChangesLoader.TreeSubscription? _treeSubscription;

    public async Task LoadDataAsync()
    {
        using var cancellationTokenSource = new CancellationTokenSource();
        var cancellationToken = cancellationTokenSource.Token;

        try
        {
            var dimensionQuery = new DimensionsAllQuery(currentContextService.Connection!);
            var dimensionResult = await sender.Send(dimensionQuery, cancellationToken);

            if (dimensionResult.IsFailure)
            {
                await FrontEndServiceLocator.Get
                    .GetRequiredService<IMessageBoxHelper>()
                    .ShowDataLoadErrorMsgBoxAsync(EntityTypes.Dimension, dimensionResult.Error);
                return;
            }

            var dimensions = dimensionResult.Value;
            var descriptorsTask = LoadData<DescriptorsByDimensionQuery, IDescriptor>(
                dimensions,
                primaryKey => new DescriptorsByDimensionQuery(currentContextService.Connection!, primaryKey),
                EntityTypes.Descriptor,
                dict =>
                {
                    _descriptorsByDimension = new Dictionary<int, List<IDescriptor>>(dict);
                    _descriptorsByDimensionSubject.OnNext(_descriptorsByDimension);
                },
                cancellationToken
            );

            var treeNodesTask = LoadData<TreeByDimensionQuery, ITree>(
                dimensions,
                primaryKey => new TreeByDimensionQuery(currentContextService.Connection!, primaryKey),
                EntityTypes.Tree,
                dict =>
                {
                    _treeNodesByDimension = new Dictionary<int, List<ITree>>(dict);
                    _treeNodesByDimensionSubject.OnNext(_treeNodesByDimension);
                },
                cancellationToken
            );

            await Task.WhenAll(descriptorsTask, treeNodesTask);

            var allDescriptors = _descriptorsByDimension.Values.SelectMany(x => x).ToList();
            _descriptorsSubscription = new ChangesLoader.DescriptorSubscription(OnChangeDescriptorData, allDescriptors, true);
            currentContextService.ChangesLoader?.Subscribe(_descriptorsSubscription);

            var allTrees = _treeNodesByDimension.Values.SelectMany(x => x).ToList();
            _treeSubscription = new ChangesLoader.TreeSubscription(OnChangeTreeData, allTrees, true);
            currentContextService.ChangesLoader?.Subscribe(_treeSubscription);
        }
        catch (Exception ex)
        {
            await cancellationTokenSource.CancelAsync();
            await FrontEndServiceLocator.Get
                .GetRequiredService<IMessageBoxHelper>()
                .ShowErrorMsgBoxAsync(
                    Resources.Resources.LoadData_Error_Title,
                    string.Format(Resources.Resources.LoadData_Error_Description, ex.HResult, "", ex.Message));
        }
    }

    Task OnChangeTreeData(ChangesLoader.ObjectChangeReason reason, List<ITree> trees, List<int> primaryKeys)
    {
        foreach (var tree in trees)
        {
            if (!_treeNodesByDimension.TryGetValue(tree.Dimension.PrimaryKey, out var treeList))
            {
                treeList =
                [
                    tree
                ];
                _treeNodesByDimension[tree.Dimension.PrimaryKey] = treeList;
            }
            else
            {

                var index = treeList.FindIndex(t => t.PrimaryKey == tree.PrimaryKey);
                if (index != -1)
                {
                    treeList[index] = tree;
                }
                else
                {
                    treeList.Add(tree);
                }
            }
        }

        _treeNodesByDimensionSubject.OnNext(_treeNodesByDimension);
        return Task.CompletedTask;
    }

    Task OnChangeDescriptorData(ChangesLoader.ObjectChangeReason reason, List<IDescriptor> descriptors, List<int> primaryKeys)
    {
        foreach (var descriptor in descriptors)
        {
            if (!_descriptorsByDimension.TryGetValue(descriptor.Dimension.PrimaryKey, out var descriptorList))
            {
                descriptorList =
                [
                    descriptor
                ];
                _descriptorsByDimension[descriptor.Dimension.PrimaryKey] = descriptorList;
            }
            else
            {

                var index = descriptorList.FindIndex(d => d.PrimaryKey == descriptor.PrimaryKey);
                if (index != -1)
                {
                    descriptorList[index] = descriptor;
                }
                else
                {
                    descriptorList.Add(descriptor);
                }
            }
        }

        _descriptorsByDimensionSubject.OnNext(_descriptorsByDimension);
        return Task.CompletedTask;
    }

    private async Task LoadData<TQuery, TEntity>(
        List<IDimension> dimensions,
        Func<int, TQuery> queryFactory,
        string entityType,
        Action<Dictionary<int, List<TEntity>>> updateAction,
        CancellationToken cancellationToken)
        where TQuery : IQuery<List<TEntity>>
    {
        Dictionary<int, List<TEntity>> tempDict = new();
        var errors = new List<(string Name, Error Error)>();

        foreach (var dimension in dimensions)
        {
            var query = queryFactory(dimension.PrimaryKey);
            var result = await sender.Send(query, cancellationToken);

            if (result.IsFailure)
            {
                if (result.Error.Type == ErrorType.NoData)
                {
                    tempDict[dimension.PrimaryKey] = new List<TEntity>();
                }
                else
                {
                    errors.Add((dimension.Name, result.Error));
                }
                continue;
            }
            tempDict[dimension.PrimaryKey] = result.Value;
        }

        if (errors.Any())
        {
            if (errors.Any(e => e.Error == Error.Silent)) return;
            await FrontEndServiceLocator.Get
               .GetRequiredService<IMessageBoxHelper>()
               .ShowMultipleDataLoadErrorMsgBoxAsync(entityType, errors.Count);
        }

        updateAction(new Dictionary<int, List<TEntity>>(tempDict));
    }

    public void Dispose()
    {
        _descriptorsSubscription?.Dispose();
        _treeSubscription?.Dispose();
        GC.SuppressFinalize(this);
    }
}
