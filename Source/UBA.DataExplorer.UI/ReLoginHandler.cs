using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.UI.Services;

namespace UBA.DataExplorer.UI;

public class ReLoginHandler(CurrentContextService context)
    : IReLoginHandler
{
    public Task<bool> WasWindowsAccountLogin(CancellationToken cancellationToken)
    {
        var result = context.Login?.IsWindowsAccountLogin ?? true;
        context.ClearLoginAndShowReLoginMessage();
        return Task.FromResult(result);
    }

    // TODO display message box
    public Task<IReLoginHandler.Response> RequestPassword(string userId, string? message, CancellationToken cancellationToken)
        => Task.FromResult(IReLoginHandler.Response.ReturnPassword(""));

    public Task MessageServerChanged(IMessageServer messageServer)
    {
        if (context.Connection == null || context.Connection.IsDisposed) return Task.CompletedTask;
        context.SetMessageServer(messageServer);
        return Task.CompletedTask;
    }

    public Task LoginChanged(LoginModel loginModel)
    {
        context.SetLogin(loginModel);
        return Task.CompletedTask;
    }

    public Task Logoff()
    {
        context.ClearLogin();
        return Task.CompletedTask;
    }
}