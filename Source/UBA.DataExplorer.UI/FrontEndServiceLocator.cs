﻿using Microsoft.Extensions.DependencyInjection;

namespace UBA.DataExplorer.UI;

public static class FrontEndServiceLocator
{
    static IServiceProvider _provider = null!;
    static IServiceScope? _scope;

    public static void SetProvider(IServiceProvider provider)
    {
        ResetScope();
        _provider = provider;
    }

    public static IServiceProvider Get
    {
        get
        {
            _scope ??= _provider.CreateScope();
            return _scope.ServiceProvider;
        }
    }

    public static void ResetScope()
    {
        _scope?.Dispose();
        _scope = null;
    }
}