﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UI;
public interface IMessageBoxHelper
{
    Task ShowDataLoadErrorMsgBoxAsync(string entityType, Error error);
    Task ShowMultipleDataLoadErrorMsgBoxAsync(string entityType, int count);
    Task ShowDataWriteErrorMsgBoxAsync(string entityType, Error error);
    Task ShowMultipleDataWriteErrorMsgBoxAsync(string entityType, int count);
    Task ShowErrorMsgBoxAsync(string title, string message);
}