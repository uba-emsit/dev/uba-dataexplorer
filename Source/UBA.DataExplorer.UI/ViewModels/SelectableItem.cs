﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class SelectableItem(IIdAndLabelModel model, bool initialState, ViewScope viewScope) : IdLabelToggleViewModel(model, viewScope)
{
    [ObservableProperty] bool _isSelected = initialState;

    [RelayCommand]
    void ToggleSelected()
    {
        IsSelected = !IsSelected;
    }

    public override string ToString()
    {
        return MainText;
    }
}

public class SelectableItem<TModel>(TModel model, bool initialState, ViewScope viewScope)
    : SelectableItem(model, initialState, viewScope)
    where TModel : IPrimaryKey, IIdAndLabelModel
{
    public readonly int PrimaryKey = model.PrimaryKey;
    public readonly TModel Item = model;
}