﻿using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class ComboBoxDataViewModel<T> : ViewModelBase
{
    [ObservableProperty]
    private ObservableCollection<T> _items = [];

    [ObservableProperty]
    private T? _selectedItem;

    public ComboBoxDataViewModel()
    {
    }

    public ComboBoxDataViewModel(ICollection<T> items, T selectedItem)
    {
        Items = new(items);
        SelectedItem = selectedItem;
    }

    // This method is used to create a ComboBoxDataViewModel from a given enum, adding all the values of the enum to the Items property
    public static ComboBoxDataViewModel<T> FromEnum(T defaultSelectedItem)
    {
        var items = Enum.GetValues(typeof(T)).Cast<T>().ToList();
        return new(items, defaultSelectedItem);
    }
}