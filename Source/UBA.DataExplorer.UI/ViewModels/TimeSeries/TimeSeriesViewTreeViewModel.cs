﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public class TimeSeriesViewTreeViewModel : ViewModelBase, IPrimaryKey, IPredecessorLeaf
{
    public List<TimeSeriesViewTreeViewModel> Children { get; set; }
    public int Level { get; set; }
    public TimeSeriesViewViewModel Entity { get; set; }
    public string Label => Entity.Name;
    public int PrimaryKey => Entity.PrimaryKey;
    public int PredecessorNr => Entity.PredecessorNr;
    public bool IsLeaf => Entity.IsLeaf;

    public TimeSeriesViewTreeViewModel(ITimeSeriesView entity) : this(new TimeSeriesViewViewModel(entity))
    {
    }

    public TimeSeriesViewTreeViewModel(TimeSeriesViewViewModel item)
    {
        Entity = item;
        Children = new List<TimeSeriesViewTreeViewModel>();
    }

    public TimeSeriesViewTreeViewModel(IEntityTreeNode<ITimeSeriesView> entity)
    {
        Level = entity.Level;
        Entity = new TimeSeriesViewViewModel(entity.Entity);
        Children = entity.Children
            .Where(child => !child.Entity.IsSystemView)
            .OrderBy(child => child.Entity.IsLeaf ? 1 : 0)
            .ThenBy(child => child.Entity.SortNr)
            .Select(child => new TimeSeriesViewTreeViewModel(child))
            .ToList();
    }
    public void SortChildren()
    {
        var sortedChildren = Children
            .OrderBy(child => child.Entity.IsLeaf ? 1 : 0)
            .ThenBy(child => child.Entity.SortNr)
            .ToList();


        Children.Clear();
        foreach (var child in sortedChildren)
        {
            Children.Add(child);
        }
    }

    public override string ToString()
    {
        return Label;
    }

}