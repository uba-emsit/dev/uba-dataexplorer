﻿using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesViewViewModel(ITimeSeriesView model)
    : ViewModelBase, IPrimaryKey, IPredecessorLeaf
{
    public int PrimaryKey { get; } = model.PrimaryKey;

    [ObservableProperty] int _predecessorNr = model.PredecessorNr;

    [ObservableProperty] string _id = model.Id;

    [ObservableProperty] string _name = model.Name;

    [ObservableProperty] int _sortNr = model.SortNr;

    [ObservableProperty] bool _isLeaf = model.IsLeaf;

    [ObservableProperty] string _description = model.Description;

    [ObservableProperty] IAuditInfo _auditInfo = model.AuditInfo;

    [ObservableProperty] ITimeSeriesViewTimeSettings _timeSettings = model.TimeSettings;

    [ObservableProperty] ITimeSeriesViewSettings _settings = model.Settings;
}