﻿using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Hypotheses;


#if DEBUG
using Bogus;
using UBA.DataExplorer.Domain.Faker;
#endif

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesDataViewModel : ViewModelBase, IEquatable<TimeSeriesDataViewModel>
{
    [ObservableProperty] string _period;
    [ObservableProperty] double? _value;
    [ObservableProperty] bool _hasDocumentation;
    [ObservableProperty] string? _noValueReason;
    [ObservableProperty] bool _isMapped;
    [ObservableProperty] bool _isVirtual;
    [ObservableProperty] bool _isInherited;
    [ObservableProperty] bool _isWriteProtected;
    [ObservableProperty] bool _isFiltered;
    [ObservableProperty] bool _isDirty;
    bool _isInitializing = true;

    ITimeSeriesData? _model;
    public ITimeSeriesData? Model => _model;
    public ITimeSeriesDataCoordinates? Coordinates => _model?.Coordinates;
    public string FormattedValue => Value?.ToString("N1") ?? "-";

    partial void OnValueChanged(double? value)
    {
        OnPropertyChanged(nameof(FormattedValue));
        if (_model != null && _model.Value != value)
        {
            _model.Value = value;
            if (!_isInitializing)
            {
                IsDirty = true;
            }
        }
    }

    partial void OnNoValueReasonChanged(string? value)
    {
        if (_model != null && _model.NoValueReason != value)
        {
            _model.NoValueReason = value;
            IsDirty = true;
        }
    }

#if DEBUG
    #region Design
    public TimeSeriesDataViewModel()
    {
        if (Design.IsDesignMode)
        {
            // Generate design data
            var fakeData = new TimeSeriesDataFaker().Generate();
            Period = fakeData.Coordinates.TimePeriod1.ToString() ?? "";
            _model = fakeData;
            Value = fakeData.Value;
            HasDocumentation = fakeData.HasDocumentation;
            IsMapped = new Faker().Random.Bool();
            IsInherited = false;
            IsWriteProtected = fakeData.IsWriteProtected;
            IsVirtual = false;
            IsFiltered = false;
            NoValueReason = fakeData.NoValueReason;

        }
        else
        {
            Period = "";
        }
    }
    #endregion
#endif
    TimeSeriesDataViewModel(ITimeSeriesDataCoordinates coordinates)
    {
        Period = coordinates.TimePeriod1.ToString() ?? "";
    }

    public static IEnumerable<TimeSeriesDataViewModel> CreateFrom(ITimeSeries timeSeries, List<ITimeSeriesData> timeSeriesDataList) =>
        timeSeriesDataList.Select(x => new TimeSeriesDataViewModel(x.Coordinates)
        {
            _model = x,
            Value = x.Value,
            HasDocumentation = x.HasDocumentation,
            IsMapped = false,
            IsInherited = false,
            IsWriteProtected = x.IsWriteProtected,
            IsVirtual = timeSeries.IsVirtual,
            IsFiltered = false,
            NoValueReason = x.NoValueReason,
            _isInitializing = false
        });

    public static IEnumerable<TimeSeriesDataViewModel> CreateFrom(ITimeSeries timeSeries, List<IMappedTimeSeriesData> timeSeriesDataList) =>
        timeSeriesDataList.Select(x => new TimeSeriesDataViewModel(x.Coordinates)
        {
            _model = x,
            Value = x.Value,
            HasDocumentation = false,
            IsMapped = x.MappingResult == MappingResult.Interpolated,
            IsWriteProtected = true,
            IsVirtual = timeSeries.IsVirtual,
            IsInherited = x.MappingResult == MappingResult.Inherited,
            IsFiltered = false,
            _isInitializing = false
        });

    public static TimeSeriesDataViewModel CreateEmpty(DateTimeOffset date, TimeResolution timeResolution)
    {
        var model = new TimeSeriesDataModel
        {
            Coordinates = new TimeSeriesDataCoordinatesModel
            {
                TimePeriod1 = new TimePeriodModel
                {
                    Date = date,
                    TimeResolution = timeResolution
                }
            },
            Hypothesis = HypothesisModel.Default
        };

        return new TimeSeriesDataViewModel(model.Coordinates)
        {
            _model = model,
            _isInitializing = false
        };
    }

    public void UpdateProperties(TimeSeriesDataViewModel data)
    {
        _model = data._model;
        Period = data.Period;
        HasDocumentation = data.HasDocumentation;
        NoValueReason = data.NoValueReason;
        IsMapped = data.IsMapped;
        IsVirtual = data.IsVirtual;
        IsInherited = data.IsInherited;
        IsWriteProtected = data.IsWriteProtected;
        IsFiltered = data.IsFiltered;

        if (!IsDirty)
            Value = data.Value;
    }

    partial void OnValueChanged(double? oldValue, double? newValue)
    {
        if (!_isInitializing && (!oldValue.HasValue || !newValue.HasValue ||
                                 Math.Abs(oldValue.Value - newValue.Value) > 1e-10))
            IsDirty = true;
    }

    public override string ToString() => $"{(Value.HasValue ? Value : NoValueReason)}";

    public override bool Equals(object? obj) => Equals(obj as TimeSeriesDataViewModel);

    public bool Equals(TimeSeriesDataViewModel? other)
    {
        if (other is null) return false;
        return _model?.TimeSeriesPrimaryKey == other._model?.TimeSeriesPrimaryKey &&
               _model?.Coordinates.TimePeriod1.Equals(other._model?.Coordinates.TimePeriod1) == true &&
               _model?.Hypothesis.Id == other._model?.Hypothesis.Id;
    }

    public override int GetHashCode()
    {
        return HashCode.Combine(_model?.TimeSeriesPrimaryKey, _model?.Coordinates.TimePeriod1, _model?.Hypothesis.Id);
    }
}