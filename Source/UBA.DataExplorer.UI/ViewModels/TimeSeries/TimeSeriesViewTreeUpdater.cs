﻿using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public class TimeSeriesViewTreeUpdater
{
    public TimeSeriesViewTreeViewModel? UpdateTree(TimeSeriesViewTreeViewModel rootFolder, ChangesLoader.ObjectChangeReason reason,
        List<TimeSeriesViewViewModel> viewModels, List<int> primaryKeys)
    {
        switch (reason)
        {
            case ChangesLoader.ObjectChangeReason.ObjectChange:
                foreach (var viewModel in viewModels)
                {
                    RemoveByPrimaryKey(viewModel.PrimaryKey, rootFolder.Children);
                    HangIntoTree(viewModel, rootFolder);
                }
                break;
            case ChangesLoader.ObjectChangeReason.ObjectAddNew:
                foreach (var viewModel in viewModels)
                {
                    HangIntoTree(viewModel, rootFolder);
                }
                break;
            case ChangesLoader.ObjectChangeReason.ObjectDelete:
                if (primaryKeys.Contains(rootFolder.Entity?.PrimaryKey ?? 0))
                    return null;
                foreach (var primaryKey in primaryKeys)
                    RemoveByPrimaryKey(primaryKey, rootFolder.Children);
                break;
        }
        return rootFolder;
    }


    void HangIntoTree(TimeSeriesViewViewModel item, TimeSeriesViewTreeViewModel parentFolder)
    {
        if (item.PredecessorNr == parentFolder.Entity.PrimaryKey)
        {
            parentFolder.Children.Add(new TimeSeriesViewTreeViewModel(item));
            parentFolder.SortChildren();
            return;
        }
        foreach (var child in parentFolder.Children)
            HangIntoTree(item, child);
    }

    void RemoveByPrimaryKey(int primaryKey, List<TimeSeriesViewTreeViewModel> children)
    {
        children.RemoveAll(child =>
        {
            RemoveByPrimaryKey(primaryKey, child.Children);
            return child.Entity.PrimaryKey == primaryKey;
        });
    }
}