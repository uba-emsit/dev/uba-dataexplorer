﻿using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using System.Reactive.Disposables;
using System.Reactive.Linq;
using Avalonia.Threading;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using DynamicData;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UI.Models.Descriptors;
using UBA.DataExplorer.UI.Services;



#if DEBUG
using Avalonia.Controls;
using Bogus;
using UBA.DataExplorer.Domain.Faker;
#endif

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class DimensionHeaderViewModel : DataLoadViewModel, IDataGridHeader
{
    [ObservableProperty] string _displayText;
    [ObservableProperty] string _searchText = "";
    [ObservableProperty] string _selectedItemsPlaceholderText = "0 ##items";
    [ObservableProperty] ObservableCollection<SelectableItem> _selectedItems = [];
    [ObservableProperty] ObservableCollection<SelectableItem<IDescriptor>> _filteredItems = [];
    [ObservableProperty] ObservableCollection<SelectableTreeItem<TreeNode>> _filteredTrees = [];
    [ObservableProperty] ObservableCollection<ITree> _availableTrees = [];
    [ObservableProperty] ITree? _selectedTree;

    [ObservableProperty] IDescriptorFilterSettings.DescriptorFilterMode _filterMode = IDescriptorFilterSettings.DescriptorFilterMode.SingleDescriptors;
    [ObservableProperty] ComboBoxDataViewModel<TreeFilterMode> _comboBoxTreeFilterMode;

    readonly ViewScope _viewScope;
    readonly IDisposable? _selectItemsSubscription;
    readonly SerialDisposable _descriptorFilterVmItems = new();
    readonly IDataService? _dataService;

    SortMode _sortMode = SortMode.Alphanumeric;
    readonly ColumnDescriptorFilter? _columnDescriptorFilter;

#if DEBUG
    #region Designer

    public DimensionHeaderViewModel() : this("Design Header")
    {
        if (Design.IsDesignMode)
        {
            GenerateDesignData();
        }
    }

    private void GenerateDesignData()
    {
        var faker = new Faker();
        new DescriptorFakes().GetList(10)
            .ForEach(x => FilteredItems.Add(new SelectableItem<IDescriptor>(x, faker.Random.Bool(), _viewScope)));

        var entityTreeNodeFakes = new EntityTreeNodeFaker<ITreeNodeNode>(new TreeNodeNodeFaker());

        try
        {
            var fakes = entityTreeNodeFakes.Generate(3);
            fakes.ForEach(x =>
                {
                    var fakeNode = new TreeNode(x);
                    FilteredTrees.Add(new SelectableTreeItem<TreeNode>(fakeNode, faker.Random.Bool(), _viewScope));
                });

            AvailableTrees = new ObservableCollection<ITree>(new TreeFaker().Generate(3));
            SelectedTree = faker.PickRandom(AvailableTrees).FirstOrDefault();

            UpdateSelectedItems();
            SortItems();
        }
        catch (Exception e)
        {
            Debug.WriteLine(e);
        }
    }

    #endregion
#endif

    public DimensionHeaderViewModel(string header)
    {
        DisplayText = header;
        _viewScope = new ViewScope();
        ComboBoxTreeFilterMode = ComboBoxDataViewModel<TreeFilterMode>.FromEnum(TreeFilterMode.NodeOnly);
    }

    public DimensionHeaderViewModel(string header, ColumnDescriptorFilter? columnDescriptorFilter) : this(header)
    {
        _viewScope = columnDescriptorFilter?.ViewScope ?? new ViewScope();
        _columnDescriptorFilter = columnDescriptorFilter;
        _dataService = FrontEndServiceLocator.Get.GetRequiredService<IDataService>();

        _selectItemsSubscription = Observable
            .FromEventPattern<NotifyCollectionChangedEventHandler, NotifyCollectionChangedEventArgs>(
                h => SelectedItems.CollectionChanged += h,
                h => SelectedItems.CollectionChanged -= h)
            .Subscribe(_ => SelectedItemsPlaceholderText = $"{SelectedItems.Count} ##items");
    }


    [RelayCommand]
    void ToggleSorting(object? item)
    {
        _sortMode = _sortMode == SortMode.Alphanumeric ? SortMode.Predefined : SortMode.Alphanumeric;
        SortItems();
    }

    [RelayCommand]
    void ToggleView() => _viewScope?.NextIdLabel();

    List<SelectableItem<IDescriptor>> GenerateSelectableItems(IEnumerable<IDescriptor>? descriptors)
    {
        if (descriptors == null || _columnDescriptorFilter == null) return new List<SelectableItem<IDescriptor>>();

        return descriptors
            .OrderBy(d => d.Label)
            .Select(d => new SelectableItem<IDescriptor>(
                d,
                _columnDescriptorFilter.DescriptorFilterSettings.DescriptorPrimaryKeys.Contains(d.PrimaryKey),
                _viewScope
            ))
        .ToList();
    }

    List<SelectableTreeItem<TreeNode>> GenerateSelectableTreeItems(List<ITree>? trees)
    {
        if (trees == null || _columnDescriptorFilter == null) return new List<SelectableTreeItem<TreeNode>>();
        return trees
            .SelectMany(t => t.Structure?.RootNode.Children ?? Enumerable.Empty<IEntityTreeNode<ITreeNodeNode>>())
            .Select(n => new SelectableTreeItem<TreeNode>(
                new TreeNode(n),
                _columnDescriptorFilter.DescriptorFilterSettings.TreeNodePrimaryKeys.Contains(n.Entity.PrimaryKey),
                _viewScope
            ))
            .ToList();
    }

    partial void OnSearchTextChanged(string value)
    {
        if (_columnDescriptorFilter == null) return;

        var descriptors = _dataService?.DescriptorsByDimension?.GetValueOrDefault(_columnDescriptorFilter.DimensionPrimaryKey, []) ?? [];
        var selectableItems = GenerateSelectableItems(descriptors);

        var trees = _dataService?.TreeNodesByDimension?.GetValueOrDefault(_columnDescriptorFilter.DimensionPrimaryKey, []);
        var selectableTrees = GenerateSelectableTreeItems(trees);

        var filteredItems = selectableItems
            .Where(x => string.IsNullOrEmpty(value) || x.MainText.Contains(value, StringComparison.OrdinalIgnoreCase) ||
                        x.SubText.Contains(value, StringComparison.OrdinalIgnoreCase))
            .ToList();

        var filteredTrees = selectableTrees
            .Where(x => string.IsNullOrEmpty(value) || x.MainText.Contains(value, StringComparison.OrdinalIgnoreCase) ||
                        x.SubText.Contains(value, StringComparison.OrdinalIgnoreCase))
            .ToList();

        FilteredItems.Clear();
        FilteredItems.AddRange(filteredItems);

        FilteredTrees.Clear();
        FilteredTrees.AddRange(filteredTrees);

        SortItems();
    }


    partial void OnFilterModeChanged(IDescriptorFilterSettings.DescriptorFilterMode value)
    {
        UpdateSelectedItems();
    }

    void SortItems()
    {
        Func<SelectableItem<IDescriptor>, object> itemKeySelector;
        Func<SelectableTreeItem<TreeNode>, object> treeKeySelector;

        switch (_sortMode)
        {
            case SortMode.Alphanumeric:
                itemKeySelector = x => x.MainText;
                treeKeySelector = x => x.MainText;
                break;
            case SortMode.Predefined:
                itemKeySelector = x => x.Item.SortNr;
                treeKeySelector = x => x.Item.SortNr;
                break;
            case SortMode.None:
            default:
                return;
        }

        FilteredItems = new ObservableCollection<SelectableItem<IDescriptor>>(FilteredItems.OrderBy(itemKeySelector));
        var sortedTrees = FilteredTrees.OrderBy(treeKeySelector).ToList();
        SortTree(sortedTrees, treeKeySelector);
        FilteredTrees = new ObservableCollection<SelectableTreeItem<TreeNode>>(sortedTrees);
    }

    void SortTree<T>(List<SelectableTreeItem<TreeNode>> nodes, Func<SelectableTreeItem<TreeNode>, T> keySelector)
    {
        foreach (var tree in FilteredTrees)
        {
            tree.SortChildren(keySelector);
        }
    }

    protected async override Task LoadData(CancellationToken cancellationToken)
    {
        if (_columnDescriptorFilter == null) return;

        _dataService?.DescriptorsByDimensionObservable
             .Where(descriptorsByDimension => descriptorsByDimension != null)
             .Subscribe(descriptorsByDimension =>
             {
                 var descriptors = descriptorsByDimension?.GetValueOrDefault(_columnDescriptorFilter.DimensionPrimaryKey, []);
                 var selectableItems = GenerateSelectableItems(descriptors);

                 Dispatcher.UIThread.InvokeAsync(() =>
                 {
                     FilteredItems.Clear();
                     FilteredItems.AddRange(selectableItems);
                     UpdateSelectedItems();
                     SortItems();
                 });
             });

        _dataService?.TreeNodesByDimensionObservable
            .Where(treeNodesByDimension => treeNodesByDimension != null)
            .Subscribe(treeNodesByDimension =>
            {
                var trees = treeNodesByDimension?.GetValueOrDefault(_columnDescriptorFilter.DimensionPrimaryKey, []);
                var selectableTreeItems = GenerateSelectableTreeItems(trees);

                Dispatcher.UIThread.InvokeAsync(() =>
                {
                    FilteredTrees.Clear();
                    FilteredTrees.AddRange(selectableTreeItems);

                    if (trees != null)
                    {
                        AvailableTrees = new ObservableCollection<ITree>(trees);
                        SelectedTree = trees.FirstOrDefault(tree =>
                            tree.PrimaryKey == _columnDescriptorFilter.DescriptorFilterSettings.TreePrimaryKey);
                    }

                    UpdateSelectedItems();
                    SortItems();
                });
            });

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            FilterMode = _columnDescriptorFilter.DescriptorFilterSettings.FilterMode;
            ComboBoxTreeFilterMode.SelectedItem = _columnDescriptorFilter.DescriptorFilterSettings.TreeFilterMode;
        });

        // Load data initially
        await _dataService?.LoadDataAsync();
    }


    void UpdateSelectedItems()
    {
        SelectedItems.Clear();
        switch (FilterMode)
        {
            case IDescriptorFilterSettings.DescriptorFilterMode.SingleDescriptors:
                foreach (var selectableItem in Enumerable.Where(FilteredItems, x => x.IsSelected))
                    UpdateSelectItemList(selectableItem);
                break;
            case IDescriptorFilterSettings.DescriptorFilterMode.Tree:
                foreach (var selectableItem in FilteredTrees)
                    UpdateTreeItemAndChildren(selectableItem);
                break;
            case IDescriptorFilterSettings.DescriptorFilterMode.MasterData:
                break;
        }
    }

    void UpdateSelectItemList(SelectableItem item)
    {
        if (item.IsSelected)
            SelectedItems.Add(item);
        else
            SelectedItems.Remove(item);
    }

    void UpdateTreeItemAndChildren(SelectableTreeItem<TreeNode> item)
    {
        UpdateSelectItemList(item);
        foreach (var child in item.Children)
        {
            UpdateTreeItemAndChildren(child);
        }
    }

    public override void Dispose()
    {
        _selectItemsSubscription?.Dispose();
        _descriptorFilterVmItems.Dispose();
        base.Dispose();
    }

}