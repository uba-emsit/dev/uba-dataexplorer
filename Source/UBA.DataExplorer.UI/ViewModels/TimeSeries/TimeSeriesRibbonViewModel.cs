﻿using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using UBA.DataExplorer.UI.Models;
using UBA.DataExplorer.UI.Models.TimeSeries;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesRibbonViewModel : ViewModelBase
{
    [ObservableProperty] private TimeSeriesDataViewerColumnType _selectedColumnType = TimeSeriesDataViewerColumnType.None;
    [ObservableProperty] private bool _showTimeSeriesTab;
    [ObservableProperty] private bool _showValuesTab;
    [ObservableProperty] private int _selectedTabIndex;
    [ObservableProperty] private ComboBoxDataViewModel<TimeSeriesValuesDisplayFormat> _comboBoxValueDisplayFormatVm;
    [ObservableProperty] private ComboBoxDataViewModel<TimeSeriesViewFilterMode> _comboBoxViewFilterVm;
    [ObservableProperty] private ComboBoxDataViewModel<TimeSeriesValueMode> _comboBoxValueModeVm;
    [ObservableProperty] private ComboBoxDataViewModel<TimeSeriesProjectionMode> _comboBoxInterpolationVm;
    [ObservableProperty] private ComboBoxDataViewModel<TimeSeriesProjectionMode> _comboBoxExtrapolationVm;
    [ObservableProperty] private ComboBoxDataViewModel<ColumnWidthMode> _comboBoxColumnWidthVm;
    [ObservableProperty] private ComboBoxDataViewModel<IntermediateSumsMode> _comboBoxIntermediateSums;
    [ObservableProperty] private ComboBoxDataViewModel<OriginOptions> _comboBoxOriginOptions;
    [ObservableProperty] private ComboBoxDataViewModel<AccessLevelMode> _comboBoxAccessLevel;
    [ObservableProperty] TimePeriodViewModel _timePeriodFrom;
    [ObservableProperty] TimePeriodViewModel _timePeriodTo;

    public Func<LoadingAnimation>? ParentStartLoading { get; init; }

    [RelayCommand]
    public void InvertTimePeriod()
    {
        var date = TimePeriodTo.Date;
        using (ParentStartLoading?.Invoke())
            TimePeriodTo.Date = TimePeriodFrom.Date;
        TimePeriodFrom.Date = date;
    }

    public event EventHandler? ReloadData;
    public event EventHandler? ReloadView;
    public event EventHandler? SaveRequested;

    void OnReloadData() => ReloadData?.Invoke(this, EventArgs.Empty);
    void OnReloadView() => ReloadView?.Invoke(this, EventArgs.Empty);

    public TimeSeriesRibbonViewModel()
    {
        ComboBoxViewFilterVm = ComboBoxDataViewModel<TimeSeriesViewFilterMode>.FromEnum(TimeSeriesViewFilterMode.OnlyFilter);
        ComboBoxViewFilterVm.PropertyChanged += (_, _) => OnReloadData();//todo geht nicht?
        ComboBoxValueModeVm = ComboBoxDataViewModel<TimeSeriesValueMode>.FromEnum(TimeSeriesValueMode.Mapping);
        ComboBoxValueDisplayFormatVm = ComboBoxDataViewModel<TimeSeriesValuesDisplayFormat>.FromEnum(TimeSeriesValuesDisplayFormat.TimeSeriesSetting);
        ComboBoxInterpolationVm = ComboBoxDataViewModel<TimeSeriesProjectionMode>.FromEnum(TimeSeriesProjectionMode.TimeSeriesSetting);
        ComboBoxExtrapolationVm = ComboBoxDataViewModel<TimeSeriesProjectionMode>.FromEnum(TimeSeriesProjectionMode.TimeSeriesSetting);
        ComboBoxColumnWidthVm = ComboBoxDataViewModel<ColumnWidthMode>.FromEnum(ColumnWidthMode.Automatic);
        ComboBoxIntermediateSums = ComboBoxDataViewModel<IntermediateSumsMode>.FromEnum(IntermediateSumsMode.None);
        ComboBoxOriginOptions = ComboBoxDataViewModel<OriginOptions>.FromEnum(OriginOptions.ManualEntry);
        ComboBoxAccessLevel = ComboBoxDataViewModel<AccessLevelMode>.FromEnum(AccessLevelMode.Public);
        TimePeriodFrom = TimePeriodViewModel.Empty;
        TimePeriodFrom.PropertyChanged += (_, _) => OnReloadData();
        TimePeriodTo = TimePeriodViewModel.Empty;
        TimePeriodTo.PropertyChanged += (_, _) => OnReloadData();

        if (Design.IsDesignMode)
        {
            ShowValuesTab = true;
            ShowTimeSeriesTab = true;
        }
    }

    [RelayCommand]
    public void Reload() => OnReloadView();

    [RelayCommand]
    public void Save() => SaveRequested?.Invoke(this, EventArgs.Empty);

    partial void OnSelectedColumnTypeChanged(TimeSeriesDataViewerColumnType value)
    {
        ShowValuesTab = value == TimeSeriesDataViewerColumnType.Value;
        ShowTimeSeriesTab = true;
    }
}