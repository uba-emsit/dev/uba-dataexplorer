﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesViewsRibbonViewModel : ViewModelBase
{
    [ObservableProperty] private bool _showEditedBy = true;
    [ObservableProperty] private bool _showCreatedBy = true;

    public TimeSeriesViewsRibbonViewModel() { }
}