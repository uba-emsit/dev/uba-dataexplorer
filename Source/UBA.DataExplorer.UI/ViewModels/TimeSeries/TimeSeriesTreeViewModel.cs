﻿using Avalonia.Controls;
using Avalonia.Controls.Models.TreeDataGrid;
using Avalonia.Controls.Selection;
using Avalonia.Threading;
using CommunityToolkit.Mvvm.ComponentModel;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UseCases.MessageServer;
using UBA.DataExplorer.UseCases.TimeSeriesView;

#if DEBUG
using UBA.DataExplorer.Domain.Faker;
#endif

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesTreeViewModel : DataLoadViewModel
{
    readonly ChangesLoader _changesLoader = null!;
    readonly TimeSeriesMainViewModel _mainViewModel = null!;
    readonly ChangesLoader.TimeSeriesViewSubscription _subscription = null!;
    readonly TimeSeriesViewTreeUpdater _treeUpdater = new();
    readonly ISender _sender;
    readonly CurrentContextService _currentContextService;

    [ObservableProperty] bool _isLoading;
    [ObservableProperty] HierarchicalTreeDataGridSource<TimeSeriesViewTreeViewModel> _treeSource = new([]);
    [ObservableProperty] TimeSeriesViewsRibbonViewModel _ribbonViewModel = new();

    TimeSeriesViewTreeViewModel? _rootFolder;

    public TimeSeriesViewTreeViewModel? RootFolder
    {
        get => _rootFolder;
        set
        {
            if (value != null)
            {
                CreateTreeDataGridSource(value);
            }
            _rootFolder = value;
        }
    }

#if DEBUG
    #region Designer

    public TimeSeriesTreeViewModel()
    {
        _currentContextService = null!;
        _sender = null!;
        if (Design.IsDesignMode)
            GenerateDesignData();
    }

    void GenerateDesignData()
    {
        RootFolder = new(new TimeSeriesViewTreeViewFakes().Get());
    }
    #endregion
#endif

    public TimeSeriesTreeViewModel(CurrentContextService currentContextService, ISender sender, TimeSeriesMainViewModel mainViewModel)
    {
        if (currentContextService.Connection == null) throw new("Connection is null");

        _mainViewModel = mainViewModel;
        _changesLoader = currentContextService.ChangesLoader ?? throw new("ChangesLoader is null");
        _currentContextService = currentContextService;
        _sender = sender;

        _subscription = new(OnChanged);
        _changesLoader.Subscribe(_subscription);
    }

    private void CreateTreeDataGridSource(TimeSeriesViewTreeViewModel value)
    {
        TreeSource = new HierarchicalTreeDataGridSource<TimeSeriesViewTreeViewModel>(value)
        {
            Columns =
            {
            new HierarchicalExpanderColumn<TimeSeriesViewTreeViewModel>(
                new TemplateColumn<TimeSeriesViewTreeViewModel>(
                    Resources.Resources.Name,
                    "LabelColumn",
                    null,
                    new GridLength(3, GridUnitType.Star)
                ),
                node => node.Children
                    .Select(childNode => childNode)
                    .ToList()
            ),
            new TextColumn<TimeSeriesViewTreeViewModel, string>(
                Resources.Resources.EditedBy, x => x.Entity.AuditInfo.ChangedUserId, new GridLength(2, GridUnitType.Star)),
            new TextColumn<TimeSeriesViewTreeViewModel, DateTime?>(
                Resources.Resources.EditedOn, x => x.Entity.AuditInfo.ChangedDate, new GridLength(1, GridUnitType.Star)),
            new TextColumn<TimeSeriesViewTreeViewModel, string>(
           Resources.Resources.CreatedBy, x => x.Entity.AuditInfo.CreatedUserId, new GridLength(2, GridUnitType.Star)),
            new TextColumn<TimeSeriesViewTreeViewModel, DateTime?>(
                Resources.Resources.CreatedOn, x => x.Entity.AuditInfo.CreatedDate, new GridLength(1, GridUnitType.Star)),
            }
        };

        TreeSource.Expand(new IndexPath(0));
        TreeSource.Expand(new IndexPath(0, 0));
        // see https://github.com/AvaloniaUI/Avalonia.Controls.TreeDataGrid/blob/master/docs/selection.md
        if (TreeSource.RowSelection != null)
            TreeSource.RowSelection.SelectionChanged += OnSelectionChanged;

    }

    void OnSelectionChanged(object? sender, TreeSelectionModelSelectionChangedEventArgs<TimeSeriesViewTreeViewModel> e)
    {
        var selectedItem = e.SelectedItems.FirstOrDefault();
        if (selectedItem is { IsLeaf: true })
        {
            Task.Run(() => _mainViewModel.AddTab(selectedItem.Label, selectedItem.Entity.Id));
            Dispatcher.UIThread.Post(() =>
            {
                TreeSource.RowSelection?.Clear();
            });
        }
    }


    protected async override Task LoadData(CancellationToken cancellationToken)
    {
        using var _ = StartLoading();
        var result = await _sender.Send(new TimeSeriesViewTreeQuery(_currentContextService.Connection!), cancellationToken);

        if (result.IsFailure)
        {
            await ShowErrorMessage(result.Error);
            return;
        }
        var tree = result.Value;
        RootFolder = Traverse(tree.RootNode);
    }

    async Task OnChanged(ChangesLoader.ObjectChangeReason reason, List<ITimeSeriesView> timeSeriesViews, List<int> primaryKeys)
    {
        var viewModels = timeSeriesViews.Select(t => new TimeSeriesViewViewModel(t)).ToList();
        await Dispatcher.UIThread.InvokeAsync(
            () =>
            {
                _treeUpdater.UpdateTree(RootFolder!, reason, viewModels, primaryKeys);
                CreateTreeDataGridSource(RootFolder!);
            });
    }

    static TimeSeriesViewTreeViewModel Traverse(IEntityTreeNode<ITimeSeriesView> node)
    {
        return node.Children.Count == 0 ? new TimeSeriesViewTreeViewModel(node.Entity) : new TimeSeriesViewTreeViewModel(node);
    }

    public override void Dispose()
    {
        _changesLoader.Unsubscribe(_subscription);
        _subscription.Dispose();
        GC.SuppressFinalize(this);
    }

    static async Task ShowErrorMessage(Error error)
    {
        if (error == Error.Silent) return;
        await FrontEndServiceLocator.Get.GetRequiredService<IMessageBoxHelper>().ShowErrorMsgBoxAsync(
            Resources.Resources.LoadData_Error_Title,
            string.Format(Resources.Resources.LoadData_Error_Description, error.Code, error.Title, error.Description));
    }
}