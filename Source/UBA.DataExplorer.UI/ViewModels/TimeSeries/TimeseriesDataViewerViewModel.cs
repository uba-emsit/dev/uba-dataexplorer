﻿using System.Collections.ObjectModel;
using Microsoft.Extensions.DependencyInjection;
using Avalonia.Controls;
using Avalonia.Threading;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MediatR;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UseCases.MessageServer;
using UBA.DataExplorer.UseCases.TimeSeries;
using UBA.DataExplorer.UseCases.TimeSeriesView;
using UBA.DataExplorer.Domain;
using UBA.DataExplorer.UI.ViewModels.Descriptor;


#if DEBUG
using UBA.DataExplorer.Domain.Faker;
using Moq;
#endif

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesDataViewerViewModel : DataLoadViewModel
{
    [ObservableProperty] TimeSeriesRibbonViewModel _ribbonViewModel;

    [ObservableProperty] ObservableCollection<TimeSeriesTempDataModel> _timeSeries = [];
    [ObservableProperty] ObservableCollection<IDimension> _usedDimensions = [];
    [ObservableProperty] ObservableCollection<ContextMenuItemViewModel> _contextMenuItems = [];
    [ObservableProperty] object? _selectedItem;
    [ObservableProperty] int? _selectedIndex = 0;
    [ObservableProperty] ITimeSeriesView? _timeSeriesView;

    internal event EventHandler? AllDataLoaded;

    readonly ISender _sender;
    readonly string _timeSeriesViewId;
    readonly IMessageBoxHelper _messageBoxHelper;
    readonly CurrentContextService _currentContextService;
    ChangesLoader.Subscription? _subscription;

    Task<Result<ITimeSeriesView>>? _tsvLoadTask;
    Task<Result<TimeSeriesByViewQueryResult>>? _tsLoadTask;
    Task<bool>? _processTimeSeries;

#if DEBUG
    #region Designer
    public TimeSeriesDataViewerViewModel() : this(null!, null!, null!)
    {
        if (Design.IsDesignMode)
            GenerateDesignData();
    }

    private void GenerateDesignData()
    {
        var tsViewFaker = new TimeSeriesViewFakes();
        var dimensionFaker = new DimensionFakes();
        var descriptorFilterFaker = new DescriptorFilterSettingsFaker();
        var tsFaker = new TimeSeriesFakes();
        var tsDataFaker = new TimeSeriesDataFaker();

        TimeSeriesView = tsViewFaker.Get();
        var usedDimensions = dimensionFaker.GetList(3);
        UsedDimensions = new ObservableCollection<IDimension>(usedDimensions);

        var descriptorFilters = usedDimensions.Select(dimension =>
        {
            var filter = descriptorFilterFaker.Generate();
            var mockFilter = Mock.Get(filter);
            mockFilter.SetupGet(f => f.DimensionPrimaryKey).Returns(dimension.PrimaryKey);
            return filter;
        }).ToList();

        var mockTsFilter = Mock.Get(TimeSeriesView.TsFilter);
        mockTsFilter.SetupGet(f => f.DescriptorFilters).Returns(descriptorFilters);

        var timeSeriesList = tsFaker.GetList(3);
        var queryFake = new TimeSeriesByViewQueryResult(timeSeriesList, usedDimensions);
        var designTsData = TsResultToTsModel(queryFake);

        var tsDataArray = timeSeriesList.Select(ts =>
            TimeSeriesDataViewModel.CreateFrom(ts, tsDataFaker.Generate(5).ToList()).ToList()
        ).ToArray();

        for (var i = 0; i < designTsData.Count; i++)
            designTsData[i].Data = tsDataArray[i];

        TimeSeries = new(designTsData);

        RaiseAllDataLoaded();
    }


    #endregion
#endif

    public TimeSeriesDataViewerViewModel(CurrentContextService currentContextService, ISender sender, string timeSeriesViewId)
    {
        _sender = sender;
        _timeSeriesViewId = timeSeriesViewId;
        _messageBoxHelper = FrontEndServiceLocator.Get.GetRequiredService<IMessageBoxHelper>();
        _currentContextService = currentContextService;
        _ribbonViewModel = new() { ParentStartLoading = StartLoading };
        RibbonViewModel.ReloadData += RibbonViewModel_ReloadData;
        RibbonViewModel.ReloadView += RibbonViewModel_ReloadView;
        _ribbonViewModel.SaveRequested += async (s, e) => await SaveData();
    }

    void RibbonViewModel_ReloadView(object? sender, EventArgs e)
    {
        if (IsLoading) return;
        Task.Run(() => LoadData(CancellationToken.None));
    }

    void RibbonViewModel_ReloadData(object? sender, EventArgs args)
    {
        if (IsLoading) return;
        Task.Run(ProcessTsData);
    }

    private async Task SaveData()
    {
        if (TimeSeriesView == null)
            return;

        var changedTimeSeriesData = TimeSeries
            .SelectMany(ts => ts.Data)
            .Where(ts => ts.IsDirty && ts.Model != null)
            .GroupBy(ts => ts.Model!)
            .ToDictionary(g => g.Key, g => g.First());

        if (changedTimeSeriesData.Count == 0)
            return;

        var result = await _sender.Send(new TimeSeriesDataWriteCommand(_currentContextService.Connection!, changedTimeSeriesData.Keys.ToList()));
        if (result.IsFailure)
        {
            await _messageBoxHelper.ShowDataWriteErrorMsgBoxAsync(EntityTypes.TimeSeriesData, result.Error);
            return;
        }

        // check if the data was really saved successfully
        var errorMessages = new List<string>();
        foreach (var resultValue in result.Value)
        {
            if (resultValue.ResponseType == WriteResponseType.Failed && resultValue.ErrorText != null)
            {
                errorMessages.Add(resultValue.ErrorText);
                continue;
            }

            if (resultValue.Model != null && changedTimeSeriesData.TryGetValue(resultValue.Model, out var ts))
            {
                ts.IsDirty = false;
            }
        }

        if (errorMessages.Count != 0)
            await _messageBoxHelper.ShowMultipleDataWriteErrorMsgBoxAsync(EntityTypes.TimeSeriesData, errorMessages.Count);
    }

    protected async override Task LoadData(CancellationToken cancellationToken)
    {
        _tsvLoadTask = TsvLoadImpl(cancellationToken);
        _tsLoadTask = TsLoadImpl(cancellationToken);
        _processTimeSeries = Task.Run(ProcessTimeSeries, cancellationToken);
        await Task.Run(ProcessTsData, cancellationToken);
    }

    void RaiseAllDataLoaded()
    {
        AllDataLoaded?.Invoke(this, EventArgs.Empty);
    }

    Task<Result<ITimeSeriesView>> TsvLoadImpl(CancellationToken cancellationToken)
        => Task.Run(() => _sender.Send(new TimeSeriesViewByIdQuery(_currentContextService.Connection!, _timeSeriesViewId), cancellationToken), cancellationToken);

    Task<Result<TimeSeriesByViewQueryResult>> TsLoadImpl(CancellationToken cancellationToken)
        => Task.Run(() => _sender.Send(new TimeSeriesByViewQuery(_currentContextService.Connection!, _timeSeriesViewId), cancellationToken), cancellationToken);

    [RelayCommand]
    void OnCellPointerPressed((DataGridCell?, DataGridColumn?) cellTuple)
    {
        var (_, column) = cellTuple;
        if (column?.Tag is TimeSeriesDataViewerColumnType columnType)
        {
            RibbonViewModel.SelectedColumnType = columnType;
        }
        else
        {
            RibbonViewModel.SelectedColumnType = TimeSeriesDataViewerColumnType.None;
        }
    }


    partial void OnSelectedItemChanged(object? value)
    {
        if (value is TimeSeriesTempDataModel model)
            SelectedIndex = TimeSeries.IndexOf(model);
    }

    async Task ProcessTsData()
    {
        using var _ = StartLoading();

        if (!await _processTimeSeries!)
            return;

        var tsResult = await _tsLoadTask!.ConfigureAwait(false);
        if (tsResult.IsFailure)
            return;

        var tsdResult = await GetValues(tsResult.Value.TimeSeries);
        if (tsdResult.IsFailure)
            return;

        foreach (var vm in TimeSeries)
        {
            vm.Data = tsdResult.Value.TryGetValue(vm.TimeSeries, out var datas)
                ? datas
                : [];
        }

        RaiseAllDataLoaded();
    }

    async Task<bool> ProcessTimeSeries()
    {
        using var _ = StartLoading();
        _subscription?.Dispose();
        _subscription = null;

        var tsvLoadResult = await _tsvLoadTask!;
        var tsLoadResult = await _tsLoadTask!;

        if (tsvLoadResult.IsFailure)
        {
            await _messageBoxHelper.ShowDataLoadErrorMsgBoxAsync(EntityTypes.TimeSeriesView, tsvLoadResult.Error);
            return false;
        }

        if (tsLoadResult.IsFailure)
        {
            if (tsLoadResult.Error.Type == ErrorType.NoData)
                return false;

            await _messageBoxHelper.ShowDataLoadErrorMsgBoxAsync(EntityTypes.TimeSeries, tsLoadResult.Error);
            return false;
        }

        TimeSeriesView = tsvLoadResult.Value;

        var timeRuleSettings = TimeSeriesView.TimeSettings.TimeRuleSettings.FirstOrDefault(x => x.TimeResolution == TimeSeriesView.Settings.MainTimeResolution);

        var timePeriod = timeRuleSettings?.GetRangeFromPeriod();
        if (timePeriod?.IsSuccess == true)
            RibbonViewModel.TimePeriodFrom.SetFromTimePeriod(timePeriod.Value);
        timePeriod = timeRuleSettings?.GetRangeToPeriod();
        if (timePeriod?.IsSuccess == true)
            RibbonViewModel.TimePeriodTo.SetFromTimePeriod(timePeriod.Value);

        var tsEntities = tsLoadResult.Value.TimeSeries;

        if (tsEntities.Count == 0)
            return false;

        var newTsData = TsResultToTsModel(tsLoadResult.Value);

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            UsedDimensions = new(tsLoadResult.Value.UsedDimensions);
            TimeSeries = new(newTsData);
        });

        _subscription = new ChangesLoader.TimeSeriesDataSubscription(OnChangeTimeSeriesData, tsLoadResult.Value.TimeSeries);
        _currentContextService.ChangesLoader?.Subscribe(_subscription);

        return true;
    }

    async Task<Result<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>> GetValues(List<ITimeSeries> tsEntities)
    {
        if (_tsvLoadTask == null)
            return Result.Success<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(new());
        var tsv = await _tsvLoadTask;
        if (tsv.IsFailure)
            return Result.Success<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(new());

        var hypothesisId = "REF";
        var from = RibbonViewModel.TimePeriodFrom.Date;
        var to = RibbonViewModel.TimePeriodTo.Date;
        var timeResolution = RibbonViewModel.TimePeriodFrom.TimeResolution;

        if (from == null || to == null)
            return Result.Success<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(new());

        var tsdResult =
            RibbonViewModel.ComboBoxValueModeVm.SelectedItem switch
            {
                TimeSeriesValueMode.Input => (await _sender.Send(new TimeSeriesDataByTimeSeriesListQuery(_currentContextService.Connection!, tsEntities, hypothesisId, from.Value, to.Value, timeResolution)))
                    .Map(d => d.ToDictionary(kvp => kvp.Key, kvp => TimeSeriesDataViewModel.CreateFrom(kvp.Key, kvp.Value).ToList())),
                TimeSeriesValueMode.Mapping => (await _sender.Send(new MappedTimeSeriesDataByTimeSeriesListQuery
                    (_currentContextService.Connection!, tsEntities, hypothesisId, from.Value, to.Value, timeResolution)))
                    .Map(d => d.ToDictionary(kvp => kvp.Key, kvp => TimeSeriesDataViewModel.CreateFrom(kvp.Key, kvp.Value).ToList())),
                TimeSeriesValueMode.Inherited => Result.Failure<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(Error.Failure(f => f.NotImplemented, "TimeSeriesValueMode.Inherited not implemented")),
                _ => Result.Failure<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(Error.Failure(f => f.NotImplemented, "TimeSeriesValueMode.Inherited not implemented"))
            };

        if (tsdResult.IsFailure)
        {
            await _messageBoxHelper.ShowDataLoadErrorMsgBoxAsync(EntityTypes.TimeSeriesData, tsdResult.Error);
            return Result.Failure<Dictionary<ITimeSeries, List<TimeSeriesDataViewModel>>>(tsdResult.Error);
        }

        var small = (from < to ? from : to).Value;
        var big = (from < to ? to : from).Value;
        var dates = new List<DateTimeOffset>();
        for (var date = small; date <= big; date = date.AddPeriodStep(timeResolution))
            dates.Add(date);
        if (from > to) dates.Reverse();

        return
            Result.Success(
            tsdResult.Value.ToDictionary(kv => kv.Key, kv =>
            {
                var dic = kv.Value.ToDictionary(d => d.Coordinates.TimePeriod1.Date);
                return dates.Select(date => dic.TryGetValue(date, out var data) ? data
                    : TimeSeriesDataViewModel.CreateEmpty(date, timeResolution))
                    .ToList();
            }));
    }

    async Task OnChangeTimeSeriesData(ChangesLoader.ObjectChangeReason reason, List<int> timeSeriesNrs)
    {
        using var _ = StartLoading();

        var hashSet = timeSeriesNrs.ToHashSet();
        var models = TimeSeries
            .Where(t => hashSet.Contains(t.TimeSeries.PrimaryKey))
            .ToDictionary(m => m.TimeSeries.PrimaryKey);
        var timeSeriesList = models.Values.Select(m => m.TimeSeries).ToList();

        var tsdResult = await GetValues(timeSeriesList);
        if (tsdResult.IsFailure) return;

        foreach (var (timeSeries, datas) in tsdResult.Value)
        {
            if (!models.TryGetValue(timeSeries.PrimaryKey, out var model)) continue;

            var existingDatas = model.Data.ToDictionary(d => d.Coordinates.TimePeriod1);
            foreach (var data in datas)
            {
                if (existingDatas.TryGetValue(data.Coordinates.TimePeriod1, out var existingData))
                    existingData.UpdateProperties(data);
            }
        }
    }

    static List<TimeSeriesTempDataModel> TsResultToTsModel(TimeSeriesByViewQueryResult timeSeriesResult)
    {
        var result = new List<TimeSeriesTempDataModel>();

        var index = 1;
        foreach (var timeSeries in timeSeriesResult.TimeSeries)
        {
            var descriptors = timeSeries.TimeSeriesKeys
                .Select(k => new DescriptorViewModel
                {
                    Id = k.Descriptor.Id,
                    Name = k.Descriptor.Label,
                    Dimension = k.Descriptor.Dimension
                })
                .ToList();

            var dimensionsToAdd = timeSeriesResult.UsedDimensions
                .Except(descriptors.Select(x => x.Dimension))
                .ToList();

            descriptors.AddRange(dimensionsToAdd.Select(dimension => new DescriptorViewModel { Id = "", Name = "", Dimension = dimension }));

            result.Add(new(
                index++,
                timeSeries,
                descriptors.OrderBy(x => x.Dimension.Id).ToList(),
                timeSeries.Unit,
                "##year",
                "##REF"));
        }
        return result;
    }

    public override void Dispose()
    {
        _subscription?.Dispose();
        GC.SuppressFinalize(this);
    }
}