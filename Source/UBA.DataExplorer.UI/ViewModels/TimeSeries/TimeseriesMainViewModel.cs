﻿using System.Collections.ObjectModel;
using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MediatR;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Services;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimeSeriesMainViewModel : ViewModelBase
{
    [ObservableProperty] ObservableCollection<TimeSeriesTabItemModel> _tabItems = [];
    [ObservableProperty] TimeSeriesTabItemModel? _selectedTab;

    readonly CurrentContextService _currentContextService = null!;
    readonly ISender _sender = null!;

#if DEBUG
    #region Designer

    public TimeSeriesMainViewModel()
    {
        if (Design.IsDesignMode)
        {
            var treeViewModel = new TimeSeriesTreeViewModel();
            TabItems.Add(new TimeSeriesTabItemModel("##Home", treeViewModel, true));
        }
    }

    #endregion
#endif

    public TimeSeriesMainViewModel(CurrentContextService currentContextService, ISender sender)
    {
        _currentContextService = currentContextService;
        _sender = sender;

        // Initialize TabItems collection with a default tab (e.g., Home tab)
        var mainTab = new TimeSeriesTabItemModel("##Home", new TimeSeriesTreeViewModel(currentContextService, sender, this), true);
        TabItems.Add(mainTab);

        SelectedTab = mainTab;
    }

    public Task AddTab(string name, string timeSeriesViewId)
    {
        var viewmodel = new TimeSeriesDataViewerViewModel(_currentContextService, _sender, timeSeriesViewId);

        var newTab = new TimeSeriesTabItemModel($"{name}", viewmodel);
        TabItems.Add(newTab);
        SelectedTab = newTab;

        return Task.CompletedTask;
    }

    [RelayCommand]
    void CloseTab(TimeSeriesTabItemModel model)
    {
        TabItems.Remove(model);
        model.Dispose();
    }

    public override void Dispose()
    {
        foreach (var tab in TabItems.ToList())
            CloseTab(tab);
        GC.SuppressFinalize(this);
    }
}