﻿using System.Collections.ObjectModel;
using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.UI.Models;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;
using UBA.DataExplorer.UseCases.Databases;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class MainWindowViewModel : ViewModelBase
{
    [ObservableProperty] string? _selectedDatabase;
    [ObservableProperty] string? _currentUser;
    [ObservableProperty] ObservableCollection<NavigationItemModel> _navigationItems;
    [ObservableProperty] NavigationItemModel? _selectedNavigationItem;
    [ObservableProperty] ViewModelBase? _currentContentPage;
    [ObservableProperty] bool _isPaneOpen = true;
    [ObservableProperty] bool _navigationEnabled;
    private readonly CurrentContextService _currentContextService;

    public MainWindowViewModel() : this(FrontEndServiceLocator.Get.GetRequiredService<CurrentContextService>())
    {
        if (Design.IsDesignMode) CurrentUser = "TestUser";
    }

    MainWindowViewModel(CurrentContextService currentContextService)
    {
        _currentContextService = currentContextService;
        _currentContextService.ContextChanged += CurrentContextService_ContextChanged;
        NavigationItems =
        [
            new(typeof(ContentViewModel), "fa-solid fa-house", Resources.Resources.MainViewModel_Home),
            new(typeof(ContentViewModel), "fa-solid fa-magnifying-glass", Resources.Resources.MainViewModel_Search),
            new(typeof(TimeSeriesMainViewModel), "fa-solid fa-table", Resources.Resources.MainViewModel_TimeSeriesTree),
            new(typeof(ContentViewModel), "fa-solid fa-file-excel", Resources.Resources.MainViewModel_Reports),
            new(typeof(ContentViewModel), "fa-solid fa-calculator", Resources.Resources.MainViewModel_CalculationMethods),
            new(typeof(ContentViewModel), "fa-solid fa-robot", Resources.Resources.MainViewModel_Automation),
            new(typeof(ContentViewModel), "fa-solid fa-gear", Resources.Resources.MainViewModel_Settings)
        ];

        ShowLogin();
    }

    [RelayCommand]
    void TogglePane()
    {
        IsPaneOpen = !IsPaneOpen;
    }

    [RelayCommand]
    void SwitchDatabase()
    {
        if (_currentContextService.Connection is { IsDisposed: false })
        {
            _currentContextService.Connection.Dispose();
            var sender = FrontEndServiceLocator.Get.GetRequiredService<ISender>();
            sender.Send(new CloseDatabaseConnectionCommand(_currentContextService.Connection));
            _currentContextService.ClearDatabase();
        }
        FrontEndServiceLocator.ResetScope();
        ShowDatabaseSelector();
    }

    partial void OnSelectedNavigationItemChanged(NavigationItemModel? value)
    {
        if (value is null) return;
        ChangeContentView(value.ModelType);
    }

    void ShowLogin()
    {
        CurrentContentPage = FrontEndServiceLocator.Get.GetRequiredService<LoginViewModel>();
    }

    void ShowDatabaseSelector()
    {
        CurrentContentPage = FrontEndServiceLocator.Get.GetRequiredService<DatabaseSelectorViewModel>();
    }

    void ChangeContentView(Type viewModelType)
    {
        var viewModel = FrontEndServiceLocator.Get.GetRequiredService(viewModelType)
            ?? throw new InvalidOperationException($"Service of type {viewModelType} not found in IoC container.");

        if (viewModel is not ViewModelBase viewModelBase) return;
        CurrentContentPage = viewModelBase;
    }

    void CurrentContextService_ContextChanged(object? sender, EventArgs e)
    {
        SelectedDatabase = _currentContextService.Connection?.IsDisposed == true
            ? null
            : _currentContextService.Connection?.DatabaseInfo.Label;

        CurrentUser = _currentContextService.CurrentlyDoingReLogin
            ? Resources.Resources.ReLogin
            : _currentContextService.Login?.UserModel.Name;

        // set content view to main view and enable navigation
        NavigationEnabled = SelectedDatabase != null;

        if (_currentContextService is { CurrentlyDoingReLogin: false, Login: null })
        {
            ShowLogin();
            return;
        }

        if (SelectedDatabase != null && CurrentContentPage is DatabaseSelectorViewModel)
        {
            SelectedNavigationItem = NavigationItems[0];
            ChangeContentView(typeof(ContentViewModel));
            var dataService = FrontEndServiceLocator.Get.GetRequiredService<IDataService>();
            Task.Run(() => dataService.LoadDataAsync());
        }

        if (SelectedDatabase == null && CurrentContentPage is not DatabaseSelectorViewModel)
            SwitchDatabase();
    }
}