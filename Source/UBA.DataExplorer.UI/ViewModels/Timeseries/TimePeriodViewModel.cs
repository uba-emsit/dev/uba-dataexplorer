﻿using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.UI.ViewModels.TimeSeries;

public partial class TimePeriodViewModel : ViewModelBase
{
    [ObservableProperty] DateTimeOffset? _date;
    [ObservableProperty] TimeResolution _timeResolution = TimeResolution.Year;

    public bool ShowMonth => TimeResolution != TimeResolution.Year;
    public int? Width => ShowMonth ? null : 50;

    public TimePeriodViewModel()
    {
        if (Design.IsDesignMode) Date = DateTimeOffset.Now;
    }

    public static TimePeriodViewModel Empty => new();

    public void SetFromTimePeriod(ITimePeriod timePeriod)
    {
        Date = timePeriod.Date;
        TimeResolution = timePeriod.TimeResolution;
    }

    public override string ToString() => Date.ToString(TimeResolution);
}