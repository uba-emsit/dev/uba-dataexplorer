﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;

namespace UBA.DataExplorer.UI.ViewModels.Descriptor;

public class DescriptorViewModel
{
    public required string Id { get; init; }
    public required string Name { get; init; }
    public required IDimension Dimension { get; init; }
}