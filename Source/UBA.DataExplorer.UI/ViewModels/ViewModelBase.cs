﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace UBA.DataExplorer.UI.ViewModels;

public abstract class ViewModelBase : ObservableObject, IDisposable
{
    public virtual void Dispose() => GC.SuppressFinalize(this);
}