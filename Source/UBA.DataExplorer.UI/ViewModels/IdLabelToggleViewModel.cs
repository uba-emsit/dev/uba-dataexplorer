﻿using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UI.ViewModels;

/// <summary>
/// A view model that listens to a ViewScope and displays the ID or Name of an IIdAndNameModel.
/// </summary>
public partial class IdLabelToggleViewModel : ViewModelBase
{
    readonly IIdAndLabelModel _model;
    readonly IDisposable _subscriber;
    [ObservableProperty] string _mainText = "";
    [ObservableProperty] string _subText = "";
    [ObservableProperty] bool _hasSubText;

    public IdLabelToggleViewModel(IIdAndLabelModel model, ViewScope viewScope)
    {
        _model = model;
        _subscriber = viewScope.Subscribe(ProcessViewScope);
        ProcessViewScope(viewScope.IdLabel);
    }

    void ProcessViewScope(IdLabel idOrLabel)
    {
        MainText = idOrLabel switch
        {
            IdLabel.Id or IdLabel.IdLabel => _model.Id,
            IdLabel.Label or IdLabel.LabelId => _model.Label,
            _ => _model.Label
        };

        SubText = idOrLabel switch
        {
            IdLabel.Id or IdLabel.Label => "",
            IdLabel.IdLabel => _model.Label,
            IdLabel.LabelId => _model.Id,
            _ => ""
        };

        HasSubText = idOrLabel is IdLabel.IdLabel or IdLabel.LabelId;
    }

    public override void Dispose()
    {
        _subscriber.Dispose();
        base.Dispose();
    }
}