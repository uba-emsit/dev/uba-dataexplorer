﻿using System.Collections.ObjectModel;
using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using MediatR;
#if DEBUG
using UBA.DataExplorer.Domain.Faker;
#endif
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UseCases.Databases;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class DatabaseSelectorViewModel : ViewModelBase
{
    const int MAX_SHORTLIST_LENGTH = 5;

    [ObservableProperty] ObservableCollection<DatabaseViewModel> _shortlist = [];
    [ObservableProperty] ObservableCollection<DatabaseViewModel> _overflow = [];
    [ObservableProperty] bool _loading;
    [ObservableProperty] DatabaseViewModel? _selectedItem;
    [ObservableProperty] string? _currentUser;

    readonly CurrentContextService _currentContextService = null!;
    readonly ISender _sender = null!;
    readonly IMessageBoxHelper _messageBoxHelper;

#if DEBUG
    #region Designer
#pragma warning disable CS8618
    // For use by design mode
    public DatabaseSelectorViewModel()
#pragma warning restore CS8618
    {
        if (Design.IsDesignMode)
        {
            CurrentUser = "TestUser";
            var faker = new DatabaseFakes();
            faker.GetList(2).ForEach(x => Shortlist.Add(new(x)));
            faker.GetList(4).ForEach(x => Overflow.Add(new(x)));
        }
    }
    #endregion
#endif

    public DatabaseSelectorViewModel(CurrentContextService currentContextService, ISender sender, IMessageBoxHelper messageBoxHelper)
    {
        _currentContextService = currentContextService;
        CurrentUser = _currentContextService.Login?.UserModel.Name;
        _sender = sender;
        _messageBoxHelper = messageBoxHelper;

        Task.Run(LoadDatabases);
    }

    public async Task OnSelectDatabase(DatabaseViewModel? databaseViewModel)
    {
        if (databaseViewModel == null || _currentContextService.Login == null)
            return;

        Loading = true;

        var cancelSource = new CancellationTokenSource();
        var result = await Task.Run(() => databaseViewModel.ConnectToDatabase(_currentContextService.Login, cancelSource.Token), cancelSource.Token);

        if (result.IsSuccess)
        {
            var (connectionModel, changesLoader, objectLocker) = result.Value;
            _currentContextService.SetDatabase(connectionModel, changesLoader, objectLocker);
        }
        else
        {
            await _messageBoxHelper.ShowErrorMsgBoxAsync(
                Resources.Resources.DatabaseSelectorView_ConnectingErrorTitle,
                string.Format(Resources.Resources.DatabaseSelectorView_ConnectingError, result.Error.Code, result.Error.Title, result.Error.Description, databaseViewModel.Id));

            SelectedItem = null;
        }

        Loading = false;
    }

    private async Task LoadDatabases()
    {
        Loading = true;
        var query = new LoadAvailableProjectDatabasesQuery(_currentContextService.Login!);

        var result = await _sender.Send(query, CancellationToken.None);
        if (result.IsSuccess)
        {
            Shortlist.Clear();
            Overflow.Clear();

            var databases = new List<DatabaseViewModel>(result.Value.Select(dbInfo => new DatabaseViewModel(dbInfo)));
            var anyRecentlyUsed = databases.Any(db => db.RecentlyUsedPosition > 0);

            foreach (var database in databases.OrderBy(db => db.SortNr))
            {
                // Three cases where DBs move into the shortlist, which should not be empty:
                // * Few DBs present, no overflow needed -> add all DBs
                // * Many DBs present, but some recently used -> add recently used DBs
                // * Many DBs present, but none recently used -> add the start of the DB list
                if (databases.Count < MAX_SHORTLIST_LENGTH ||
                    ((database.RecentlyUsedPosition > 0 || !anyRecentlyUsed) && Shortlist.Count < MAX_SHORTLIST_LENGTH))
                    Shortlist.Add(database);
                else
                    Overflow.Add(database);
            }

            // Auto-login to favorite database
            if (databases.Find(db => db.IsFavorite) is { } favorite)
                await OnSelectDatabase(favorite);
        }
        else
        {
            await _messageBoxHelper.ShowErrorMsgBoxAsync(
                Resources.Resources.DatabaseSelectorView_ConnectingErrorTitle,
                string.Format(Resources.Resources.DatabaseSelectorView_ConnectingError, result.Error.Code, result.Error.Title, result.Error.Description, "System"));
        }

        Loading = false;
    }
}