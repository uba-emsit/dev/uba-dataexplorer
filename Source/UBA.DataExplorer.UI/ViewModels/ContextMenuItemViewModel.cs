﻿using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace UBA.DataExplorer.UI.ViewModels;

public class ContextMenuItemViewModel : ObservableObject
{
    public string Header { get; set; }
    public RelayCommand Command { get; set; }

    public ContextMenuItemViewModel(string header, RelayCommand command)
    {
        Header = header;
        Command = command;
    }
}