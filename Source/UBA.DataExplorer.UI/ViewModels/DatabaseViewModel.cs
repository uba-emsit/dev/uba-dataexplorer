﻿using CommunityToolkit.Mvvm.ComponentModel;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Databases;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class DatabaseViewModel(DatabaseModel model) : ObservableObject
{
    [ObservableProperty] string _guid = model.Guid;
    [ObservableProperty] string _description = model.Description;
    [ObservableProperty] string _id = model.Id;
    [ObservableProperty] string _label = model.Label;
    [ObservableProperty] int _sortNr = model.SortNr;
    [ObservableProperty] int _recentlyUsedPosition = model.RecentlyUsedPosition;
    [ObservableProperty] bool _isFavorite = model.IsFavorite;

    public async Task<Result<OpenDatabaseConnectionCommandResponse>> ConnectToDatabase(LoginModel login, CancellationToken cancellationToken)
    {
        var sender = FrontEndServiceLocator.Get.GetRequiredService<ISender>();

        return await sender.Send(new OpenDatabaseConnectionCommand(login, model), cancellationToken);
    }
}