﻿using Avalonia.Controls;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;

namespace UBA.DataExplorer.UI.ViewModels;

/// <summary>
/// Base class for data loading view models. It provides a mechanism to prevent loading the data multiple times.
/// </summary>
/// <remarks>DataLoadViewModel implements a loading mechanism. There are no properties that expose the loaded data.</remarks>
public abstract partial class DataLoadViewModel : ViewModelBase
{
    [ObservableProperty] bool _isLoading;

    // To prevent Avalonia from loading the data multiple times (when switching tabs)
    bool _firstTimeLoaded;

    readonly LoadingIncrementer _incrementer;

    protected DataLoadViewModel()
    {
        _incrementer = new(b => IsLoading = b);
    }

    [RelayCommand]
    async Task LoadDataOnStartUp(CancellationToken cancellationToken)
    {
        if (Design.IsDesignMode || _firstTimeLoaded)
            return;

        _firstTimeLoaded = true;
        await LoadData(cancellationToken);
    }

    [RelayCommand]
    Task ReloadData(CancellationToken cancellationToken) => LoadData(cancellationToken);

    protected abstract Task LoadData(CancellationToken cancellationToken);

    protected LoadingAnimation StartLoading() => new(_incrementer);
}