﻿using Avalonia.Threading;
using CommunityToolkit.Mvvm.ComponentModel;
using CommunityToolkit.Mvvm.Input;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UseCases.Abstractions;
using UBA.DataExplorer.UseCases.Login;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class LoginViewModel : ViewModelBase
{
    [ObservableProperty] string _username = "";
    [ObservableProperty] string _password = "";
    [ObservableProperty] bool _isLoggingIn;
    [ObservableProperty] string _statusMessage = "";

    readonly CurrentContextService _currentContextService;
    readonly ISender _sender;

#if DEBUG
    #region Designer
    public LoginViewModel() : this(FrontEndServiceLocator.Get.GetRequiredService<CurrentContextService>(),
        FrontEndServiceLocator.Get.GetRequiredService<ISender>())
    {
        Username = "DesignUser";
        Password = "DesignPassword";
    }
    #endregion
#endif

    public LoginViewModel(CurrentContextService currentContextService, ISender sender)
    {
        _currentContextService = currentContextService;
        _sender = sender;

        Task.Run(() => LoginAsync(new LoginWithWindowsAccountCommand(), Resources.Resources.LoginView_WindowsLoggingInMsg, true));
    }


    [RelayCommand]
    async Task Login()
    {
        await LoginAsync(new LoginCommand(_username, _password), string.Format(Resources.Resources.LoginView_LoggingInMsg, _username));
    }

    private async Task LoginAsync(ICommand<LoginCommandResponse> command, string statusMessage, bool ignoreErrors = false)
    {
        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            IsLoggingIn = true;
            StatusMessage = statusMessage;
        });

        var loginResponse = await _sender.Send(command);
        if (loginResponse.IsSuccess)
        {
            _currentContextService.SetLogin(loginResponse.Value.LoginModel);
        }
        else
        {
            if (!ignoreErrors)
            {
                await FrontEndServiceLocator.Get.GetRequiredService<IMessageBoxHelper>().ShowErrorMsgBoxAsync(
                    Resources.Resources.DatabaseSelectorView_ConnectingErrorTitle,
                    string.Format(Resources.Resources.DatabaseSelectorView_ConnectingError, loginResponse.Error.Code,
                        loginResponse.Error.Title, loginResponse.Error.Description, "System"));
            }
        }

        await Dispatcher.UIThread.InvokeAsync(() =>
        {
            IsLoggingIn = false;
            StatusMessage = "";
        });
    }

}