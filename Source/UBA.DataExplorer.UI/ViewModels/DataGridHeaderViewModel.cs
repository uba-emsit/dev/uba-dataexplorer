﻿using CommunityToolkit.Mvvm.ComponentModel;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class DataGridHeaderViewModel : ViewModelBase, IDataGridHeader
{
    [ObservableProperty] string _displayText = "";

#if DEBUG
    #region Designer
    public DataGridHeaderViewModel() : this("Design Header")
    {
    }
    #endregion
#endif

    public DataGridHeaderViewModel(string displayText)
    {
        DisplayText = displayText;
    }
}


