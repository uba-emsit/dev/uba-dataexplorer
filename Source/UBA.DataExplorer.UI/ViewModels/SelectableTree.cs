﻿using System.Collections.ObjectModel;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.UI.ViewModels;

public partial class SelectableTreeItem<T> : SelectableItem<T>
    where T : class, IEntityTreeNode<ITreeNodeNode>, IPrimaryKey, IIdAndLabelModel
{
    private ReadOnlyCollection<SelectableTreeItem<T>> _children;

    public SelectableTreeItem(T model, bool initialState, ViewScope viewScope)
        : base(model, initialState, viewScope)
    {
        _children = model.Children?
                        .OfType<T>()
                        .Select(child => new SelectableTreeItem<T>(child, false, viewScope))
                        .ToList()
                        .AsReadOnly()
                    ?? new List<SelectableTreeItem<T>>().AsReadOnly();
    }

    public ReadOnlyCollection<SelectableTreeItem<T>> Children => _children;

    public void SortChildren<TSortKey>(Func<SelectableTreeItem<T>, TSortKey> keySelector)
    {
        if (_children.Count == 0)
            return;

        var sortedChildren = _children
            .OrderBy(keySelector)
            .ToList()
            .AsReadOnly();

        _children = sortedChildren;

        foreach (var child in _children)
        {
            child.SortChildren(keySelector);
        }
    }
}
