using Avalonia;
using Avalonia.Controls;
using Avalonia.Layout;
using Projektanker.Icons.Avalonia;

namespace UBA.DataExplorer.UI.Views.Controls;

public partial class BorderLabelMenuControl : UserControl
{
    public static readonly StyledProperty<string> LabelTextProperty =
        AvaloniaProperty.Register<BorderLabelMenuControl, string>(nameof(LabelText));


    public string LabelText
    {
        get => GetValue(LabelTextProperty);
        set => SetValue(LabelTextProperty, value);
    }

    public BorderLabelMenuControl()
    {
        InitializeComponent();

        if (Design.IsDesignMode)
        {
            LabelText = "Design-time Label";
            var button = new Button
            {
                Content = new StackPanel
                {
                    Orientation = Orientation.Vertical,
                    Children =
                    {
                        new Icon
                        {
                            Value = "fa-solid fa-floppy-disk",
                            HorizontalAlignment = HorizontalAlignment.Center
                        },
                        new TextBlock
                        {
                            Text = "Save View",
                            HorizontalAlignment = HorizontalAlignment.Center
                        }
                    }
                }
            };

            button.Classes.Add("MenuLarge");
            Content = button;
        }
    }

}