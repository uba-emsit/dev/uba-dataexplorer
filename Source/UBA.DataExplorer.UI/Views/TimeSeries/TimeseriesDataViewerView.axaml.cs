using System.Collections.ObjectModel;
using System.Collections.Specialized;
using System.Diagnostics;
using Avalonia;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Markup.Xaml.Templates;
using Avalonia.Threading;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.UI.Models.Descriptors;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.ViewModels;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;


namespace UBA.DataExplorer.UI.Views.TimeSeries;

public partial class TimeSeriesDataViewerView : UserControl
{
    HashSet<TimeSeriesViewGridColumn> _visibleColumns = [];
    readonly ViewScope _viewScope = new();

    const string SingleHeaderTag = "SingleLineHeader";
    const string IndexHeaderTag = "IndexHeader";
    const string DimensionColumnHeaderTag = "DimensionColumnHeader";

    public TimeSeriesDataViewerView()
    {
        InitializeComponent();
        TsDataGrid.GetObservable(DataGrid.ItemsSourceProperty).Subscribe(OnItemsSourceChanged);
        TsDataGrid.CellPointerPressed += TsDataGrid_CellPointerPressed;
        TsDataGrid.ColumnReordered += TsDataGrid_ColumnReordered;
        TsDataGrid.ColumnDisplayIndexChanged += TsDataGrid_ColumnDisplayIndexChanged;

#if DEBUG
        #region  Designer
        if (Design.IsDesignMode)
        {
            DataContext = new TimeSeriesDataViewerViewModel();
        }
        #endregion
#endif
    }

    void TsDataGrid_ColumnReordered(object? sender, DataGridColumnEventArgs e)
    {
        if (e.Column.DisplayIndex == 0 && TsDataGrid.Columns.Count > 1) e.Column.DisplayIndex = 1;
    }

    void TsDataGrid_ColumnDisplayIndexChanged(object? sender, DataGridColumnEventArgs e)
    {
        //todo sort time series and recalculate numbers
    }

    // Subscribe to data changes so we can dynamically add columns
    void OnItemsSourceChanged(object newItemSource)
    {
        if (TsDataGrid.ItemsSource is not ObservableCollection<TimeSeriesTempDataModel> itemsSource)
            return;

        if (newItemSource is INotifyCollectionChanged notifyCollection)
        {
            // Subscribe to CollectionChanged event to observe item-level changes
            notifyCollection.CollectionChanged += OnCollectionChanged;
            OnCollectionChanged(newItemSource, new NotifyCollectionChangedEventArgs(NotifyCollectionChangedAction.Reset));
        }

        if (DataContext is TimeSeriesDataViewerViewModel viewModel)
        {
            viewModel.AllDataLoaded -= GridViewModelOnAllDataLoaded;
            viewModel.AllDataLoaded += GridViewModelOnAllDataLoaded;
        }
    }

    void GridViewModelOnAllDataLoaded(object? sender, EventArgs e)
    {
        Dispatcher.UIThread.InvokeAsync(() =>
        {
            if (sender is not TimeSeriesDataViewerViewModel vm)
                return;

            var item = vm.TimeSeries.FirstOrDefault();

            if (item == null)
                return;

            foreach (var column in TsDataGrid.Columns
                         .Where(c => c.Tag as TimeSeriesDataViewerColumnType? == TimeSeriesDataViewerColumnType.Value)
                         .ToList())
                TsDataGrid.Columns.Remove(column);

            var index = 0;
            foreach (var yearData in item.Data)
            {
                AddDataColumn(yearData.Period, index++);
            }
        });
    }

    // Dynamically add columns based on the added data
    void OnCollectionChanged(object? sender, NotifyCollectionChangedEventArgs e)
    {
        if (sender is not ObservableCollection<TimeSeriesTempDataModel> dataCollection)
            return;

        var item = dataCollection.FirstOrDefault();

        if (item == null)
            return;

        var ctx = DataContext as TimeSeriesDataViewerViewModel;

        Debug.Assert(ctx != null, $"ctx should be {nameof(TimeSeriesDataViewerViewModel)}");

        TsDataGrid.Columns.Clear();

        if (!Design.IsDesignMode)
        {
            _visibleColumns =
            [
                ..ctx.TimeSeriesView!.Settings.Columns
                .Where(c => c.IsVisible)
                .Select(c => c.GridColumn)
            ];
        }

        AddColumn("", "Number", TimeSeriesDataViewerColumnType.None, templateTag: IndexHeaderTag);
        AddColumn("Id", "Id", TimeSeriesDataViewerColumnType.TimeSeries, gridColumn: TimeSeriesViewGridColumn.Name, canUserReorder: true);
        AddColumn("Label", "Label", TimeSeriesDataViewerColumnType.TimeSeries, gridColumn: TimeSeriesViewGridColumn.Name, canUserReorder: true);

        var allDimensions = ctx.UsedDimensions
            .OrderBy(x => x.Id)
            .ToList();

        // Add a column for each year in the data
        var index = 0;
        foreach (var dimension in allDimensions)
        {
            var filter = ctx.TimeSeriesView!.TsFilter.DescriptorFilters
                .FirstOrDefault(x => x.DimensionPrimaryKey == dimension.PrimaryKey);

            if (filter == null)
                continue;

            var columnDescriptorFilter = new ColumnDescriptorFilter(filter, dimension.PrimaryKey, _viewScope);

            AddColumn(dimension.Name, $"Descriptors[{index++}].Id", TimeSeriesDataViewerColumnType.Dimension,
                gridColumn: (TimeSeriesViewGridColumn?)dimension.PrimaryKey, templateTag: DimensionColumnHeaderTag,
                columnDescriptorFilter: columnDescriptorFilter);
        }

        AddColumn("Unit", "Unit", TimeSeriesDataViewerColumnType.Unit, TimeSeriesViewGridColumn.Unit);
        AddColumn("Hypothesis", "Hypothesis", TimeSeriesDataViewerColumnType.Hypothesis, TimeSeriesViewGridColumn.Hypo, templateTag: DimensionColumnHeaderTag);
        AddColumn("TimeResolution", "TimeResolution", TimeSeriesDataViewerColumnType.TimeResolution, TimeSeriesViewGridColumn.TimeKey);

        // TODO: this need loaded from the view in the future, currently frozen column count is to hight, so we disable it
        //TsDataGrid.FrozenColumnCount = TsDataGrid.Columns.Count;

        // Add a column for each year in the data
        index = 0;
        foreach (var yearData in item.Data)
        {
            var period = yearData.Period;
            AddDataColumn(period, index++);
        }

        TsDataGrid.InvalidateVisual();
    }

    void AddColumn(string header, string valueBindingPath, TimeSeriesDataViewerColumnType tag, TimeSeriesViewGridColumn? gridColumn = null,
        string templateTag = SingleHeaderTag, bool canUserReorder = false, ColumnDescriptorFilter? columnDescriptorFilter = null)
    {
        IDataGridHeader headerViewModel = new DataGridHeaderViewModel(header);
        if (columnDescriptorFilter != null)
        {
            headerViewModel = new DimensionHeaderViewModel(header, columnDescriptorFilter);
        }

        var column = new DataGridTextColumn
        {
            IsVisible = !gridColumn.HasValue || _visibleColumns.Contains(gridColumn.Value) || Design.IsDesignMode,
            Header = headerViewModel,
            Binding = new Binding(valueBindingPath),
            Tag = tag,
            CanUserReorder = canUserReorder,
            CanUserSort = false,
            IsReadOnly = true
        };

        if (Resources.TryGetValue(templateTag, out var headerTemplate) && headerTemplate is DataTemplate template)
        {
            column.HeaderTemplate = template;
        }

        //column.HeaderPointerPressed += Column_HeaderPointerPressed;
        AddColumnAndBinding(column);
    }

    private Dictionary<string, Binding> _bindingCache = new Dictionary<string, Binding>();

    void AddDataColumn(string header, int index, string templateTag = SingleHeaderTag)
    {
        var headerViewModel = new DataGridHeaderViewModel(header);
        var bindingPath = $"Data[{index}]";

        var column = new DataGridTimeSeriesValueColumn
        {
            Header = headerViewModel,
            Tag = TimeSeriesDataViewerColumnType.Value,
            Binding = new Binding($"{bindingPath}.Value"),
            DataIndex = index,
            IsReadOnly = false,
        };

        if (Resources.TryGetValue(templateTag, out var headerTemplate) && headerTemplate is DataTemplate template)
        {
            column.HeaderTemplate = template;
        }

        //column.HeaderPointerPressed += Column_HeaderPointerPressed;
        AddColumnAndBinding(column);
    }

    // Because we're dynamically creating columns here, we can't set it up in the axaml file
    void AddColumnAndBinding(DataGridColumn column)
    {
        TsDataGrid.Columns.Add(column);
    }

    void TsDataGrid_CellPointerPressed(object? sender, DataGridCellPointerPressedEventArgs e)
    {
        var cellColumn = (e.Cell, e.Column);
        var viewModel = DataContext as TimeSeriesDataViewerViewModel;
        if (viewModel?.CellPointerPressedCommand != null && viewModel.CellPointerPressedCommand.CanExecute(cellColumn))
        {
            viewModel.CellPointerPressedCommand.Execute(cellColumn);
        }
    }
}