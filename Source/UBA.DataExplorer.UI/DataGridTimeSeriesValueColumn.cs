﻿using Avalonia;
using Avalonia.Controls;
using Avalonia.Data;
using Avalonia.Input;
using UBA.DataExplorer.UI.Views.Controls;

namespace UBA.DataExplorer.UI;

/// <summary>
/// A custom DataGrid column used to display time series values.
/// This class uses the <see cref="TimeSeriesValueCell"/> to render the cell content.
/// </summary>
public class DataGridTimeSeriesValueColumn : DataGridTextColumn
{
    public static readonly StyledProperty<int> DataIndexProperty =
        AvaloniaProperty.Register<DataGridTimeSeriesValueColumn, int>(nameof(DataIndex));

    public int DataIndex
    {
        get => GetValue(DataIndexProperty);
        set => SetValue(DataIndexProperty, value);
    }

    protected override Control GenerateElement(DataGridCell cell, object dataItem)
    {
        var element = new TimeSeriesValueCell();
        var binding = (Binding)Binding;
        if (binding != null)
        {
            element.Bind(StyledElement.DataContextProperty, new Binding($"Data[{DataIndex}]")
            {
                Mode = BindingMode.OneWay
            });
        }

        return element;
    }

    protected override Control GenerateEditingElementDirect(DataGridCell cell, object dataItem)
    {
        var element = base.GenerateEditingElementDirect(cell, dataItem);
        if (element is TextBox textBox)
        {
            textBox.Bind(InputElement.IsEnabledProperty, new Binding($"!Data[{DataIndex}].IsWriteProtected"));
            return textBox;
        }
        return element;
    }
}



