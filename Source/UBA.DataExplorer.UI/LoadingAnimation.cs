namespace UBA.DataExplorer.UI;

public class LoadingAnimation : IDisposable
{
    readonly LoadingIncrementer _incrementer;

    internal LoadingAnimation(LoadingIncrementer incrementer)
    {
        _incrementer = incrementer;
        incrementer.Increment();
    }

    public void Dispose() => _incrementer.Decrement();
}