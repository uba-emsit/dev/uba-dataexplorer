﻿namespace UBA.DataExplorer.UI.Models;

public record NavigationItemModel(Type ModelType, string Icon, string Label);
