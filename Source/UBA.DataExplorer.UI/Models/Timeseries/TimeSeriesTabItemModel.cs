﻿using UBA.DataExplorer.UI.ViewModels;

namespace UBA.DataExplorer.UI.Models.TimeSeries;

public record TimeSeriesTabItemModel(string Header, ViewModelBase ViewModel, bool IsMain = false) : IDisposable
{
    public void Dispose()
    {
        ViewModel.Dispose();
        GC.SuppressFinalize(this);
    }
}