﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum TimeSeriesProjectionMode
{
    TimeSeriesSetting,
    Linear
}