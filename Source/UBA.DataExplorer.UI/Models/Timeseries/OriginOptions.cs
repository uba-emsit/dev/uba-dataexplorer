﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum OriginOptions
{
    Analyst,
    ManualEntry,
    Importer
}