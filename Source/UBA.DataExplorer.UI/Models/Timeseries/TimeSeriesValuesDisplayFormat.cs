﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;
public enum TimeSeriesValuesDisplayFormat
{
    TimeSeriesSetting,
    Automatic,
    FixedPoint,
    Exponential
}