namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum TimeSeriesViewFilterMode
{
    OnlyFilter,
    OnlyList,
    FilterList,
    FilterOrList
}