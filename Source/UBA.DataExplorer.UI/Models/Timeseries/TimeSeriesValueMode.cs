namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum TimeSeriesValueMode
{
    Input,
    Mapping,
    Inherited
}