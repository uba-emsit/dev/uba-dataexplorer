﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum TimeSeriesDataViewerColumnType
{
    None,
    Dimension,
    Unit,
    Value,
    Hypothesis,
    TimeResolution,
    TimeSeries
}