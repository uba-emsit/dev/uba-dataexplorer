﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;

// TODO: change later to actual TimeSeries ViewModel
public record TimeSeriesViewListItemModel(string Name, List<string> Ids, string LastOpened);