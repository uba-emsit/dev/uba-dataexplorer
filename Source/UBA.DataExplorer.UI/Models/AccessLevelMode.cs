﻿namespace UBA.DataExplorer.UI.Models;

public enum AccessLevelMode
{
    Public,
    Confidential
}