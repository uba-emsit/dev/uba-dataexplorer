﻿namespace UBA.DataExplorer.UI.Models;

public enum ColumnWidthMode
{
    Automatic,
    UserDefined
}