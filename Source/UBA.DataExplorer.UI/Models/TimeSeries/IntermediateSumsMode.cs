﻿namespace UBA.DataExplorer.UI.Models.TimeSeries;

public enum IntermediateSumsMode
{
    None,
    SourceGroup,
    Region,
    ValueType
}