﻿using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UI.ViewModels.Descriptor;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;

namespace UBA.DataExplorer.UI.Models.TimeSeries;

// WIP class for Data grid testing, not finished
public partial class TimeSeriesTempDataModel : ObservableObject
{
    public readonly ITimeSeries TimeSeries;
    [ObservableProperty] int _number;
    [ObservableProperty] string _timeResolution;
    [ObservableProperty] string _hypothesis;
    [ObservableProperty] List<DescriptorViewModel> _descriptors;
    [ObservableProperty] string _unit;
    [ObservableProperty] List<TimeSeriesDataViewModel> _data = [];
    [ObservableProperty] bool _isChanged;
    [ObservableProperty] string _id;
    [ObservableProperty] string _label;

    public TimeSeriesTempDataModel(int number, ITimeSeries timeSeries,
        List<DescriptorViewModel> descriptors, string unit, string timeResolution, string hypothesis,
        bool isChanged = false)
    {
        TimeSeries = timeSeries;
        Id = timeSeries.Id;
        Label = timeSeries.Name;

        Number = number;
        TimeResolution = timeResolution;
        Hypothesis = hypothesis;
        Descriptors = descriptors;
        Unit = unit;
        IsChanged = isChanged;
    }
}
