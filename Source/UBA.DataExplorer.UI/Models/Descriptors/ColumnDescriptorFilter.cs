﻿using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

#if DEBUG
using UBA.DataExplorer.Domain.Faker;
#endif  

namespace UBA.DataExplorer.UI.Models.Descriptors;

public class ColumnDescriptorFilter(
    IDescriptorFilterSettings descriptorFilterSettings,
    int dimensionPrimaryKey, ViewScope viewScope)
{
    public IDescriptorFilterSettings DescriptorFilterSettings { get; } = descriptorFilterSettings;
    public int DimensionPrimaryKey { get; } = dimensionPrimaryKey;
    public ViewScope ViewScope { get; } = viewScope;

#if DEBUG
    #region Designer
    public ColumnDescriptorFilter() : this(new DescriptorFilterSettingsFaker().Generate(), 0, new())
    {
    }
    #endregion
#endif

}