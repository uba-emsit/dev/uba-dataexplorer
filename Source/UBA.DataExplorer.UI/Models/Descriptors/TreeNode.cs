﻿using System.Collections.ObjectModel;
using CommunityToolkit.Mvvm.ComponentModel;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.UI.Models.Descriptors;

public class TreeNode : ObservableObject, IEntityTreeNode<ITreeNodeNode>, IPrimaryKey, IIdAndLabelModel
{
    private readonly ObservableCollection<IEntityTreeNode<ITreeNodeNode>> _children = new();

    public string Name;
    public int Level { get; }
    public IDescriptor Descriptor { get; }
    public ITreeNodeNode Entity { get; }
    public int PrimaryKey { get; }

    private string _id = string.Empty;
    public string Id
    {
        get => Descriptor.Id;
        set => _id = value;
    }

    private string _label = string.Empty;
    public string Label
    {
        get => Descriptor.Label;
        set => _label = value;
    }

    public ReadOnlyCollection<IEntityTreeNode<ITreeNodeNode>> Children =>
        new(_children.Cast<IEntityTreeNode<ITreeNodeNode>>().ToList());

    public int SortNr => Entity.SortNr;

    public TreeNode(IEntityTreeNode<ITreeNodeNode> entityNode)
    {
        Entity = entityNode.Entity;
        Descriptor = Entity.Descriptor;
        Name = Descriptor?.Label ?? "Unnamed";
        Level = entityNode.Level;
        PrimaryKey = entityNode.PrimaryKey;

        foreach (var child in entityNode.Children)
        {
            _children.Add(new TreeNode(child));
        }
    }

    public override string ToString()
    {
        return Name;
    }
}