﻿using System;
using Avalonia;
using Microsoft.Extensions.Configuration;
using Microsoft.Extensions.DependencyInjection;
using NLog.Extensions.Logging;
using Projektanker.Icons.Avalonia;
using Projektanker.Icons.Avalonia.FontAwesome;
using UBA.DataExplorer.Core.SQL;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UseCases;

namespace UBA.DataExplorer;

static class Program
{
    // Initialization code. Don't use any Avalonia, third-party APIs or any
    // SynchronizationContext-reliant code before AppMain is called: things aren't initialized
    // yet and stuff might break.
    [STAThread]
    public static void Main(string[] args) => BuildAvaloniaApp()
        .StartWithClassicDesktopLifetime(args);

    // Avalonia configuration, don't remove; also used by visual designer.
    static AppBuilder BuildAvaloniaApp()
    {
        IconProvider.Current
            .Register<FontAwesomeIconProvider>();

        return AppBuilder.Configure<App>()
            .AfterSetup(ConfigureServices)
            .UsePlatformDetect()
            .WithInterFont()
            .LogToTrace();
    }

    static void ConfigureServices(AppBuilder appBuilder)
    {
        var config = new ConfigurationBuilder()
            .SetBasePath(System.IO.Directory.GetCurrentDirectory())
            .AddJsonFile("appsettings.json", optional: true, reloadOnChange: true)
            .Build();

        ServiceCollection serviceCollection = [];
        serviceCollection
            .AddLogging(c => c.AddNLog(config))
            .AddBackendFactories() // Sets the backend factories to the SQL/Mesap implementation
            .AddSingletonServices()
            .AddViewModels()
            .AddUseCases();

        UI.DependencyInjection.SetServiceProvider(serviceCollection.BuildServiceProvider());
    }
}