﻿using UBA.DataExplorer.Core.SQL.EntityOperations;

namespace UBA.DataExplorer.Core.SQL;

abstract class RepositoryBase<TReader, TEntity, TKey>(TReader reader)
    where TReader : ThrottledEntityOperations
{
    /// <summary>
    /// Provides access to the underlying (unmapped) entities. All calls are throttled.
    /// </summary>
    internal TReader RawReader => reader;
}