﻿using Mesap.Framework.DataAccess;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

class FrameworkThrottler(ILoggerFactory loggerFactory, AppServiceProvider appServiceProvider, ILoginRepository loginRepository, IDatabaseRepository databaseRepository)
{
    readonly ILogger<FrameworkThrottler> _logger = loggerFactory.CreateLogger<FrameworkThrottler>();
    const int NumberOfParallelCalls = 6;
    readonly SemaphoreSlim _throttleSemaphore = new(NumberOfParallelCalls, NumberOfParallelCalls);
    readonly SemaphoreSlim _loginSemaphore = new(1, 1);
    readonly Lazy<IReLoginHandler> _reLoginHandler = new(() => appServiceProvider.Provider.GetRequiredService<IReLoginHandler>());

    public async Task<Result<TResult>> Execute<TResult>(Func<TResult> result, MesapConnection mesapConnection, CancellationToken cancellationToken)
        => await Execute(() => Task.FromResult(result()), mesapConnection, cancellationToken);

    public async Task<Result<TResult>> Execute<TResult>(Func<Task<TResult>> method, MesapConnection mesapConnection, CancellationToken cancellationToken)
    {
        await _throttleSemaphore.WaitAsync(cancellationToken);
        try
        {
            if (mesapConnection.IsDisposed) return SilentFailure();
            return await method();
        }
        catch (SessionTokenExpiredException ex)
        {
            _logger.LogWarning(ex, "Session token expired");
            await _loginSemaphore.WaitAsync(cancellationToken);
            try
            {
                if (mesapConnection.IsDisposed) return SilentFailure();
                return await method();
            }
            catch (SessionTokenExpiredException)
            {
                if (!await ReLogin(mesapConnection, cancellationToken))
                {
                    mesapConnection.Dispose();
                    await _reLoginHandler.Value.Logoff();
                    return SilentFailure();
                }
                try
                {
                    return await method();
                }
                catch (Exception e)
                {
                    return ExceptionFailure(e);
                }
            }
            catch (Exception e)
            {
                return ExceptionFailure(e);
            }
            finally
            {
                _loginSemaphore.Release();
            }
        }
        catch (Exception ex)
        {
            return ExceptionFailure(ex);
        }
        finally
        {
            _throttleSemaphore.Release();
        }

        Result<TResult> ExceptionFailure(Exception e)
        {
            _logger.LogError(e, "Error while executing action");
            return Result.Failure<TResult>(Error.Failure(f => f.BackendErrors.GeneralBackendFailure, e));
        }
        Result<TResult> SilentFailure()
            => Result.Failure<TResult>(Error.Silent);
    }

    async Task<bool> ReLogin(MesapConnection mesapConnection, CancellationToken cancellationToken)
    {
        Result<LoginModel>? loginResult = null;
        if (await _reLoginHandler.Value.WasWindowsAccountLogin(cancellationToken))
            loginResult = await loginRepository.LoginWithWindowsAccount(cancellationToken);
        var errorMessage = loginResult is { IsFailure: true } ? loginResult.Error.Description : null;
    login:
        while (loginResult?.IsSuccess != true)
        {
            var response = await _reLoginHandler.Value.RequestPassword(mesapConnection.UserId, errorMessage, cancellationToken);
            switch (response)
            {
                case IReLoginHandler.Response.LogoffResponse:
                    return false;
                case IReLoginHandler.Response.PasswordResponse passwordResponse:
                    loginResult = await loginRepository.Login(mesapConnection.UserId, passwordResponse.Password, cancellationToken);
                    break;
                case IReLoginHandler.Response.WindowsAccountLoginResponse:
                    loginResult = await loginRepository.LoginWithWindowsAccount(cancellationToken);
                    break;
                default:
                    throw new ArgumentOutOfRangeException(nameof(response));
            }
            if (loginResult.IsFailure)
                errorMessage = loginResult.Error.Description;
        }

        var openResult = await databaseRepository.OpenConnection(loginResult.Value, mesapConnection.MesapDatabaseInfo, cancellationToken);
        if (openResult.IsFailure)
        {
            errorMessage = openResult.Error.Description;
            loginResult = null;
            goto login;
        }

        var (connectionModel, messageServer) = openResult.Value;
        var newMesapConnection = (connectionModel as MesapConnection) ?? throw new("MesapConnection expected");
        await _reLoginHandler.Value.LoginChanged(loginResult.Value);
        mesapConnection.DatabaseReopened(newMesapConnection.DatabaseUserContext);
        await _reLoginHandler.Value.MessageServerChanged(messageServer);
        return true;
    }
}