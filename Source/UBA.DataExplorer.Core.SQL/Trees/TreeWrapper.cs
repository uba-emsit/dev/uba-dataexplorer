﻿using System.Collections.ObjectModel;
using Mesap.Framework.DataAccess.Tables.Internal;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Core.SQL.Trees;

class TreeWrapper(Tree entity, ProcessingContext context)
    : ITree, IFrameworkEntityContainer<Tree>
{
    public Tree FrameworkEntity { get; } = entity;
    public int PrimaryKey => FrameworkEntity.PrimaryKey;
    public string Id { get => FrameworkEntity.Id; set => FrameworkEntity.Id = value; }
    public string Name { get => FrameworkEntity.Name; set => FrameworkEntity.Name = value; }
    public string Description { get => FrameworkEntity.Description; set => FrameworkEntity.Description = value; }
    public IDimension Dimension => context.Mappers.Dimension.ToModel(FrameworkEntity.Dimension);
    public ITreeNodeTree Structure => new TreeNodeTreeWrapper(FrameworkEntity.Structure, context);
    public IAuditInfo AuditInfo => SQL.AuditInfo.GetChangeAuditInfo(
        FrameworkEntity.ChangedUserId,
        FrameworkEntity.ChangedDate);
    public override string ToString() => Id;
}

class TreeNodeTreeWrapper(EntityTree<TreeNode, int> entity, ProcessingContext context)
    : ITreeNodeTree, IFrameworkEntityContainer<EntityTree<TreeNode, int>>
{
    public EntityTree<TreeNode, int> FrameworkEntity => entity;

    public IEntityTreeNode<ITreeNodeNode> RootNode => context.Mappers.TreeNodeNode.ToModel(FrameworkEntity.RootNode);

    public int Count => FrameworkEntity.Count;
}

class TreeNodeWrapper(TreeNode entity, ProcessingContext context)
    : ITreeNode, IFrameworkEntityContainer<TreeNode>
{
    public TreeNode FrameworkEntity { get; } = entity;
    public int PrimaryKey => FrameworkEntity.PrimaryKey;
    public IDescriptor Descriptor => context.Mappers.Descriptor.ToModel(FrameworkEntity.Descriptor);
    public int SortNr => FrameworkEntity.LevelSortNr;
    public int NodePredecessorNr => FrameworkEntity.NodePredecessorNr;
    public int TreeNr => FrameworkEntity.TreeNr;
}

class TreeNodeNodeWrapper(EntityTree<TreeNode, int>.Node entity, ProcessingContext context)
    : ITreeNodeNode, IFrameworkEntityContainer<EntityTree<TreeNode, int>.Node>
{
    public EntityTree<TreeNode, int>.Node FrameworkEntity => entity;

    public ReadOnlyCollection<IEntityTreeNode<ITreeNodeNode>> Children
        => new(context.Mappers.TreeNodeNode
            .ToModel(FrameworkEntity.Children).Cast<IEntityTreeNode<ITreeNodeNode>>().ToList());

    public int PrimaryKey => FrameworkEntity.Entity.PrimaryKey;
    public int SortNr => FrameworkEntity.Entity.LevelSortNr;
    public int Level => FrameworkEntity.Level;
    public ITreeNodeNode Entity => context.Mappers.TreeNodeNode.ToModel(FrameworkEntity);
    public IDescriptor Descriptor => context.Mappers.Descriptor.ToModel(FrameworkEntity.Entity.Descriptor);
}