﻿using Mesap.Framework.Collections;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Trees;

class TreeReader(ProcessingContext context)
    : ThrottledEntityReaderBase<Tree, TreeKey>(context)
{
    public Task<Result<List<Tree>>> Read(CancellationToken cancellationToken) =>
        ExecuteThrottled(() => MesapConnection.MesapDatabase.Trees.ToList(), cancellationToken);

    public Task<Result<List<Tree>>> ReadByDimension(int dimensionNr, CancellationToken cancellationToken) =>
        ExecuteThrottled(() => MesapConnection.MesapDatabase.Trees
                .Where(t => t.Dimension.PrimaryKey == dimensionNr).ToList(),
            cancellationToken);

    protected override Tree Read(TreeKey key)
    {
        MesapConnection.MesapDatabase.Trees.TryGet(key, out var dim);
        return dim;
    }

    protected override Tree Read(int primaryKey)
    {
        MesapConnection.MesapDatabase.Trees.TryGet(primaryKey, out var dim);
        return dim;
    }

    protected override List<Tree> Read(List<int> primaryKeys)
        => MesapConnection.MesapDatabase.Trees.Get(primaryKeys);

    protected override List<Tree> Read(List<TreeKey> keyList)
        => MesapConnection.MesapDatabase.Trees.Get(keyList);
}