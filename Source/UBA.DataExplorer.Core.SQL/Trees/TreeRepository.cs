﻿
using Mesap.Framework.Collections;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using FWE = Mesap.Framework.Entities;


namespace UBA.DataExplorer.Core.SQL.Trees;

class TreeRepository : RepositoryBase<TreeReader, FWE.Tree, TreeKey>, ITreeRepository
{
    readonly ReadRepositoryCompanion<ITree, FWE.Tree, TreeWrapper, TreeKey> _companion;

    public TreeRepository(ProcessingContext context) : base(new(context))
    {
        _companion = new(RawReader, context.Mappers.Tree, TreeErrors.NotFound);
    }

    public Task<Result<ITree>> Read(int primaryKey, CancellationToken cancellationToken)
        => _companion.Read(primaryKey, cancellationToken);

    public Task<Result<List<ITree>>> Read(List<int> primaryKeys, CancellationToken cancellationToken)
        => _companion.Read(primaryKeys, cancellationToken);

    public async Task<Result<List<ITree>>> ReadByDimension(int dimensionNr, CancellationToken cancellationToken)
        => _companion.Matching.Match((await RawReader.ReadByDimension(dimensionNr, cancellationToken)));

    public async Task<Result<List<ITree>>> ReadAll(CancellationToken cancellationToken)
        => _companion.Matching.Match(await RawReader.Read(cancellationToken));
}