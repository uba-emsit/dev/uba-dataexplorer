﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using UBA.DataExplorer.Domain.Databases;

namespace UBA.DataExplorer.Core.SQL.Database;

class Favorite(UserContext userContext)
{
    internal void SetFavorite(List<DatabaseModel> databases)
    {
        var favoriteDb = userContext.GetUserInfo().Settings.FavoriteDatabaseId;

        foreach (var db in databases)
            db.IsFavorite = db.Id.Equals(favoriteDb);
    }
}
