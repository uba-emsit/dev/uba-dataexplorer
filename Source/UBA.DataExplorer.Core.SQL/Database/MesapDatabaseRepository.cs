﻿using Mesap.Framework;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Core.SQL.Login;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;
using Result = UBA.DataExplorer.Domain.Shared.Result;

namespace UBA.DataExplorer.Core.SQL.Database;

class MesapDatabaseRepository(AppServiceProvider serviceProvider, Application application)
    : IDatabaseRepository
{
    Task<Result<List<DatabaseModel>>> IDatabaseRepository.GetAvailableProjectDatabases(LoginModel login, CancellationToken cancellationToken)
    {
        List<DatabaseModel> result = [];

        if (login is not LoginWrapper loginWrapper)
            return Task.FromResult(Result.Failure<List<DatabaseModel>>(MesapConnectionErrors.IncorrectLoginType()));

        try
        {
            var userContext = loginWrapper.UserContext;
            var databaseInfos = new SystemDatabase(userContext)
                .DatabaseInfos
                .Where(info => info.IsLeaf && !info.Disabled)
                .OrderBy(info => info.SortNr);

            result.AddRange(databaseInfos.Select(dbInfo => new DatabaseWrapper(dbInfo.PrimaryKey, dbInfo.DatabaseGuid, dbInfo.Id, dbInfo.Description,
                dbInfo.Name, dbInfo.SortNr)));

            new RecentlyUsed(userContext).SetRecentlyUsedPosition(result);
            new Favorite(userContext).SetFavorite(result);
        }
        catch (Exception e)
        {
            return Task.FromResult(Result.Failure<List<DatabaseModel>>(Error.Failure(f => f.BackendErrors.DatabaseError, e)));
        }

        return Task.FromResult<Result<List<DatabaseModel>>>(result);
    }

    Task<Result<(ConnectionModel, IMessageServer)>> IDatabaseRepository.OpenConnection(LoginModel login, DatabaseModel database,
        CancellationToken cancellationToken)
    {
        if (login is not LoginWrapper loginWrapper)
            return Task.FromResult(Result.Failure<(ConnectionModel, IMessageServer)>(MesapConnectionErrors.IncorrectLoginType()));

        if (database is not DatabaseWrapper mesapDatabase)
            return Task.FromResult(Result.Failure<(ConnectionModel, IMessageServer)>(MesapConnectionErrors.IncorrectConnectionType()));

        var userContext = loginWrapper.UserContext;

        try
        {
            if (!application.TryOpenDatabase(mesapDatabase.Id, userContext, out var databaseUserContext))
            {
                return Task.FromResult(Result.Failure<(ConnectionModel, IMessageServer)>(Error.Failure(f => f.BackendErrors.DatabaseError,
                    databaseUserContext.Validness.ToString())));
            }
            var connection = new MesapConnection(mesapDatabase, databaseUserContext, new(databaseUserContext));
            var messageServer = ActivatorUtilities.CreateInstance<MessageServer.MessageServer>(serviceProvider.Provider, connection, application);

            return Task.FromResult<Result<(ConnectionModel, IMessageServer)>>((connection, messageServer));
        }
        catch (Exception e)
        {
            return Task.FromResult(Result.Failure<(ConnectionModel, IMessageServer)>(Error.Failure(f => f.BackendErrors.DatabaseError, e)));
        }
    }

    async Task<Result> IDatabaseRepository.CloseConnection(ConnectionModel connection, CancellationToken cancellationToken)
    {
        if (connection is not MesapConnection(_, var databaseUserContext, _))
            return Result.Failure(MesapConnectionErrors.IncorrectConnectionType());

        try
        {
            application.CloseDatabase(databaseUserContext);
        }
        catch (Exception e)
        {
            return Result.Failure(Error.Failure(f => f.BackendErrors.DatabaseError, e));
        }

        return await Task.FromResult(Result.Success());
    }

    async Task<Result> IDatabaseRepository.SaveConnectionToRecentlyUsed(ConnectionModel connection,
        CancellationToken cancellationToken)
    {
        if (connection is not MesapConnection mesapConnection)
            return Result.Failure(MesapConnectionErrors.IncorrectConnectionType());

        var userContext = mesapConnection.DatabaseUserContext.UserContext;

        try
        {
            await new RecentlyUsed(userContext).AddToUserSettings(mesapConnection.MesapDatabaseInfo.PrimaryKey);
        }
        catch (Exception e)
        {
            return Result.Failure(Error.Failure(f => f.BackendErrors.DatabaseError, e));
        }

        return Result.Success();
    }
}