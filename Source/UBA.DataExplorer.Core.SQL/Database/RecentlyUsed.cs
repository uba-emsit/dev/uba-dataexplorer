﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.DataAccess.Views;
using UBA.DataExplorer.Domain.Databases;

namespace UBA.DataExplorer.Core.SQL.Database;

class RecentlyUsed(UserContext userContext)
{
    const int MaxRecentDbs = 5;
    const string RecentDbsKey = "RecentDBs";
    const string M4UserInterfaceSection = "UserInterface";

    internal void SetRecentlyUsedPosition(List<DatabaseModel> databases)
    {
        var recentDbs = GetFromUserSettings();

        foreach (var db in databases)
            db.RecentlyUsedPosition = recentDbs.IndexOf(db.PrimaryKey) + 1;
    }

    internal Task AddToUserSettings(int dbKey)
    {
        var recentDbs = GetFromUserSettings();

        recentDbs.Remove(dbKey);
        recentDbs.Insert(0, dbKey);
        recentDbs = recentDbs.Take(MaxRecentDbs).ToList();

        var userInfo = new UserInfos(userContext).Get(userContext.UserNr);
        var settings = userInfo.Settings;

        settings.SetCustomEntry(RecentDbsKey, string.Join(',', recentDbs));

        var userInfoWriter = new UserInfoWriter(userContext);
        userInfoWriter.UpdateSettings(settings);

        return Task.CompletedTask;
    }

    List<int> GetFromUserSettings()
    {
        var userInfo = userContext.GetUserInfo();
        var settings = userInfo.Settings;

        if (!settings.TryGetCustomEntry(RecentDbsKey, out var recentDbs))
        {
            // Fall-back to Mesap Explorer in case there is no DataExplorer entry available
            if (settings.Mesap4IniSettings.Exists(M4UserInterfaceSection, RecentDbsKey))
                recentDbs = settings.Mesap4IniSettings.GetStringValueOrDefault(M4UserInterfaceSection, RecentDbsKey);
            else
                return [];
        }

        return recentDbs
            .Split(',')
            .Select(s => int.TryParse(s, out var i) ? i : 0)
            .Where(i => i > 0)
            .Take(MaxRecentDbs)
            .ToList();
    }
}
