﻿using UBA.DataExplorer.Domain.Databases;

namespace UBA.DataExplorer.Core.SQL.Database;

class DatabaseWrapper(int primaryKey, string guid, string id, string description, string label, int sortNr)
    : DatabaseModel(primaryKey, guid, id, description, label, sortNr);