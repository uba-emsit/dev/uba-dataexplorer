﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

/// <summary>
/// Provides a way to match back end results to front end models.
/// If the result is successful, the entities are mapped to models.
/// Failures are passed through.
/// </summary>
/// <typeparam name="TInterface"></typeparam>
/// <typeparam name="TEntity"></typeparam>
/// <typeparam name="TMesapModel"></typeparam>
/// <param name="mapper"></param>
/// <param name="notFoundError"></param>
class Matching<TInterface, TEntity, TMesapModel>(Mapper<TInterface, TEntity, TMesapModel> mapper, Func<string, Error> notFoundError)
    where TMesapModel : TInterface, IFrameworkEntityContainer<TEntity>
    where TInterface : IDomainModel
{
    internal Result<List<TInterface>> Match(Result<List<TEntity>> entities)
        => entities.IsFailure
            ? Result.Failure<List<TInterface>>(entities.Error)
            : Match(entities.Value);

    internal Result<TInterface> Match(Result<TEntity> entity, string id)
        => entity.IsFailure
            ? Result.Failure<TInterface>(entity.Error)
            : Match(entity.Value, id);

    internal Result<TInterface> Match(Result<TEntity> entity, int primaryKey)
        => entity.IsFailure
            ? Result.Failure<TInterface>(entity.Error)
            : Match(entity.Value, primaryKey);

    internal Result<List<TInterface>> Match(List<TEntity>? entities)
    {
        if (entities == null)
        {
            return Result.Failure<List<TInterface>>(notFoundError(""));
        }

        return entities.Count == 0
            ? Result.Failure<List<TInterface>>(Error.NoData(typeof(TEntity).Name))
            : Result.Success(mapper.ToModel(entities));
    }

    internal Result<TInterface> Match(TEntity? entity, string id)
        => entity == null
            ? Result.Failure<TInterface>(notFoundError(id))
            : Result.Success(mapper.ToModel(entity));

    internal Result<TInterface> Match(TEntity? entity, int primaryKey)
        => entity == null
            ? Result.Failure<TInterface>(notFoundError(primaryKey.ToString()))
            : Result.Success(mapper.ToModel(entity));
}