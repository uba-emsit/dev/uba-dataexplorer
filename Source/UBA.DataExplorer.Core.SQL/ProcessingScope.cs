﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Core.SQL.Mapping;

namespace UBA.DataExplorer.Core.SQL;

class ProcessingContext
{
    public MesapConnection MesapConnection { get; set; }
    public required IServiceProvider RepositoryProvider { get; init; }
    public Mappers Mappers => RepositoryProvider.GetRequiredService<Mappers>();
}