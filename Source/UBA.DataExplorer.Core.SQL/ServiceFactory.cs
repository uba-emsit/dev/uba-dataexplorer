﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

class ServiceFactory(BackendFactory backendFactory)
    : IServiceFactory
{
    public T GetService<T>(ConnectionModel connection) where T : IService
        => backendFactory.CreateScope(connection).ServiceProvider.GetRequiredService<T>();
}