﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

class AuditInfo : IAuditInfo
{
    void SetChanged(string changedUserId, DateTime changedDate)
    {
        ChangedUserId = changedUserId;
        ChangedDate = changedDate;
        HasChangedUserId = true;
        HasChangedDate = true;
    }

    void SetCreated(string createdUserId, DateTime createdDate)
    {
        CreatedUserId = createdUserId;
        CreatedDate = createdDate;
        HasCreatedUserId = true;
        HasCreatedDate = true;
    }

    public static AuditInfo GetFullAuditInfo(string createdUserId, DateTime createdDate, string changedUserId, DateTime changedDate)
    {
        var auditInfo = new AuditInfo();

        auditInfo.SetCreated(createdUserId, createdDate);
        auditInfo.SetChanged(changedUserId, changedDate);

        return auditInfo;
    }

    public static AuditInfo GetChangeAuditInfo(string changedUserId, DateTime changedDate)
    {
        var auditInfo = new AuditInfo();

        auditInfo.SetChanged(changedUserId, changedDate);

        return auditInfo;
    }


    public static AuditInfo GetCreateAuditInfo(string createdUserId, DateTime createdDate)
    {
        var auditInfo = new AuditInfo();

        auditInfo.SetCreated(createdUserId, createdDate);

        return auditInfo;
    }

    public string? CreatedUserId { get; private set; }
    public DateTime? CreatedDate { get; private set; }
    public string? ChangedUserId { get; private set; }
    public DateTime? ChangedDate { get; private set; }

    public bool HasCreatedUserId { get; private set; }
    public bool HasCreatedDate { get; private set; }
    public bool HasChangedUserId { get; private set; }
    public bool HasChangedDate { get; private set; }
}
