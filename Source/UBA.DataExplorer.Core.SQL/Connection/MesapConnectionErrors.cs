﻿using UBA.DataExplorer.Core.SQL.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Connection;
internal static class MesapConnectionErrors
{
    public static Error IncorrectLoginType()
        => ConnectionErrors.IncorrectLoginType(typeof(LoginWrapper));

    public static Error IncorrectConnectionType()
        => ConnectionErrors.IncorrectConnectionType(typeof(MesapConnection));
}
