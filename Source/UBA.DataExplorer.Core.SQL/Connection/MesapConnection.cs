﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using UBA.DataExplorer.Core.SQL.Database;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Connection;

record MesapConnection(
    DatabaseWrapper MesapDatabaseInfo,
    DatabaseUserContext DatabaseUserContext,
    Mesap.Framework.Database MesapDatabase)
    : ConnectionModel(MesapDatabaseInfo)
{
    public DatabaseUserContext DatabaseUserContext { get; private set; } = DatabaseUserContext;
    public Mesap.Framework.Database MesapDatabase { get; private set; } = MesapDatabase;
    public string UserId { get; set; } = DatabaseUserContext.GetCurrentDbAccessUser().Id;

    internal void DatabaseReopened(DatabaseUserContext databaseUserContext)
    {
        DatabaseUserContext = databaseUserContext;
        MesapDatabase = new(databaseUserContext);
    }
}