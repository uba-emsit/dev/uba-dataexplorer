﻿using System.Collections.Immutable;
using Mesap.Framework;
using Mesap.Framework.Entities;
using Mesap.MessageServer;
using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Domain.Shared;
using Application = Mesap.Framework.Application;
using Task = System.Threading.Tasks.Task;

namespace UBA.DataExplorer.Core.SQL.MessageServer;

class MessageServer : IMessageServer, IDisposable
{
    readonly Application _application;
    readonly MesapConnection _mesapConnection;
    readonly ILogger<MessageServer> _logger;
    readonly string _registrationKey = Guid.NewGuid().ToString();
    readonly List<Listener> _listeners = [];
    ImmutableList<LockResult.Success> _locks = [];

    public MessageServer(Application application, MesapConnection mesapConnection, ILogger<MessageServer> logger)
    {
        _application = application;
        _mesapConnection = mesapConnection;
        _logger = logger;

        var databaseUserContext = mesapConnection.DatabaseUserContext;
        var result = _application.MessageServer.Dispatcher.RegisterConsumer
            (_registrationKey, new MessageConsumer(databaseUserContext, OnChange), databaseUserContext);

        if (!result.ToBoolean())
            throw new InvalidOperationException($"Failed to register message consumer: {result.ErrorMessage}");
    }

    async void OnChange(string objectName, List<Change<long>> changes, ObjectChangeReason reason)
    {
        try
        {
            if (!ObjectClasses.ByObjectName.TryGetValue(objectName, out var objectClass)) return;
            List<Listener> relevantListeners;
            lock (_listeners)
            {
                _listeners.RemoveAll(l => l.Disposed);
                relevantListeners = _listeners
                    .Where(listener => listener.ObjectClass == objectClass).ToList();
            }

            await Task.WhenAll(relevantListeners
                .Select(listener => listener.OnChanged(reason, changes)));
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while processing change messages");
        }
    }

    public void RegisterListener(Listener listener)
    {
        lock (_listeners)
            _listeners.Add(listener);
    }

    public bool UnregisterListener(Listener listener)
    {
        lock (_listeners)
            return _listeners.Remove(listener);
    }

    public LockResult Lock(MessageServerObjectClass objectClass, int primaryKey)
    {
        if (!ObjectClasses.ByObjectClass.TryGetValue(objectClass, out var objectName))
            return new LockResult.Error(ProcessingResult.Failed, $"Unknown object class: {objectClass}");

        try
        {
            var result = _application.MessageServer.ApplicationClient.IsLocked(
                _mesapConnection.MesapDatabase.DatabaseUserContext.DatabaseContext.DatabaseNr,
                objectName,
                primaryKey,
                out var isLocked,
                out _,
                out var otherUserNr,
                out var otherLockUtcTime);

            if (!result.ToBoolean())
                return GetError(result);

            var databaseUserContext = _mesapConnection.DatabaseUserContext;
            var userContext = databaseUserContext.UserContext;

            if (isLocked)
            {
                UserInfo? userInfo = null;
                if (otherUserNr.HasValue)
                {
                    try
                    {
                        var systemDatabase = new SystemDatabase(userContext);
                        systemDatabase.UserInfos.TryGet(otherUserNr.Value, out userInfo);
                    }
                    catch (Exception e)
                    {
                        _logger.LogWarning(e, "Failed to get user info for user number {userNr}", otherUserNr);
                    }
                }
                return new LockResult.Locked(userInfo == null ? null : new UserModel(userInfo.Name, userInfo.Id),
                    otherLockUtcTime.HasValue
                        ? new DateTimeOffset(otherLockUtcTime.Value, TimeSpan.Zero).ToLocalTime()
                        : DateTimeOffset.MinValue);
            }

            result = _application.MessageServer.ApplicationClient.Lock(
                userContext.ContextId,
                databaseUserContext.ContextId,
                userContext.UserNr,
                databaseUserContext.DatabaseContext.DatabaseNr,
                objectName,
                primaryKey);
            if (result.Result != Mesap.MessageServer.ProcessingResult.Ok)
                return GetError(result);

            var lockResult = new LockResult.Success(l => Unlock(l, objectClass, primaryKey));
            _locks = _locks.Add(lockResult);
            return lockResult;

            LockResult.Error GetError(ICommandQueryResult queryResult)
                => new((ProcessingResult)queryResult.Result, queryResult.ErrorMessage);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while locking object {objectName} with primary key {primaryKey}", objectName, primaryKey);
            return new LockResult.Error(ProcessingResult.Failed, e.Message);
        }
    }

    void Unlock(LockResult lockResult, MessageServerObjectClass objectClass, int primaryKey)
    {
        if (!ObjectClasses.ByObjectClass.TryGetValue(objectClass, out var objectName))
            return;
        var databaseUserContext = _mesapConnection.DatabaseUserContext;
        var userContext = databaseUserContext.UserContext;

        try
        {
            var result = _application.MessageServer.ApplicationClient.Unlock(
                userContext.ContextId,
                databaseUserContext.ContextId,
                userContext.UserNr,
                databaseUserContext.DatabaseContext.DatabaseNr,
                objectName,
                primaryKey);
            if (lockResult is LockResult.Success success)
                _locks = _locks.Remove(success);
            if (result.ToBoolean()) return;

            _logger.LogWarning("Failed to unlock object {objectName} with primary key {primaryKey}: {errorMessage}",
                objectName, primaryKey, result.ErrorMessage);
        }
        catch (Exception e)
        {
            _logger.LogError(e, "Error while unlocking object {objectName} with primary key {primaryKey}", objectName, primaryKey);
        }
    }

    public void Dispose()
    {
        foreach (var lockResult in _locks)
            lockResult.Dispose();
        _application.MessageServer.Dispatcher.UnregisterConsumer(_registrationKey);
    }
}