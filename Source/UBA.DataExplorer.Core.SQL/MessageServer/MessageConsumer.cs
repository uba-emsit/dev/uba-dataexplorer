﻿using Mesap.Framework;
using Mesap.Framework.Messaging;
using Mesap.MessageServer;
using Mesap.MessageServer.Model;

namespace UBA.DataExplorer.Core.SQL.MessageServer;

class MessageConsumer(DatabaseUserContext databaseUserContext,
    Action<string, List<Change<long>>, ObjectChangeReason> onChange)
    : MessageConsumer<DatabaseUserContext>(databaseUserContext,
        new SubscriptionData(DatabaseSubscriptionSpecification.LoggedIn,
            ObjectNameSubscriptionSpecification.Specific([.. RelevantObjectNames])))
{
    static readonly HashSet<string> RelevantObjectNames = [.. ObjectClasses.ByObjectClass.Values];

    public override void HandleChangeMessages(List<ChangeMessage> changeMessages,
        List<MessageLostInfo> messageLostInfos, DatabaseUserContext target)
    {
        var messages = GroupMessages(changeMessages, target);
        foreach (var (objectName, dictionary) in messages)
            foreach (var (reason, changes) in dictionary)
                onChange(objectName, changes, reason);
    }

    static Dictionary<string, Dictionary<ObjectChangeReason, List<Change<long>>>> GroupMessages(
        List<ChangeMessage> changeMessages, DatabaseUserContext target)
        => changeMessages
            .Where(c => c.DatabaseNr == target.DatabaseContext.DatabaseNr && RelevantObjectNames.Contains(c.ObjectName))
            .GroupBy(c => c.ObjectName)
            .ToDictionary(g => g.Key,
                g => g.GroupBy(c => (ObjectChangeReason)c.Reason)
                    .ToDictionary(g2 => g2.Key,
                        g2 => g2.SelectMany(c =>
                                (c.ParentNrs.Count == 0
                                    ? c.ObjectNrs.Select(primary => (parent: 0, primary))
                                    : c.ParentNrs.Zip(c.ObjectNrs, (parent, primary) => (parent: (int)parent, primary)))
                                .Select(t => new Change<long>(t.parent, t.primary)))
                            .ToList()));
}