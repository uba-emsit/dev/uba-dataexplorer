﻿using Mesap.Framework.Common;

namespace UBA.DataExplorer.Core.SQL.MessageServer;

static class ObjectClasses
{
    public static readonly Dictionary<MessageServerObjectClass, string> ByObjectClass
        = new()
        {
            { MessageServerObjectClass.TimeSeries, ObjectNames.Timeseries },
            { MessageServerObjectClass.TimeSeriesAxis, ObjectNames.TimeseriesAxis },
            { MessageServerObjectClass.TimeSeriesData, ObjectNames.TimeseriesData },
            { MessageServerObjectClass.TimeSeriesProperty, ObjectNames.TimeseriesProperty },
            { MessageServerObjectClass.TimeSeriesView, ObjectNames.TimeseriesView },
            { MessageServerObjectClass.Descriptor, ObjectNames.Descriptor },
            { MessageServerObjectClass.Tree, ObjectNames.Tree },
            { MessageServerObjectClass.Trees, ObjectNames.Trees },
        };

    public static readonly Dictionary<string, MessageServerObjectClass> ByObjectName
        = ByObjectClass.ToDictionary(kv => kv.Value, kv => kv.Key);
}