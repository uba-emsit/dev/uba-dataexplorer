﻿using Mesap.Framework.DataAccess;
using Mesap.Framework.DataAccess.BulkWriter;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Mapping;

class WriteResponseMapper
{
    public List<WriteResponse<TDomainModel>> ToModel<TDomainModel, TFwEntity, TMesapModel>(
        WriteResult<TFwEntity> frameworkWriteResult,
        Mapper<TDomainModel, TFwEntity, TMesapModel> modelMapper)
        where TDomainModel : IDomainModel
        where TMesapModel : TDomainModel, IFrameworkEntityContainer<TFwEntity>
    {
        var response = new List<WriteResponse<TDomainModel>>();

        response.AddRange(modelMapper.ToModel(frameworkWriteResult.AddedEntities).Select(x => new WriteResponse<TDomainModel>(x, WriteResponseType.Added)));
        response.AddRange(modelMapper.ToModel(frameworkWriteResult.UnchangedEntities).Select(x => new WriteResponse<TDomainModel>(x, WriteResponseType.Unchanged)));
        response.AddRange(modelMapper.ToModel(frameworkWriteResult.UpdatedEntities).Select(x => new WriteResponse<TDomainModel>(x, WriteResponseType.Updated)));

        foreach (var failedEntity in frameworkWriteResult.FailedEntities)
        {
            var reason = BulkWriterResult.FromUpdateFailedReason(failedEntity.Reason);
            response.Add(new(modelMapper.ToModel(failedEntity.Item), BulkWriterResultTranslator.GetBulkWriterResultString(reason)));
        }

        return response;
    }
}