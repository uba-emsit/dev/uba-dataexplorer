﻿namespace UBA.DataExplorer.Core.SQL.Mapping;

interface IFrameworkEntityContainer<out T>
{
    T FrameworkEntity { get; }
}