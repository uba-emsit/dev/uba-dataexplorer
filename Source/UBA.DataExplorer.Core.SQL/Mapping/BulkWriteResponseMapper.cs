﻿using Mesap.Framework.DataAccess.BulkWriter;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Mapping;

class BulkWriteResponseMapper(ProcessingContext context)
{
    public List<WriteResponse<TDomainModel>> ToModel<TDomainModel, TFwEntity>(
        IEnumerable<BulkEntityBulkWriterResult<TFwEntity>> frameworkWriteResult,
        List<TDomainModel> domainModels)
        where TDomainModel : IDomainModel
        where TFwEntity : BulkEntity
    {
        var response = new List<WriteResponse<TDomainModel>>();
        var timezoneDefinitions = context.MesapConnection.MesapDatabase.SystemDatabase.TimezoneDefinitions;

        foreach (var (fwWriterResult, domainModel) in frameworkWriteResult.Zip(domainModels))
        {
            if (fwWriterResult.IsError)
            {
                var translation = BulkWriterResultTranslator.GetErrorTranslation(fwWriterResult, timezoneDefinitions);
                response.Add(new(domainModel, translation.Error));
                continue;
            }

            var result = fwWriterResult.Results.First();
            switch (result)
            {
                case BulkWriterResultOkUpdated:
                    response.Add(new(domainModel, WriteResponseType.Updated));
                    break;
                case BulkWriterResultOkAdded:
                    response.Add(new(domainModel, WriteResponseType.Added));
                    break;
                case BulkWriterResultOkUnchanged:
                case BulkWriterResultOkNoRelevantChange:
                    response.Add(new(domainModel, WriteResponseType.Unchanged));
                    break;
                case BulkWriterResultOkDeleted:
                    response.Add(new(domainModel, WriteResponseType.Deleted));
                    break;
                default:
                    throw new InvalidOperationException($"Unknown result type {result.GetType().Name}");
            }
        }
        return response;
    }
}