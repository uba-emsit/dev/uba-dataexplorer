﻿using Mesap.Framework;
using Mesap.Framework.Collections;
using Mesap.Framework.DataAccess.Tables.Internal;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Descriptors;
using UBA.DataExplorer.Core.SQL.Dimensions;
using UBA.DataExplorer.Core.SQL.Hypotheses;
using UBA.DataExplorer.Core.SQL.TimeSeries;
using UBA.DataExplorer.Core.SQL.Trees;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Core.SQL.Mapping;

class Mappers(ProcessingContext context)
{
    public readonly Mapper<IAnnexSearchCriterion, AnnexSearchCriterion, AnnexSearchCriterionWrapper> AnnexSearchCriterion = new(x => new(x));
    public readonly Mapper<IReadOnlyAxesOfTimeSeries, ReadOnlyAxesOfTimeseries, ReadOnlyAxesOfTimeSeriesWrapper> AxesOfTimeSeries = new(x => new(x));
    public readonly Mapper<IDescriptor, Descriptor, DescriptorWrapper> Descriptor = new(x => new(x, context));
    public readonly Mapper<IDescriptorFilterSettings, DescriptorFilterSettings, DescriptorFilterSettingsWrapper> DescriptorFilterSettings = new(x => new(x, context));
    public readonly Mapper<IDescriptorKey, DescriptorKey, DescriptorKeyWrapper> DescriptorKey = new(x => new(x));
    public readonly Mapper<IDimension, Dimension, DimensionWrapper> Dimension = new(x => new(x));
    public readonly Mapper<IDynamicDate, DynamicDate, DynamicDateWrapper> DynamicDate = new(x => new(x));
    public readonly Mapper<IHypothesis, Hypothesis, HypothesisWrapper> Hypothesis = new(x => new(x));
    public readonly Mapper<IMappedTimeSeriesData, MappedTimeseriesData, MappedTimeSeriesDataWrapper> MappedTimeSeriesData = new(x => new(x, context));
    public readonly Mapper<IMasterDataCollectiveQuerySettings, MasterDataCollectiveQuerySettings, MasterDataCollectiveQuerySettingsWrapper> MasterDataCollectiveQuerySettings = new(x => new(x, context));
    public readonly Mapper<IReadOnlyPropertiesOfTimeSeries, ReadOnlyPropertiesOfTimeseries, PropertiesOfTimeSeriesWrapper> PropertiesOfTimeSeries = new(x => new(x));
    public readonly Mapper<ITimeAxisAbsolute, TimeAxisAbsolute, TimeAxisAbsoluteWrapper> TimeAxisAbsolute = new(x => new(x));
    public readonly Mapper<ITimePeriod, TimePeriod, TimePeriodWrapper> TimePeriod = new(x => new(x));
    public readonly Mapper<ITimeSeries, Timeseries, TimeSeriesWrapper> TimeSeries = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesData, TimeseriesData, TimeSeriesDataWrapper> TimeSeriesData = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesDataCoordinates, TimeseriesDataCoordinates, TimeSeriesDataCoordinatesWrapper> TimeSeriesDataCoordinates = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesDataFilterCriterionSettings, TimeseriesDataFilterCriterionSettings, TimeSeriesDataFilterCriterionSettingsWrapper> TimeSeriesDataFilterCriterionSettings = new(x => new(x));
    public readonly Mapper<ITimeFilter, TimeFilter, TimeFilterWrapper> TimeFilter = new(x => new(x));
    public readonly Mapper<ITimeFilterSettings, TimeFilterSettings, TimeFilterSettingsWrapper> TimeFilterSettings = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesDataFilterSettings, TimeseriesDataFilterSettings, TimeSeriesDataFilterSettingsWrapper> TimeSeriesDataFilterSettings = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesFilterSettings, TimeseriesFilterSettings, TimeSeriesFilterSettingsWrapper> TimeSeriesFilterSettings = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesKey, TimeseriesKey, TimeSeriesKeyWrapper> TimeSeriesKey = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesViewColumnSettings, TimeseriesViewColumnSettings, TimeSeriesViewColumnSettingsWrapper> TimeSeriesViewColumnSettings = new(x => new(x));
    public readonly Mapper<ITimeSeriesViewSettings, TimeseriesViewViewSettings, TimeSeriesViewSettingsWrapper> TimeSeriesViewSettings = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesViewTree, EntityTree<TimeseriesView, int>, TimeSeriesViewTreeWrapper> TimeSeriesViewTree = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesViewTreeNode, EntityTree<TimeseriesView, int>.Node, TimeSeriesViewTreeNodeWrapper> TimeSeriesViewTreeNode = new(x => new(x, context));
    public readonly Mapper<ITimeShiftSettings, TimeShiftSettings, TimeShiftSettingsWrapper> TimeShiftSettings = new(x => new(x));
    public readonly Mapper<ITimeSeriesView, TimeseriesView, TimeSeriesViewWrapper> TimeSeriesView = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesViewTimeRuleSettings, TimeseriesViewTimeRuleSettings, TimeSeriesViewTimeRuleSettingsWrapper> TimeSeriesViewTimeRuleSettings = new(x => new(x, context));
    public readonly Mapper<ITimeSeriesViewTimeSettings, TimeseriesViewTimeSettings, TimeSeriesViewTimeSettingsWrapper> TimeSeriesViewTimeSettings = new(x => new(x, context));
    public readonly Mapper<ITree, Tree, TreeWrapper> Tree = new(x => new(x, context));
    public readonly Mapper<ITreeNodeTree, EntityTree<TreeNode, int>, TreeNodeTreeWrapper> TreeNodeTree = new(x => new(x, context));
    public readonly Mapper<ITreeNodeNode, EntityTree<TreeNode, int>.Node, TreeNodeNodeWrapper> TreeNodeNode = new(x => new(x, context));
    public readonly Mapper<ITreeNode, TreeNode, TreeNodeWrapper> TreeNode = new(x => new(x, context));

    public readonly WriteResponseMapper WriteResponse = new();
}

class Mapper<TDomainModel, TEntity, TMesapModel>(Func<TEntity, TMesapModel> createMesapModel)
    where TMesapModel : TDomainModel, IFrameworkEntityContainer<TEntity>
    where TDomainModel : IDomainModel
{
    internal TMesapModel? ToModelNullable(TEntity? entity)
        => entity == null ? default : createMesapModel(entity);

    internal TDomainModel ToModel(TEntity entity)
        => createMesapModel(entity);

    internal List<TDomainModel> ToModel(IEnumerable<TEntity> entities)
        => entities.Select(ToModel).ToList();

    internal TEntity ToEntity(TDomainModel mesapModel)
        => ((IFrameworkEntityContainer<TEntity>)mesapModel).FrameworkEntity;

    internal List<TEntity> ToEntity(IEnumerable<TDomainModel> mesapModels)
        => mesapModels.Select(ToEntity)
            .ToList();
}