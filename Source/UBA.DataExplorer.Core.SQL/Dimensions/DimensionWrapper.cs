﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Dimensions;

class DimensionWrapper(Dimension entity) : IDimension, IFrameworkEntityContainer<Dimension>
{
    public Dimension FrameworkEntity { get; } = entity;

    public int PrimaryKey => FrameworkEntity.PrimaryKey;
    public string Id { get => FrameworkEntity.Id; set => FrameworkEntity.Id = value; }
    public string Name { get => FrameworkEntity.Name; set => FrameworkEntity.Name = value; }
    public string? Description { get => FrameworkEntity.Description; set => FrameworkEntity.Description = value; }

    public IAuditInfo AuditInfo => SQL.AuditInfo.GetChangeAuditInfo(
        FrameworkEntity.ChangedUserId,
        FrameworkEntity.ChangedDate);

    public bool Equals(IDimension? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Equals(other.PrimaryKey, PrimaryKey);
    }

    public override int GetHashCode()
    {
        return PrimaryKey.GetHashCode();
    }

    public override string ToString()
    {
        return Id;
    }
}