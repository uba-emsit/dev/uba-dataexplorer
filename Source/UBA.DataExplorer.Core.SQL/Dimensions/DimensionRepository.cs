﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Dimensions;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using DimensionWriter = Mesap.Framework.DataAccess.DimensionWriter;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.Dimensions;

class DimensionRepository : RepositoryBase<DimensionReader, FWE.Dimension, string>, IDimensionRepository
{
    readonly ProcessingContext _context;
    readonly ReadRepositoryCompanion<IDimension, FWE.Dimension, DimensionWrapper, string> _companion;

    public DimensionRepository(ProcessingContext context) : base(new DimensionReader(context))
    {
        _context = context;
        _companion = new(RawReader, context.Mappers.Dimension, DimensionErrors.NotFound);
    }

    public Task<Result<IDimension>> Read(string id, CancellationToken cancellationToken) =>
        _companion.Read(id, cancellationToken);

    public Task<Result<IDimension>> Read(int primaryKey, CancellationToken cancellationToken) =>
        _companion.Read(primaryKey, cancellationToken);

    public async Task<Result<List<IDimension>>> Read(CancellationToken cancellationToken)
        => _companion.Matching.Match(await RawReader.Read(cancellationToken));

    // Model is
    //  OR a MesapDimensionModel
    //  OR a new to-be-created DimensionModel
    //  OR a new to-be-updated DimensionModel
    [Obsolete("This is demo code only. It is not intended to be used in production.")]
    public Result<IDimension> Write(ConnectionModel connection, IDimension model)
    {
        if (connection is not MesapConnection mesapConnection)
            return Result.Failure<IDimension>(MesapConnectionErrors.IncorrectConnectionType());

        FWE.Dimension dim;

        if (model is DimensionWrapper mesapModel)
        {
            dim = _context.Mappers.Dimension.ToEntity(mesapModel);
            var x = new DimensionWriter(mesapConnection.DatabaseUserContext).Write(dim);
        }
        else
        {
            dim = new FWE.Dimension(model.Id, model.Name) { Description = model.Description };
            var y = new DimensionWriter(mesapConnection.DatabaseUserContext).Write(dim);
        }

        return Result.Success(_context.Mappers.Dimension.ToModel(dim));
    }
}