﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;

namespace UBA.DataExplorer.Core.SQL.Hypotheses;

class HypothesisWrapper(Hypothesis entity) : IHypothesis, IFrameworkEntityContainer<Hypothesis>
{
    public Hypothesis FrameworkEntity { get; } = entity;

    public string Id { get => FrameworkEntity.Id; set => FrameworkEntity.Id = value; }
    public string Name { get => FrameworkEntity.Name; set => FrameworkEntity.Name = value; }
    public string? Description { get => FrameworkEntity.Description; set => FrameworkEntity.Description = value; }
}