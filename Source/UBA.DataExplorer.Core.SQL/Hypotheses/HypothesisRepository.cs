﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Hypotheses;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.Hypotheses;

class HypothesisRepository : RepositoryBase<HypothesisReader, FWE.Hypothesis, string>, IHypothesisRepository
{
    readonly ReadRepositoryCompanion<IHypothesis, FWE.Hypothesis, HypothesisWrapper, string> _companion;

    public HypothesisRepository(ProcessingContext context) : base(new HypothesisReader(context))
    {
        _companion = new(RawReader, context.Mappers.Hypothesis, HypothesisErrors.NotFound);
    }

    public Task<Result<IHypothesis>> Read(string id, CancellationToken cancellationToken)
        => _companion.Read(id, cancellationToken);

    public Task<Result<IHypothesis>> Read(int primaryKey, CancellationToken cancellationToken)
        => _companion.Read(primaryKey, cancellationToken);

    public async Task<Result<List<IHypothesis>>> Read(CancellationToken cancellationToken)
        => _companion.Matching.Match(await RawReader.Read(cancellationToken));
}