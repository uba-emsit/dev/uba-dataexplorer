﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Security;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Login;

class MesapLoginRepository(Application application) : ILoginRepository
{
    public Task<Result<LoginModel>> Login(string username, string password, CancellationToken cancellationToken)
        => InternalLogin(a => a.Logon(username, password), false, cancellationToken);

    public Task<Result<LoginModel>> LoginWithWindowsAccount(CancellationToken cancellationToken)
        => InternalLogin(a => a.Logon(new WindowsIdentityCredentials()), true, cancellationToken);

    async Task<Result<LoginModel>> InternalLogin(Func<Application, UserContext> loginFunc,
        bool isWindowsAccountLogin, CancellationToken cancellationToken)
    {
        UserContext? userContext;
        try
        {
            userContext = await Task.Run(() => loginFunc(application), cancellationToken);
        }
        catch (Exception e)
        {
            return Result.Failure<LoginModel>(DatabaseErrors.NotAvailable(e.Message));
        }

        if (userContext.LogonResult != LogonResult.LoggedOn)
            return Result.Failure<LoginModel>(DatabaseErrors.LogonFailed(userContext.GetLoginResultTranslation()));

        var userInfo = userContext.GetUserInfo();
        var userModel = new UserModel(userInfo.Name, userInfo.Id);

        return new LoginWrapper(userModel, isWindowsAccountLogin, userContext);
    }

    public Task<Result> Logout(LoginModel login, CancellationToken cancellationToken)
    {
        if (login is not LoginWrapper wrapper)
            return Task.FromResult(Result.Failure(MesapConnectionErrors.IncorrectConnectionType()));

        try
        {
            application.Logoff(wrapper.UserContext);
        }
        catch (Exception e)
        {
            return Task.FromResult(Result.Failure(Error.Failure(f => f.BackendErrors.DatabaseError, e)));
        }

        return Task.FromResult(Result.Success());
    }
}