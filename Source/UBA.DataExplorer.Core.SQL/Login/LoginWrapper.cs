﻿using Mesap.Framework;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Login;

class LoginWrapper(UserModel user, bool isWindowsAccountLogin, UserContext userContext)
    : LoginModel(user, isWindowsAccountLogin)
{
    public UserContext UserContext { get; } = userContext;
}