﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

class BackendFactory(IServiceProvider appServiceProvider)
{
    public ServiceProvider LocalServiceProvider { get; }
        = new ServiceCollection()
            .AddInternal(appServiceProvider)
            .AddSingleton(_ => new AppServiceProvider { Provider = appServiceProvider })
            .AddRepositories()
            .BuildServiceProvider();

    public IServiceScope CreateScope(ConnectionModel connection)
    {
        if (connection is not MesapConnection mesapConnection)
            throw new ArgumentOutOfRangeException(MesapConnectionErrors.IncorrectConnectionType().Description);

        var scope = LocalServiceProvider.CreateScope();

        var context = scope.ServiceProvider.GetRequiredService<ProcessingContext>();

        context.MesapConnection = mesapConnection;

        return scope;
    }
}

class AppServiceProvider
{
    /// <summary>
    /// This is the service provider of the application. Use it for logging e.g.
    /// </summary>
    public required IServiceProvider Provider { get; init; }
}