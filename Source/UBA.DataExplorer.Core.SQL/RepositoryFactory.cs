﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL;

class RepositoryFactory(BackendFactory backendFactory)
    : IRepositoryFactory
{
    public ILoginRepository GetLoginRepository()
        => backendFactory.LocalServiceProvider.GetRequiredService<ILoginRepository>();

    public IDatabaseRepository GetDatabaseRepository()
        => backendFactory.LocalServiceProvider.GetRequiredService<IDatabaseRepository>();

    public T GetRepository<T>(ConnectionModel connection) where T : IRepository
    {
        var serviceScope = backendFactory.CreateScope(connection);
        return serviceScope.ServiceProvider.GetRequiredService<T>();
    }
}