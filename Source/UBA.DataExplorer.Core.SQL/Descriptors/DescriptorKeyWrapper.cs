﻿using Mesap.Framework.Collections;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;

namespace UBA.DataExplorer.Core.SQL.Descriptors;

class DescriptorKeyWrapper(DescriptorKey entity) : IDescriptorKey, IFrameworkEntityContainer<DescriptorKey>
{
    public DescriptorKey FrameworkEntity { get; } = entity;

    public int DimensionPrimaryKey => FrameworkEntity.DimensionPrimaryKey;

    public string Id => FrameworkEntity.Id;

    public bool Equals(IDescriptorKey? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return DimensionPrimaryKey == other.DimensionPrimaryKey && string.Equals(Id, other.Id, StringComparison.CurrentCultureIgnoreCase);
    }

    public override int GetHashCode() =>
        unchecked(DimensionPrimaryKey * 397) ^ (Id != null ? StringComparer.InvariantCultureIgnoreCase.GetHashCode(Id) : 0);
}