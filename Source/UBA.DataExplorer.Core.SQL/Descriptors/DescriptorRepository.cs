﻿using Mesap.Framework.Collections;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Dimensions;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Descriptors;

class DescriptorRepository : RepositoryBase<DescriptorReader, Descriptor, DescriptorKey>, IDescriptorRepository
{
    readonly ReadRepositoryCompanion<IDescriptor, Descriptor, DescriptorWrapper, DescriptorKey> _companion;

    public DescriptorRepository(ProcessingContext context)
        : base(new(context))
    {
        _companion = new(RawReader, context.Mappers.Descriptor, DescriptorErrors.NotFound);
    }

    public Task<Result<List<IDescriptor>>> Read(List<int> primaryKeys, CancellationToken cancellationToken) => _companion.Read(primaryKeys, cancellationToken);


    public async Task<Result<List<IDescriptor>>> ReadByDimension(int dimensionPk, CancellationToken cancellationToken)
        => _companion.Matching.Match(await RawReader.ReadByDimension(dimensionPk, cancellationToken));
}