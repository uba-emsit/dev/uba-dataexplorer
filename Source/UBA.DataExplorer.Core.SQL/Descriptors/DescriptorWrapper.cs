﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.Descriptors;

class DescriptorWrapper(Descriptor entity, ProcessingContext context) : IDescriptor, IFrameworkEntityContainer<Descriptor>
{
    public Descriptor FrameworkEntity { get; } = entity;

    public int PrimaryKey => FrameworkEntity.PrimaryKey;

    public IDescriptorKey Key => context.Mappers.DescriptorKey.ToModel(FrameworkEntity.Key);

    public int IconNr => FrameworkEntity.IconNr;
    public int SortNr => FrameworkEntity.SortNr;
    public string Id { get => FrameworkEntity.Id; set => FrameworkEntity.Id = value; }
    public string Label { get => FrameworkEntity.Name; set => FrameworkEntity.Name = value; }
    public string? Description { get => FrameworkEntity.Description; set => FrameworkEntity.Description = value; }

    public IDimension Dimension => context.Mappers.Dimension.ToModel(FrameworkEntity.Dimension);

    public IAuditInfo AuditInfo => SQL.AuditInfo.GetChangeAuditInfo(
        FrameworkEntity.ChangedUserId,
        FrameworkEntity.ChangedDate);

    public bool Equals(IDescriptor? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Equals(other.Key, Key);
    }

    public override int GetHashCode()
    {
        return Key.GetHashCode();
    }

    public override string ToString() => Id;
}