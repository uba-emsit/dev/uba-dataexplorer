﻿using Mesap.Framework;
using Microsoft.Extensions.DependencyInjection;
using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Core.SQL.Database;
using UBA.DataExplorer.Core.SQL.Descriptors;
using UBA.DataExplorer.Core.SQL.Dimensions;
using UBA.DataExplorer.Core.SQL.Hypotheses;
using UBA.DataExplorer.Core.SQL.Login;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Core.SQL.TimeSeries;
using UBA.DataExplorer.Core.SQL.Trees;

namespace UBA.DataExplorer.Core.SQL;

public static class DependencyInjection
{
    /// <summary>
    /// Adds the repository factory to the application service collection.
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    public static IServiceCollection AddBackendFactories(this IServiceCollection services)
        => services
            .AddSingleton<BackendFactory>()
            .AddTransient<IRepositoryFactory, RepositoryFactory>()
            .AddTransient<IServiceFactory, ServiceFactory>();

    /// <summary>
    /// Adds the internal services to the internal service collection.
    /// </summary>
    /// <param name="services"></param>
    /// <param name="appServiceProvider"></param>
    /// <returns></returns>
    internal static IServiceCollection AddInternal(this IServiceCollection services, IServiceProvider appServiceProvider)
        => services
            .AddSingleton<FrameworkThrottler>()
            .AddSingleton(new Application("UBA.DataExplorer"))
            .AddScoped(p => new ProcessingContext { RepositoryProvider = p })
            .AddScoped<Mappers>()
            .AddTransient(_ => appServiceProvider.GetRequiredService<ILoggerFactory>())
            .AddTransient<ITimeAxisService, TimeAxisService>()
            .AddTransient<IMessageServer, MessageServer.MessageServer>();

    /// <summary>
    /// Adds the Mesap Framework repositories to the internal service collection (for the public IRepositoryFactory to be able to resolve them).
    /// </summary>
    /// <param name="services"></param>
    /// <returns></returns>
    internal static IServiceCollection AddRepositories(this IServiceCollection services)
        => services
            .AddTransient<ILoginRepository, MesapLoginRepository>()
            .AddTransient<IDatabaseRepository, MesapDatabaseRepository>()
            .AddTransient<IDimensionRepository, DimensionRepository>()
            .AddTransient<IDescriptorRepository, DescriptorRepository>()
            .AddTransient<IHypothesisRepository, HypothesisRepository>()
            .AddTransient<ITimeSeriesRepository, TimeSeriesRepository>()
            .AddTransient<ITimeSeriesDataRepository, TimeSeriesDataRepository>()
            .AddTransient<ITimeSeriesViewRepository, TimeSeriesViewRepository>()
            .AddTransient<ITreeRepository, TreeRepository>();
}