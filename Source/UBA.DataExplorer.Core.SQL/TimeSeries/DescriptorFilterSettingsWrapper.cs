﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class DescriptorFilterSettingsWrapper(DescriptorFilterSettings entity, ProcessingContext context)
    : IDescriptorFilterSettings, IFrameworkEntityContainer<DescriptorFilterSettings>
{
    public DescriptorFilterSettings FrameworkEntity => entity;

    public IDescriptorFilterSettings.DescriptorFilterMode FilterMode
        => entity.IsMasterDataQuery
            ? IDescriptorFilterSettings.DescriptorFilterMode.MasterData
            : entity.TreePrimaryKey > 0
                ? IDescriptorFilterSettings.DescriptorFilterMode.Tree
                : IDescriptorFilterSettings.DescriptorFilterMode.SingleDescriptors;

    public List<int> DescriptorPrimaryKeys => entity.DescriptorPrimaryKeys;
    public int DimensionPrimaryKey => entity.DimensionPrimaryKey;
    public TreeFilterMode TreeFilterMode => (TreeFilterMode)entity.TreeFilterMode;
    public bool IsDescriptorFilterPlaceholder => entity.IsDescriptorFilterPlaceholder;
    public bool IsEnabled => entity.IsEnabled;
    public bool IsNotEmpty => entity.IsNotEmpty;
    public IMasterDataCollectiveQuerySettings MasterDataQuery => context.Mappers.MasterDataCollectiveQuerySettings.ToModel(entity.MasterDataQuery);
    public bool UseMultipleAllocationOpen => entity.UseMultipleAllocationOpen;
    public bool UseMultipleAllocationExclusive => entity.UseMultipleAllocationExclusive;
    public bool UseMultipleAllocationNone => entity.UseMultipleAllocationNone;
    public bool OrIsEmpty => entity.OrIsEmpty;
    public List<int> TreeNodePrimaryKeys => entity.TreeNodePrimaryKeys;
    public int TreePrimaryKey => entity.TreePrimaryKey;
}