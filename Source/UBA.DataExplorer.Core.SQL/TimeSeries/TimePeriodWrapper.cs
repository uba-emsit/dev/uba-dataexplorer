﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimePeriodWrapper(TimePeriod entity) : ITimePeriod, IFrameworkEntityContainer<TimePeriod>
{
    public TimePeriod FrameworkEntity => entity;

    public string TimeZoneId => FrameworkEntity.TimezoneId;

    /// <summary>Defines the length of the represented period of time.</summary>
    public TimeResolution TimeResolution => (TimeResolution)entity.TimeResolution;

    /// <summary>DateTimeOffset that defines the beginning of the represented period of time.</summary>
    public DateTimeOffset Date => entity.Date;

    public override string ToString() => Date.ToString(TimeResolution);
}