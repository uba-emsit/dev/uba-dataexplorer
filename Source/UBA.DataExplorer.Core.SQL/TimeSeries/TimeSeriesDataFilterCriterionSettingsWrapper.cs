﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesDataFilterCriterionSettingsWrapper(FWE.TimeseriesDataFilterCriterionSettings entity)
    : ITimeSeriesDataFilterCriterionSettings, IFrameworkEntityContainer<FWE.TimeseriesDataFilterCriterionSettings>
{
    public FWE.TimeseriesDataFilterCriterionSettings FrameworkEntity => entity;

    public SearchCondition Condition { get; } = (SearchCondition)entity.Condition;
    public bool IsEnabled { get; } = entity.IsEnabled;
    public SearchLogicalOp LogicalOperator { get; } = (SearchLogicalOp)entity.LogicalOperator;
    public TimeSeriesDataQueryItem QueryItem { get; } = (TimeSeriesDataQueryItem)entity.QueryItem;
    public string SerializedValue { get; } = entity.SerializedValue;
}