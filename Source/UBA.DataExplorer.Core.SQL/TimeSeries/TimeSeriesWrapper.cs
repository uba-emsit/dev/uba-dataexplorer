﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesWrapper(Timeseries entity, ProcessingContext context) : ITimeSeries, IFrameworkEntityContainer<Timeseries>
{
    public Timeseries FrameworkEntity => entity;

    public string Id => FrameworkEntity.Id;
    public string Key => FrameworkEntity.Key;
    public string Name => FrameworkEntity.Name;

    public IReadOnlyList<ITimeSeriesKey> TimeSeriesKeys => context.Mappers.TimeSeriesKey.ToModel(FrameworkEntity.Keys);

    public int PrimaryKey => FrameworkEntity.PrimaryKey;

    public IReadOnlyAxesOfTimeSeries Axes => context.Mappers.AxesOfTimeSeries.ToModel(FrameworkEntity.Axes);

    public IReadOnlyPropertiesOfTimeSeries Properties => context.Mappers.PropertiesOfTimeSeries.ToModel(FrameworkEntity.Properties);

    public int SortNr => FrameworkEntity.SortNr;

    public string Unit => FrameworkEntity.CalcUnit;

    public bool IsVirtual => FrameworkEntity.VirtualType == TimeseriesVirtualType.Virtual;

    public bool Equals(ITimeSeries? other)
    {
        if (ReferenceEquals(null, other)) return false;
        if (ReferenceEquals(this, other)) return true;
        return Equals(other.PrimaryKey, PrimaryKey);
    }

    public override int GetHashCode() => PrimaryKey;
}