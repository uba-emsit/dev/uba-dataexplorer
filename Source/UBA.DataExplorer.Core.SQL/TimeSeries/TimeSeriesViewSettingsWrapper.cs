﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewSettingsWrapper(FWE.TimeseriesViewViewSettings entity, ProcessingContext context)
    : ITimeSeriesViewSettings, IFrameworkEntityContainer<FWE.TimeseriesViewViewSettings>
{
    public FWE.TimeseriesViewViewSettings FrameworkEntity => entity;

    public BooleanTriState AggregationAllowIncomplete => (BooleanTriState)FrameworkEntity.AggregationAllowIncomplete;

    public AggregationCalendar AggregationCalendar => (AggregationCalendar)FrameworkEntity.AggregationCalendar;

    public AggregationMappingMode AggregationMappingMode => (AggregationMappingMode)FrameworkEntity.AggregationMappingMode;

    public AggregationRule AggregationRule => (AggregationRule)FrameworkEntity.AggregationRule;

    public CellAlignment AlignmentOfData => (CellAlignment)FrameworkEntity.AlignmentOfData;

    public CellAlignment AlignmentOfDescriptors => (CellAlignment)FrameworkEntity.AlignmentOfDescriptors;

    public BooleanTriState AllowInheritance => (BooleanTriState)FrameworkEntity.AllowInheritance;

    public List<ITimeSeriesViewColumnSettings> Columns => context.Mappers.TimeSeriesViewColumnSettings.ToModel(FrameworkEntity.Columns);

    public bool ColumnWidthAuto => FrameworkEntity.ColumnWidthAuto;

    public int ColumnWidthOfValues => FrameworkEntity.ColumnWidthOfValues;

    public int ColumnWidthPortraitColumn1 => FrameworkEntity.ColumnWidthPortraitColumn1;

    public int ColumnWidthPortraitColumns => FrameworkEntity.ColumnWidthPortraitColumns;

    public TimeseriesViewGridDataMode DataMode => (TimeseriesViewGridDataMode)FrameworkEntity.DataMode;

    public bool DimensionModeAuto => FrameworkEntity.DimensionModeAuto;

    public DisaggregationRule DisAggregationRule => (DisaggregationRule)FrameworkEntity.DisAggregationRule;

    public int DispDecimalPlaces => FrameworkEntity.DispDecimalPlaces;

    public NumberFormat DisplayFormat => (NumberFormat)FrameworkEntity.DisplayFormat;

    public string DisplayUnit => FrameworkEntity.DisplayUnit;

    public List<int> ExpandedTimeSeriesPrimaryKeys => FrameworkEntity.ExpandedTimeseriesPrimaryKeys;

    public ExtrapolationRule ExtrapolationRule => (ExtrapolationRule)FrameworkEntity.ExtrapolationRule;

    public int FreezePosition => FrameworkEntity.FreezePosition;

    public List<int> HypothesesPrimaryKeys => FrameworkEntity.HypothesesPrimaryKeys;

    public double InterpolationGradient => FrameworkEntity.InterpolationGradient;

    public InterpolationRule InterpolationRule => (InterpolationRule)FrameworkEntity.InterpolationRule;

    public BooleanTriState IsConstOMP => (BooleanTriState)FrameworkEntity.IsConstOMP;

    public bool RowAuto => FrameworkEntity.RowAuto;

    public bool RowAutoAffectsFilteredValues => FrameworkEntity.RowAutoAffectsFilteredValues;

    public bool RowHeightAuto => FrameworkEntity.RowHeightAuto;

    public int RowHeightFixCount => FrameworkEntity.RowHeightFixCount;

    public int SubtotalsFunction => FrameworkEntity.SubtotalsFunction;

    public bool SubtotalsOn => FrameworkEntity.SubtotalsOn;

    public int SubtotalsUpToColumn => FrameworkEntity.SubtotalsUpToColumn;

    public TimeResolution MainTimeResolution => (TimeResolution)FrameworkEntity.MainTimeResolution;

    public List<TimeResolution> TimeResolutions => FrameworkEntity.TimeResolutions.Select(x => (TimeResolution)x).ToList();

    public bool UseListSortOrder => FrameworkEntity.UseListSortOrder;

    public string ViewTimezoneId => FrameworkEntity.ViewTimezoneId;
}
