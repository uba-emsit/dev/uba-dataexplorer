﻿using Mesap.Framework.DataAccess.Tables.Internal;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewRepository : RepositoryBase<TimeSeriesViewReader, FWE.TimeseriesView, string>, ITimeSeriesViewRepository
{
    readonly ReadRepositoryCompanion<ITimeSeriesView, FWE.TimeseriesView, TimeSeriesViewWrapper, string> _companion;
    readonly Matching<ITimeSeriesViewTree, EntityTree<FWE.TimeseriesView, int>, TimeSeriesViewTreeWrapper> _matchingTree;

    public TimeSeriesViewRepository(ProcessingContext context) : base(new(context))
    {
        _companion = new(RawReader, context.Mappers.TimeSeriesView, TimeSeriesViewErrors.NotFound);
        _matchingTree = new(context.Mappers.TimeSeriesViewTree, TimeSeriesViewTreeErrors.NotFound);
    }

    public Task<Result<ITimeSeriesView>> Read(string id, CancellationToken cancellationToken) =>
        _companion.Read(id, cancellationToken);

    public Task<Result<List<ITimeSeriesView>>> Read(List<string> idList, CancellationToken cancellationToken) =>
        _companion.Read(idList, cancellationToken);

    public Task<Result<List<ITimeSeriesView>>> Read(List<int> primaryKeys, CancellationToken cancellationToken) =>
        _companion.Read(primaryKeys, cancellationToken);

    public async Task<Result<ITimeSeriesViewTree>> ReadTree(CancellationToken cancellationToken)
        => _matchingTree.Match(await RawReader.ReadTree(cancellationToken), "");
}