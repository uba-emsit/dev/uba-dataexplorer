﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class DynamicDateWrapper(FWE.DynamicDate entity) : IDynamicDate, IFrameworkEntityContainer<FWE.DynamicDate>
{
    public FWE.DynamicDate FrameworkEntity => entity;
    public bool ExcludeEndDate => entity.ExcludeEndDate;
    public DateTimeOffset FixDate => entity.FixDate;
    public bool IsFix => entity.IsFix;
    public TimeResolution NowTimeResolution => (TimeResolution)entity.NowTimeResolution;
    public int Offset => entity.Offset;
    public TimeResolution OffsetTimeResolution => (TimeResolution)entity.OffsetTimeResolution;
    // TODO: Implement GetTimezone method
    public int Timezone => -1;
    public string TimezoneId => entity.TimezoneId;

}