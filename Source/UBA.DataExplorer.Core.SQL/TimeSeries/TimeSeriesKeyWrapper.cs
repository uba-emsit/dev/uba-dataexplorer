﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesKeyWrapper(TimeseriesKey entity, ProcessingContext context) : ITimeSeriesKey, IFrameworkEntityContainer<TimeseriesKey>
{
    public TimeseriesKey FrameworkEntity => entity;

    public int PrimaryKey => FrameworkEntity.PrimaryKey;

    public int DimensionNr => FrameworkEntity.DimensionNr;

    public IDescriptor Descriptor => context.Mappers.Descriptor.ToModel(FrameworkEntity.Descriptor);
}