﻿using System.Collections.ObjectModel;
using Mesap.Framework.DataAccess.Tables.Internal;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewTreeWrapper(EntityTree<TimeseriesView, int> entity, ProcessingContext context)
    : ITimeSeriesViewTree, IFrameworkEntityContainer<EntityTree<TimeseriesView, int>>
{
    public EntityTree<TimeseriesView, int> FrameworkEntity => entity;

    public IEntityTreeNode<ITimeSeriesView> RootNode => context.Mappers.TimeSeriesViewTreeNode.ToModel(FrameworkEntity.RootNode);

    public int Count => FrameworkEntity.Count;
}

class TimeSeriesViewTreeNodeWrapper(EntityTree<TimeseriesView, int>.Node entity, ProcessingContext context)
    : ITimeSeriesViewTreeNode, IFrameworkEntityContainer<EntityTree<TimeseriesView, int>.Node>
{
    public EntityTree<TimeseriesView, int>.Node FrameworkEntity => entity;

    public ReadOnlyCollection<IEntityTreeNode<ITimeSeriesView>> Children
        => new(context.Mappers.TimeSeriesViewTreeNode
            .ToModel(FrameworkEntity.Children).Cast<IEntityTreeNode<ITimeSeriesView>>().ToList());

    public int PrimaryKey => FrameworkEntity.Entity.PrimaryKey;

    public int SortNr => FrameworkEntity.Entity.SortNr;

    public int Level => FrameworkEntity.Level;

    public ITimeSeriesView Entity => context.Mappers.TimeSeriesView.ToModel(FrameworkEntity.Entity);
}
