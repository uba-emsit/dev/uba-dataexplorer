﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeFilterSettingsWrapper(TimeFilterSettings entity, ProcessingContext context) : ITimeFilterSettings, IFrameworkEntityContainer<TimeFilterSettings>
{
    public TimeFilterSettings FrameworkEntity => entity;

    public List<ITimeFilter> TimeFilters => context.Mappers.TimeFilter.ToModel(FrameworkEntity.TimeFilters);
}