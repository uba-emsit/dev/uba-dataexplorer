﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class MasterDataCollectiveQuerySettingsWrapper(FWE.MasterDataCollectiveQuerySettings entity, ProcessingContext context)
    : IMasterDataCollectiveQuerySettings, IFrameworkEntityContainer<FWE.MasterDataCollectiveQuerySettings>
{
    public FWE.MasterDataCollectiveQuerySettings FrameworkEntity => entity;

    public IDynamicDate QueryDynamicDate => context.Mappers.DynamicDate.ToModel(entity.QueryDynamicDate);
    public TimeResolution QueryTimeResolution => (TimeResolution)entity.QueryTimeResolution;
    public List<IAnnexSearchCriterion> SearchCriteria => context.Mappers.AnnexSearchCriterion.ToModel(entity.SearchCriteria);
    public bool TreeIsUsed => entity.TreeIsUsed;
    public int TreeLevelForAnd => entity.TreeLevelForAnd;
    public int TreeLevelForResults => entity.TreeLevelForResults;
    public int TreeNr => entity.TreeNr;
}
