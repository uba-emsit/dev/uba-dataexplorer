﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewTimeSettingsWrapper(FWE.TimeseriesViewTimeSettings entity, ProcessingContext context) : ITimeSeriesViewTimeSettings, IFrameworkEntityContainer<FWE.TimeseriesViewTimeSettings>
{
    public FWE.TimeseriesViewTimeSettings FrameworkEntity => entity;
    public IDynamicDate DynamicStartDate => context.Mappers.DynamicDate.ToModel(FrameworkEntity.DynamicStartDate);
    public int ModelPrimaryKey => entity.ModelPrimaryKey;
    public bool RangeUseDuration => entity.RangeUseDuration;
    public bool SortDescending => entity.SortDescending;
    public bool SyncStartDates => entity.SyncStartDates;
    public ITimeFilterSettings TimeFilter => context.Mappers.TimeFilterSettings.ToModel(FrameworkEntity.TimeFilter);
    public TimeSeriesViewTimeRule TimeRule => (TimeSeriesViewTimeRule)FrameworkEntity.TimeRule;
    public List<ITimeSeriesViewTimeRuleSettings> TimeRuleSettings => context.Mappers.TimeSeriesViewTimeRuleSettings.ToModel(FrameworkEntity.TimeRuleSettings);
    public bool UseDynamicStartDate => entity.UseDynamicStartDate;
    public bool UseOnlyUsedPeriodsWithValuesInTimeRange => entity.UseOnlyUsedPeriodsWithValuesInTimeRange;
    public bool UseWebTimeAxis => entity.UseWebTimeAxis;

}