﻿using Mesap.Framework.Collections;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class PropertiesOfTimeSeriesWrapper(ReadOnlyPropertiesOfTimeseries entity) : IReadOnlyPropertiesOfTimeSeries, IFrameworkEntityContainer<ReadOnlyPropertiesOfTimeseries>
{
    public ReadOnlyPropertiesOfTimeseries FrameworkEntity => entity;

    // Expand when needed
}