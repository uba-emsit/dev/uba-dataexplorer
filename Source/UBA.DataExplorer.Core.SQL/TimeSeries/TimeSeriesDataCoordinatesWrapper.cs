﻿using Mesap.Framework.Collections;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesDataCoordinatesWrapper(TimeseriesDataCoordinates entity, ProcessingContext context)
    : ITimeSeriesDataCoordinates, IFrameworkEntityContainer<TimeseriesDataCoordinates>
{
    public TimeseriesDataCoordinates FrameworkEntity => entity;

    public ITimePeriod TimePeriod1 => context.Mappers.TimePeriod.ToModel(FrameworkEntity.TimePeriod1);
}
