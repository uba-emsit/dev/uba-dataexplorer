﻿using Mesap.Framework.Collections;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class ReadOnlyAxesOfTimeSeriesWrapper(ReadOnlyAxesOfTimeseries entity) : IReadOnlyAxesOfTimeSeries, IFrameworkEntityContainer<ReadOnlyAxesOfTimeseries>
{
    public ReadOnlyAxesOfTimeseries FrameworkEntity => entity;

    // Expand when needed
}