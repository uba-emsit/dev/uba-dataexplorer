﻿using Mesap.Framework;
using Mesap.Framework.DataAccess;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewTimeRuleSettingsWrapper(FWE.TimeseriesViewTimeRuleSettings entity, ProcessingContext context)
    : ITimeSeriesViewTimeRuleSettings, IFrameworkEntityContainer<FWE.TimeseriesViewTimeRuleSettings>
{
    public FWE.TimeseriesViewTimeRuleSettings FrameworkEntity => entity;

    public int IntervalCount => entity.IntervalCount;
    public int IntervalIncrement => entity.IntervalIncrement;
    public TimeResolution IntervalIncrementTimeResolution => (TimeResolution)entity.IntervalIncrement;
    public Result<ITimePeriod> IntervalStartPeriod => CreateTimePeriod(entity.IntervalStartPeriod);
    public List<Result<ITimePeriod>> PeriodList => CreateTimePeriod(entity.PeriodList);
    public int RangeDuration => entity.RangeDuration;
    public TimeResolution RangeDurationTimeResolution => (TimeResolution)entity.RangeDurationTimeResolution;
    public Result<ITimePeriod> GetRangeFromPeriod() => CreateTimePeriod(entity.RangeFromPeriod);

    public Result<ITimePeriod> GetRangeToPeriod() => CreateTimePeriod(entity.RangeToPeriod);
    public string TimeDisplayFormat => entity.TimeDisplayFormat;
    public TimeResolution TimeResolution => (TimeResolution)entity.TimeResolution;
    public ITimeShiftSettings TimeShift => context.Mappers.TimeShiftSettings.ToModel(FrameworkEntity.TimeShift);

    public bool Equals(ITimeSeriesViewTimeRuleSettings? other) => throw new NotImplementedException();

    Result<ITimePeriod> CreateTimePeriod(int periodNr)
    {
        var dtz = context.MesapConnection.DatabaseUserContext.GetDefaultTimezoneDefinition();

        return TimePeriodConverter.TryCreateTimePeriod(periodNr, entity.TimeResolution, dtz, out var timePeriod)
            ? Result.Success(context.Mappers.TimePeriod.ToModel(timePeriod))
            : Result.Failure<ITimePeriod>(TimePeriodErrors.CreationFailed());
    }

    List<Result<ITimePeriod>> CreateTimePeriod(List<int> periodNrs) => periodNrs.Select(CreateTimePeriod).ToList();
}