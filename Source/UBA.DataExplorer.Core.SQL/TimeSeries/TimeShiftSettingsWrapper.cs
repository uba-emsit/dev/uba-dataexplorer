﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeShiftSettingsWrapper(TimeShiftSettings entity) : ITimeShiftSettings, IFrameworkEntityContainer<TimeShiftSettings>
{
    public TimeShiftSettings FrameworkEntity => entity;
    public bool Equals(ITimeShiftSettings? other) => throw new NotImplementedException();

    public TimeResolution MainTimeResolution => (TimeResolution)entity.MainTimeResolution;
    public int ShiftLargeIncrement => entity.ShiftLargeIncrement;
    public TimeResolution ShiftLargeTimeResolution => (TimeResolution)entity.ShiftLargeTimeResolution;
    public int ShiftSmallIncrement => entity.ShiftSmallIncrement;
    public TimeResolution ShiftSmallTimeResolution => (TimeResolution)entity.ShiftSmallTimeResolution;
}