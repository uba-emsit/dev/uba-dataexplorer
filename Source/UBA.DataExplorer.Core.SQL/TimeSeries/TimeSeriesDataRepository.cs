﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWC = Mesap.Framework.Common;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesDataRepository(ProcessingContext context)
    : RepositoryBase<TimeSeriesDataReader, FWE.TimeseriesData, string>(new(context)), ITimeSeriesDataRepository
{
    readonly RepositoryCompanion<ITimeSeriesData, FWE.TimeseriesData, TimeSeriesDataWrapper> _readCompanion = new(context.Mappers.TimeSeriesData, TimeSeriesErrors.NotFound);

    // TODO: This method assumes that the interfaces are implemented by the classes X_Wrapper, where X is the name of the interface
    public async Task<Result<List<ITimeSeriesData>>> Read(ICollection<ITimeSeries> timeSeries, IHypothesis hypothesis,
        ITimeAxisAbsolute timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken)
    {
        var tsEntities = context.Mappers.TimeSeries.ToEntity(timeSeries);
        var hypo = context.Mappers.Hypothesis.ToEntity(hypothesis);
        var ta = context.Mappers.TimeAxisAbsolute.ToEntity(timeAxis);
        var ts = (FWC.TimeResolution)timeResolution;

        var tsdEntities = await RawReader.Read(tsEntities, hypo, ta, ts, cancellationToken);

        return _readCompanion.Matching.Match(tsdEntities);
    }

    // TODO: This method assumes that the interfaces are implemented by the classes X_Wrapper, where X is the name of the interface
    public async Task<Result<List<IMappedTimeSeriesData>>> ReadMapped(ICollection<ITimeSeries> timeSeries, IHypothesis hypothesis,
        ITimeAxisAbsolute timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken)
    {
        var tsEntities = context.Mappers.TimeSeries.ToEntity(timeSeries);
        var hypo = context.Mappers.Hypothesis.ToEntity(hypothesis);
        var ta = context.Mappers.TimeAxisAbsolute.ToEntity(timeAxis);
        var ts = (FWC.TimeResolution)timeResolution;

        var tsdMappedEntities = await RawReader.ReadMapped(tsEntities, hypo, ta, ts, cancellationToken);

        var mappedMatcher = new Matching<IMappedTimeSeriesData, MappedTimeseriesData, MappedTimeSeriesDataWrapper>(context.Mappers.MappedTimeSeriesData, TimeSeriesDataErrors.NotFound);

        return mappedMatcher.Match(tsdMappedEntities);
    }

    public async Task<Result<List<WriteResponse<ITimeSeriesData>>>> Write(ICollection<ITimeSeriesData> timeSeriesData, CancellationToken cancellationToken)
    {
        var tsdEntities = context.Mappers.TimeSeriesData.ToEntity(timeSeriesData);
        var writeResult = await new TimeSeriesDataWriter(context).Write(tsdEntities, cancellationToken);

        if (writeResult.IsFailure)
            return Result.Failure<List<WriteResponse<ITimeSeriesData>>>(writeResult.Error);

        var result = new BulkWriteResponseMapper(context)
            .ToModel(writeResult.Value, timeSeriesData.ToList());

        return Result.Success(result);
    }
}