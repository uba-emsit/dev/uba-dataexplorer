﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class AnnexSearchCriterionWrapper(FWE.AnnexSearchCriterion entity) : IAnnexSearchCriterion, IFrameworkEntityContainer<FWE.AnnexSearchCriterion>
{
    public FWE.AnnexSearchCriterion FrameworkEntity => entity;

    public int ComponentNr => entity.ComponentNr;
    public SearchCondition Condition => (SearchCondition)entity.Condition;
    public bool IsEnabled => entity.IsEnabled;
    public int ItemNr => entity.ItemNr;
    public AnnexItemType ItemType => (AnnexItemType)entity.ItemType;
    public SearchLogicalOp LogicalOp => (SearchLogicalOp)entity.LogicalOp;
    public int PrimaryKey => entity.PrimaryKey;
    public string Unit => entity.Unit;
    public object Value => entity.Value;
}