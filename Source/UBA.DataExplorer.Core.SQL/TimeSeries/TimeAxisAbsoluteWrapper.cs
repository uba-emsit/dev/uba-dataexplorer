﻿using Mesap.Framework;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeAxisAbsoluteWrapper(TimeAxisAbsolute entity) : ITimeAxisAbsolute, IFrameworkEntityContainer<TimeAxisAbsolute>
{
    public TimeAxisAbsolute FrameworkEntity => entity;
}