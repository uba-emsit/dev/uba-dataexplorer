﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesDataFilterSettingsWrapper(TimeseriesDataFilterSettings entity, ProcessingContext context)
    : ITimeSeriesDataFilterSettings, IFrameworkEntityContainer<TimeseriesDataFilterSettings>
{
    public TimeseriesDataFilterSettings FrameworkEntity => entity;

    public List<ITimeSeriesDataFilterCriterionSettings> Criterions =>
        context.Mappers.TimeSeriesDataFilterCriterionSettings.ToModel(entity.Criterions);
}