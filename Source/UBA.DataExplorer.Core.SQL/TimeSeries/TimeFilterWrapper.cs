﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeFilterWrapper(TimeFilter entity) : ITimeFilter, IFrameworkEntityContainer<TimeFilter>
{
    public TimeFilter FrameworkEntity => entity;

    public string AdditionalCriteria => FrameworkEntity.AdditionalCriteria;

    public bool Exclude => FrameworkEntity.Exclude;

    public bool IsEnabled => FrameworkEntity.IsEnabled;

    public TimeFilterRule TimeFilterRule => (TimeFilterRule)FrameworkEntity.TimeFilterRule;
}