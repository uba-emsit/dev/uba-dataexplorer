﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class MappedTimeSeriesDataWrapper(FWE.MappedTimeseriesData entity, ProcessingContext context)
    : IMappedTimeSeriesData, IFrameworkEntityContainer<FWE.MappedTimeseriesData>
{
    public FWE.MappedTimeseriesData FrameworkEntity => entity;

    public ITimeSeriesDataCoordinates Coordinates => context.Mappers.TimeSeriesDataCoordinates.ToModel(FrameworkEntity.Key);
    public double? Value { get => FrameworkEntity.Value; set => throw new NotSupportedException("Settings value on mapped value not supported"); }
    public int TimeSeriesPrimaryKey => FrameworkEntity.Timeseries.PrimaryKey;
    public IHypothesis Hypothesis => context.Mappers.Hypothesis.ToModel(FrameworkEntity.Hypothesis);
    public TimeResolution TimeResolution => (TimeResolution)FrameworkEntity.TimePeriod.TimeResolution;
    public MappingResult MappingResult => (MappingResult)FrameworkEntity.MappingResult;

    // TODO: this needs to be implemented in the Framework first and then added here
    public DateTime ChangedDate { get; }
    public string? ChangedUserId { get; }
    public string? DataSource { get; set; }
    public bool IsWriteProtected { get; set; }
    public string? NoValueReason { get; set; }
    public int? ProtectionLevel { get; set; }
    public int? Quality { get; set; }
    public bool HasDocumentation { get; }
}
