﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.SQL.EntityOperations;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesRepository : RepositoryBase<TimeSeriesReader, FWE.Timeseries, string>, ITimeSeriesRepository
{
    readonly ReadRepositoryCompanion<ITimeSeries, FWE.Timeseries, TimeSeriesWrapper, string> _companion;
    readonly ProcessingContext _context;

    public TimeSeriesRepository(ProcessingContext context) : base(new(context))
    {
        _context = context;
        _companion = new(RawReader, context.Mappers.TimeSeries, TimeSeriesErrors.NotFound);
    }

    public Task<Result<ITimeSeries>> Read(string id, CancellationToken cancellationToken) => _companion.Read(id, cancellationToken);

    public Task<Result<List<ITimeSeries>>> Read(List<string> idList, CancellationToken cancellationToken) => _companion.Read(idList, cancellationToken);

    public Task<Result<List<ITimeSeries>>> Read(List<int> primaryKeys, CancellationToken cancellationToken) => _companion.Read(primaryKeys, cancellationToken);

    public async Task<Result<List<ITimeSeries>>> ReadFromFilter(ITimeSeriesFilterSettings timeSeriesFilter, CancellationToken cancellationToken)
    {
        var filter = _context.Mappers.TimeSeriesFilterSettings.ToEntity(timeSeriesFilter);

        return _companion.Matching.Match(await RawReader.ReadFiltered(filter, cancellationToken));
    }

    public async Task<Result<List<ITimeSeries>>> ReadFromViewId(string timeSeriesViewId,
        CancellationToken cancellationToken)
    {
        var tsvResult = await new TimeSeriesViewRepository(_context).RawReader.Read(timeSeriesViewId, cancellationToken);

        return await tsvResult.Map(async v =>
        {
            var timeSeriesList = await RawReader.ExecuteThrottled(r => r.Read(v), cancellationToken);
            return _companion.Matching.Match(timeSeriesList);
        });
    }
}