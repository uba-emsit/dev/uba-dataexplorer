﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesFilterSettingsWrapper(TimeseriesFilterSettings entity, ProcessingContext context)
    : ITimeSeriesFilterSettings, IFrameworkEntityContainer<TimeseriesFilterSettings>
{
    public TimeseriesFilterSettings FrameworkEntity => entity;

    public ITimeSeriesDataFilterSettings DataFilter => context.Mappers.TimeSeriesDataFilterSettings.ToModel(entity.DataFilter);
    public List<IDescriptorFilterSettings> DescriptorFilters => context.Mappers.DescriptorFilterSettings.ToModel(entity.DescriptorFilters);
    public bool IgnoreMultipleAllocation => entity.IgnoreMultipleAllocation;
    public List<int> TimeSeriesPrimaryKeys => entity.TimeseriesPrimaryKeys;
    public int TimeZone => entity.Timezone;
    public string TimeZoneId => entity.TimezoneId;
    public TimeSeriesFilterUsage FilterUsage => (TimeSeriesFilterUsage)entity.FilterUsage;
    public bool UseTimeZoneFilter => entity.UseTimezoneFilter;
}