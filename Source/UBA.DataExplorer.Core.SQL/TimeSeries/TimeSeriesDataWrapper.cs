﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesDataWrapper(TimeseriesData entity, ProcessingContext context) : ITimeSeriesData, IFrameworkEntityContainer<TimeseriesData>
{
    public TimeseriesData FrameworkEntity => entity;

    public int TimeSeriesPrimaryKey => entity.Timeseries.PrimaryKey;

    public double? Value
    {
        get => entity.Value;
        set => entity.Value = value;
    }

    public int? Quality
    {
        get => entity.Quality;
        set => entity.Quality = value;
    }

    public bool HasDocumentation => entity.AnnexObjNr > 0;

    public int? ProtectionLevel
    {
        get => entity.ProtectionLevel;
        set => entity.ProtectionLevel = value;
    }

    /// <summary>
    /// IsWriteProtected is true when:
    /// - entity.IsWriteProtected
    /// - 
    /// </summary>
    public bool IsWriteProtected
    {
        get => entity.IsWriteProtected;
        set => entity.IsWriteProtected = value;
    }

    public string? NoValueReason
    {
        get => entity.NoValueReason == Mesap.Framework.Common.DataNoValueReason.None ? null : entity.NoValueReason.ToString();
        set => throw new NotImplementedException();
    }

    public ITimeSeriesDataCoordinates Coordinates => context.Mappers.TimeSeriesDataCoordinates.ToModel(entity.Coordinates);

    public ITimeSeriesDataCoordinates Key => context.Mappers.TimeSeriesDataCoordinates.ToModel(entity.Key);

    public DateTime ChangedDate => entity.ChangedDate;

    public string? ChangedUserId => entity.ChangedUserId;

    public string? DataSource
    {
        get => entity.DataSource;
        set => entity.DataSource = value;
    }

    public IHypothesis Hypothesis => context.Mappers.Hypothesis.ToModel(FrameworkEntity.Hypothesis);

    public TimeResolution TimeResolution => (TimeResolution)FrameworkEntity.TimeResolution;
}
