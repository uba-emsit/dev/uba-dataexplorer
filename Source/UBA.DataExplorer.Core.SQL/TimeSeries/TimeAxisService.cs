﻿using Mesap.Framework;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeAxisService(Mappers mappers) : ITimeAxisService
{
    public ITimeAxisAbsolute CreateAbsolute(DateTimeOffset from, DateTimeOffset to)
    {
        if (from > to) (from, to) = (to, from);
        return mappers.TimeAxisAbsolute.ToModel(TimeAxis.CreateAbsolute(from, to));
    }
}
