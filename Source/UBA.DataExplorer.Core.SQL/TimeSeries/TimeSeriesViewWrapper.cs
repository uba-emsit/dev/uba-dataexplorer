﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewWrapper(TimeseriesView entity, ProcessingContext context) : ITimeSeriesView, IFrameworkEntityContainer<TimeseriesView>
{
    public TimeseriesView FrameworkEntity => entity;

    public string Id => FrameworkEntity.Id;
    public string Key => FrameworkEntity.Key;

    public int PrimaryKey => FrameworkEntity.PrimaryKey;

    public IAuditInfo AuditInfo => SQL.AuditInfo.GetFullAuditInfo(
        FrameworkEntity.CreatedUserId,
        FrameworkEntity.CreatedDate,
        FrameworkEntity.ChangedUserId,
        FrameworkEntity.ChangedDate);

    public string Name => FrameworkEntity.Name;

    public int SortNr => FrameworkEntity.SortNr;

    public int PredecessorNr => FrameworkEntity.PredecessorNr;

    public bool IsLeaf => FrameworkEntity.IsLeaf;

    public bool IsSystemView => FrameworkEntity.IsSystemView;

    public string Description => FrameworkEntity.Description;

    public ITimeSeriesViewTimeSettings TimeSettings => context.Mappers.TimeSeriesViewTimeSettings.ToModel(FrameworkEntity.TimeSettings);

    public ITimeSeriesViewSettings Settings => context.Mappers.TimeSeriesViewSettings.ToModel(FrameworkEntity.ViewSettings);

    public ITimeSeriesFilterSettings TsFilter => context.Mappers.TimeSeriesFilterSettings.ToModel(FrameworkEntity.TimeseriesFilterSettings);
}
