﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.SQL.TimeSeries;

class TimeSeriesViewColumnSettingsWrapper(TimeseriesViewColumnSettings entity) : ITimeSeriesViewColumnSettings, IFrameworkEntityContainer<TimeseriesViewColumnSettings>
{
    public TimeseriesViewColumnSettings FrameworkEntity => entity;

    public int ColumnWidth => entity.ColumnWidth;

    public TimeSeriesViewGridColumn GridColumn => (TimeSeriesViewGridColumn)entity.GridColumn;

    public bool IsMerged => entity.IsMerged;

    public bool IsVisible => entity.IsVisible;

    public SortMode SortMode => (SortMode)entity.SortMode;

    public int SortNr => entity.SortNr;

    public bool UseId => entity.UseId;
}