﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class HypothesisReader(ProcessingContext context)
    : ThrottledEntityReader<Hypothesis>(context)
{
    internal Task<Result<List<Hypothesis>>> Read(CancellationToken cancellationToken) =>
        ExecuteThrottled(() => MesapConnection.MesapDatabase.Hypotheses.ToList(), cancellationToken);

    protected override Hypothesis Read(string key)
    {
        MesapConnection.MesapDatabase.Hypotheses.TryGet(key, out var dim);
        return dim;
    }

    protected override Hypothesis Read(int primaryKey)
    {
        MesapConnection.MesapDatabase.Hypotheses.TryGet(primaryKey, out var dim);
        return dim;
    }

    protected override List<Hypothesis> Read(List<int> primaryKeys)
        => MesapConnection.MesapDatabase.Hypotheses.Get(primaryKeys);

    protected override List<Hypothesis> Read(List<string> keyList)
        => MesapConnection.MesapDatabase.Hypotheses.Get(keyList);
}