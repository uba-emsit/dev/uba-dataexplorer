﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

/// <summary>
/// Provides throttled read access to entities, including internal access to the framework reader via ExecuteThrottled.
/// </summary>
abstract class ThrottledEntityWriter<TFrameworkWriter, TEntity, TKey>(ProcessingContext context, Func<TFrameworkWriter> createWriter)
    : ThrottledEntityOperations(context)
{
    /// <summary>
    /// Allows internal access to the framework reader to execute throttled calls.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="execute"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal Task<Result<TResult>> ExecuteThrottled<TResult>(Func<TFrameworkWriter, TResult> execute, CancellationToken cancellationToken) =>
        Throttler.Execute(() => execute(createWriter()), MesapConnection, cancellationToken);

    /// <summary>
    /// The underlying framework writer.
    /// </summary>
    protected TFrameworkWriter Writer => createWriter();
}

/// <summary>
/// Provides throttled read access to entities.
/// </summary>
abstract class ThrottledEntityWriter<TEntity, TKey>(ProcessingContext context)
: ThrottledEntityOperations(context)
{
    /// <summary>
    /// Throttled write of a list of entities.
    /// </summary>
    /// <param name="entities"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<List<TEntity>>> Write(List<TEntity> entities, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Write(entities), MesapConnection, cancellationToken);

    /// <summary>
    /// Throttled write of an entity.
    /// </summary>
    /// <param name="entity"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<TEntity>> Write(TEntity entity, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Write(entity), MesapConnection, cancellationToken);

    protected abstract TEntity Write(TEntity entity);
    protected abstract List<TEntity> Write(List<TEntity> entities);
}