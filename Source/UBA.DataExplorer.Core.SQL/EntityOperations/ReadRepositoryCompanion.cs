﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

/// <summary>
/// Provides basic read-and-match operations for a repository.
/// </summary>
/// <typeparam name="TInterface"></typeparam>
/// <typeparam name="TEntity"></typeparam>
/// <typeparam name="TMesapModel"></typeparam>
/// <param name="reader"></param>
/// <param name="mapper"></param>
/// <param name="notFound"></param>
sealed class ReadRepositoryCompanion<TInterface, TEntity, TMesapModel, TKey>(
    ThrottledEntityReaderBase<TEntity, TKey> reader,
    Mapper<TInterface, TEntity, TMesapModel> mapper,
    Func<string, Error> notFound)
    : RepositoryCompanion<TInterface, TEntity, TMesapModel>(mapper, notFound)
    where TMesapModel : TInterface, IFrameworkEntityContainer<TEntity>
    where TInterface : IDomainModel
{
    public async Task<Result<TInterface>> Read(int primaryKey, CancellationToken cancellationToken)
        => Matching.Match(await reader.Read(primaryKey, cancellationToken), primaryKey);

    public async Task<Result<TInterface>> Read(TKey key, CancellationToken cancellationToken)
        => Matching.Match(await reader.Read(key, cancellationToken), key?.ToString() ?? "");

    public async Task<Result<List<TInterface>>> Read(List<int> primaryKeys, CancellationToken cancellationToken)
        => Matching.Match(await reader.Read(primaryKeys, cancellationToken));

    public async Task<Result<List<TInterface>>> Read(List<TKey> keyList, CancellationToken cancellationToken)
        => Matching.Match(await reader.Read(keyList, cancellationToken));
}