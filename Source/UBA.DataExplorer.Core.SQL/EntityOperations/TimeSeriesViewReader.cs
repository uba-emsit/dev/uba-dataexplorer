﻿using Mesap.Framework.DataAccess;
using Mesap.Framework.DataAccess.Tables.Internal;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class TimeSeriesViewReader(ProcessingContext context)
    : ThrottledEntityReader<TimeseriesViewReader, TimeseriesView>(context, () => new(context.MesapConnection.DatabaseUserContext))
{
    internal Task<Result<EntityTree<TimeseriesView, int>>> ReadTree(CancellationToken cancellationToken) =>
        ExecuteThrottled(() => Reader.ReadTree(), cancellationToken);

    protected override TimeseriesView Read(string id) => Reader.Read(id);
    protected override TimeseriesView Read(int primaryKey) => Reader.Read(primaryKey);
    protected override List<TimeseriesView> Read(List<int> primaryKeys) => Reader.Read(primaryKeys);
    protected override List<TimeseriesView> Read(List<string> idList) => Reader.Read(idList);
}