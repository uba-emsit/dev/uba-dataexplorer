﻿using UBA.DataExplorer.Core.SQL.Mapping;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class RepositoryCompanion<TInterface, TEntity, TMesapModel>(
    Mapper<TInterface, TEntity, TMesapModel> mapper,
    Func<string, Error> notFound)
    where TMesapModel : TInterface, IFrameworkEntityContainer<TEntity>
    where TInterface : IDomainModel
{
    internal readonly Matching<TInterface, TEntity, TMesapModel> Matching = new(mapper, notFound);
}