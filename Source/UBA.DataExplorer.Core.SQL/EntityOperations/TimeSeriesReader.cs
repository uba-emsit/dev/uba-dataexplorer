﻿using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class TimeSeriesReader(ProcessingContext context)
    : ThrottledEntityReader<TimeseriesReader, Timeseries>(context, () => new(context.MesapConnection.DatabaseUserContext))
{
    internal Task<Result<List<Timeseries>>> ReadFiltered(TimeseriesFilterSettings timeSeriesFilter, CancellationToken cancellationToken)
        => ExecuteThrottled(r => r.Read(timeSeriesFilter), cancellationToken);

    protected override Timeseries Read(string id) => Reader.Read(id);
    protected override Timeseries Read(int primaryKey) => Reader.Read(primaryKey);
    protected override List<Timeseries> Read(List<int> primaryKeys) => Reader.Read(primaryKeys);
    protected override List<Timeseries> Read(List<string> idList) => Reader.Read(idList);
}