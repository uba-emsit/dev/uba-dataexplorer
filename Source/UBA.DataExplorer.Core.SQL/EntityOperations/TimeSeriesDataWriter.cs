﻿using Mesap.Framework.Common;
using Mesap.Framework.DataAccess.BulkWriter;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;
using FWD = Mesap.Framework.DataAccess;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class TimeSeriesDataWriter(ProcessingContext context)
    : ThrottledEntityWriter<FWD.TimeseriesDataBulkWriter, TimeseriesData, string>(context, () => new(context.MesapConnection.DatabaseUserContext, DataWriterOrigin.DataSheet))
{
    internal Task<Result<IEnumerable<BulkEntityBulkWriterResult<BulkTimeseriesData>>>> Write(List<TimeseriesData> entities, CancellationToken cancellationToken) =>
        ExecuteThrottled(() =>
        {
            var bulkEntities = new List<BulkTimeseriesData>();
            bulkEntities.AddRange(entities.Select(entity => new BulkTimeseriesData
            {
                Timeseries = entity.Timeseries,
                Value = entity.Value.HasValue ? (ValueSpecification)entity.Value.Value : ValueSpecification.MarkAsDeletedIfExisting,
                Quality = entity.Quality.HasValue ? (QualitySpecification)entity.Quality.Value : QualitySpecification.None,
                ProtectionLevel = entity.ProtectionLevel.HasValue ? (ProtectionLevelSpecification)entity.ProtectionLevel.Value : ProtectionLevelSpecification.None,
                TimePeriod = (TimePeriodSpecification)entity.TimePeriod,
                Hypothesis = (HypothesisSpecification)entity.Hypothesis,
                GrowthRate = entity.GrowthRate.HasValue ? (GrowthRateSpecification)entity.GrowthRate.Value : GrowthRateSpecification.None,
                DataSource = (DataSourceSpecification)entity.DataSource
            }));

            return Writer.Write(bulkEntities);
        }, cancellationToken);
}
