﻿using Mesap.Framework;
using Mesap.Framework.Common;
using Mesap.Framework.DataAccess;
using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class TimeSeriesDataReader(ProcessingContext context)
    : ThrottledEntityReader<TimeseriesDataReader, TimeseriesData>(context, () => new(context.MesapConnection.DatabaseUserContext))
{
    internal Task<Result<List<TimeseriesData>>> Read(ICollection<Timeseries> tsEntities, Hypothesis hypothesis, TimeAxis timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken)
        => ExecuteThrottled(r => r.Read(tsEntities, hypothesis, timeAxis, timeResolution).ToList(), cancellationToken);

    internal Task<Result<List<MappedTimeseriesData>>> ReadMapped(ICollection<Timeseries> tsEntities, Hypothesis hypothesis, TimeAxis timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken)
        => ExecuteThrottled(r => r.ReadMapped(tsEntities, hypothesis, timeAxis, timeResolution).ToList(), cancellationToken);

    protected override TimeseriesData Read(string id) => throw new NotSupportedException();
    protected override TimeseriesData Read(int primaryKey) => throw new NotSupportedException();
    protected override List<TimeseriesData> Read(List<int> primaryKeys) => throw new NotSupportedException();
    protected override List<TimeseriesData> Read(List<string> idList) => throw new NotSupportedException();
}