﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

/// <summary>
/// Provides throttled read access to entities, including internal access to the framework reader via ExecuteThrottled.
/// </summary>
abstract class ThrottledEntityReader<TFrameworkReader, TEntity>(ProcessingContext context, Func<TFrameworkReader> createReader)
    : ThrottledEntityReader<TEntity>(context)
{
    /// <summary>
    /// Allows internal access to the framework reader to execute throttled calls.
    /// </summary>
    /// <typeparam name="TResult"></typeparam>
    /// <param name="execute"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal Task<Result<TResult>> ExecuteThrottled<TResult>(Func<TFrameworkReader, TResult> execute, CancellationToken cancellationToken) =>
        Throttler.Execute(() => execute(createReader()), MesapConnection, cancellationToken);

    /// <summary>
    /// The underlying framework reader.
    /// </summary>
    protected TFrameworkReader Reader => createReader();
}

/// <summary>
/// Provides throttled read access to entities where the entity key is a string.
/// </summary>
abstract class ThrottledEntityReader<TEntity>(ProcessingContext context) :
    ThrottledEntityReaderBase<TEntity, string>(context);

/// <summary>
/// Provides throttled read access to entities.
/// </summary>
abstract class ThrottledEntityReaderBase<TEntity, TKey>(ProcessingContext context)
: ThrottledEntityOperations(context)
{
    /// <summary>
    /// Throttled read of a list of entities by primary key.
    /// </summary>
    /// <param name="primaryKeys"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<List<TEntity>>> Read(List<int> primaryKeys, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Read(primaryKeys), MesapConnection, cancellationToken);

    /// <summary>
    /// Throttled read of a list of entities by key.
    /// </summary>
    /// <param name="keyList"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<List<TEntity>>> Read(List<TKey> keyList, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Read(keyList), MesapConnection, cancellationToken);

    /// <summary>
    /// Throttled read of an entity by primary key.
    /// </summary>
    /// <param name="primaryKey"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<TEntity>> Read(int primaryKey, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Read(primaryKey), MesapConnection, cancellationToken);

    /// <summary>
    /// Throttled read of an entity by key.
    /// </summary>
    /// <param name="key"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    internal async Task<Result<TEntity>> Read(TKey key, CancellationToken cancellationToken)
        => await Throttler.Execute(() => Read(key), MesapConnection, cancellationToken);

    protected abstract TEntity Read(int primaryKey);
    protected abstract TEntity Read(TKey key);
    protected abstract List<TEntity> Read(List<int> primaryKeys);
    protected abstract List<TEntity> Read(List<TKey> keyList);
}