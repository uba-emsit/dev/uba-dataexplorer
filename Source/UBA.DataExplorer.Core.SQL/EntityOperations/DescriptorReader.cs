﻿using Mesap.Framework.Collections;
using Mesap.Framework.DataAccess;
using UBA.DataExplorer.Domain.Shared;
using FWE = Mesap.Framework.Entities;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class DescriptorReader(ProcessingContext context)
    : ThrottledEntityReaderBase<FWE.Descriptor, DescriptorKey>(context)
{
    public Task<Result<List<FWE.Descriptor>>> ReadByDimension(int dimensionNr, CancellationToken cancellationToken)
        => ExecuteThrottled(() => MesapConnection.MesapDatabase.Dimensions.TryGet(dimensionNr, out var dimension)
                ? dimension.Descriptors(MesapConnection.DatabaseUserContext).ToList()
                : [],
            cancellationToken);

    protected override FWE.Descriptor Read(DescriptorKey key)
    {
        MesapConnection.MesapDatabase.Descriptors.TryGet(key, out var dim);
        return dim;
    }

    protected override FWE.Descriptor Read(int primaryKey)
    {
        MesapConnection.MesapDatabase.Descriptors.TryGet(primaryKey, out var dim);
        return dim;
    }

    protected override List<FWE.Descriptor> Read(List<int> primaryKeys)
        => MesapConnection.MesapDatabase.Descriptors.Get(primaryKeys);

    protected override List<FWE.Descriptor> Read(List<DescriptorKey> keyList)
        => MesapConnection.MesapDatabase.Descriptors.Get(keyList);
}