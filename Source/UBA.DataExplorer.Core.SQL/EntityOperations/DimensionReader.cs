﻿using Mesap.Framework.Entities;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

class DimensionReader(ProcessingContext context)
    : ThrottledEntityReader<Dimension>(context)
{
    internal Task<Result<List<Dimension>>> Read(CancellationToken cancellationToken) =>
        ExecuteThrottled(() => MesapConnection.MesapDatabase.Dimensions.ToList(), cancellationToken);

    protected override Dimension Read(string key)
    {
        MesapConnection.MesapDatabase.Dimensions.TryGet(key, out var dim);
        return dim;
    }

    protected override Dimension Read(int primaryKey)
    {
        MesapConnection.MesapDatabase.Dimensions.TryGet(primaryKey, out var dim);
        return dim;
    }

    protected override List<Dimension> Read(List<int> primaryKeys)
        => MesapConnection.MesapDatabase.Dimensions.Get(primaryKeys);

    protected override List<Dimension> Read(List<string> keyList)
        => MesapConnection.MesapDatabase.Dimensions.Get(keyList);
}