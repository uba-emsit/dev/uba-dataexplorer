﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.SQL.Connection;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.SQL.EntityOperations;

/// <summary>
/// Provides throttled access to the framework.
/// </summary>
abstract class ThrottledEntityOperations(ProcessingContext context)
{
    //protected readonly ProcessingContext Context = context;
    protected readonly FrameworkThrottler Throttler = context.RepositoryProvider.GetRequiredService<FrameworkThrottler>();
    protected readonly MesapConnection MesapConnection = context.MesapConnection;

    protected Task<Result<TResult>> ExecuteThrottled<TResult>(Func<TResult> execute, CancellationToken cancellationToken) =>
        Throttler.Execute(execute, MesapConnection, cancellationToken);
}