﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

class TimeSeriesViewByPrimaryKeysQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesViewByPrimaryKeysQuery, List<ITimeSeriesView>>
{
    public Task<Result<List<ITimeSeriesView>>> Handle(TimeSeriesViewByPrimaryKeysQuery request,
        CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesViewRepository>(request.Connection).Read(request.PrimaryKeys, cancellationToken);
}