﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

public record TimeSeriesViewByPrimaryKeysQuery(ConnectionModel Connection, List<int> PrimaryKeys) : IQuery<List<ITimeSeriesView>>;