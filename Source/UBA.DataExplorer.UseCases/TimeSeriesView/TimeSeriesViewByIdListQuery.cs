﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

public record TimeSeriesViewByIdListQuery(ConnectionModel Connection, List<string> TimeSeriesViewIdList) : IQuery<List<ITimeSeriesView>>;
