﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

class TimeSeriesViewTreeQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesViewTreeQuery, ITimeSeriesViewTree>
{
    public Task<Result<ITimeSeriesViewTree>> Handle(TimeSeriesViewTreeQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesViewRepository>(request.Connection).ReadTree(cancellationToken);
}