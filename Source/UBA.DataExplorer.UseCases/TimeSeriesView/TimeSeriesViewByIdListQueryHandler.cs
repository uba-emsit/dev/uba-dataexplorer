﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

class TimeSeriesViewByIdListQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<TimeSeriesViewByIdListQuery, List<ITimeSeriesView>>
{
    public Task<Result<List<ITimeSeriesView>>> Handle(TimeSeriesViewByIdListQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesViewRepository>(request.Connection).Read(request.TimeSeriesViewIdList, cancellationToken);
}
