﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeriesView;

class TimeSeriesViewByIdQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesViewByIdQuery, ITimeSeriesView>
{
    public Task<Result<ITimeSeriesView>> Handle(TimeSeriesViewByIdQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesViewRepository>(request.Connection).Read(request.TimeSeriesViewId, cancellationToken);
}
