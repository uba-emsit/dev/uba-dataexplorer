﻿using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

public record LoginWithWindowsAccountCommand : ICommand<LoginCommandResponse>;