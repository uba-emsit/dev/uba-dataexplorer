﻿using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

public record LoginCommand(string UserName, string Password) : ICommand<LoginCommandResponse>;