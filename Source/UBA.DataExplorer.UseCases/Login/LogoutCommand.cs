﻿using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

public record LogoutCommand(LoginModel LoginModel) : ICommand;