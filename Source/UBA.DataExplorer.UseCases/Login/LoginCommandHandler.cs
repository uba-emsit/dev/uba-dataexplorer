﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

class LoginCommandHandler(IRepositoryFactory factory)
    : ICommandHandler<LoginCommand, LoginCommandResponse>
{
    public async Task<Result<LoginCommandResponse>> Handle(LoginCommand request, CancellationToken cancellationToken)
    {
        var repository = factory.GetLoginRepository();
        var result = await repository.Login(request.UserName, request.Password, cancellationToken);
        return result.Map(l => new LoginCommandResponse(l));
    }
}