﻿using UBA.DataExplorer.Domain.Login;

namespace UBA.DataExplorer.UseCases.Login;

public record LoginCommandResponse(LoginModel LoginModel);