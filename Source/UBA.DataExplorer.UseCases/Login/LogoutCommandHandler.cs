﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

class LogoutCommandHandler(IRepositoryFactory factory)
    : ICommandHandler<LogoutCommand>
{
    public async Task<Result> Handle(LogoutCommand request, CancellationToken cancellationToken)
    {
        var repository = factory.GetLoginRepository();
        return await repository.Logout(request.LoginModel, cancellationToken);
    }
}