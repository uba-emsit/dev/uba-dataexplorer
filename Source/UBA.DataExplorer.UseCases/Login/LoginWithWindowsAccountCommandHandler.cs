﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Login;

class LoginWithWindowsAccountCommandHandler(IRepositoryFactory factory)
    : ICommandHandler<LoginWithWindowsAccountCommand, LoginCommandResponse>
{
    public async Task<Result<LoginCommandResponse>> Handle(LoginWithWindowsAccountCommand request, CancellationToken cancellationToken)
    {
        var repository = factory.GetLoginRepository();
        var result = await repository.LoginWithWindowsAccount(cancellationToken);
        return result.Map(l => new LoginCommandResponse(l));
    }
}