﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Descriptors;

class DescriptorsByDimensionQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<DescriptorsByDimensionQuery, List<IDescriptor>>
{
    public Task<Result<List<IDescriptor>>> Handle(DescriptorsByDimensionQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<IDescriptorRepository>(request.Connection).ReadByDimension(request.DimensionPk, cancellationToken);
}