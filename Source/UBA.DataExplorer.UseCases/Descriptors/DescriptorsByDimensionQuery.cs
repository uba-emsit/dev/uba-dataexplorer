﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Descriptors;

public record DescriptorsByDimensionQuery(ConnectionModel Connection, int DimensionPk) : IQuery<List<IDescriptor>>;