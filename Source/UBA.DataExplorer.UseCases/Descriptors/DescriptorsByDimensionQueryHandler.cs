﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Descriptors;

class DescriptorsByPrimaryKeysQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<DescriptorsByPrimaryKeysQuery, List<IDescriptor>>
{
    public Task<Result<List<IDescriptor>>> Handle(DescriptorsByPrimaryKeysQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<IDescriptorRepository>(request.Connection).Read(request.PrimaryKeys, cancellationToken);
}