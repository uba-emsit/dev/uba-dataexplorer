﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Descriptors;

public record DescriptorsByPrimaryKeysQuery(ConnectionModel Connection, List<int> PrimaryKeys) : IQuery<List<IDescriptor>>;