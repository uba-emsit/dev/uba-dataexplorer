﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Hypotheses;

class HypothesisAllQueryHandler(IRepositoryFactory factory) : IQueryHandler<HypothesisAllQuery, List<IHypothesis>>
{
    public Task<Result<List<IHypothesis>>> Handle(HypothesisAllQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<IHypothesisRepository>(request.Connection).Read(cancellationToken);
}