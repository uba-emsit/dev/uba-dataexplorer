﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Hypotheses;

public record HypothesisAllQuery(ConnectionModel Connection) : IQuery<List<IHypothesis>>;