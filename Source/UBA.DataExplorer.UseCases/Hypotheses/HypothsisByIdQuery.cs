﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Hypotheses;

public record HypothesisByIdQuery(ConnectionModel Connection, string Id) : IQuery<IHypothesis>;