﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Hypotheses;

class HypothesisByIdQueryHandler(IRepositoryFactory factory) : IQueryHandler<HypothesisByIdQuery, IHypothesis>
{
    public Task<Result<IHypothesis>> Handle(HypothesisByIdQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<IHypothesisRepository>(request.Connection).Read(request.Id, cancellationToken);
}