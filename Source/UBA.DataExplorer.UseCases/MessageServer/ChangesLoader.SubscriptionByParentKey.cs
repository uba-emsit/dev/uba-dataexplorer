﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public abstract class SubscriptionByParentKey<TParent>(
        Func<ObjectChangeReason, List<int>, Task> onChange,
        IEnumerable<TParent> objectsToMonitor,
        bool all,
        bool alsoNew)
        : Subscription([.. objectsToMonitor.Select(o => o.PrimaryKey)], all, alsoNew)
        where TParent : IDomainModel, IPrimaryKey
    {
        protected SubscriptionByParentKey(Func<ObjectChangeReason, List<int>, Task> onChange)
            : this(onChange, [], true, false) { }

        protected SubscriptionByParentKey(Func<ObjectChangeReason, List<int>, Task> onChange, IEnumerable<TParent> objectsToMonitor, bool alsoNew = false)
            : this(onChange, objectsToMonitor, false, alsoNew) { }

        Func<ObjectChangeReason, List<int>, Task> OnChange { get; } = onChange;

        public async override Task OnChangeSubscriptions(List<Subscription> baseSubscriptions, ObjectChangeReason reason, List<Change<long>> changes, ConnectionModel connectionModel,
            ISender sender)
        {
            var subscriptions = baseSubscriptions.OfType<SubscriptionByParentKey<TParent>>().ToList();
            var parentKeys = changes.Select(c => Convert.ToInt32(c.ParentKey)).ToList();
            var firstSubscription = subscriptions.FirstOrDefault();
            if (firstSubscription == null) return;
            await Task.WhenAll(subscriptions.Select(subscription =>
            {
                var relevantKeys =
                    subscription.All || reason == ObjectChangeReason.ObjectAddNew && subscription.AlsoNew
                        ? parentKeys
                        : parentKeys.Where(k => subscription.PrimaryKeys.Contains(k)).ToList();
                if (relevantKeys.Count == 0) return Task.CompletedTask;
                return subscription.OnChange(reason, relevantKeys);
            }));
        }
    }
}