﻿using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public class ObjectLocker(IMessageServer messageServer)
{
    IMessageServer _messageServer = messageServer;

    public LockResult Lock(MessageServerObjectClass objectClass, int primaryKey)
        => _messageServer.Lock(objectClass, primaryKey).ToDomain();

    public void SetMessageServer(IMessageServer newMessageServer)
        => _messageServer = newMessageServer;
}

public abstract record LockResult(ProcessingResult ProcessingResult, bool IsSuccess) : IDisposable
{
    public record Success(Action<LockResult> Unlock) : LockResult(ProcessingResult.Ok, true)
    {
        public override void Dispose() => Unlock(this);
    }

    public record Error(ProcessingResult ProcessingResult, string ErrorMessage) : LockResult(ProcessingResult, false);

    public record Locked(UserModel? OtherUser, DateTimeOffset LockedSince)
        : LockResult(ProcessingResult.Ok, false);

    public virtual void Dispose() { }
}

public static class LockResultExtension
{
    public static LockResult ToDomain(this Core.LockResult lockResult) => lockResult switch
    {
        Core.LockResult.Success success => new LockResult.Success(_ => success.Unlock(success)),
        Core.LockResult.Error error => new LockResult.Error((ProcessingResult)error.ProcessingResult, error.ErrorMessage),
        Core.LockResult.Locked locked => new LockResult.Locked(locked.OtherUser, locked.LockedSince),
        _ => new LockResult.Error(ProcessingResult.ErrorUnexpected, "Unexpected lock result")
    };
}

public enum ProcessingResult
{
    Ok,
    Failed,
    FailedAccessDenied,
    FailedInvalidOperation,
    FailedFormat,
    NotConnected,
    ErrorUnexpected,
    ErrorNotLicenced,
    ServerNotAvailable,
}