﻿using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public class TimeSeriesDataSubscription : SubscriptionByParentKey<ITimeSeries>
    {
        public TimeSeriesDataSubscription(Func<ObjectChangeReason, List<int>, Task> onChange)
            : base(onChange) { }

        public TimeSeriesDataSubscription(Func<ObjectChangeReason, List<int>, Task> onChange, IEnumerable<ITimeSeries> objectsToMonitor, bool alsoNew = false)
            : base(onChange, objectsToMonitor, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.TimeSeriesData;
    }
}