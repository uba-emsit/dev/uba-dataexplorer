﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Descriptors;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public class DescriptorSubscription : SubscriptionWithLoadByPrimaryKey<IDescriptor>
    {
        public DescriptorSubscription(Func<ObjectChangeReason, List<IDescriptor>, List<int>, Task> onChange)
            : base(onChange) { }

        public DescriptorSubscription(Func<ObjectChangeReason, List<IDescriptor>, List<int>, Task> onChange,
            IEnumerable<IDescriptor> objectsToMonitor, bool all = false, bool alsoNew = false)
            : base(onChange, objectsToMonitor, all, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.Descriptor;

        protected async override Task<List<IDescriptor>> LoadObjects(ISender sender,
            ConnectionModel connectionModel, List<int> primaryKeys)
        {
            var result = await sender.Send(new DescriptorsByPrimaryKeysQuery(connectionModel, primaryKeys));
            return result.IsFailure ? [] : result.Value;
        }
    }
}