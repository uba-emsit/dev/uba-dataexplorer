﻿using System.Collections.Immutable;
using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    class Connection : IDisposable
    {
        readonly IMessageServer _messageServer;
        readonly MessageServerObjectClass _objectClass;
        readonly Listener _listener;

        public Connection(IMessageServer messageServer, MessageServerObjectClass objectClass, ConnectionModel connectionModel,
            ISender sender)
        {
            _messageServer = messageServer;
            _objectClass = objectClass;
            _listener = new(objectClass, (reason, list) => OnChanged(reason, list, connectionModel, sender));
            messageServer.RegisterListener(_listener);
        }

        public ImmutableList<Subscription> Subscriptions { get; set; } = [];

        async Task OnChanged(Core.ObjectChangeReason reason, List<Change<long>> changes, ConnectionModel connectionModel,
            ISender sender)
            => await Task.WhenAll(Subscriptions
                .GroupBy(s => s.GetType())
                .Select(g =>
                    g.First().OnChangeSubscriptions(g.ToList(), (ObjectChangeReason)reason, changes, connectionModel, sender)));

        public void Dispose() => _messageServer.UnregisterListener(_listener);
        public override string ToString() => _objectClass.ToString();
    }
}