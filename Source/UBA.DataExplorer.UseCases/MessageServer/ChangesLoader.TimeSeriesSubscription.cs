﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.TimeSeries;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public class TimeSeriesSubscription : SubscriptionWithLoadByPrimaryKey<ITimeSeries>
    {
        public TimeSeriesSubscription(Func<ObjectChangeReason, List<ITimeSeries>, List<int>, Task> onChange)
            : base(onChange) { }

        public TimeSeriesSubscription(Func<ObjectChangeReason, List<ITimeSeries>, List<int>, Task> onChange, IEnumerable<ITimeSeries> objectsToMonitor, bool alsoNew = false)
            : base(onChange, objectsToMonitor, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.TimeSeries;

        protected async override Task<List<ITimeSeries>> LoadObjects(ISender sender, ConnectionModel connectionModel,
            List<int> primaryKeys)
        {
            var result = await sender.Send(new TimeSeriesByPrimaryKeysQuery(connectionModel, primaryKeys));
            return result.IsFailure ? [] : result.Value;
        }
    }

    public class TimeSeriesPropertySubscription : SubscriptionByParentKey<ITimeSeries>
    {
        public TimeSeriesPropertySubscription(Func<ObjectChangeReason, List<int>, Task> onChange)
            : base(onChange) { }

        public TimeSeriesPropertySubscription(Func<ObjectChangeReason, List<int>, Task> onChange, IEnumerable<ITimeSeries> objectsToMonitor, bool alsoNew = false)
            : base(onChange, objectsToMonitor, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.TimeSeriesProperty;
    }

    public class TimeSeriesAxisSubscription : SubscriptionByParentKey<ITimeSeries>
    {
        public TimeSeriesAxisSubscription(Func<ObjectChangeReason, List<int>, Task> onChange)
            : base(onChange) { }

        public TimeSeriesAxisSubscription(Func<ObjectChangeReason, List<int>, Task> onChange, IEnumerable<ITimeSeries> objectsToMonitor, bool alsoNew = false)
            : base(onChange, objectsToMonitor, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.TimeSeriesAxis;
    }
}