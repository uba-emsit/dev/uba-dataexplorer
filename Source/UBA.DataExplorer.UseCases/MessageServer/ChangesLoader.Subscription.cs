﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public abstract class Subscription(HashSet<int> primaryKeys, bool all, bool alsoNew)
        : IDisposable
    {
        internal ChangesLoader? ChangesLoader { get; set; }
        protected HashSet<int> PrimaryKeys { get; } = primaryKeys;
        protected bool All { get; } = all;
        protected bool AlsoNew { get; } = alsoNew;

        public abstract MessageServerObjectClass ObjectClass { get; }
        public abstract Task OnChangeSubscriptions(List<Subscription> subscriptions, ObjectChangeReason reason, List<Change<long>> changes, ConnectionModel connectionModel, ISender sender);

        public void Dispose() => ChangesLoader?.Unsubscribe(this);
        public override string ToString()
            => ObjectClass + " " + (All ? "All" : string.Join(",", PrimaryKeys.Take(10) + (PrimaryKeys.Count > 10 ? "..." : "") + (AlsoNew ? " AlsoNew" : "")));
    }
}