﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public abstract class SubscriptionWithLoadByPrimaryKey<TEntity>(
        Func<ObjectChangeReason, List<TEntity>, List<int>, Task> onChange,
        IEnumerable<TEntity> objectsToMonitor,
        bool all,
        bool alsoNew)
        : Subscription([.. objectsToMonitor.Select(o => o.PrimaryKey)], all, alsoNew)
        where TEntity : IDomainModel, IPrimaryKey
    {
        protected SubscriptionWithLoadByPrimaryKey(Func<ObjectChangeReason, List<TEntity>, List<int>, Task> onChange)
            : this(onChange, [], true, false) { }

        protected SubscriptionWithLoadByPrimaryKey(Func<ObjectChangeReason, List<TEntity>, List<int>, Task> onChange, IEnumerable<TEntity> objectsToMonitor, bool alsoNew = false)
            : this(onChange, objectsToMonitor, false, alsoNew) { }

        Func<ObjectChangeReason, List<TEntity>, List<int>, Task> OnChange { get; } = onChange;

        protected abstract Task<List<TEntity>> LoadObjects(ISender sender, ConnectionModel connectionModel, List<int> primaryKeys);

        public async override Task OnChangeSubscriptions(List<Subscription> baseSubscriptions, ObjectChangeReason reason, List<Change<long>> changes, ConnectionModel connectionModel,
            ISender sender)
        {
            var subscriptions = baseSubscriptions.OfType<SubscriptionWithLoadByPrimaryKey<TEntity>>().ToList();
            var primaryKeys = changes.Select(c => Convert.ToInt32(c.PrimaryKey)).ToList();
            var firstSubscription = subscriptions.FirstOrDefault();
            if (firstSubscription == null) return;
            var primaryKeysToLoad = reason == ObjectChangeReason.ObjectDelete
                ? []
                : subscriptions.Any(s => s.All) || reason == ObjectChangeReason.ObjectAddNew && subscriptions.Any(s => s.AlsoNew)
                    ? primaryKeys
                    : subscriptions.SelectMany(s => s.PrimaryKeys)
                        .Intersect(primaryKeys)
                        .Distinct()
                        .ToList();
            var objects = primaryKeysToLoad.Count == 0 ? [] : await firstSubscription.LoadObjects(sender, connectionModel, primaryKeysToLoad);
            await Task.WhenAll(subscriptions.Select(subscription =>
            {
                var relevantObjects =
                    subscription.All || reason == ObjectChangeReason.ObjectAddNew && subscription.AlsoNew
                        ? objects
                        : objects.Where(o => subscription.PrimaryKeys.Contains(o.PrimaryKey)).ToList();
                var relevantKeys =
                    reason != ObjectChangeReason.ObjectDelete
                        ? []
                        : subscription.All
                            ? primaryKeys
                            : primaryKeys.Where(k => subscription.PrimaryKeys.Contains(k)).ToList();
                if (relevantObjects.Count == 0 && relevantKeys.Count == 0) return Task.CompletedTask;
                return subscription.OnChange(reason, relevantObjects, relevantKeys);
            }));
        }
    }
}