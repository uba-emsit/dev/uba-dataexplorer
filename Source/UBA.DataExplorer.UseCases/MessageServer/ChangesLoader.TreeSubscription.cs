﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Trees;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public class TreeSubscription : SubscriptionWithLoadByPrimaryKey<ITree>
    {
        public TreeSubscription(Func<ObjectChangeReason, List<ITree>, List<int>, Task> onChange)
            : base(onChange) { }

        public TreeSubscription(Func<ObjectChangeReason, List<ITree>, List<int>, Task> onChange,
            IEnumerable<ITree> objectsToMonitor, bool all = false, bool alsoNew = false)
            : base(onChange, objectsToMonitor, all, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.Tree;

        protected async override Task<List<ITree>> LoadObjects(ISender sender,
            ConnectionModel connectionModel, List<int> primaryKeys)
        {
            var result = await sender.Send(new TreeByPrimaryKeysQuery(connectionModel, primaryKeys));
            return result.IsFailure ? [] : result.Value;
        }
    }
}