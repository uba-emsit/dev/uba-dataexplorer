﻿using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.TimeSeriesView;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader
{
    public class TimeSeriesViewSubscription : SubscriptionWithLoadByPrimaryKey<ITimeSeriesView>
    {
        public TimeSeriesViewSubscription(Func<ObjectChangeReason, List<ITimeSeriesView>, List<int>, Task> onChange)
            : base(onChange) { }

        public TimeSeriesViewSubscription(Func<ObjectChangeReason, List<ITimeSeriesView>, List<int>, Task> onChange,
            IEnumerable<ITimeSeriesView> objectsToMonitor, bool alsoNew = false)
            : base(onChange, objectsToMonitor, alsoNew) { }

        public override MessageServerObjectClass ObjectClass => MessageServerObjectClass.TimeSeriesView;

        protected async override Task<List<ITimeSeriesView>> LoadObjects(ISender sender,
            ConnectionModel connectionModel, List<int> primaryKeys)
        {
            var result = await sender.Send(new TimeSeriesViewByPrimaryKeysQuery(connectionModel, primaryKeys));
            return result.IsFailure ? [] : result.Value;
        }
    }
}