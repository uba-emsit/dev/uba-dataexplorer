﻿using System.Collections.Concurrent;
using MediatR;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.MessageServer;

public partial class ChangesLoader(ConnectionModel connectionModel, IMessageServer messageServer, ISender sender)
{
    readonly ConcurrentDictionary<MessageServerObjectClass, Connection> _connections = [];
    IMessageServer _messageServer = messageServer;

    public void Subscribe(Subscription subscription)
    {
        lock (_connections)
        {
            subscription.ChangesLoader = this;
            var connection = _connections.GetOrAdd(subscription.ObjectClass, o => new(_messageServer, o, connectionModel, sender));
            connection.Subscriptions = connection.Subscriptions.Add(subscription);
        }
    }

    public bool Unsubscribe(Subscription subscription)
    {
        lock (_connections)
        {
            if (!_connections.TryGetValue(subscription.ObjectClass, out var connection)) return false;
            if (!connection.Subscriptions.Contains(subscription)) return false;
            connection.Subscriptions = connection.Subscriptions.Remove(subscription);
            if (connection.Subscriptions.Count == 0)
            {
                _connections.Remove(subscription.ObjectClass, out _);
                connection.Dispose();
            }
            return true;
        }
    }

    public void SetMessageServer(IMessageServer messageServer)
    {
        lock (_connections)
        {
            var subscriptions = _connections.Values.SelectMany(c => c.Subscriptions).ToList();
            _connections.Clear();

            _messageServer = messageServer;

            foreach (var subscription in subscriptions)
                Subscribe(subscription);
        }
    }

    public enum ObjectChangeReason
    {
        ObjectChange = 1,
        ObjectAddNew = 2,
        ObjectDelete = 3
    }
}