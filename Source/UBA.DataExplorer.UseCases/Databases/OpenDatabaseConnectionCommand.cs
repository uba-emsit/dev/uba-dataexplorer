﻿using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Databases;

public record OpenDatabaseConnectionCommand(LoginModel Login, DatabaseModel Database)
    : ICommand<OpenDatabaseConnectionCommandResponse>;
