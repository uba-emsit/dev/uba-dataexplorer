﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.UseCases.Databases;

public record OpenDatabaseConnectionCommandResponse(
    ConnectionModel ConnectionModel,
    ChangesLoader ChangesLoader,
    ObjectLocker ObjectLocker);