﻿using MediatR;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;
using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.UseCases.Databases;

class OpenDatabaseConnectionCommandHandler(IRepositoryFactory factory, ISender sender)
    : ICommandHandler<OpenDatabaseConnectionCommand, OpenDatabaseConnectionCommandResponse>
{
    public async Task<Result<OpenDatabaseConnectionCommandResponse>> Handle(OpenDatabaseConnectionCommand request,
        CancellationToken cancellationToken)
    {
        var repository = factory.GetDatabaseRepository();
        var result = await repository.OpenConnection(request.Login, request.Database, cancellationToken);

        if (result.IsFailure)
            return Result.Failure<OpenDatabaseConnectionCommandResponse>(result.Error);

        var (connectionModel, messageServer) = result.Value;

        await repository.SaveConnectionToRecentlyUsed(connectionModel, cancellationToken);

        var changesLoader = new ChangesLoader(connectionModel, messageServer, sender);

        var objectLocker = new ObjectLocker(messageServer);

        return Result.Success(new OpenDatabaseConnectionCommandResponse(connectionModel, changesLoader, objectLocker));
    }
}