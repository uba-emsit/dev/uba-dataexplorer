﻿using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Databases;

class LoadAvailableProjectDatabasesQueryHandler(IRepositoryFactory factory, ILoggerFactory loggerFactory)
    : IQueryHandler<LoadAvailableProjectDatabasesQuery, List<DatabaseModel>>
{
    readonly ILogger _logger = loggerFactory.CreateLogger<LoadAvailableProjectDatabasesQueryHandler>();

    public async Task<Result<List<DatabaseModel>>> Handle(LoadAvailableProjectDatabasesQuery request,
        CancellationToken cancellationToken)
    {
        var repository = factory.GetDatabaseRepository();
        var all = await repository.GetAvailableProjectDatabases(request.Login, cancellationToken);

        if (all.IsSuccess)
            _logger.LogInformation("Loaded {Count} databases", all.Value.Count);

        return all;
    }
}