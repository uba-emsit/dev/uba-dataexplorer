﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Databases;

class CloseDatabaseConnectionCommandHandler(IRepositoryFactory factory)
    : ICommandHandler<CloseDatabaseConnectionCommand>
{
    public async Task<Result> Handle(CloseDatabaseConnectionCommand request,
        CancellationToken cancellationToken)
    {
        var repository = factory.GetDatabaseRepository();
        var result = await repository.CloseConnection(request.Connection, cancellationToken);

        if (result.IsSuccess)
        {
            // here we unregister the connection from the Message Server
        }

        if (result.IsFailure)
        {
            // _logger.LogError(all.Error.Message);
        }

        return result;
    }
}
