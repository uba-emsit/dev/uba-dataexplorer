﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Databases;

public record CloseDatabaseConnectionCommand(ConnectionModel Connection) : ICommand;
