﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Dimensions;

class DimensionsAllQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<DimensionsAllQuery, List<IDimension>>
{
    public Task<Result<List<IDimension>>> Handle(DimensionsAllQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<IDimensionRepository>(request.Connection).Read(cancellationToken);
}