﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Dimensions;

public record DimensionsAllQuery(ConnectionModel Connection) : IQuery<List<IDimension>>;