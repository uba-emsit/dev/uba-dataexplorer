﻿using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesDataWriteCommandHandler(IRepositoryFactory factory, ILoggerFactory loggerFactory)
    : ICommandHandler<TimeSeriesDataWriteCommand, List<WriteResponse<ITimeSeriesData>>>
{
    readonly ILogger _logger = loggerFactory.CreateLogger<TimeSeriesDataWriteCommandHandler>();

    public async Task<Result<List<WriteResponse<ITimeSeriesData>>>> Handle(
        TimeSeriesDataWriteCommand request, CancellationToken cancellationToken)
    {
        var repo = factory.GetRepository<ITimeSeriesDataRepository>(request.Connection);

        var response = await repo.Write(request.TimeSeriesDataList, cancellationToken);

        if (response.IsFailure)
        {
            _logger.LogError("Error while writing time series data: {Error}", response.Error.Description);
        }
        else if (response.Value.Any(r => !string.IsNullOrEmpty(r.ErrorText)))
        {
            var errorResponses = response.Value.Where(r => !string.IsNullOrEmpty(r.ErrorText)).ToList();
            var errorCount = errorResponses.Count;
            var errorMessages = string.Join(Environment.NewLine, errorResponses.Select(r => r.ErrorText));

            _logger.LogError("Error while writing time series data. Count: {ErrorCount}. Errors: {ErrorMessages}", errorCount, errorMessages);
        }

        return response;
    }
}