﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.UseCases.TimeSeries;

public record TimeSeriesByViewQueryResult(List<ITimeSeries> TimeSeries, List<IDimension> UsedDimensions);
