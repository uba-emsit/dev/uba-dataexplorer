﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

public record MappedTimeSeriesDataByTimeSeriesListQuery(ConnectionModel Connection, List<ITimeSeries> TimeSeriesList,
    string HypothesisId, DateTimeOffset From, DateTimeOffset To, TimeResolution TimeResolution)
    : IQuery<Dictionary<ITimeSeries, List<IMappedTimeSeriesData>>>;