﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesByIdListQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesByIdListQuery, List<ITimeSeries>>
{
    public Task<Result<List<ITimeSeries>>> Handle(TimeSeriesByIdListQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesRepository>(request.Connection).Read(request.TimeSeriesIdList, cancellationToken);
}