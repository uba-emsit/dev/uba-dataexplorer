﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesByViewQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesByViewQuery, TimeSeriesByViewQueryResult>
{
    public async Task<Result<TimeSeriesByViewQueryResult>> Handle(TimeSeriesByViewQuery request, CancellationToken cancellationToken)
    {
        var tsRepository = factory.GetRepository<ITimeSeriesRepository>(request.Connection);
        var tsResult = await tsRepository.ReadFromViewId(request.TimeSeriesViewId, cancellationToken);

        if (tsResult.IsFailure)
            return Result.Failure<TimeSeriesByViewQueryResult>(tsResult.Error);

        var usedDimensions = tsResult.Value
            .SelectMany(ts => ts.TimeSeriesKeys.Select(tsk => tsk.Descriptor))
            .Distinct()
            .Select(d => d.Dimension)
            .Distinct()
            .ToList();

        return Result.Success(new TimeSeriesByViewQueryResult(tsResult.Value, usedDimensions));
    }
}
