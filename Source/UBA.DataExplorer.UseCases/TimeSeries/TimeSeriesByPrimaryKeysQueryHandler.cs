﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesByPrimaryKeysQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesByPrimaryKeysQuery, List<ITimeSeries>>
{
    public Task<Result<List<ITimeSeries>>> Handle(TimeSeriesByPrimaryKeysQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesRepository>(request.Connection).Read(request.PrimaryKeys, cancellationToken);
}