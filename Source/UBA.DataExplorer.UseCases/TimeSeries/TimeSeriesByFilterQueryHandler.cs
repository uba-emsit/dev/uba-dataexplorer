﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesByFilterQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesByFilterQuery, List<ITimeSeries>>
{
    public Task<Result<List<ITimeSeries>>> Handle(TimeSeriesByFilterQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesRepository>(request.Connection).ReadFromFilter(request.TimeSeriesFilter, cancellationToken);
}
