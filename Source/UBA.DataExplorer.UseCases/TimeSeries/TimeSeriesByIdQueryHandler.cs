﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesByIdQueryHandler(IRepositoryFactory factory) : IQueryHandler<TimeSeriesByIdQuery, ITimeSeries>
{
    public Task<Result<ITimeSeries>> Handle(TimeSeriesByIdQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITimeSeriesRepository>(request.Connection).Read(request.TimeSeriesId, cancellationToken);
}
