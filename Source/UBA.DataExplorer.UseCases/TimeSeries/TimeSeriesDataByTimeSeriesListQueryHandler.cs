﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

class TimeSeriesDataByTimeSeriesListQueryHandler(IRepositoryFactory factory, IServiceFactory serviceFactory)
    : IQueryHandler<TimeSeriesDataByTimeSeriesListQuery, Dictionary<ITimeSeries, List<ITimeSeriesData>>>
{
    public async Task<Result<Dictionary<ITimeSeries, List<ITimeSeriesData>>>> Handle(TimeSeriesDataByTimeSeriesListQuery request,
        CancellationToken cancellationToken)
    {
        var hypothesisRepository = factory.GetRepository<IHypothesisRepository>(request.Connection);
        var hypoReadResult = await hypothesisRepository.Read(request.HypothesisId, cancellationToken);

        if (hypoReadResult.IsFailure)
            return Result.Failure<Dictionary<ITimeSeries, List<ITimeSeriesData>>>(hypoReadResult.Error);

        var hypothesis = hypoReadResult.Value;

        var timeAxisService = serviceFactory.GetService<ITimeAxisService>(request.Connection);
        var axis = timeAxisService.CreateAbsolute(request.From, request.To);

        var timeSeriesDataRepository = factory.GetRepository<ITimeSeriesDataRepository>(request.Connection);
        var dataResult = await timeSeriesDataRepository.Read(request.TimeSeriesList, hypothesis,
            axis, request.TimeResolution, cancellationToken);

        if (dataResult.IsFailure)
            return Result.Failure<Dictionary<ITimeSeries, List<ITimeSeriesData>>>(dataResult.Error);

        return dataResult.Map(x => request.TimeSeriesList
            .ToDictionary(ts => ts, ts => x
                    .Where(tsd => tsd.TimeSeriesPrimaryKey == ts.PrimaryKey)
                    .ToList()));
    }
}