﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

public record TimeSeriesDataWriteCommand(ConnectionModel Connection, List<ITimeSeriesData> TimeSeriesDataList)
    : ICommand<List<WriteResponse<ITimeSeriesData>>>;