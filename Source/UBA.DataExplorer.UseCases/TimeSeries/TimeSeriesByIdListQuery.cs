﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

public record TimeSeriesByIdListQuery(ConnectionModel Connection, List<string> TimeSeriesIdList) : IQuery<List<ITimeSeries>>;