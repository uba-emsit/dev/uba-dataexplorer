﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.TimeSeries;

public record TimeSeriesByViewQuery(ConnectionModel Connection, string TimeSeriesViewId) : IQuery<TimeSeriesByViewQueryResult>;
