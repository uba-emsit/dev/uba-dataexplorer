﻿using MediatR;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Abstractions;

public interface ICommandHandler<in TCommand>
    : IRequestHandler<TCommand, Result>
    where TCommand : ICommand;

public interface ICommandHandler<in TCommand, TResponse>
    : IRequestHandler<TCommand, Result<TResponse>>
    where TCommand : ICommand<TResponse>;
