﻿using MediatR;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Abstractions;

public interface IQuery : IRequest<Result>;

public interface IQuery<TResponse> : IRequest<Result<TResponse>>;
