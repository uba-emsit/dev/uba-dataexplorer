﻿using MediatR;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Abstractions;

public interface IQueryHandler<in TQuery, TResponse> : IRequestHandler<TQuery, Result<TResponse>>
    where TQuery : IQuery<TResponse>;

public interface IQueryHandler<in TQuery> : IRequestHandler<TQuery, Result>
    where TQuery : IQuery;
