﻿using System.Diagnostics;
using MediatR;
using Microsoft.Extensions.Logging;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Abstractions.Logging;

public sealed class RequestLoggingPipelineBehavior<TRequest, TResponse>(
    ILogger<RequestLoggingPipelineBehavior<TRequest, TResponse>> logger)
    : IPipelineBehavior<TRequest, TResponse>
    where TRequest : class
    where TResponse : Result
{
    public async Task<TResponse> Handle(TRequest request, RequestHandlerDelegate<TResponse> next, CancellationToken cancellationToken)
    {
        var requestName = typeof(TRequest).Name;

        var timer = new Stopwatch();
        timer.Start();

        logger.LogInformation("[{RequestName}] processing", requestName);

        var result = await next().ConfigureAwait(false);

        timer.Stop();

        if (result.IsSuccess)
            logger.LogInformation("[{RequestName}] completed in {duration} ms", requestName, timer.Elapsed.TotalMilliseconds);
        else
            logger.LogError(result.Error.Exception, "[{RequestName}] completed with error {code} {title} ({description})",
                requestName, result.Error.Code, result.Error.Title, result.Error.Description);

        return result;
    }
}