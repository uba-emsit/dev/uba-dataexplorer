﻿using MediatR;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Abstractions;

public interface ICommand : IRequest<Result>, IBaseCommand;

public interface ICommand<TResponse> : IRequest<Result<TResponse>>, IBaseCommand;

public interface IBaseCommand;
