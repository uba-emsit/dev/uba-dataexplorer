﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

class TreeByDimensionQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<TreeByDimensionQuery, List<ITree>>
{
    public Task<Result<List<ITree>>> Handle(TreeByDimensionQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITreeRepository>(request.Connection).ReadByDimension(request.DimensionNr, cancellationToken);
}