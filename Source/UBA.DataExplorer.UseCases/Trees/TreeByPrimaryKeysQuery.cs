﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

public record TreeByPrimaryKeysQuery(ConnectionModel Connection, List<int> PrimaryKeys) : IQuery<List<ITree>>;