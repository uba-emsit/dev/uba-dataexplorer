﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

public record TreeAllQuery(ConnectionModel Connection) : IQuery<List<ITree>>;