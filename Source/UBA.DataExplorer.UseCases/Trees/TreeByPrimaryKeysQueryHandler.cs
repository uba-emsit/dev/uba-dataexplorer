﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

class TreeByPrimaryKeysQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<TreeByPrimaryKeysQuery, List<ITree>>
{
    public Task<Result<List<ITree>>> Handle(TreeByPrimaryKeysQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITreeRepository>(request.Connection).Read(request.PrimaryKeys, cancellationToken);
}