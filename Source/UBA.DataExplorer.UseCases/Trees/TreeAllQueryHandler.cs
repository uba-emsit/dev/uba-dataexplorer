﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

class TreeAllQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<TreeAllQuery, List<ITree>>
{
    public Task<Result<List<ITree>>> Handle(TreeAllQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITreeRepository>(request.Connection).ReadAll(cancellationToken);
}