﻿using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UseCases.Abstractions;

namespace UBA.DataExplorer.UseCases.Trees;

class TreeByPrimaryKeyQueryHandler(IRepositoryFactory factory)
    : IQueryHandler<TreeByPrimaryKeyQuery, ITree>
{
    public Task<Result<ITree>> Handle(TreeByPrimaryKeyQuery request, CancellationToken cancellationToken)
        => factory.GetRepository<ITreeRepository>(request.Connection).Read(request.PrimaryKey, cancellationToken);
}