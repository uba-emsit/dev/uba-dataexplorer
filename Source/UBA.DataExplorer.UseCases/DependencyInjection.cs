﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.UseCases.Abstractions.Logging;

namespace UBA.DataExplorer.UseCases;

public static class DependencyInjection
{
    public static IServiceCollection AddUseCases(this IServiceCollection services)
    {
        return services
            .AddLogging()
            .AddMediatR(config =>
            {
                config.RegisterServicesFromAssembly(typeof(DependencyInjection).Assembly);
                config.AddOpenBehavior(typeof(RequestLoggingPipelineBehavior<,>));
            });
    }
}