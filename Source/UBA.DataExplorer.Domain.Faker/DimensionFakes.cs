﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for IDimension. Use it in the design mode of views or in unit tests.
/// </summary>
public class DimensionFakes
{
    private static Faker<IDimension> GetFaker()
    {
        return new Faker<IDimension>()
            .CustomInstantiator(f =>
            {
                var mockDimension = new Mock<IDimension>();

                mockDimension.SetupProperty(p => p.Id, f.Address.Country().ToString());
                mockDimension.SetupProperty(p => p.Name, f.Address.Country().ToString());
                mockDimension.SetupProperty(p => p.Description, f.Lorem.Sentence());

                mockDimension.SetupGet(p => p.PrimaryKey).Returns(f.Random.Int(0, 100));
                mockDimension.SetupGet(p => p.AuditInfo).Returns(Mock.Of<IAuditInfo>());

                return mockDimension.Object;
            });
    }

    public IDimension Get()
    {
        return GetFaker().Generate();
    }

    public List<IDimension> GetList(int count)
    {
        return GetFaker().Generate(count);
    }
}
