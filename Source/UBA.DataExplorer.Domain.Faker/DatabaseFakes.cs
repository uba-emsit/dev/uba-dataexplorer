﻿using AutoBogus;
using AutoBogus.Conventions;
using UBA.DataExplorer.Domain.Databases;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for the DatabaseModel class. Use it in the design mode of views or in unit tests.
/// </summary>
public class DatabaseFakes
{
    public List<DatabaseModel> GetList(int count)
    {
        AutoFaker.Configure(builder =>
        {
            builder.WithConventions();
        });

        return AutoFaker.Generate<DatabaseModel>(count);
    }
}
