using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesViewColumnSettings. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesViewColumnSettingsFaker : Faker<ITimeSeriesViewColumnSettings>
{
    public TimeSeriesViewColumnSettingsFaker()
    {
        CustomInstantiator(f =>
        {
            var mock = new Mock<ITimeSeriesViewColumnSettings>();
            mock.SetupGet(m => m.ColumnWidth).Returns(f.Random.Int(1, 100));
            mock.SetupGet(m => m.GridColumn).Returns(f.PickRandom<TimeSeriesViewGridColumn>());
            mock.SetupGet(m => m.IsMerged).Returns(f.Random.Bool());
            mock.SetupGet(m => m.IsVisible).Returns(f.Random.Bool());
            mock.SetupGet(m => m.SortMode).Returns(f.PickRandom<SortMode>());
            mock.SetupGet(m => m.UseId).Returns(f.Random.Bool());
            mock.SetupGet(m => m.SortNr).Returns(f.Random.Int(1, 100));

            return mock.Object;
        });
    }
}
