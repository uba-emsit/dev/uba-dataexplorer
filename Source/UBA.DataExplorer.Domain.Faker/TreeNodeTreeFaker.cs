﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

public class TreeNodeTreeFaker : Faker<ITreeNodeTree>
{
    public TreeNodeTreeFaker()
    {
        CustomInstantiator(f =>
        {
            var mockTreeNodeTree = new Mock<ITreeNodeTree>();
            mockTreeNodeTree.SetupAllProperties();
            mockTreeNodeTree.Setup(t => t.RootNode).Returns(new EntityTreeNodeFaker<ITreeNodeNode>(new TreeNodeNodeFaker()).Generate());
            return mockTreeNodeTree.Object;
        });

        RuleFor(t => t.Count, f => 2);
    }
}

