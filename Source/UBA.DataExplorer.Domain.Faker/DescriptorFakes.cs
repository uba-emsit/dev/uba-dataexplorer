﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for IDescriptor. Use it in the design mode of views or in unit tests.
/// </summary>
public class DescriptorFakes
{
    private static Faker<IDescriptor> GetDescriptorFaker()
    {
        return new Faker<IDescriptor>()
            .CustomInstantiator(f =>
            {
                var mockDescriptor = new Mock<IDescriptor>();

                mockDescriptor.SetupGet(p => p.PrimaryKey).Returns(f.Random.Int(1, 1000));
                mockDescriptor.SetupGet(p => p.Key).Returns(Mock.Of<IDescriptorKey>());
                mockDescriptor.SetupGet(p => p.Id).Returns(f.Address.CountryCode().ToString());
                mockDescriptor.SetupProperty(p => p.Label, f.Address.Country().ToString());
                mockDescriptor.SetupProperty(p => p.Description, f.Lorem.Sentence());
                mockDescriptor.SetupGet(p => p.IconNr).Returns(f.Random.Int(0, 100));
                mockDescriptor.SetupGet(p => p.SortNr).Returns(f.Random.Int(0, 100));
                mockDescriptor.SetupGet(p => p.Dimension).Returns(Mock.Of<IDimension>());
                mockDescriptor.SetupGet(p => p.AuditInfo).Returns(Mock.Of<IAuditInfo>());

                mockDescriptor.Setup(d => d.ToString()).Returns(mockDescriptor.Object.Label);
                return mockDescriptor.Object;
            });
    }

    public IDescriptor Get()
    {
        var descriptorFaker = GetDescriptorFaker();
        return descriptorFaker.Generate();
    }

    public List<IDescriptor> GetList(int count)
    {
        var descriptorFaker = GetDescriptorFaker();
        return descriptorFaker.Generate(count);
    }
}
