﻿using System.Collections.ObjectModel;
using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for IEntityTreeNode<ITimeSeriesView>. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesViewTreeViewFakes(int maxDepth = 3)
{
    public IEntityTreeNode<ITimeSeriesView> Get()
    {
        return GetInternal(0);
    }

    public IEntityTreeNode<ITimeSeriesView> GetInternal(int currentDepth)
    {
        var faker = new Faker<IEntityTreeNode<ITimeSeriesView>>()
            .CustomInstantiator(f =>
            {
                var mock = new Mock<IEntityTreeNode<ITimeSeriesView>>();
                var fakeEntity = new TimeSeriesViewFakes().Get(currentDepth != 0 && currentDepth >= maxDepth);
                mock.Setup(m => m.Entity).Returns(fakeEntity);

                var tree = Mock.Of<IEntityTreeNode<ITimeSeriesView>>();
                mock.Setup(m => m.Level).Returns(currentDepth);

                var children = new List<IEntityTreeNode<ITimeSeriesView>>();

                if (currentDepth < maxDepth)
                {
                    var childrenCount = f.Random.Int(1, 3);
                    for (var i = 0; i < childrenCount; i++)
                    {
                        children.Add(GetInternal(currentDepth + 1));
                    }
                }

                mock.Setup(m => m.Children).Returns(new ReadOnlyCollection<IEntityTreeNode<ITimeSeriesView>>(children));

                return mock.Object;
            });

        return faker.Generate();
    }
}