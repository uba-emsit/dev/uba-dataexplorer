using Bogus;
using Moq;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITreeNodeNode. Use it in the design mode of views or in unit tests.
/// </summary>
public class TreeNodeNodeFaker : Faker<ITreeNodeNode>
{
    public TreeNodeNodeFaker()
    {
        CustomInstantiator(f =>
        {
            var mockNode = new Mock<ITreeNodeNode>();
            mockNode.SetupAllProperties();
            mockNode.SetupGet(t => t.Descriptor).Returns(new DescriptorFakes().Get());
            return mockNode.Object;
        });

        RuleFor(t => t.Children, f => new List<IEntityTreeNode<ITreeNodeNode>>().AsReadOnly());
        RuleFor(t => t.Level, f => f.Random.Int(0, 5));
    }
}

