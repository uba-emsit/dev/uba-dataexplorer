﻿namespace UBA.DataExplorer.Domain.Faker;

public class UnitFakes
{
    private readonly Bogus.Faker _faker = new();

    public static string[] SiUnits = new string[]
    {
        "m", "kg", "s", "A", "K", "mol", "cd", "N", "Pa", "J", "W", "V", "C", "F", "Ω", "S", "H", "T", "Bq", "Sv", "Gy", "lx", "m/s", "m²", "m³", "g", "Hz"
    };

    public string Get()
    {
        return SiUnits[_faker.Random.Int(0, SiUnits.Length - 1)];
    }
}