using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesFilterSettings. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesFilterSettingsFaker : Faker<ITimeSeriesFilterSettings>
{
    public TimeSeriesFilterSettingsFaker()
    {
        CustomInstantiator(f =>
        {
            var mock = new Mock<ITimeSeriesFilterSettings>();

            var dataFilterMock = new Mock<ITimeSeriesDataFilterSettings>();
            dataFilterMock.SetupGet(m => m.Criterions).Returns(new List<ITimeSeriesDataFilterCriterionSettings>());

            var descriptorFilterMock = new Mock<IDescriptorFilterSettings>();
            descriptorFilterMock.SetupGet(m => m.FilterMode).Returns(f.PickRandom<IDescriptorFilterSettings.DescriptorFilterMode>());
            descriptorFilterMock.SetupGet(m => m.DescriptorPrimaryKeys).Returns(new List<int>());
            descriptorFilterMock.SetupGet(m => m.DimensionPrimaryKey).Returns(f.Random.Int(1, 100));
            descriptorFilterMock.SetupGet(m => m.TreeFilterMode).Returns(f.PickRandom<TreeFilterMode>());
            descriptorFilterMock.SetupGet(m => m.IsDescriptorFilterPlaceholder).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.IsEnabled).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.IsNotEmpty).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.UseMultipleAllocationOpen).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.UseMultipleAllocationExclusive).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.UseMultipleAllocationNone).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.OrIsEmpty).Returns(f.Random.Bool());
            descriptorFilterMock.SetupGet(m => m.TreeNodePrimaryKeys).Returns(new List<int>());
            descriptorFilterMock.SetupGet(m => m.TreePrimaryKey).Returns(f.Random.Int(-1, 100));

            mock.SetupGet(m => m.DataFilter).Returns(dataFilterMock.Object);
            mock.SetupGet(m => m.DescriptorFilters).Returns(new List<IDescriptorFilterSettings> { descriptorFilterMock.Object });
            mock.SetupGet(m => m.IgnoreMultipleAllocation).Returns(f.Random.Bool());
            mock.SetupGet(m => m.TimeSeriesPrimaryKeys).Returns(new List<int>());
            mock.SetupGet(m => m.TimeZoneId).Returns("Europe/Berlin");
            mock.SetupGet(m => m.FilterUsage).Returns(f.PickRandom<TimeSeriesFilterUsage>());
            mock.SetupGet(m => m.UseTimeZoneFilter).Returns(f.Random.Bool());

            return mock.Object;
        });
    }
}
