using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesData. Use it in the design mode of views or in unit tests.
/// </summary>
public sealed class TimeSeriesDataFaker : Faker<ITimeSeriesData>
{
    public TimeSeriesDataFaker()
    {
        CustomInstantiator(f =>
        {
            var mock = new Mock<ITimeSeriesData>();

            var coordinatesFaker = new Faker<ITimeSeriesDataCoordinates>()
                .CustomInstantiator(_ =>
                {
                    var mockCoordinates = new Mock<ITimeSeriesDataCoordinates>();
                    mockCoordinates.SetupGet(c => c.TimePeriod1).Returns(new TimePeriodFaker().Generate());
                    return mockCoordinates.Object;
                });

            var hypothesisFaker = new Faker<IHypothesis>()
                .CustomInstantiator(fh =>
                {
                    var mockHypothesis = new Mock<IHypothesis>();
                    mockHypothesis.SetupGet(h => h.Id).Returns(fh.Random.String2(10));
                    mockHypothesis.SetupGet(h => h.Name).Returns(fh.Random.Word());
                    mockHypothesis.SetupGet(h => h.Description).Returns(fh.Random.Words(5));
                    return mockHypothesis.Object;
                });

            mock.SetupGet(m => m.ChangedDate).Returns(f.Date.Past());
            mock.SetupGet(m => m.ChangedUserId).Returns(f.Name.FullName());
            mock.SetupProperty(m => m.DataSource, f.Company.CompanyName());
            mock.SetupProperty(m => m.IsWriteProtected, f.Random.Bool());
            mock.SetupProperty(m => m.NoValueReason, f.PickRandom(null, null, null, null, "Delete", "Error"));
            mock.SetupProperty(m => m.ProtectionLevel, f.Random.Int(0, 5));
            mock.SetupProperty(m => m.Quality, f.Random.Int(0, 100));
            mock.SetupGet(m => m.HasDocumentation).Returns(f.Random.Bool());
            mock.SetupGet(m => m.TimeSeriesPrimaryKey).Returns(f.Random.Int(1, 1000));
            mock.SetupGet(m => m.Coordinates).Returns(coordinatesFaker.Generate());
            mock.SetupProperty(m => m.Value, f.Random.Double(0, 1000));
            mock.SetupGet(m => m.Hypothesis).Returns(hypothesisFaker.Generate());
            mock.SetupGet(m => m.TimeResolution).Returns(f.PickRandom<TimeResolution>());

            return mock.Object;
        });
    }
}