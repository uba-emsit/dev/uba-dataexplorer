﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesView. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesViewFakes
{
    public ITimeSeriesView Get(bool isLeaf = true)
    {
        var personFaker = new Faker<ITimeSeriesView>()
            .CustomInstantiator(f =>
            {
                var tsv = Mock.Of<ITimeSeriesView>();
                var mock = Mock.Get(tsv);

                mock.SetupGet(p => p.PrimaryKey).Returns(f.Random.Number());
                mock.SetupGet(p => p.Name).Returns(f.Name.JobTitle());
                mock.SetupGet(p => p.Description).Returns(f.Name.JobDescriptor());
                mock.SetupGet(p => p.IsLeaf).Returns(isLeaf);
                mock.SetupGet(p => p.TsFilter).Returns(new TimeSeriesFilterSettingsFaker().Generate());
                mock.SetupGet(p => p.Settings).Returns(new TimeSeriesViewSettingsFaker().Generate());
                mock.SetupGet(p => p.AuditInfo).Returns(new AuditInfoFaker().Generate());

                return tsv;
            });

        return personFaker.Generate();
    }
}