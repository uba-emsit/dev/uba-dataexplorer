﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesViewSettings. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesViewSettingsFaker : Faker<ITimeSeriesViewSettings>
{
    public TimeSeriesViewSettingsFaker()
    {
        CustomInstantiator(f =>
        {
            var mockSettings = new Mock<ITimeSeriesViewSettings>();

            mockSettings.Setup(s => s.AggregationAllowIncomplete).Returns(f.PickRandom<BooleanTriState>());
            mockSettings.Setup(s => s.AggregationCalendar).Returns(f.PickRandom<AggregationCalendar>());
            mockSettings.Setup(s => s.AggregationMappingMode).Returns(f.PickRandom<AggregationMappingMode>());
            mockSettings.Setup(s => s.AggregationRule).Returns(f.PickRandom<AggregationRule>());
            mockSettings.Setup(s => s.AlignmentOfData).Returns(f.PickRandom<CellAlignment>());
            mockSettings.Setup(s => s.AlignmentOfDescriptors).Returns(f.PickRandom<CellAlignment>());
            mockSettings.Setup(s => s.AllowInheritance).Returns(f.PickRandom<BooleanTriState>());
            mockSettings.Setup(s => s.Columns).Returns(new TimeSeriesViewColumnSettingsFaker().Generate(3));
            mockSettings.Setup(s => s.ColumnWidthAuto).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.ColumnWidthOfValues).Returns(f.Random.Int(1, 100));
            mockSettings.Setup(s => s.ColumnWidthPortraitColumn1).Returns(f.Random.Int(1, 100));
            mockSettings.Setup(s => s.ColumnWidthPortraitColumns).Returns(f.Random.Int(1, 100));
            mockSettings.Setup(s => s.DataMode).Returns(f.PickRandom<TimeseriesViewGridDataMode>());
            mockSettings.Setup(s => s.DimensionModeAuto).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.DisAggregationRule).Returns(f.PickRandom<DisaggregationRule>());
            mockSettings.Setup(s => s.DispDecimalPlaces).Returns(f.Random.Int(0, 10));
            mockSettings.Setup(s => s.DisplayFormat).Returns(f.PickRandom<NumberFormat>());
            mockSettings.Setup(s => s.DisplayUnit).Returns(f.Random.Word());
            mockSettings.Setup(s => s.ExpandedTimeSeriesPrimaryKeys).Returns(new List<int>());
            mockSettings.Setup(s => s.ExtrapolationRule).Returns(f.PickRandom<ExtrapolationRule>());
            mockSettings.Setup(s => s.FreezePosition).Returns(f.Random.Int(0, 10));
            mockSettings.Setup(s => s.HypothesesPrimaryKeys).Returns(new List<int>());
            mockSettings.Setup(s => s.InterpolationGradient).Returns(f.Random.Double(0, 1));
            mockSettings.Setup(s => s.InterpolationRule).Returns(f.PickRandom<InterpolationRule>());
            mockSettings.Setup(s => s.IsConstOMP).Returns(f.PickRandom<BooleanTriState>());
            mockSettings.Setup(s => s.RowAuto).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.RowAutoAffectsFilteredValues).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.RowHeightAuto).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.RowHeightFixCount).Returns(f.Random.Int(1, 100));
            mockSettings.Setup(s => s.SubtotalsFunction).Returns(f.Random.Int(0, 10));
            mockSettings.Setup(s => s.SubtotalsOn).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.SubtotalsUpToColumn).Returns(f.Random.Int(0, 10));
            mockSettings.Setup(s => s.MainTimeResolution).Returns(f.PickRandom<TimeResolution>());
            mockSettings.Setup(s => s.TimeResolutions).Returns(new List<TimeResolution>());
            mockSettings.Setup(s => s.UseListSortOrder).Returns(f.Random.Bool());
            mockSettings.Setup(s => s.ViewTimezoneId).Returns("Europe/Berlin");

            return mockSettings.Object;
        });
    }
}


