﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for IDescriptorFilterSettings. Use it in the design mode of views or in unit tests.
/// </summary>
public class DescriptorFilterSettingsFaker : Faker<IDescriptorFilterSettings>
{
    public DescriptorFilterSettingsFaker()
    {
        CustomInstantiator(f =>
        {
            var mockDescriptor = new Mock<IDescriptorFilterSettings>();

            mockDescriptor.SetupGet(p => p.DimensionPrimaryKey).Returns(f.Random.Int(1, 1000));
            mockDescriptor.SetupGet(p => p.DescriptorPrimaryKeys)
                .Returns(Enumerable.Range(1, 10).Select(_ => f.Random.Int(1)).ToList());

            return mockDescriptor.Object;
        });
    }
}
