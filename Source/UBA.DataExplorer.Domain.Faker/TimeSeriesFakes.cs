﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeries. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesFakes
{
    private static Faker<ITimeSeries> GetTimeSeriesFaker()
    {
        return new Faker<ITimeSeries>()
            .CustomInstantiator(f =>
            {
                var mockTimeSeries = new Mock<ITimeSeries>();

                mockTimeSeries.SetupGet(p => p.Id).Returns(f.Random.String2(5));
                mockTimeSeries.SetupGet(p => p.Key).Returns(f.Random.String2(10));
                mockTimeSeries.SetupGet(p => p.Name).Returns(f.Name.JobTitle());
                mockTimeSeries.SetupGet(p => p.SortNr).Returns(f.Random.Int(0, 100));
                mockTimeSeries.SetupGet(p => p.Unit).Returns(new UnitFakes().Get());
                mockTimeSeries.SetupGet(p => p.Axes).Returns(Mock.Of<IReadOnlyAxesOfTimeSeries>());
                mockTimeSeries.SetupGet(p => p.Properties).Returns(Mock.Of<IReadOnlyPropertiesOfTimeSeries>());
                mockTimeSeries.SetupGet(p => p.TimeSeriesKeys).Returns(new TimeSeriesKeyFakes().GetList(5));

                return mockTimeSeries.Object;
            });
    }

    public ITimeSeries Get()
    {
        var descriptorFaker = GetTimeSeriesFaker();
        return descriptorFaker.Generate();
    }

    public List<ITimeSeries> GetList(int count)
    {
        var descriptorFaker = GetTimeSeriesFaker();
        return descriptorFaker.Generate(count);
    }
}
