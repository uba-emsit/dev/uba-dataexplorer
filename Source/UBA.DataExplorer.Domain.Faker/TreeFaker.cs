﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

public class TreeFaker : Faker<ITree>
{
    public TreeFaker()
    {
        CustomInstantiator(f =>
        {
            var mockTree = new Mock<ITree>();
            mockTree.Setup(t => t.Id).Returns(f.Random.Guid().ToString());
            mockTree.Setup(t => t.Name).Returns(f.Company.CompanyName());
            mockTree.Setup(t => t.Description).Returns(f.Lorem.Sentence());
            mockTree.Setup(t => t.AuditInfo).Returns(new AuditInfoFaker().Generate());
            mockTree.Setup(t => t.Structure).Returns(new TreeNodeTreeFaker().Generate());

            return mockTree.Object;
        });
    }
}