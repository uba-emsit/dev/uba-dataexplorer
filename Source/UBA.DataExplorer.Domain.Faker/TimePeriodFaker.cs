using Bogus;
using Moq;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimePeriod. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimePeriodFaker : Faker<ITimePeriod>
{
    public TimePeriodFaker()
    {
        CustomInstantiator(f =>
        {
            var mock = new Mock<ITimePeriod>();
            mock.SetupGet(m => m.Date).Returns(f.Date.PastOffset());
            mock.SetupGet(m => m.TimeResolution).Returns(f.PickRandom<TimeResolution>());
            mock.SetupGet(m => m.TimeZoneId).Returns("Europe/Berlin");
            return mock.Object;
        });
    }
}

