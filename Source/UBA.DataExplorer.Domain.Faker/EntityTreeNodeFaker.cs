﻿using System.Collections.ObjectModel;
using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for IEntityTreeNode. Use it in the design mode of views or in unit tests.
/// </summary>
public class EntityTreeNodeFaker<T> : Faker<IEntityTreeNode<T>> where T : class, IDomainModel
{
    private readonly int _maxDepth;
    private readonly Faker<T> _entityFaker;

    public EntityTreeNodeFaker(Faker<T> entityFaker, int maxDepth = 3)
    {
        _maxDepth = maxDepth;
        _entityFaker = entityFaker;

        CustomInstantiator(f => CreateNode(f, 0));

        RuleFor(n => n.PrimaryKey, f => f.Random.Int(1, 100));
        RuleFor(n => n.SortNr, f => f.Random.Int(1, 100));
    }

    private IEntityTreeNode<T> CreateNode(Bogus.Faker f, int currentDepth)
    {
        var mockNode = new Mock<IEntityTreeNode<T>>();
        mockNode.SetupAllProperties();
        mockNode.SetupGet(p => p.Entity).Returns(_entityFaker.Generate());
        mockNode.SetupGet(p => p.Level).Returns(f.Random.Int(0, 10));

        if (currentDepth < _maxDepth)
        {
            var childCount = f.Random.Int(0, 3);
            var children = new List<IEntityTreeNode<T>>();

            for (int i = 0; i < childCount; i++)
            {
                children.Add(CreateNode(f, currentDepth + 1));
            }

            mockNode.SetupGet(p => p.Children)
                .Returns(new ReadOnlyCollection<IEntityTreeNode<T>>(children));
        }
        else
        {
            mockNode.SetupGet(p => p.Children)
                .Returns(new ReadOnlyCollection<IEntityTreeNode<T>>(new List<IEntityTreeNode<T>>()));
        }

        return mockNode.Object;
    }
}