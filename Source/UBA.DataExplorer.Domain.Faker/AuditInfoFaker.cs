﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Faker;

public class AuditInfoFaker : Faker<IAuditInfo>
{
    public AuditInfoFaker()
    {
        CustomInstantiator(f =>
        {
            var mock = new Mock<IAuditInfo>();
            mock.SetupGet(m => m.CreatedUserId).Returns(f.Name.FullName());
            mock.SetupGet(m => m.CreatedDate).Returns(f.Date.Past());
            mock.SetupGet(m => m.ChangedUserId).Returns(f.Name.FullName());
            mock.SetupGet(m => m.ChangedDate).Returns(f.Date.Past());
            mock.SetupGet(m => m.HasCreatedUserId).Returns(true);
            mock.SetupGet(m => m.HasCreatedDate).Returns(true);
            mock.SetupGet(m => m.HasChangedUserId).Returns(true);

            return mock.Object;
        });
    }
}