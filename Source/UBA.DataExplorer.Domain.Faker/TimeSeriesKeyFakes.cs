﻿using Bogus;
using Moq;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Faker;

/// <summary>
/// Generates fake data for ITimeSeriesKey. Use it in the design mode of views or in unit tests.
/// </summary>
public class TimeSeriesKeyFakes
{
    private static Faker<ITimeSeriesKey> GetFaker()
    {
        return new Faker<ITimeSeriesKey>()
            .CustomInstantiator(f =>
            {
                var mockTimeSeriesKey = new Mock<ITimeSeriesKey>();

                mockTimeSeriesKey.Setup(t => t.PrimaryKey).Returns(f.Random.Int());
                mockTimeSeriesKey.Setup(t => t.DimensionNr).Returns(f.Random.Int());

                var descriptorFaker = new DescriptorFakes();
                mockTimeSeriesKey.Setup(t => t.Descriptor).Returns(descriptorFaker.Get());

                return mockTimeSeriesKey.Object;
            });
    }

    public ITimeSeriesKey Get()
    {
        return GetFaker().Generate();
    }

    public List<ITimeSeriesKey> GetList(int count)
    {
        return GetFaker().Generate(count);
    }
}
