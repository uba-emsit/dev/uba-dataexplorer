﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Login;

public class LoginModel(UserModel userModel, bool isWindowsAccountLogin)
{
    public UserModel UserModel { get; } = userModel;
    public bool IsWindowsAccountLogin { get; } = isWindowsAccountLogin;
}