﻿namespace UBA.DataExplorer.Domain.Shared;

/// <summary>
/// Specifies that a model has an ID and a Label.
/// </summary>
public interface IIdAndLabelModel
{
    string Id { get; set; }
    string Label { get; set; }
}