﻿namespace UBA.DataExplorer.Domain.Shared;

public enum ErrorType
{
    Failure = 0,
    NotFound = 1,
    NoData
}