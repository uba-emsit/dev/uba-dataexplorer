﻿namespace UBA.DataExplorer.Domain.Shared;

public record UserModel(string Name, string Id);