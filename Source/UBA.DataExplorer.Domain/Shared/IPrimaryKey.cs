﻿namespace UBA.DataExplorer.Domain.Shared;

public interface IPrimaryKey
{
    int PrimaryKey { get; }
}