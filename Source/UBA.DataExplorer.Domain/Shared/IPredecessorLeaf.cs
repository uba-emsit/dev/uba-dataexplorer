﻿namespace UBA.DataExplorer.Domain.Shared;

public interface IPredecessorLeaf
{
    int PredecessorNr { get; }
    bool IsLeaf { get; }
}