﻿using System.Diagnostics.CodeAnalysis;

namespace UBA.DataExplorer.Domain.Shared;

public class Result
{
    protected Result(bool isSuccess, Error error)
    {
        if (isSuccess && error != Error.None ||
            !isSuccess && error == Error.None)
            throw new ArgumentException(@"Invalid error", nameof(error));

        IsSuccess = isSuccess;
        Error = error;
    }

    /// <summary>
    /// Gets a value indicating whether the result is a success.
    /// </summary>
    /// <value>
    /// <c>true</c> if the result is a success; otherwise, <c>false</c>.
    /// </value>
    public bool IsSuccess { get; }

    /// <summary>
    /// Gets a value indicating whether the result is a technical failure.
    /// Business logic error like invalid quality etc. will still be a IsSuccess in this result.
    /// </summary>
    /// <value>
    /// <c>true</c> if the result is a failure; otherwise, <c>false</c>.
    /// </value>
    public bool IsFailure => !IsSuccess;

    public Error Error { get; }

    public static Result Success() => new(true, Error.None);

    public static Result<TValue> Success<TValue>(TValue value) =>
        new(value, true, Error.None);

    public static Result Failure(Error error) => new(false, error);

    public static Result<TValue> Failure<TValue>(Error error) =>
        new(default, false, error);

    public override string ToString() => IsSuccess
        ? "Success"
        : $"Failure: {Error}";
}

public class Result<TValue>(TValue? value, bool isSuccess, Error error)
    : Result(isSuccess, error)
{
    [NotNull]
    public TValue Value => IsSuccess
        ? value!
        : throw new InvalidOperationException($"The value of a failure result can't be accessed: {Error}");

    public static implicit operator Result<TValue>(TValue? value) =>
        value is not null ? Success(value) : Failure<TValue>(Error.NullValue);

    public static Result<TValue> ValidationFailure(Error error) =>
        new(default, false, error);

    public Result<TNewValue> Map<TNewValue>(Func<TValue, TNewValue> map) =>
        IsSuccess
            ? Success(map(Value))
            : Failure<TNewValue>(Error);

    public Result<TNewValue> Map<TNewValue>(Func<TValue, Result<TNewValue>> map) =>
        IsSuccess
            ? map(Value)
            : Failure<TNewValue>(Error);

    public async Task<Result<TNewValue>> Map<TNewValue>(Func<TValue, Task<TNewValue>> map) =>
        IsSuccess
            ? Success(await map(Value))
            : Failure<TNewValue>(Error);

    public async Task<Result<TNewValue>> Map<TNewValue>(Func<TValue, Task<Result<TNewValue>>> map) =>
        IsSuccess
            ? await map(Value)
            : Failure<TNewValue>(Error);

    public override string ToString() => IsSuccess
        ? $"Success: {Value}"
        : $"Failure: {Error}";
}