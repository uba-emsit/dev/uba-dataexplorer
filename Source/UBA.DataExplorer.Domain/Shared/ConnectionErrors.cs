﻿namespace UBA.DataExplorer.Domain.Shared;

public static class ConnectionErrors
{
    public static Error IncorrectLoginType(Type expectedType) => Error.Failure(
        f => f.BackendErrors.IncorrectLoginType,
        $"The login is expected to be of type {expectedType.Name}");

    public static Error IncorrectConnectionType(Type expectedType) => Error.Failure(
        f => f.BackendErrors.IncorrectConnectionType,
        $"The connection is expected to be of type {expectedType.Name}");
}