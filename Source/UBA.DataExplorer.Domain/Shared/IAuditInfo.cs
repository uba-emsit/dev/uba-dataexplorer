﻿namespace UBA.DataExplorer.Domain.Shared;

public interface IAuditInfo
{
    public string? CreatedUserId { get; }
    public DateTime? CreatedDate { get; }
    public string? ChangedUserId { get; }
    public DateTime? ChangedDate { get; }
    public bool HasCreatedUserId { get; }
    public bool HasCreatedDate { get; }
    public bool HasChangedUserId { get; }
    public bool HasChangedDate { get; }
}