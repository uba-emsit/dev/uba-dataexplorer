﻿using System.Linq.Expressions;

namespace UBA.DataExplorer.Domain.Shared;

public class Error
{
    static readonly FailureCodes FailureCodes = new();

    public string Code { get; }
    public string Title { get; }
    public string Description { get; }
    public ErrorType Type { get; }
    public Exception? Exception { get; }

    public static readonly Error None = new(f => f.None, "", ErrorType.Failure);
    public static readonly Error NullValue = Failure(f => f.NullValue, "Null value was provided");

    // silent error occurs after logging out and should not be displayed to the user
    public static readonly Error Silent = new(f => f.Silent, "", ErrorType.Failure);

    public static Error Failure(Expression<Func<FailureCodes, FailureCode>> failureCode, Exception exception) =>
        new(failureCode, exception.ToString(), ErrorType.Failure, exception);

    public static Error Failure(Expression<Func<FailureCodes, FailureCode>> failureCode, string description = "") =>
        new(failureCode, description, ErrorType.Failure);

    public static Error Failure(Expression<Func<FailureCodes, FailureCode>> failureCode, string description, params object[] args) =>
        new(failureCode, string.Format(description, args), ErrorType.Failure);

    public static Error NotFound(string entityType, string id) =>
            new(f => f.DomainErrors.NotFound, $"{entityType} {id}", ErrorType.NotFound);

    public static Error NoData(string description) =>
        new(f => f.DomainErrors.NoData, description, ErrorType.NoData);

    Error(Expression<Func<FailureCodes, FailureCode>> failureCode, string description, ErrorType type, Exception? exception = null)
    {
        var code = failureCode.Compile()(FailureCodes);
        var title = ((MemberExpression)failureCode.Body).Member.Name;

        Code = code;
        Title = title;
        Description = description;
        Type = type;
        Exception = exception;
    }

    public override string ToString() => $"{Code}: {Title} - {Description}";
}