﻿namespace UBA.DataExplorer.Domain.Shared;

public class FailureCode
{
    string Code { get; }

    internal FailureCode(string code)
    {
        Code = code;
    }

    public static implicit operator string(FailureCode code) => code.Code;

    public override string ToString() => Code;
}
public class FailureCodes
{
    public readonly FailureCode None = new("X000");
    public readonly FailureCode NotImplemented = new("X001");
    public readonly FailureCode NullValue = new("X002");
    public readonly FailureCode Silent = new("X003");

    public readonly BackendErrors BackendErrors = new();
    public readonly DomainErrors DomainErrors = new();
}

public class BackendErrors
{
    // mapped to 5xx server errors
    public readonly FailureCode GeneralBackendFailure = new("BE500");
    public readonly FailureCode NotAvailable = new("BE503");
    // UBA-explorer specific errors
    public readonly FailureCode IncorrectConnectionType = new("BE520");
    public readonly FailureCode IncorrectLoginType = new("BE521");
    public readonly FailureCode DatabaseError = new("BE522");
}

public class DomainErrors
{
    // mapped to 4xx client errors
    public readonly FailureCode LogonFailed = new("FE401");
    public readonly FailureCode NotFound = new("FE404");
    // UBA-explorer specific errors
    public readonly FailureCode NoTimeRuleSettingsFor = new("FE420");
    public readonly FailureCode TimePeriodCreation = new("FE421");
    public readonly FailureCode NoData = new("FE422");
}