﻿namespace UBA.DataExplorer.Domain.Shared;

/// <summary>
/// Specifies that a model has a SortNr.
/// </summary>
public interface ISortNr
{
    public int SortNr { get; }
}