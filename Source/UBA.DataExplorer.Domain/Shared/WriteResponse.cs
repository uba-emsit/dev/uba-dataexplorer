﻿namespace UBA.DataExplorer.Domain.Shared;

public enum WriteResponseType
{
    Unchanged,
    Added,
    Updated,
    Deleted,
    Failed
}

public class WriteResponse<TDomainModel>(TDomainModel model, WriteResponseType writeResponseType)
    where TDomainModel : IDomainModel
{
    public TDomainModel Model => model;
    public WriteResponseType ResponseType { get; } = writeResponseType;
    public string? ErrorText { get; }

    public WriteResponse(TDomainModel model, string errorText) : this(model, WriteResponseType.Failed)
    {
        ErrorText = errorText;
    }
}