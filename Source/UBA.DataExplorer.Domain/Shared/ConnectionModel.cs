﻿using UBA.DataExplorer.Domain.Databases;

namespace UBA.DataExplorer.Domain.Shared;

public record ConnectionModel(DatabaseModel DatabaseInfo)
    : IDisposable
{
    public bool IsDisposed { get; private set; }

    public void Dispose() => IsDisposed = true;
}