﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.Tree.Interfaces;

public interface ITree : IDomainModel, IPrimaryKey
{
    string Id { get; }
    string Name { get; }
    string Description { get; }
    IAuditInfo AuditInfo { get; }
    ITreeNodeTree Structure { get; }
    IDimension Dimension { get; }
}

public interface ITreeNodeTree : IEntityTree<ITreeNodeNode>;

public interface ITreeNodeNode : IEntityTreeNode<ITreeNodeNode>
{
    IDescriptor Descriptor { get; }
}

public interface ITreeNode : IDomainModel, IPrimaryKey, ISortNr
{
    IDescriptor Descriptor { get; }
    int NodePredecessorNr { get; }
    int TreeNr { get; }
}