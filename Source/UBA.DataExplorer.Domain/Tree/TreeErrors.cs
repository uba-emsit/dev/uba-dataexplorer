﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Tree;

public static class TreeErrors
{

    public static Error NotFound(string id) => Error.NotFound(EntityTypes.Tree, id);
}
