﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Hypotheses.Interfaces;

public interface IHypothesis : IDomainModel
{
    string Id { get; set; }
    string Name { get; set; }
    string? Description { get; set; }
}