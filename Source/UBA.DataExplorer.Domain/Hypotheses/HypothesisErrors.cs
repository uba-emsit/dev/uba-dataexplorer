﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Hypotheses;

public static class HypothesisErrors
{
    public static Error NotFound(string id) => Error.NotFound(EntityTypes.Hypothesis, id);
}
