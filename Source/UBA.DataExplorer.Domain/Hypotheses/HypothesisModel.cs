﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;

namespace UBA.DataExplorer.Domain.Hypotheses;

public class HypothesisModel : IHypothesis
{
    public required string Id { get; set; }
    public required string Name { get; set; }
    public string? Description { get; set; }

    public static HypothesisModel Default => new()
    {
        Id = "Ref",
        Name = "Referenz",
        Description = null
    };
}
