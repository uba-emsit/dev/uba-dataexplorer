﻿using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.TimeSeries;

public class TimeSeriesDataCoordinatesModel : ITimeSeriesDataCoordinates
{
    public required ITimePeriod TimePeriod1 { get; set; }

    public decimal? NumericValue { get; set; }
}
