﻿using System.Runtime.Serialization;

namespace UBA.DataExplorer.Domain.TimeSeries;

/// <summary>
/// Enumeration of time resolutions used in a system, with corresponding integer values.
/// </summary>
/// <remarks>
/// The values are used to represent different time intervals, ranging from seconds to years.
/// TimeResolution.Hour represents an interval of one hour.
/// </remarks>
public enum TimeResolution
{
    [EnumMember(Value = "-2")]
    Timestamp = -2,
    [EnumMember(Value = "-1")]
    TID = -1,
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    Second = 1,
    [EnumMember(Value = "2")]
    Minute = 2,
    [EnumMember(Value = "3")]
    FifteenMinutes = 3,
    [EnumMember(Value = "4")]
    Hour = 4,
    [EnumMember(Value = "5")]
    Day = 5,
    [EnumMember(Value = "6")]
    Week = 6,
    [EnumMember(Value = "7")]
    Month = 7,
    [EnumMember(Value = "8")]
    ThreeMonths = 8,
    [EnumMember(Value = "9")]
    SixMonths = 9,
    [EnumMember(Value = "10")]
    Year = 10,
    [EnumMember(Value = "11")]
    ThirtyMinutes = 11
};