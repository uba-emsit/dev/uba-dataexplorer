﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IPeriodFilterSettings : IDomainModel
{
    public List<IPeriodFilter> PeriodFilters { get; }
}