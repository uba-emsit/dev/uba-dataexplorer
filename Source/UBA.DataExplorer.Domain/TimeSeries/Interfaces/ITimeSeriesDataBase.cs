﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesDataBase
{
    int TimeSeriesPrimaryKey { get; }
    ITimeSeriesDataCoordinates Coordinates { get; }
    double? Value { get; set; }
    IHypothesis Hypothesis { get; }
    TimeResolution TimeResolution { get; }
}