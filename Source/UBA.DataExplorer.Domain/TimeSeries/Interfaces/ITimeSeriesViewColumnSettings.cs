﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewColumnSettings : IDomainModel, ISortNr
{
    public int ColumnWidth { get; }

    public TimeSeriesViewGridColumn GridColumn { get; }

    public bool IsMerged { get; }

    public bool IsVisible { get; }

    public SortMode SortMode { get; }

    public bool UseId { get; }
}