﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeFilterSettings : IDomainModel
{
    List<ITimeFilter> TimeFilters { get; }
}