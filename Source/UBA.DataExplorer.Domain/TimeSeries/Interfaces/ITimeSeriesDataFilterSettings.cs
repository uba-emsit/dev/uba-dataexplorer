﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesDataFilterSettings : IDomainModel
{
    public List<ITimeSeriesDataFilterCriterionSettings> Criterions { get; }
}