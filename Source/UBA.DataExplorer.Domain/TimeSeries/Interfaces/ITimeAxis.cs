﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeAxis : IDomainModel
{
}

public interface ITimeAxis<T, TInterval> : ITimeAxis
    where TInterval : ITimeInterval<T>
{
}
