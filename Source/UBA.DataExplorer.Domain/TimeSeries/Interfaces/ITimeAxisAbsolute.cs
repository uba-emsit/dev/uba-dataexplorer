﻿namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeAxisAbsolute : ITimeAxis<DateTimeOffset, ITimeIntervalAbsolute>;