﻿namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeInterval<out T>
{
    public T From { get; }

    public T To { get; }
}