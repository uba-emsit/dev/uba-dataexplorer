﻿namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeIntervalAbsolute : ITimeInterval<DateTimeOffset>;