﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IReadOnlyAxesOfTimeSeries : IDomainModel
{
    // Expand when needed
}