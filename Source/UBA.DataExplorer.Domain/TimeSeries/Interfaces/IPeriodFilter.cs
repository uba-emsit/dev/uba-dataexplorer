﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IPeriodFilter : IDomainModel
{
    public IDynamicDate DynamicDateFrom { get; }
    public IDynamicDate DynamicDateTo { get; }
    public bool IsEnabled { get; }
    public TimeResolution TimeResolution { get; }
}