﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IMappedTimeSeriesData : ITimeSeriesData, IDomainModel
{
    public MappingResult MappingResult { get; }
}