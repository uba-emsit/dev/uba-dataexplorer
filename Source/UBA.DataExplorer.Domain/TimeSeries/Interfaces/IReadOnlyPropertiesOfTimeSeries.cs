﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IReadOnlyPropertiesOfTimeSeries : IDomainModel
{
    // Expand when needed
}