﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesDataCoordinates : IDomainModel
{
    ITimePeriod TimePeriod1 { get; }
}
