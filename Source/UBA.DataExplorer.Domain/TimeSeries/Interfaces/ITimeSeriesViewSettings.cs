﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewSettings : IDomainModel
{
    public BooleanTriState AggregationAllowIncomplete { get; }

    public AggregationCalendar AggregationCalendar { get; }

    public AggregationMappingMode AggregationMappingMode { get; }

    public AggregationRule AggregationRule { get; }

    public CellAlignment AlignmentOfData { get; }

    public CellAlignment AlignmentOfDescriptors { get; }

    public BooleanTriState AllowInheritance { get; }

    public List<ITimeSeriesViewColumnSettings> Columns { get; }

    public bool ColumnWidthAuto { get; }

    public int ColumnWidthOfValues { get; }

    public int ColumnWidthPortraitColumn1 { get; }

    public int ColumnWidthPortraitColumns { get; }

    public TimeseriesViewGridDataMode DataMode { get; }

    public bool DimensionModeAuto { get; }

    public DisaggregationRule DisAggregationRule { get; }

    public int DispDecimalPlaces { get; }

    public NumberFormat DisplayFormat { get; }

    public string DisplayUnit { get; }

    public List<int> ExpandedTimeSeriesPrimaryKeys { get; }

    public ExtrapolationRule ExtrapolationRule { get; }

    public int FreezePosition { get; }

    public List<int> HypothesesPrimaryKeys { get; }

    public double InterpolationGradient { get; }

    public InterpolationRule InterpolationRule { get; }

    public BooleanTriState IsConstOMP { get; }

    public bool RowAuto { get; }

    public bool RowAutoAffectsFilteredValues { get; }

    public bool RowHeightAuto { get; }

    public int RowHeightFixCount { get; }

    public int SubtotalsFunction { get; }

    public bool SubtotalsOn { get; }

    public int SubtotalsUpToColumn { get; }

    public TimeResolution MainTimeResolution { get; }

    public List<TimeResolution> TimeResolutions { get; }

    public bool UseListSortOrder { get; }

    public string ViewTimezoneId { get; }
}