﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IMasterDataCollectiveQuerySettings : IDomainModel
{
    public IDynamicDate QueryDynamicDate { get; }
    public TimeResolution QueryTimeResolution { get; }
    public List<IAnnexSearchCriterion> SearchCriteria { get; }
    public bool TreeIsUsed { get; }
    public int TreeLevelForAnd { get; }
    public int TreeLevelForResults { get; }
    public int TreeNr { get; }
}