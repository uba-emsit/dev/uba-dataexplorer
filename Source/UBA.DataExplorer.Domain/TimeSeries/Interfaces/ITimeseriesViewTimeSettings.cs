﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewTimeSettings : IDomainModel
{
    IDynamicDate DynamicStartDate { get; }
    int ModelPrimaryKey { get; }
    bool RangeUseDuration { get; }
    bool SortDescending { get; }
    bool SyncStartDates { get; }
    ITimeFilterSettings TimeFilter { get; }
    TimeSeriesViewTimeRule TimeRule { get; }
    List<ITimeSeriesViewTimeRuleSettings> TimeRuleSettings { get; }
    bool UseDynamicStartDate { get; }
    bool UseOnlyUsedPeriodsWithValuesInTimeRange { get; }
    bool UseWebTimeAxis { get; }
}