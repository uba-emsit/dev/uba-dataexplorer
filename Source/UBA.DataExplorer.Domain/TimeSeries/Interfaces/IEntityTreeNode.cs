﻿using System.Collections.ObjectModel;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IEntityTreeNode<T> : IDomainModel, IPrimaryKey, ISortNr
    where T : class, IDomainModel
{
    ReadOnlyCollection<IEntityTreeNode<T>> Children { get; }
    int Level { get; }
    T Entity { get; }
}