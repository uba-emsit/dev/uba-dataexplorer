﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IDynamicDate : IDomainModel
{
    bool ExcludeEndDate { get; }
    DateTimeOffset FixDate { get; }
    bool IsFix { get; }
    TimeResolution NowTimeResolution { get; }
    int Offset { get; }
    TimeResolution OffsetTimeResolution { get; }
    int Timezone { get; }
    string TimezoneId { get; }
}