﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewTimeRuleSettings : IDomainModel, IEquatable<ITimeSeriesViewTimeRuleSettings>
{
    int IntervalCount { get; }
    int IntervalIncrement { get; }
    TimeResolution IntervalIncrementTimeResolution { get; }
    Result<ITimePeriod> IntervalStartPeriod { get; }
    List<Result<ITimePeriod>> PeriodList { get; }
    int RangeDuration { get; }
    TimeResolution RangeDurationTimeResolution { get; }
    Result<ITimePeriod> GetRangeFromPeriod();
    Result<ITimePeriod> GetRangeToPeriod();
    string TimeDisplayFormat { get; }
    TimeResolution TimeResolution { get; }
    ITimeShiftSettings TimeShift { get; }
}