﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesKey : IDomainModel
{
    int PrimaryKey { get; }

    int DimensionNr { get; }

    IDescriptor Descriptor { get; }
}