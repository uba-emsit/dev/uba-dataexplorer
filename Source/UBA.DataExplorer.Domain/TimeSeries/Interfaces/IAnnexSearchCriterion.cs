﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IAnnexSearchCriterion : IDomainModel
{
    public int ComponentNr { get; }
    public SearchCondition Condition { get; }
    public bool IsEnabled { get; }
    public int ItemNr { get; }
    public AnnexItemType ItemType { get; }
    public SearchLogicalOp LogicalOp { get; }
    public int PrimaryKey { get; }
    public string Unit { get; }
    public object Value { get; }
}