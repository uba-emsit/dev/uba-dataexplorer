﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesDataFilterCriterionSettings : IDomainModel
{
    public SearchCondition Condition { get; }
    public bool IsEnabled { get; }
    public SearchLogicalOp LogicalOperator { get; }
    public TimeSeriesDataQueryItem QueryItem { get; }
    public string SerializedValue { get; }
}