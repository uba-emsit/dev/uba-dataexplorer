﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IDescriptorFilterSettings : IDomainModel
{
    public DescriptorFilterMode FilterMode { get; }
    public List<int> DescriptorPrimaryKeys { get; }
    public int DimensionPrimaryKey { get; }
    public TreeFilterMode TreeFilterMode { get; }
    public bool IsDescriptorFilterPlaceholder { get; }
    public bool IsEnabled { get; }
    public bool IsNotEmpty { get; }
    public IMasterDataCollectiveQuerySettings MasterDataQuery { get; }
    /// <summary>
    /// Time Series are filtered where the selected descriptors may appear in combination with any other descriptor. Example: DE* Xyz, BBB*Xyz, Xyz* DE, Xyz*BBB
    /// </summary>
    public bool UseMultipleAllocationOpen { get; }
    /// <summary>
    /// Time Series are filtered where exactly the combination of selected descriptors is used as a multiple allocation. Example: DE* BBB, BBB*DE
    /// </summary>
    public bool UseMultipleAllocationExclusive { get; }
    /// <summary>
    /// Only those Time Series are filtered where no Multiple Allocation of Descriptors can be found in this Dimension. Example: DE, BBB
    /// </summary>
    public bool UseMultipleAllocationNone { get; }
    /// <summary>
    /// In case of Descriptor List or Tree Filter, this flag indicates the user wants to see the time series without descriptors in this dimension.
    /// </summary>
    public bool OrIsEmpty { get; }
    /// <summary>
    /// A list of TreeNode PK's in case of a Tree Filter (=TreePrimaryKey > 0). Case of a minus-PK (e.g. -11), the TreeNode is excluded.
    /// </summary>
    public List<int> TreeNodePrimaryKeys { get; }
    /// <summary>
    /// -1 in case of Descriptor List, else the PK of the selected tree (and TreeNodePrimaryKeys is filled)
    /// </summary>
    int TreePrimaryKey { get; }

    public enum DescriptorFilterMode
    {
        SingleDescriptors,
        Tree,
        MasterData
    }
}