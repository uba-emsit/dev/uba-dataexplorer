﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesData : ITimeSeriesDataBase, IDomainModel
{
    DateTime ChangedDate { get; }
    string? ChangedUserId { get; }
    string? DataSource { get; set; }
    bool IsWriteProtected { get; set; }
    string? NoValueReason { get; set; }
    int? ProtectionLevel { get; set; }
    int? Quality { get; set; }
    bool HasDocumentation { get; }
}