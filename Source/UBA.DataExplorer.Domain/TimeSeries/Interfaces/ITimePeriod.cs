﻿
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimePeriod : IDomainModel
{
    DateTimeOffset Date { get; }
    TimeResolution TimeResolution { get; }
    string TimeZoneId { get; }
}