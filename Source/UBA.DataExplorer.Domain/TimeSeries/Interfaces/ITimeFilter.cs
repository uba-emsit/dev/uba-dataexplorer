﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeFilter : IDomainModel
{
    string AdditionalCriteria { get; }
    bool Exclude { get; }
    bool IsEnabled { get; }
    TimeFilterRule TimeFilterRule { get; }
}
