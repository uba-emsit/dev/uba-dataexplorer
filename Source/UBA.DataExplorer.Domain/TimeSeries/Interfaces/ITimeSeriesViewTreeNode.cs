﻿namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewTreeNode : IEntityTreeNode<ITimeSeriesView>;