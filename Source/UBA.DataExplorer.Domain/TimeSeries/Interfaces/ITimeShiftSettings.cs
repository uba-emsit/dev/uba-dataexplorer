﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeShiftSettings : IDomainModel, IEquatable<ITimeShiftSettings>
{
    TimeResolution MainTimeResolution { get; }
    int ShiftLargeIncrement { get; }
    TimeResolution ShiftLargeTimeResolution { get; }
    int ShiftSmallIncrement { get; }
    TimeResolution ShiftSmallTimeResolution { get; }
}