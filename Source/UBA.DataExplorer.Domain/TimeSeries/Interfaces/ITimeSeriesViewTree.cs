﻿namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesViewTree : IEntityTree<ITimeSeriesView>;