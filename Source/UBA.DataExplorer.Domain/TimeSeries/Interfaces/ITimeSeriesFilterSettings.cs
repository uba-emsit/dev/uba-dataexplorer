﻿using UBA.DataExplorer.Domain.Settings;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesFilterSettings : IDomainModel
{
    public ITimeSeriesDataFilterSettings DataFilter { get; }

    /// <summary>
    /// A list of descriptor filters.
    /// </summary>
    public List<IDescriptorFilterSettings> DescriptorFilters { get; }
    public bool IgnoreMultipleAllocation { get; }
    /// <summary>
    /// A list of time series numbers to filter on in case of Filter Usage is or includes a List.
    /// </summary>
    public List<int> TimeSeriesPrimaryKeys { get; }
    /// <summary>
    /// A time zone to filter by.
    /// </summary>
    public string TimeZoneId { get; }
    /// <summary>
    /// Specifies how to combine the time series number list and the descriptor filters.
    /// </summary>
    public TimeSeriesFilterUsage FilterUsage { get; }
    /// <summary>
    /// Use specified time zone in filter.
    /// </summary>
    public bool UseTimeZoneFilter { get; }
}