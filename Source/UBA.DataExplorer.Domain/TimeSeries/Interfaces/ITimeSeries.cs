﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeries : IDomainModel, IPrimaryKey, IEquatable<ITimeSeries>, ISortNr
{
    string Id { get; }
    string Key { get; }
    string Name { get; }
    string Unit { get; }
    IReadOnlyList<ITimeSeriesKey> TimeSeriesKeys { get; }
    IReadOnlyAxesOfTimeSeries? Axes { get; }
    IReadOnlyPropertiesOfTimeSeries? Properties { get; }

    bool IsVirtual { get; }
}