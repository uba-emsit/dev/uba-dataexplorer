﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface IEntityTree<T> : IDomainModel
    where T : class, IDomainModel
{
    IEntityTreeNode<T> RootNode { get; }

    int Count { get; }
}