﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries.Interfaces;

public interface ITimeSeriesView : IDomainModel, IPrimaryKey, IPredecessorLeaf, ISortNr
{
    string Id { get; }

    string Name { get; }

    bool IsSystemView { get; }

    string Description { get; }

    IAuditInfo AuditInfo { get; }

    ITimeSeriesViewTimeSettings TimeSettings { get; }

    ITimeSeriesViewSettings Settings { get; }

    ITimeSeriesFilterSettings TsFilter { get; }
}