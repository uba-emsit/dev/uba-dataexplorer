﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries;

public static class TimeSeriesViewTreeErrors
{

    public static Error NotFound(string id) => Error.NotFound(EntityTypes.TimeSeriesViewTree, id);
}
