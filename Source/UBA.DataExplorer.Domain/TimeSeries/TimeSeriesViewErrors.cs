﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries;

public static class TimeSeriesViewErrors
{
    public static Error NotFound(string id) => Error.NotFound(EntityTypes.TimeSeriesView, id);

    public static Error NoTimeRuleSettingsFor(string period) =>
        Error.Failure(f => f.DomainErrors.NoTimeRuleSettingsFor, ErrorDescriptions.NoTimeRuleSettingsFor, period);

}
