﻿using System.Runtime.Serialization;

namespace UBA.DataExplorer.Domain.TimeSeries;
/// <summary>
/// Enumeration representing the possible results of a mapping operation.
/// </summary>
[DataContract]
public enum MappingResult
{
    /// <summary>
    /// Occurs when a value is read protected
    /// </summary>
    [EnumMember(Value = "-6")]
    ErrorProtected = -6,
    /// <summary>
    /// 
    /// </summary>
    [EnumMember(Value = "-5")]
    GradientError = -5,
    /// <summary>
    /// 
    /// </summary>
    [EnumMember(Value = "-4")]
    ZRuleError = -4,
    [EnumMember(Value = "-2")]
    NoRule = -2,
    [EnumMember(Value = "-1")]
    Error = -1,
    [EnumMember(Value = "0")]
    Empty = 0,
    /// <summary>
    /// The value is an original value (unmapped)
    /// </summary>
    [EnumMember(Value = "1")]
    Original = 1,
    /// <summary>
    /// 
    /// </summary>
    [EnumMember(Value = "2")]
    Inherited = 2,
    /// <summary>
    /// The value is interpolated or extrapolated
    /// </summary>
    [EnumMember(Value = "3")]
    Interpolated = 3,
};
