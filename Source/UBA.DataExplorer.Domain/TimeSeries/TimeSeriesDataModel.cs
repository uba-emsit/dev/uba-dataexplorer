﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.TimeSeries;

public class TimeSeriesDataModel : ITimeSeriesData
{
    public int TimeSeriesPrimaryKey => -1;
    public ITimeSeriesDataCoordinates Coordinates { get; set; } = null!;
    public double? Value { get; set; }
    public required IHypothesis Hypothesis { get; set; }
    public TimeResolution TimeResolution { get; set; }
    public DateTime ChangedDate { get; set; }
    public string? ChangedUserId { get; set; }
    public string? DataSource { get; set; }
    public bool IsWriteProtected { get; set; }
    public string? NoValueReason { get; set; }
    public int? ProtectionLevel { get; set; }
    public int? Quality { get; set; }
    public bool HasDocumentation { get; set; }
}
