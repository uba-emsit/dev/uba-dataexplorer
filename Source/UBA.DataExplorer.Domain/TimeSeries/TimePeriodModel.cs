﻿using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Domain.TimeSeries;

public class TimePeriodModel : ITimePeriod, IEquatable<TimePeriodModel>
{
    /// <summary>The id of the time zone of this TimePeriod.</summary>
    public string TimeZoneId { get; init; } = "";

    /// <summary>Defines the length of the represented period of time.</summary>
    public required TimeResolution TimeResolution { get; init; }

    /// <summary>DateTimeOffset that defines the beginning of the represented period of time.</summary>
    public DateTimeOffset Date { get; init; }

    public override string ToString() => Date.ToString(TimeResolution);

    public bool Equals(TimePeriodModel? other)
    {
        if (other is null) return false;
        return TimeResolution == other.TimeResolution &&
               Date.Equals(other.Date);
    }

    public override bool Equals(object? obj) => Equals(obj as TimePeriodModel);

    public override int GetHashCode() => HashCode.Combine(TimeResolution, Date);

    public static bool operator ==(TimePeriodModel left, TimePeriodModel right) => Equals(left, right);

    public static bool operator !=(TimePeriodModel left, TimePeriodModel right) => !Equals(left, right);
}
