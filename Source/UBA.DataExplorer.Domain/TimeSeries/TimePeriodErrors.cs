﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.TimeSeries;

public static class TimePeriodErrors
{
    public static Error CreationFailed() =>
        Error.Failure(f => f.DomainErrors.TimePeriodCreation, ErrorDescriptions.TimePeriodCreation);
}
