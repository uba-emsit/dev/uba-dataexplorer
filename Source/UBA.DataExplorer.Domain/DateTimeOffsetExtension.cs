﻿using System.Globalization;
using UBA.DataExplorer.Domain.TimeSeries;

namespace UBA.DataExplorer.Domain;

public static class DateTimeOffsetExtension
{
    public static DateTimeOffset AddPeriodStep(this DateTimeOffset date, TimeResolution timeResolution)
        => timeResolution switch
        {
            TimeResolution.Second => date.AddSeconds(1),
            TimeResolution.Minute => date.AddMinutes(1),
            TimeResolution.FifteenMinutes => date.AddMinutes(15),
            TimeResolution.Hour => date.AddHours(1),
            TimeResolution.Day => date.AddDays(1),
            TimeResolution.Week => date.AddDays(7),
            TimeResolution.Month => date.AddMonths(1),
            TimeResolution.ThreeMonths => date.AddMonths(3),
            TimeResolution.SixMonths => date.AddMonths(6),
            TimeResolution.Year => date.AddYears(1),
            TimeResolution.ThirtyMinutes => date.AddMinutes(30),
            _ => throw new ArgumentOutOfRangeException(nameof(timeResolution), timeResolution, null)
        };

    public static string ToString(this DateTimeOffset? date, TimeResolution timeResolution)
        => date.HasValue ? date.Value.ToString(timeResolution) : string.Empty;

    public static string ToString(this DateTimeOffset date, TimeResolution timeResolution)
        => timeResolution switch
        {
            TimeResolution.Year => date.Year.ToString(),
            TimeResolution.SixMonths => $"{(date.Month / 6) + 1}-{date.Year}",
            TimeResolution.ThreeMonths => $"{(date.Month / 3) + 1}-{date.Year}",
            TimeResolution.Month => date.ToString("MM-yyyy"),
            TimeResolution.Week =>
                $"{CultureInfo.CurrentCulture.Calendar.GetWeekOfYear(((DateTimeOffset?)date).Value.DateTime, CalendarWeekRule.FirstFourDayWeek, DayOfWeek.Monday) + 1}-{date.Year}",
            TimeResolution.Day => date.ToString("dd-MM-yyyy"),
            TimeResolution.Hour => date.ToString("dd-MM-yyyy HH:00"),
            TimeResolution.Minute or TimeResolution.ThirtyMinutes or TimeResolution.FifteenMinutes =>
                date.ToString("dd-MM-yyyy HH:mm"),
            TimeResolution.Second => date.ToString("dd-MM-yyyy HH:mm:ss"),
            _ => date.ToString("dd-MM-yyyy HH:mm:ss")
        };
}