﻿namespace UBA.DataExplorer.Domain.Databases;

public class DatabaseModel(int primaryKey, string guid, string id, string description, string label, int sortNr)
{
    public string Guid { get; set; } = guid;
    public string Id { get; set; } = id;
    public int PrimaryKey { get; set; } = primaryKey;
    public string Description { get; set; } = description;
    public string Label { get; set; } = label;
    public int SortNr { get; set; } = sortNr;

    /// <summary>
    /// The one-based position of the database in the recently used list, if found; otherwise, 0.
    /// </summary>
    public int RecentlyUsedPosition { get; set; } = 0;

    /// <summary>
    /// The user's favorite database, should be logged into automatically on start-up.
    /// </summary>
    public bool IsFavorite { get; set; } = false;
}
