﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Databases;

public static class DatabaseErrors
{
    public static Error LogonFailed(string message) => Error.Failure(
        f => f.DomainErrors.LogonFailed,
        message);

    public static Error NotAvailable(string message) => Error.Failure(
        f => f.BackendErrors.NotAvailable,
        message);
}
