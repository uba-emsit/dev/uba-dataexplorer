﻿using UBA.DataExplorer.Domain.Resource;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Dimensions;

public static class DescriptorErrors
{
    public static Error NotFound(string id) => Error.NotFound(EntityTypes.Descriptor, id);
}
