﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Dimensions.Interfaces;

public interface IDimension : IDomainModel, IEquatable<IDimension>
{
    string Id { get; set; }
    int PrimaryKey { get; }
    string Name { get; set; }
    string? Description { get; set; }
    IAuditInfo AuditInfo { get; }
}