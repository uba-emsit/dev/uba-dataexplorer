﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Dimensions.Interfaces;

public interface IDescriptor : IDomainModel, IPrimaryKey, IIdAndLabelModel, IEquatable<IDescriptor>, ISortNr
{
    IDescriptorKey Key { get; }
    string? Description { get; set; }
    int IconNr { get; }
    IDimension Dimension { get; }
    IAuditInfo AuditInfo { get; }

}