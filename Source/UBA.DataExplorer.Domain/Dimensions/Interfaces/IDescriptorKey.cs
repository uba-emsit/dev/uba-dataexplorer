﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Domain.Dimensions.Interfaces;

public interface IDescriptorKey : IDomainModel
{
    int DimensionPrimaryKey { get; }
    string Id { get; }
}