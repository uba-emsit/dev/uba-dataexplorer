﻿using System.Runtime.Serialization;

namespace UBA.DataExplorer.Domain.Settings;

/// <summary>
/// Represents a tri-state Boolean value, which can be true, false, or indeterminate.
/// </summary>
public enum BooleanTriState
{
    [EnumMember(Value = "0")]
    False = 0,
    [EnumMember(Value = "-1")]
    True = -1,
    [EnumMember(Value = "-2")]
    UseDefault = -2,
}

/// <summary>
/// Enum representing the type of aggregation calendar.
/// </summary>
[DataContract]
public enum AggregationCalendar
{
    [EnumMember(Value = "0")]
    Standard = 0,
    [EnumMember(Value = "1")]
    Gas = 1
};

/// <summary>
/// Enumeration representing the different modes of aggregation mapping.
/// </summary>
[DataContract]
public enum AggregationMappingMode
{
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    InputDataOnly = 1,
    [EnumMember(Value = "2")]
    InterpolExtrapol = 2,
    [EnumMember(Value = "3")]
    CompleteMapping = 3
};

/// <summary>
/// Enumeration of aggregation rules.
/// </summary>
[DataContract]
public enum AggregationRule
{
    [EnumMember(Value = "0")]
    None = 0,
    [EnumMember(Value = "1")]
    Sum = 1,
    [EnumMember(Value = "2")]
    Average = 2,
    [EnumMember(Value = "3")]
    Minimum = 3,
    [EnumMember(Value = "4")]
    Maximum = 4,
    [EnumMember(Value = "5")]
    StdDev = 5,
    [EnumMember(Value = "6")]
    Count = 6
};


/// <summary>
/// Enumeration of possible cell alignments.
/// </summary>
[DataContract]
public enum CellAlignment
{
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    Left = 1,
    [EnumMember(Value = "2")]
    Center = 2,
    [EnumMember(Value = "3")]
    Right = 3,
}

/// <summary>
/// Enum representing the different modes for displaying data in a timeseries view grid.
/// </summary>
[DataContract]
public enum TimeseriesViewGridDataMode
{
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    InputValues = 1,
    [EnumMember(Value = "2")]
    InputRates = 2,
    [EnumMember(Value = "3")]
    ModelResults = 3,
    [EnumMember(Value = "4")]
    Both = 4,
    [EnumMember(Value = "5")]
    Inheritance = 5,
    [EnumMember(Value = "6")]
    Mapping = 6,
    [EnumMember(Value = "7")]
    Quality = 7,
}

/// <summary>
/// Enumeration of possible disaggregation rules.
/// </summary>
[DataContract]
public enum DisaggregationRule
{
    [EnumMember(Value = "0")]
    None = 0,
    [EnumMember(Value = "1")]
    EqualValue = 1,
    [EnumMember(Value = "2")]
    Distribution = 2,
    [EnumMember(Value = "3")]
    Count = 3
};

/// <summary>
/// Enum representing different number formats.
/// </summary>
[DataContract]
public enum NumberFormat
{
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    Fix = 1,
    [EnumMember(Value = "2")]
    Exp = 2,
    [EnumMember(Value = "3")]
    Auto = 3
};

/// <summary>
/// Enumeration of extrapolation rules.
/// </summary>
[DataContract]
public enum ExtrapolationRule
{
    [EnumMember(Value = "0")]
    None = 0,
    [EnumMember(Value = "1")]
    Exponential = 1
};

/// <summary>
/// Enumeration of interpolation rules.
/// </summary>
[DataContract]
public enum InterpolationRule
{
    [EnumMember(Value = "0")]
    None = 0,
    [EnumMember(Value = "1")]
    Linear = 1,
    [EnumMember(Value = "2")]
    Exponential = 2,
    [EnumMember(Value = "3")]
    Step = 3,
    [EnumMember(Value = "4")]
    Logistic = 4
};

/// <summary>
/// Enum representing the columns of a time series view grid.
/// </summary>
[DataContract]
public enum TimeSeriesViewGridColumn
{
    [EnumMember(Value = "0")]
    Name = 0,
    [EnumMember(Value = "-1")]
    Unknown = -1,
    [EnumMember(Value = "-2")]
    Unit = -2,
    [EnumMember(Value = "-3")]
    TimeKey = -3,
    [EnumMember(Value = "-4")]
    Hypo = -4,
    [EnumMember(Value = "-5")]
    Case = -5,
    [EnumMember(Value = "-6")]
    Storage = -6,
    [EnumMember(Value = "-7")]
    Timezone = -7,
    [EnumMember(Value = "-99")]
    Data = -99,
}

/// <summary>
/// Enumeration of sorting modes.
/// </summary>
public enum SortMode
{
    None = 0,
    Alphanumeric = 1,
    Predefined = 2,
};

/// <summary>
/// Enum representing the sort direction, with Ascending being 1 and Descending being -1.
/// </summary>
public enum SortDirection
{
    Ascending = 1,
    Descending = -1
};


/// <summary>
/// Enum representing different rules for filtering time-based data.
/// </summary>
public enum TimeFilterRule
{
    None,
    SixMonthsOfYear,
    QuartersOfYear,
    MonthsOfYear,
    WeeksOfYear,
    DaysOfYear,
    DaysOfMonth,
    DaysOfWeek,
    HoursOfDay,
    ThirtyMinutesOfHour,
    FifteenMinutesOfHour,
    MinutesOfHour,
    SecondsOfMinute,
    Holidays,
}

/// <summary>
/// Enum representing different rules for time series view time.
/// </summary>
public enum TimeSeriesViewTimeRule
{
    [EnumMember(Value = "0")] None,
    [EnumMember(Value = "1")] Model,
    [EnumMember(Value = "2")] Range,
    [EnumMember(Value = "3")] Interval,
    [EnumMember(Value = "4")] TimeList,
}

public enum TimeSeriesFilterUsage
{
    /// <summary>
    /// Time series to show is defined by a filter.
    /// </summary>
    [EnumMember(Value = "1")]
    FilterOnly = 1,
    /// <summary>
    /// Time series to show is defined by a list.
    /// </summary>
    [EnumMember(Value = "2")]
    ListOnly = 2,
    /// <summary>
    /// Time series to show is defined by a filter and a list.
    /// </summary>
    [EnumMember(Value = "3")]
    FilterAndList = 3,
    /// <summary>
    /// Time series to show is defined by a filter or a list.
    /// </summary>
    [EnumMember(Value = "4")]
    FilterOrList = 4
}

public enum SearchCondition
{
    Unknown = 0,
    Equal = 1,
    UnEqual = 2,
    Contains = 3,
    Greater = 4,
    GreaterEqual = 5,
    Less = 6,
    LessEqual = 7,
    StartsWith = 8,
    EndsWith = 9,
    IsEmpty = 10,
    IsNotEmpty = 11,
    In = 12
}

public enum SearchLogicalOp
{
    Unknown = 0,
    And = 1,
    Or = 2
}

public enum TimeSeriesDataQueryItem
{
    Unknown = 0,
    Value = 1,
    GrowthRate = 2,
    Quality = 3,
    ModelNr = 4,
    IsWriteProtected = 5,
    ChangeDate = 6,
    ChangeId = 7,
    NoValueReason = 8,
    AnnexObjNr = 9,
    Protection = 10,
    Origin = 11,
    Source = 12,

    EditLock = 99,
}

public enum TreeFilterMode
{
    [EnumMember(Value = "1")]
    NodeOnly = 1,
    [EnumMember(Value = "2")]
    BranchOnly = 2,
    [EnumMember(Value = "3")]
    BranchAndNode = 3,
}

/// <summary>
/// Enum representing the different types of items that can be included in an annex.
/// </summary>
public enum AnnexItemType
{
    [EnumMember(Value = "0")]
    Unknown = 0,
    [EnumMember(Value = "1")]
    TextFree = 1,
    [EnumMember(Value = "2")]
    TextFixSingle = 2,
    [EnumMember(Value = "3")]
    TextFixMulti = 3,
    [EnumMember(Value = "4")]
    TextOpenSingle = 4,
    [EnumMember(Value = "5")]
    TextOpenMulti = 5,
    [EnumMember(Value = "6")]
    Date = 6,
    [EnumMember(Value = "7")]
    Boolean = 7,
    [EnumMember(Value = "8")]
    Descriptor = 8,
    [EnumMember(Value = "9")]
    TsValue = 9,
    [EnumMember(Value = "10")]
    Binary = 10,
    [EnumMember(Value = "11")]
    Number = 11,
    [EnumMember(Value = "12")]
    DbKeyField = 12,
    [EnumMember(Value = "13")]
    DbDisplayField = 13,
    [EnumMember(Value = "14")]
    Calculated = 14,
    [EnumMember(Value = "15")]
    MdTreeObject = 15,
    [EnumMember(Value = "16")]
    MultiDescriptor = 16,
    [EnumMember(Value = "17")]
    MdKeyField = 17,
    [EnumMember(Value = "18")]
    MdDisplayField = 18,
    [EnumMember(Value = "19")]
    Time = 19,
    [EnumMember(Value = "20")]
    TreeNodeParent = 20,
}