﻿using UBA.DataExplorer.Domain.Login;

namespace UBA.DataExplorer.Core;

public interface IReLoginHandler
{
    Task<bool> WasWindowsAccountLogin(CancellationToken cancellationToken);
    Task<Response> RequestPassword(string userId, string? errorMessage, CancellationToken cancellationToken);
    Task MessageServerChanged(IMessageServer messageServer);
    Task LoginChanged(LoginModel loginModel);
    Task Logoff();

    public abstract record Response
    {
        public static Response Logoff = new LogoffResponse();
        public static Response WindowsAccountLogin = new WindowsAccountLoginResponse();
        public static Response ReturnPassword(string password) => new PasswordResponse(password);

        public record PasswordResponse(string Password) : Response;
        public record LogoffResponse : Response;
        public record WindowsAccountLoginResponse : Response;
    }
}