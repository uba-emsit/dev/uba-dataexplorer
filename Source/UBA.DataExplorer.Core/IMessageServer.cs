﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core;

public interface IMessageServer
{
    void RegisterListener(Listener listener);
    bool UnregisterListener(Listener listener);
    LockResult Lock(MessageServerObjectClass objectClass, int primaryKey);
}

public abstract record LockResult(ProcessingResult ProcessingResult, bool IsSuccess) : IDisposable
{
    public record Success(Action<LockResult> Unlock) : LockResult(ProcessingResult.Ok, true)
    {
        public override void Dispose() => Unlock(this);
    }

    public record Error(ProcessingResult ProcessingResult, string ErrorMessage) : LockResult(ProcessingResult, false);

    public record Locked(UserModel? OtherUser, DateTimeOffset LockedSince)
        : LockResult(ProcessingResult.Ok, false);

    public virtual void Dispose() { }
}

public enum ProcessingResult
{
    Ok,
    Failed,
    FailedAccessDenied,
    FailedInvalidOperation,
    FailedFormat,
    NotConnected,
    ErrorUnexpected,
    ErrorNotLicenced,
    ServerNotAvailable,
}

public class Listener(MessageServerObjectClass objectClass, Func<ObjectChangeReason, List<Change<long>>, Task> onChanged) : IDisposable
{
    public MessageServerObjectClass ObjectClass { get; } = objectClass;
    public Func<ObjectChangeReason, List<Change<long>>, Task> OnChanged { get; } = onChanged;
    public bool Disposed { get; private set; }
    public void Dispose() => Disposed = true;
}

public record Change<T>(int ParentKey, T PrimaryKey);

public enum ObjectChangeReason
{
    ObjectChange = 1,
    ObjectAddNew = 2,
    ObjectDelete = 3,
    TreeUpdate = 4
}

public enum MessageServerObjectClass
{
    TimeSeriesView,
    TimeSeries,
    TimeSeriesProperty,
    TimeSeriesAxis,
    TimeSeriesData,
    Descriptor,
    Tree,
    Trees
}