﻿using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IServiceFactory
{
    T GetService<T>(ConnectionModel connection) where T : IService;
}