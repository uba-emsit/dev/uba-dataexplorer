﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.Repositories;

public interface ITimeSeriesDataRepository : IRepository
{
    Task<Result<List<ITimeSeriesData>>> Read(ICollection<ITimeSeries> timeSeries, IHypothesis hypothesis,
        ITimeAxisAbsolute timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken);

    Task<Result<List<IMappedTimeSeriesData>>> ReadMapped(ICollection<ITimeSeries> timeSeries, IHypothesis hypothesis,
        ITimeAxisAbsolute timeAxis, TimeResolution timeResolution, CancellationToken cancellationToken);

    Task<Result<List<WriteResponse<ITimeSeriesData>>>> Write(
        ICollection<ITimeSeriesData> timeSeriesData, CancellationToken cancellationToken);
}