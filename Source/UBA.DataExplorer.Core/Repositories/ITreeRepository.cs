﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.Tree.Interfaces;

namespace UBA.DataExplorer.Core.Repositories;

public interface ITreeRepository : IRepository
{
    Task<Result<ITree>> Read(int primaryKey, CancellationToken cancellationToken);
    Task<Result<List<ITree>>> ReadByDimension(int dimensionNr, CancellationToken cancellationToken);
    Task<Result<List<ITree>>> ReadAll(CancellationToken cancellationToken);
    Task<Result<List<ITree>>> Read(List<int> requestPrimaryKeys, CancellationToken cancellationToken);
}