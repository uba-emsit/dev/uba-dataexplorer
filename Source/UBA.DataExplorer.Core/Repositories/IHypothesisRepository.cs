﻿using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IHypothesisRepository : IRepository
{
    Task<Result<IHypothesis>> Read(string id, CancellationToken cancellationToken);
    Task<Result<IHypothesis>> Read(int primaryKey, CancellationToken cancellationToken);
    Task<Result<List<IHypothesis>>> Read(CancellationToken cancellationToken);
}