﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.Repositories;

public interface ITimeSeriesViewRepository : IRepository
{
    Task<Result<ITimeSeriesViewTree>> ReadTree(CancellationToken cancellationToken);

    Task<Result<ITimeSeriesView>> Read(string id, CancellationToken cancellationToken);

    Task<Result<List<ITimeSeriesView>>> Read(List<string> idList, CancellationToken cancellationToken);

    Task<Result<List<ITimeSeriesView>>> Read(List<int> primaryKeys, CancellationToken cancellationToken);
}