﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IDimensionRepository : IRepository
{
    Task<Result<IDimension>> Read(string id, CancellationToken cancellationToken);
    Task<Result<IDimension>> Read(int primaryKey, CancellationToken cancellationToken);
    Task<Result<List<IDimension>>> Read(CancellationToken cancellationToken);
}