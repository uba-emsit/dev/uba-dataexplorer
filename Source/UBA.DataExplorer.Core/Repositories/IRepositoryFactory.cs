﻿using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IRepositoryFactory
{
    ILoginRepository GetLoginRepository();
    IDatabaseRepository GetDatabaseRepository();
    T GetRepository<T>(ConnectionModel connection) where T : IRepository;
}