﻿using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.Repositories;

public interface ITimeSeriesRepository : IRepository
{
    Task<Result<ITimeSeries>> Read(string id, CancellationToken cancellationToken);
    Task<Result<List<ITimeSeries>>> Read(List<string> idList, CancellationToken cancellationToken);
    Task<Result<List<ITimeSeries>>> ReadFromViewId(string timeSeriesViewId, CancellationToken cancellationToken);
    Task<Result<List<ITimeSeries>>> Read(List<int> primaryKeys, CancellationToken cancellationToken);
    Task<Result<List<ITimeSeries>>> ReadFromFilter(ITimeSeriesFilterSettings timeSeriesFilter, CancellationToken cancellationToken);
}