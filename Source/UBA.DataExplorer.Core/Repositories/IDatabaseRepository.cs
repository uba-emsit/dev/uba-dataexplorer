﻿using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IDatabaseRepository : IRepository
{
    /// <summary>
    /// Returns a list of available project databases the user can open. 
    /// </summary>
    /// <param name="login">The login to use</param>
    /// <param name="cancellationToken"></param>
    /// <returns>A list of objects containing information about the databases the user can open, or an error.</returns>
    Task<Result<List<DatabaseModel>>> GetAvailableProjectDatabases(LoginModel login, CancellationToken cancellationToken);

    /// <summary>
    /// Establishes a connection to the database.
    /// </summary>
    /// <param name="login">The login to use</param>
    /// <param name="database">The database to open</param>
    /// <param name="cancellationToken"></param>
    /// <returns>Information about the opened connection, or an error.</returns>
    Task<Result<(ConnectionModel connectionModel, IMessageServer messageServer)>> OpenConnection(LoginModel login, DatabaseModel database, CancellationToken cancellationToken);

    /// <summary>
    /// Stores the provided connection in the list of recently used connections.
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Result> SaveConnectionToRecentlyUsed(ConnectionModel connection, CancellationToken cancellationToken);

    /// <summary>
    /// Closes the connection to the database.
    /// </summary>
    /// <param name="connection"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Result> CloseConnection(ConnectionModel connection, CancellationToken cancellationToken);
}