﻿using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface IDescriptorRepository : IRepository
{
    Task<Result<List<IDescriptor>>> ReadByDimension(int dimensionPk, CancellationToken cancellationToken);
    Task<Result<List<IDescriptor>>> Read(List<int> requestPrimaryKeys, CancellationToken cancellationToken);
}