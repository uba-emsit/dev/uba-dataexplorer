﻿using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.Core.Repositories;

public interface ILoginRepository : IRepository
{
    /// <summary>
    /// Logs the user in.
    /// </summary>
    /// <param name="username"></param>
    /// <param name="password"></param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Result<LoginModel>> Login(string username, string password, CancellationToken cancellationToken);

    /// <summary>
    /// Logs the user in using the Windows account.
    /// </summary>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Result<LoginModel>> LoginWithWindowsAccount(CancellationToken cancellationToken);

    /// <summary>
    /// Logs the user out.
    /// </summary>
    /// <param name="login">The login that should be logged out</param>
    /// <param name="cancellationToken"></param>
    /// <returns></returns>
    Task<Result> Logout(LoginModel login, CancellationToken cancellationToken);
}