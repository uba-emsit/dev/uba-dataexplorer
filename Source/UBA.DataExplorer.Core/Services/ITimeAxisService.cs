﻿using UBA.DataExplorer.Domain.TimeSeries.Interfaces;

namespace UBA.DataExplorer.Core.Services;

public interface ITimeAxisService : IService
{
    ITimeAxisAbsolute CreateAbsolute(DateTimeOffset from, DateTimeOffset to);
}
