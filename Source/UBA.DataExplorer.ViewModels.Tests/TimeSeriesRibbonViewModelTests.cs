﻿using Shouldly;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimeSeriesRibbonViewModelTests
{
    private TimeSeriesRibbonViewModel _viewModel = null!;

    [TestInitialize]
    public void Setup()
    {
        _viewModel = new()
        {
            TimePeriodFrom = new() { Date = new DateTime(2000, 1, 1), TimeResolution = TimeResolution.Year },
            TimePeriodTo = new() { Date = new DateTime(2010, 1, 1), TimeResolution = TimeResolution.Year }
        };
    }

    [TestMethod]
    public void InvertTimePeriod()
    {
        _viewModel.InvertTimePeriod();
        _viewModel.TimePeriodFrom.Date.ShouldBe(new DateTime(2010, 1, 1));
        _viewModel.TimePeriodTo.Date.ShouldBe(new DateTime(2000, 1, 1));
    }
}