using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Login;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UI.Resources;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels;
using UBA.DataExplorer.UseCases.Databases;
using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class MainWindowViewModelTests
{
    [TestMethod]
    public void CurrentUserShowsReLoginMessageTest()
    {
        // arrange
        var dbLabel = "label";
        var userName = "user";
        var senderMock = Mock.Of<ISender>();
        var msgBox = Mock.Of<IMessageBoxHelper>();
        Mock.Get(senderMock)
            .Setup(x => x.Send(It.IsAny<CloseDatabaseConnectionCommand>(), It.IsAny<CancellationToken>()));

        var ccs = new CurrentContextService();
        var dsMock = Mock.Of<IDataService>();

        var provider = new ServiceCollection()
            .AddLogging()
            .AddSingleton(_ => ccs)
            .AddSingleton<LoginViewModel>()
            .AddSingleton<DatabaseSelectorViewModel>()
            .AddViewModels()
            .AddTransient<IRepositoryFactory>(p => new TestBackendFactory(p))
            .AddSingleton(_ => senderMock)
            .AddSingleton(_ => msgBox)
            .AddSingleton(_ => dsMock)
            .BuildServiceProvider();

        DependencyInjection.SetServiceProvider(provider);

        var sender = provider.GetRequiredService<ISender>();

        var ms = Mock.Of<IMessageServer>();
        var cm = new ConnectionModel(new DatabaseModel(1, "", "", "", dbLabel, 1));
        var cl = new ChangesLoader(cm, ms, sender);

        var model = new MainWindowViewModel();

        ccs.SetLogin(new LoginModel(new UserModel(userName, "userid"), true));
        ccs.SetDatabase(cm, cl, new ObjectLocker(ms));

        // act
        ccs.ClearLoginAndShowReLoginMessage();

        // assert
        model.CurrentUser.ShouldBe(Resources.ReLogin);
        model.CurrentUser.ShouldNotBe(userName);
        model.SelectedDatabase.ShouldBe(dbLabel);

    }
}