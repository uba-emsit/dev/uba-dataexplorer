using Moq;
using Shouldly;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UI.ViewModels;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class IdOrLabelViewModelTests
{
    const string Id = "Id";
    const string Label = "Name";

    [TestMethod]
    public void InitialState()
    {
        // arrange
        var (viewModel, _) = GetViewModel();

        // act
        //  none

        // assert
        viewModel.MainText.ShouldBe(Label);
        viewModel.SubText.ShouldBe("");
    }

    [TestMethod]
    public void ToggleOnceTest()
    {
        // arrange
        var (viewModel, viewScope) = GetViewModel();

        // act
        viewScope.NextIdLabel(); // To IdLabel

        // assert
        viewModel.MainText.ShouldBe(Id);
        viewModel.SubText.ShouldBe(Label);
    }

    [TestMethod]
    public void ToggleTwiceTest()
    {
        // arrange
        var (viewModel, viewScope) = GetViewModel();

        // act
        viewScope.NextIdLabel(); // To IdLabel
        viewScope.NextIdLabel(); // To LabelId

        // assert
        viewModel.MainText.ShouldBe(Label);
        viewModel.SubText.ShouldBe(Id);
    }

    [TestMethod]
    public void ToggleThreeTimesTest()
    {
        // arrange
        var (viewModel, viewScope) = GetViewModel();

        // act
        viewScope.NextIdLabel(); // To IdLabel
        viewScope.NextIdLabel(); // To LabelId
        viewScope.NextIdLabel(); // To Id

        // assert
        viewModel.MainText.ShouldBe(Id);
        viewModel.SubText.ShouldBe("");
    }

    [TestMethod]
    public void ToggleFourTimesTest()
    {
        // arrange
        var (viewModel, viewScope) = GetViewModel();

        // act
        viewScope.NextIdLabel(); // To IdLabel
        viewScope.NextIdLabel(); // To LabelId
        viewScope.NextIdLabel(); // To Id
        viewScope.NextIdLabel(); // To Label

        // assert
        viewModel.MainText.ShouldBe(Label);
        viewModel.SubText.ShouldBe("");
    }

    [TestMethod]
    public void DirectSetTest()
    {
        // arrange
        var (viewModel, viewScope) = GetViewModel();

        // act
        viewScope.SetIdLabel(IdLabel.LabelId);

        // assert
        viewModel.MainText.ShouldBe(Label);
        viewModel.SubText.ShouldBe(Id);
    }

    static (IdLabelToggleViewModel, ViewScope) GetViewModel()
    {
        var viewScope = new ViewScope();
        var model = Mock.Of<IIdAndLabelModel>();

        Mock.Get(model)
            .SetupGet(p => p.Id)
            .Returns(Id);
        Mock.Get(model)
            .SetupGet(p => p.Label)
            .Returns(Label);

        var viewModel = new IdLabelToggleViewModel(model, viewScope);
        return (viewModel, viewScope);
    }
}
