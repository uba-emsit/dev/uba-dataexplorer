using Shouldly;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimePeriodViewModelTests
{
    [TestMethod]
    public void TestTimePeriodViewModel()
    {
        // arrange
        var timePeriod = new TimePeriodModel()
        {
            Date = new DateTime(2024, 1, 1),
            TimeResolution = TimeResolution.Year
        };
        var vm = new TimePeriodViewModel();
        var raised = false;
        vm.PropertyChanged += (_, _) => raised = true;

        // act
        vm.SetFromTimePeriod(timePeriod);

        // assert
        vm.ShouldNotBeNull();
        vm.Date.ShouldBe(new DateTime(2024, 1, 1));
        vm.TimeResolution.ShouldBe(TimeResolution.Year);
        raised.ShouldBeTrue();
    }
}