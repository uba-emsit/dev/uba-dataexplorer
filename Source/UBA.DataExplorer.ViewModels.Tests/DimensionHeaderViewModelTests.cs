using System.Collections.ObjectModel;
using System.Reactive.Linq;
using System.Reactive.Subjects;
using Avalonia.Threading;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Dimensions.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.Domain.Tree.Interfaces;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UI.Models.Descriptors;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;
using UBA.DataExplorer.UseCases.Abstractions.Logging;
using UBA.DataExplorer.UseCases.Databases;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class DimensionHeaderViewModelTests
{
    [TestMethod]
    public async Task SearchMainTextTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.SearchText = "Name2";

        // Assert
        viewScope.IdLabel.ShouldBe(IdLabel.Label);
        model.FilteredItems.Count.ShouldBe(1);
        model.FilteredTrees.Count.ShouldBe(1);
    }

    [TestMethod]
    public async Task SearchSubTextFailsTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.SearchText = "Id2";

        // Assert
        viewScope.IdLabel.ShouldBe(IdLabel.Label);
        model.FilteredItems.Count.ShouldBe(0);
        model.FilteredTrees.Count.ShouldBe(0);
    }

    [TestMethod]
    public async Task SearchSubTextTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        viewScope.SetIdLabel(IdLabel.LabelId);

        // Act
        model.SearchText = "Id2";

        // Assert
        model.FilteredItems.Count.ShouldBe(1);
        model.FilteredTrees.Count.ShouldBe(1);
    }

    [TestMethod]
    public async Task SearchMainTextAsIdTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        viewScope.SetIdLabel(IdLabel.IdLabel);

        // Act
        model.SearchText = "Id2";

        // Assert
        model.FilteredItems.Count.ShouldBe(1);
        model.FilteredTrees.Count.ShouldBe(1);
    }


    [TestMethod]
    public async Task SortDefaultTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        //  none

        // Assert
        model.FilteredItems.Count.ShouldBe(3);
        model.FilteredItems.First().MainText.ShouldBe("Name1");

        model.FilteredTrees.Count.ShouldBe(3);
        model.FilteredTrees.First().MainText.ShouldBe("Name1");
    }

    [TestMethod]
    public async Task SortToggleOnceTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.ToggleSortingCommand.Execute(null); // from alphanumeric to SortNr

        // Assert
        model.FilteredItems.Count.ShouldBe(3);
        model.FilteredItems.First().MainText.ShouldBe("Name10");

        model.FilteredTrees.Count.ShouldBe(3);
        model.FilteredTrees.First().MainText.ShouldBe("Name10");
    }


    [TestMethod]
    public async Task SortToggleTwiceTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.ToggleSortingCommand.Execute(null); // from alphanumeric to SortNr
        model.ToggleSortingCommand.Execute(null); // back to alphanumeric

        // Assert
        model.FilteredItems.Count.ShouldBe(3);
        model.FilteredItems.First().MainText.ShouldBe("Name1");

        model.FilteredTrees.Count.ShouldBe(3);
        model.FilteredTrees.First().MainText.ShouldBe("Name1");
    }

    [TestMethod]
    public async Task SortAndSearchTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.ToggleSortingCommand.Execute(null); // from alphanumeric to SortNr

        model.SearchText = "Name1";

        // Assert
        model.FilteredItems.Count.ShouldBe(2);
        model.FilteredItems.First().MainText.ShouldBe("Name10");
        model.FilteredItems.Last().MainText.ShouldBe("Name1");

        model.FilteredTrees.Count.ShouldBe(2);
        model.FilteredTrees.First().MainText.ShouldBe("Name10");
        model.FilteredTrees.Last().MainText.ShouldBe("Name1");
    }

    [TestMethod]
    public async Task SortAndSearchTwiceTest()
    {
        // Arrange
        var viewScope = new ViewScope();
        var model = await GetDimensionHeaderViewModel(viewScope);

        // Act
        model.ToggleSortingCommand.Execute(null); // from alphanumeric to SortNr

        model.SearchText = "Name1";

        model.ToggleSortingCommand.Execute(null); // back to alphanumeric 

        // Assert
        model.FilteredItems.Count.ShouldBe(2);
        model.FilteredItems.First().MainText.ShouldBe("Name1");
        model.FilteredItems.Last().MainText.ShouldBe("Name10");

        model.FilteredTrees.Count.ShouldBe(2);
        model.FilteredTrees.First().MainText.ShouldBe("Name1");
        model.FilteredTrees.Last().MainText.ShouldBe("Name10");
    }

    async Task<DimensionHeaderViewModel> GetDimensionHeaderViewModel(ViewScope viewScope)
    {
        const int dimPk1 = 1;
        const int selectedDescriptorPk1 = 1;
        const int unselectedDescriptorPk2 = 2;
        const int unselectedDescriptorPk3 = 10;

        var descriptorRepository = Mock.Of<IDescriptorRepository>();
        var descriptor1 = GetDescriptorMock(selectedDescriptorPk1);
        var descriptor2 = GetDescriptorMock(unselectedDescriptorPk2);
        var descriptor3 = GetDescriptorMock(unselectedDescriptorPk3);
        var descriptors = new List<IDescriptor>([descriptor1, descriptor2, descriptor3]);

        var treeNode = GetTreeMock(new List<IDescriptor> { descriptor1, descriptor2, descriptor3 });
        var treeNodes = new List<ITree> { treeNode };

        Mock.Get(descriptorRepository)
            .Setup(p => p.ReadByDimension(It.IsAny<int>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(descriptors);

        var ccs = new CurrentContextService();
        ccs.SetDatabase(new ConnectionModel(null!), null!, null!);

        var mockDataService = new Mock<IDataService>();
        var descriptorsByDimensionSubject = new Subject<IReadOnlyDictionary<int, List<IDescriptor>>?>();
        var treeNodesByDimensionSubject = new Subject<IReadOnlyDictionary<int, List<ITree>>?>();

        mockDataService
            .Setup(ds => ds.DescriptorsByDimension)
            .Returns(new ReadOnlyDictionary<int, List<IDescriptor>>(
                new Dictionary<int, List<IDescriptor>> {
                    { dimPk1, descriptors }
                }));

        mockDataService
            .Setup(ds => ds.TreeNodesByDimension)
            .Returns(new ReadOnlyDictionary<int, List<ITree>>(
                new Dictionary<int, List<ITree>> {
                    { dimPk1, treeNodes }
                }));

        mockDataService
            .Setup(ds => ds.DescriptorsByDimensionObservable)
            .Returns(descriptorsByDimensionSubject.AsObservable());

        mockDataService
            .Setup(ds => ds.TreeNodesByDimensionObservable)
            .Returns(treeNodesByDimensionSubject.AsObservable());

        var provider = new ServiceCollection()
            .AddLogging()
            .AddMediatR(c =>
                c.RegisterServicesFromAssemblyContaining<LoadAvailableProjectDatabasesQuery>()
                    .AddOpenBehavior(typeof(RequestLoggingPipelineBehavior<,>)))
            .AddTransient<IRepositoryFactory, TestBackendFactory>(p => new TestBackendFactory(p))
            .AddTransient(_ => descriptorRepository)
            .AddSingleton(_ => ccs)
            .AddSingleton(_ => mockDataService.Object)
            .BuildServiceProvider();

        UI.DependencyInjection.SetServiceProvider(provider);

        var mockDescriptorFilter = Mock.Of<IDescriptorFilterSettings>();

        Mock.Get(mockDescriptorFilter)
            .SetupGet(p => p.DimensionPrimaryKey)
            .Returns(dimPk1);
        Mock.Get(mockDescriptorFilter)
            .SetupGet(p => p.DescriptorPrimaryKeys)
            .Returns([selectedDescriptorPk1]); // Only the first descriptor is selected
        Mock.Get(mockDescriptorFilter)
            .SetupGet(p => p.TreeNodePrimaryKeys)
            .Returns([selectedDescriptorPk1]);

        var filter = new ColumnDescriptorFilter(mockDescriptorFilter, dimPk1, viewScope);
        var model = new DimensionHeaderViewModel("header", filter);

        _ = model.LoadDataOnStartUpCommand.ExecuteAsync(CancellationToken.None);
        Dispatcher.UIThread.RunJobs();

        descriptorsByDimensionSubject.OnNext(new ReadOnlyDictionary<int, List<IDescriptor>>(
            new Dictionary<int, List<IDescriptor>> {
                { dimPk1, descriptors }
            }));

        treeNodesByDimensionSubject.OnNext(new ReadOnlyDictionary<int, List<ITree>>(
            new Dictionary<int, List<ITree>> {
                { dimPk1, treeNodes }
            }));

        Dispatcher.UIThread.RunJobs();

        return model;
    }

    static IDescriptor GetDescriptorMock(int pk)
    {
        var descriptor = Mock.Of<IDescriptor>();
        Mock.Get(descriptor)
            .SetupGet(d => d.PrimaryKey)
            .Returns(pk);
        Mock.Get(descriptor)
            .SetupGet(d => d.Id)
            .Returns("Id" + pk);
        Mock.Get(descriptor)
            .SetupGet(d => d.Label)
            .Returns("Name" + pk);
        Mock.Get(descriptor)
            .SetupGet(d => d.SortNr)
            .Returns(100 - pk); // higher sort number for lower keys

        return descriptor;
    }

    ITree GetTreeMock(List<IDescriptor> descriptors)
    {
        var parentDescriptor = GetDescriptorMock(5555);

        var mockTreeParentNode = new Mock<ITreeNodeNode>();
        mockTreeParentNode.SetupAllProperties();
        mockTreeParentNode.SetupGet(n => n.Descriptor).Returns(parentDescriptor);
        mockTreeParentNode.SetupGet(n => n.PrimaryKey).Returns(1234);

        var childNodes = descriptors.Select(descriptor =>
        {
            var mockTreeNodeNode = new Mock<ITreeNodeNode>();
            mockTreeNodeNode.SetupAllProperties();
            mockTreeNodeNode.SetupGet(n => n.Descriptor).Returns(descriptor);
            mockTreeNodeNode.SetupGet(n => n.PrimaryKey).Returns(descriptor.PrimaryKey);
            mockTreeNodeNode.SetupGet(n => n.SortNr).Returns(descriptor.SortNr);

            var mockChildEntityTreeNode = new Mock<IEntityTreeNode<ITreeNodeNode>>();
            mockChildEntityTreeNode.SetupAllProperties();
            mockChildEntityTreeNode.SetupGet(n => n.Entity).Returns(mockTreeNodeNode.Object);
            mockChildEntityTreeNode.SetupGet(n => n.PrimaryKey).Returns(descriptor.PrimaryKey);
            mockChildEntityTreeNode.SetupGet(n => n.Children).Returns(new ReadOnlyCollection<IEntityTreeNode<ITreeNodeNode>>(new List<IEntityTreeNode<ITreeNodeNode>>()));

            return mockChildEntityTreeNode.Object;
        }).ToList();

        var mockParentEntityTreeNode = new Mock<IEntityTreeNode<ITreeNodeNode>>();
        mockParentEntityTreeNode.SetupAllProperties();
        mockParentEntityTreeNode.SetupGet(n => n.Entity).Returns(mockTreeParentNode.Object);
        mockParentEntityTreeNode.SetupGet(n => n.PrimaryKey).Returns(1234);
        mockParentEntityTreeNode.SetupGet(n => n.Children).Returns(new ReadOnlyCollection<IEntityTreeNode<ITreeNodeNode>>(childNodes));

        var mockTreeNodeTree = new Mock<ITreeNodeTree>();
        mockTreeNodeTree.SetupAllProperties();
        mockTreeNodeTree.SetupGet(t => t.RootNode).Returns(mockParentEntityTreeNode.Object);

        var mockTree = new Mock<ITree>();
        mockTree.SetupAllProperties();
        mockTree.SetupGet(t => t.Structure).Returns(mockTreeNodeTree.Object);

        return mockTree.Object;
    }

}





