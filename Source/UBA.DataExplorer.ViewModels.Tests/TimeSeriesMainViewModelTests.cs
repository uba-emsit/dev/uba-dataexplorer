using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Databases;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;
using UBA.DataExplorer.UseCases.Databases;
using UBA.DataExplorer.UseCases.MessageServer;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimeSeriesMainViewModelTests
{
    [TestMethod]
    public void SwitchDatabaseShouldCloseAllTabsTest()
    {
        // arrange
        FrontEndServiceLocator.ResetScope();

        var senderMock = Mock.Of<ISender>();
        Mock.Get(senderMock)
            .Setup(x => x.Send(It.IsAny<CloseDatabaseConnectionCommand>(), It.IsAny<CancellationToken>()));

        var provider = new ServiceCollection()
            .AddLogging()
            .AddSingletonServices()
            .AddViewModels()
            .AddTransient<IRepositoryFactory>(p => new TestBackendFactory(p))
            .AddSingleton(_ => senderMock)
            .BuildServiceProvider();

        var sender = provider.GetRequiredService<ISender>();

        var ccs = provider.GetRequiredService<CurrentContextService>();
        var ms = Mock.Of<IMessageServer>();
        var cm = new ConnectionModel(new DatabaseModel(1, "", "", "", "", 1));
        var cl = new ChangesLoader(cm, ms, sender);

        ccs.SetDatabase(cm, cl, new ObjectLocker(ms));

        DependencyInjection.SetServiceProvider(provider);
        var vm1 = FrontEndServiceLocator.Get.GetRequiredService<TimeSeriesMainViewModel>();

        vm1.AddTab("Test1", "1");
        vm1.AddTab("Test2", "2");

        var vm1OpenTabs = vm1.TabItems;

        // act

        // SwitchDatabaseCommand executes
        // - FrontEndServiceLocator.ResetScope() which resets the scope of the service provider
        // - CurrentContextService.ClearDatabase() which sets the Connection to null
        new MainWindowViewModel().SwitchDatabaseCommand.Execute(null);

        // assert
        ccs.Connection.ShouldBeNull();
        Mock.Get(senderMock)
            .Verify(x => x.Send(It.IsAny<CloseDatabaseConnectionCommand>(), It.IsAny<CancellationToken>()), Times.Once);

        // get a new scoped view model
        ccs.SetDatabase(cm, cl, new ObjectLocker(ms));
        var vm2 = FrontEndServiceLocator.Get.GetRequiredService<TimeSeriesMainViewModel>();

        // new vm should have one tab (home)
        vm2.TabItems.Count.ShouldBe(1);

        // previous vm should have no tabs
        vm1OpenTabs.Count.ShouldBe(0);
    }
}