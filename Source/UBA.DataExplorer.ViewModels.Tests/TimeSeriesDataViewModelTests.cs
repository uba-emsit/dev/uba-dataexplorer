﻿using Moq;
using Shouldly;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimeSeriesDataViewModelTests
{
#pragma warning disable CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.
    private TimeSeriesDataViewModel _viewModel;
    private Mock<ITimeSeriesDataCoordinates> _coordinatesMock;
    private Mock<ITimeSeriesData> _timeSeriesDataMock;
    private Mock<ITimeSeries> _timeSeriesMock;
#pragma warning restore CS8618 // Non-nullable field must contain a non-null value when exiting constructor. Consider declaring as nullable.

    [TestInitialize]
    public void Setup()
    {
        _coordinatesMock = new Mock<ITimeSeriesDataCoordinates>();
        _timeSeriesDataMock = new Mock<ITimeSeriesData>();
        _timeSeriesMock = new Mock<ITimeSeries>();

        _coordinatesMock.Setup(c => c.TimePeriod1)
            .Returns(new TimePeriodModel { Date = new DateTime(2024, 1, 1), TimeResolution = TimeResolution.Year });

        _timeSeriesDataMock.Setup(d => d.Coordinates).Returns(_coordinatesMock.Object);
        _timeSeriesDataMock.Setup(d => d.Value).Returns(100.5);
        _timeSeriesDataMock.Setup(d => d.HasDocumentation).Returns(true);
        _timeSeriesDataMock.Setup(d => d.IsWriteProtected).Returns(false);
        _timeSeriesDataMock.Setup(d => d.NoValueReason).Returns("Test reason");

        _timeSeriesMock.Setup(t => t.IsVirtual).Returns(true);

        _viewModel = TimeSeriesDataViewModel
            .CreateFrom(_timeSeriesMock.Object, [_timeSeriesDataMock.Object])
            .First();
    }

    [TestMethod]
    public void ViewModel_ShouldInitialize_WithCorrectValues()
    {
        _viewModel.ShouldNotBeNull();
        _viewModel.Coordinates.ShouldNotBeNull();
        _viewModel.Period.ShouldBe("2024");
        _viewModel.Value.ShouldBe(100.5);
        _viewModel.HasDocumentation.ShouldBeTrue();
        _viewModel.IsMapped.ShouldBeFalse();
        _viewModel.IsInherited.ShouldBeFalse();
        _viewModel.IsWriteProtected.ShouldBeFalse();
        _viewModel.IsVirtual.ShouldBeTrue();
        _viewModel.IsFiltered.ShouldBeFalse();
        _viewModel.NoValueReason.ShouldBe("Test reason");
    }

    [TestMethod]
    public void ViewModel_ShouldSet_IsDirty_WhenPropertyChanges()
    {
        _viewModel.IsDirty = false;
        _viewModel.Value = 200.0;
        _viewModel.IsDirty.ShouldBeTrue();
    }

    [TestMethod]
    public void CreateFrom_ShouldGenerateCorrectViewModels_FromMappedTimeSeriesData()
    {
        var mappedDataMock = new Mock<IMappedTimeSeriesData>();
        mappedDataMock.Setup(m => m.Coordinates).Returns(_coordinatesMock.Object);
        mappedDataMock.Setup(m => m.Value).Returns(50.0);
        mappedDataMock.Setup(m => m.MappingResult).Returns(MappingResult.Interpolated);

        var dataList = new List<IMappedTimeSeriesData> { mappedDataMock.Object };

        var result = TimeSeriesDataViewModel.CreateFrom(_timeSeriesMock.Object, dataList).ToList();

        result.ShouldNotBeNull();
        result.ShouldHaveSingleItem();
        result[0].Value.ShouldBe(50.0);
        result[0].IsMapped.ShouldBeTrue();
    }

    [TestMethod]
    public void CreateEmpty_ShouldCreate_ViewModelWithCorrectDate()
    {
        var date = new DateTime(2025, 1, 1);
        var result = TimeSeriesDataViewModel.CreateEmpty(date, TimeResolution.Year);

        result.ShouldNotBeNull();
        result.Coordinates.ShouldNotBeNull();
        result.Coordinates.TimePeriod1.Date.ShouldBe(date);
    }

    [TestMethod]
    public void UpdateProperties_ShouldUpdateAllProperties()
    {
        // Arrange
        var newTimeSeriesData = new Mock<ITimeSeriesData>();
        newTimeSeriesData.Setup(d => d.Coordinates).Returns(_coordinatesMock.Object);
        newTimeSeriesData.Setup(d => d.Value).Returns(200);
        newTimeSeriesData.Setup(d => d.HasDocumentation).Returns(true);
        newTimeSeriesData.Setup(d => d.IsWriteProtected).Returns(false);
        newTimeSeriesData.Setup(d => d.NoValueReason).Returns("Test reason");

        var newViewModel = TimeSeriesDataViewModel
            .CreateFrom(_timeSeriesMock.Object, [newTimeSeriesData.Object])
            .First();

        // Act
        _viewModel.UpdateProperties(newViewModel);

        // Assert
        Assert.AreEqual(newViewModel.Period, _viewModel.Period);
        Assert.AreEqual(newViewModel.HasDocumentation, _viewModel.HasDocumentation);
        Assert.AreEqual(newViewModel.NoValueReason, _viewModel.NoValueReason);
        Assert.AreEqual(newViewModel.IsMapped, _viewModel.IsMapped);
        Assert.AreEqual(newViewModel.IsVirtual, _viewModel.IsVirtual);
        Assert.AreEqual(newViewModel.IsInherited, _viewModel.IsInherited);
        Assert.AreEqual(newViewModel.IsWriteProtected, _viewModel.IsWriteProtected);
        Assert.AreEqual(newViewModel.IsFiltered, _viewModel.IsFiltered);
        Assert.AreEqual(newViewModel.Value, _viewModel.Value);
    }

    [TestMethod]
    public void UpdateProperties_ShouldNotUpdateValue_WhenIsDirtyIsTrue()
    {
        // Arrange
        var newTimeSeriesData = new Mock<ITimeSeriesData>();
        newTimeSeriesData.Setup(d => d.Coordinates).Returns(_coordinatesMock.Object);
        newTimeSeriesData.Setup(d => d.Value).Returns(200);
        newTimeSeriesData.Setup(d => d.HasDocumentation).Returns(true);
        newTimeSeriesData.Setup(d => d.IsWriteProtected).Returns(false);
        newTimeSeriesData.Setup(d => d.NoValueReason).Returns("Test reason");

        var newViewModel = TimeSeriesDataViewModel
            .CreateFrom(_timeSeriesMock.Object, [newTimeSeriesData.Object])
            .First();

        // Act
        _viewModel.Value = 300;
        _viewModel.UpdateProperties(newViewModel);

        // Assert
        Assert.AreEqual(newViewModel.Period, _viewModel.Period);
        Assert.AreEqual(newViewModel.HasDocumentation, _viewModel.HasDocumentation);
        Assert.AreEqual(newViewModel.NoValueReason, _viewModel.NoValueReason);
        Assert.AreEqual(newViewModel.IsMapped, _viewModel.IsMapped);
        Assert.AreEqual(newViewModel.IsVirtual, _viewModel.IsVirtual);
        Assert.AreEqual(newViewModel.IsInherited, _viewModel.IsInherited);
        Assert.AreEqual(newViewModel.IsWriteProtected, _viewModel.IsWriteProtected);
        Assert.AreEqual(newViewModel.IsFiltered, _viewModel.IsFiltered);
        Assert.IsNotNull(newViewModel.Value);
        Assert.AreEqual(_viewModel.Value, 300.0);
    }
}