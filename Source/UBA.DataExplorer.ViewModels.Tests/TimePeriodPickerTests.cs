using Avalonia.Controls;
using Shouldly;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;
using UBA.DataExplorer.UI.Views.Controls;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimePeriodPickerTests
{
    [TestMethod]
    public void TestTimePeriodPicker()
    {
        // arrange
        var vm = new TimePeriodViewModel();
        vm.SetFromTimePeriod(new TimePeriodModel { Date = new DateTime(2025, 1, 1), TimeResolution = TimeResolution.Year });
        var picker = new TimePeriodPicker
        {
            DataContext = vm
        };
        var raised = false;
        vm.PropertyChanged += (_, _) => raised = true;

        // act
        var datePicker = picker.Content as DatePicker;
        datePicker.ShouldNotBeNull();
        datePicker.SelectedDate = new DateTime(2026, 1, 1);

        // assert
        picker.ShouldNotBeNull();
        raised.ShouldBeTrue();
        vm.Date.ShouldBe(new DateTime(2026, 1, 1));
    }
}