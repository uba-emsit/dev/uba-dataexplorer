using System.Collections.ObjectModel;
using MediatR;
using Microsoft.Extensions.DependencyInjection;
using Moq;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UI;
using UBA.DataExplorer.UI.Models.TimeSeries;
using UBA.DataExplorer.UI.Services;
using UBA.DataExplorer.UI.ViewModels.Descriptor;
using UBA.DataExplorer.UI.ViewModels.TimeSeries;
using UBA.DataExplorer.UseCases.TimeSeries;
using UBA.DataExplorer.UseCases.TimeSeriesView;

namespace UBA.DataExplorer.ViewModels.Tests;

[TestClass]
public class TimeSeriesDataViewerViewModelTests
{
    private readonly IServiceProvider _provider;
    private readonly ISender _sender;
    private readonly IMessageBoxHelper _msgBox;
    private readonly CurrentContextService _ccs;

    public TimeSeriesDataViewerViewModelTests()
    {
        var tsv = Mock.Of<ITimeSeriesView>();
        _msgBox = Mock.Of<IMessageBoxHelper>();
        var senderMock = Mock.Of<ISender>();
        Mock.Get(senderMock)
            .Setup(x => x.Send(It.IsAny<TimeSeriesViewByIdQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(Result.Success(tsv));

        _provider = new ServiceCollection()
            .AddLogging()
            .AddSingleton(_ => new CurrentContextService())
            .AddSingleton(_ => senderMock)
            .AddSingleton(_ => _msgBox)
            .BuildServiceProvider();

        FrontEndServiceLocator.SetProvider(_provider);

        _sender = _provider.GetRequiredService<ISender>();
        _ccs = _provider.GetRequiredService<CurrentContextService>();
    }

    [TestMethod]
    public async Task ProcessTimeSeriesResultsInNoDataTest()
    {
        await RunTest(Error.NoData(""), Times.Never());
    }

    [TestMethod]
    public async Task ProcessTimeSeriesResultsInMessageBoxTest1()
    {
        await RunTest(Error.NotFound("", ""), Times.Once());
    }

    [TestMethod]
    public async Task ProcessTimeSeriesResultsInMessageBoxTest2()
    {
        await RunTest(Error.Failure(x => x.BackendErrors.GeneralBackendFailure, ""), Times.Once());
    }

    [TestMethod]
    public async Task SaveDataSuccessTest()
    {
        var timeSeriesDataMock = new Mock<ITimeSeriesData>();
        var coordinatesMock = new Mock<ITimeSeriesDataCoordinates>();
        var hypothesisMock = new Mock<IHypothesis>();

        coordinatesMock.Setup(c => c.TimePeriod1)
            .Returns(new TimePeriodModel { Date = new DateTime(2024, 1, 1), TimeResolution = TimeResolution.Year });

        timeSeriesDataMock.Setup(d => d.Coordinates).Returns(coordinatesMock.Object);
        timeSeriesDataMock.Setup(d => d.TimeSeriesPrimaryKey).Returns(4711);
        timeSeriesDataMock.Setup(d => d.Hypothesis).Returns(hypothesisMock.Object);
        timeSeriesDataMock.Setup(d => d.IsWriteProtected).Returns(false);

        var writeResponses = new List<WriteResponse<ITimeSeriesData>>
    {
        new WriteResponse<ITimeSeriesData>(timeSeriesDataMock.Object, WriteResponseType.Updated)
    };

        var timeSeriesDataVm = TimeSeriesDataViewModel
            .CreateFrom(Mock.Of<ITimeSeries>(), new List<ITimeSeriesData> { timeSeriesDataMock.Object })
            .First();

        timeSeriesDataVm.Value = 1;

        await RunSaveDataTest(Result.Success(writeResponses), timeSeriesDataVm, Times.Never());

        Assert.IsFalse(timeSeriesDataVm.IsDirty, "The isDirty property should be false.");
    }


    [TestMethod]
    public async Task SaveDataFailureTest()
    {
        var timeSeriesDataMock = new Mock<ITimeSeriesData>();
        var coordinatesMock = new Mock<ITimeSeriesDataCoordinates>();
        var hypothesisMock = new Mock<IHypothesis>();

        coordinatesMock.Setup(c => c.TimePeriod1)
            .Returns(new TimePeriodModel { Date = new DateTime(2024, 1, 1), TimeResolution = TimeResolution.Year });

        timeSeriesDataMock.Setup(d => d.Coordinates).Returns(coordinatesMock.Object);
        timeSeriesDataMock.Setup(d => d.TimeSeriesPrimaryKey).Returns(4711);
        timeSeriesDataMock.Setup(d => d.Hypothesis).Returns(hypothesisMock.Object);
        timeSeriesDataMock.Setup(d => d.IsWriteProtected).Returns(false);

        var timeSeriesDataVm = TimeSeriesDataViewModel
            .CreateFrom(Mock.Of<ITimeSeries>(), new List<ITimeSeriesData> { timeSeriesDataMock.Object })
            .First();

        timeSeriesDataVm.Value = 1;

        await RunSaveDataTest(Result.Failure<List<WriteResponse<ITimeSeriesData>>>(Error.Failure(x => x.DomainErrors.NoData, "")), timeSeriesDataVm, Times.Once());
    }

    [TestMethod]
    public async Task SaveDataPartialUpdateTest()
    {
        var timeSeriesDataMock = new Mock<ITimeSeriesData>();
        var coordinatesMock = new Mock<ITimeSeriesDataCoordinates>();
        var hypothesisMock = new Mock<IHypothesis>();

        coordinatesMock.Setup(c => c.TimePeriod1)
            .Returns(new TimePeriodModel { Date = new DateTime(2024, 1, 1), TimeResolution = TimeResolution.Year });

        timeSeriesDataMock.Setup(d => d.Coordinates).Returns(coordinatesMock.Object);
        timeSeriesDataMock.Setup(d => d.TimeSeriesPrimaryKey).Returns(4711);
        timeSeriesDataMock.Setup(d => d.Hypothesis).Returns(hypothesisMock.Object);
        timeSeriesDataMock.Setup(d => d.IsWriteProtected).Returns(false);

        var writeResponses = new List<WriteResponse<ITimeSeriesData>>
    {
        new WriteResponse<ITimeSeriesData>(timeSeriesDataMock.Object, WriteResponseType.Updated)
    };

        var timeSeriesDataVm = TimeSeriesDataViewModel
            .CreateFrom(Mock.Of<ITimeSeries>(), new List<ITimeSeriesData> { timeSeriesDataMock.Object })
            .First();

        timeSeriesDataVm.Value = 1;

        // Modify the mock to simulate a partial update
        var slightlyDifferentTimeSeriesDataMock = new Mock<ITimeSeriesData>();
        slightlyDifferentTimeSeriesDataMock.Setup(d => d.Coordinates).Returns(coordinatesMock.Object);
        slightlyDifferentTimeSeriesDataMock.Setup(d => d.TimeSeriesPrimaryKey).Returns(4711);
        slightlyDifferentTimeSeriesDataMock.Setup(d => d.Hypothesis).Returns(hypothesisMock.Object);
        slightlyDifferentTimeSeriesDataMock.Setup(d => d.IsWriteProtected).Returns(false);
        slightlyDifferentTimeSeriesDataMock.Setup(d => d.Value).Returns(2); // Different value

        var partialWriteResponses = new List<WriteResponse<ITimeSeriesData>>
    {
        new WriteResponse<ITimeSeriesData>(slightlyDifferentTimeSeriesDataMock.Object, WriteResponseType.Updated)
    };

        await RunSaveDataTest(Result.Success(partialWriteResponses), timeSeriesDataVm, Times.Never());

        // Assert that IsDirty remains true because the object was only partially updated
        Assert.IsTrue(timeSeriesDataVm.IsDirty, "The isDirty property should remain true for a partial update.");
    }


    /// <summary>
    /// Runs the test for loading data
    /// </summary>
    /// <param name="error">Error returned to the view model when it executes TimeSeriesByViewQuery</param>
    /// <param name="times">Response of the view model: no message box on an NoData error, else a message box</param>
    /// <returns></returns>
    async Task RunTest(Error error, Times times)
    {
        // arrange
        Mock.Get(_sender)
            .Setup(x => x.Send(It.IsAny<TimeSeriesByViewQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(Result.Failure<TimeSeriesByViewQueryResult>(error));

        var model = new TimeSeriesDataViewerViewModel(_ccs, _sender, "1");

        // act
        await model.LoadDataOnStartUpCommand.ExecuteAsync(CancellationToken.None);

        // assert
        Mock.Get(_msgBox)
            .Verify(x => x.ShowDataLoadErrorMsgBoxAsync(It.IsAny<string>(), It.IsAny<Error>()), times);
    }

    /// <summary>
    /// Runs the test for saving data
    /// </summary>
    /// <param name="result">Result returned to the view model when it executes SaveData</param>
    /// <param name="times">Response of the view model: no message box on a success, else a message box</param>
    /// <returns></returns>
    async Task RunSaveDataTest(Result<List<WriteResponse<ITimeSeriesData>>> result, TimeSeriesDataViewModel timeSeriesDataVm, Times times)
    {
        // arrange
        Mock.Get(_sender)
            .Setup(x => x.Send(It.IsAny<TimeSeriesDataWriteCommand>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(result);
        var tsv = Mock.Of<ITimeSeriesView>();
        Mock.Get(_sender)
            .Setup(x => x.Send(It.IsAny<TimeSeriesViewByIdQuery>(), It.IsAny<CancellationToken>()))
            .ReturnsAsync(Result.Success(tsv));

        var model = new TimeSeriesDataViewerViewModel(_ccs, _sender, "1");
        model.TimeSeriesView = tsv;

        var timeSeriesTempDataModel = new TimeSeriesTempDataModel(1, Mock.Of<ITimeSeries>(), new List<DescriptorViewModel>(), "unit", "timeResolution", "hypothesis")
        {
            Data = new List<TimeSeriesDataViewModel> { timeSeriesDataVm }
        };
        model.TimeSeries = new ObservableCollection<TimeSeriesTempDataModel> { timeSeriesTempDataModel };

        // Use reflection to call the private SaveData method
        var saveDataMethod = typeof(TimeSeriesDataViewerViewModel).GetMethod("SaveData", System.Reflection.BindingFlags.NonPublic | System.Reflection.BindingFlags.Instance);
        if (saveDataMethod == null)
        {
            Assert.Fail("SaveData method not found.");
        }

        // act
        Assert.IsTrue(model.TimeSeries.First().Data.First().IsDirty, "The isDirty property was not set correctly.");
        var saveDataTask = (Task)saveDataMethod.Invoke(model, null);
        await saveDataTask;

        // assert
        Mock.Get(_msgBox)
            .Verify(x => x.ShowDataWriteErrorMsgBoxAsync(It.IsAny<string>(), It.IsAny<Error>()), times);
    }



}
