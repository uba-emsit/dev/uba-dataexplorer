﻿using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.Repositories;

namespace UBA.DataExplorer.UseCases.Tests;

class ServiceProviderHelper
{
    readonly IServiceProvider _serviceProvider;

    public ServiceProviderHelper(params object[] transients)
    {
        var collection = new ServiceCollection()
            .AddLogging()
            .AddUseCases();

        foreach (var obj in transients)
            collection.AddTransient(obj.GetType());

        _serviceProvider = collection.BuildServiceProvider();
    }

    public ServiceProviderHelper(Action<IServiceCollection> configure)
    {
        var collection = new ServiceCollection()
            .AddLogging()
            .AddTransient<IRepositoryFactory, TestBackendFactory>(p => new(p))
            .AddTransient<IServiceFactory, TestBackendFactory>(p => new(p))
            .AddUseCases();

        configure(collection);

        _serviceProvider = collection.BuildServiceProvider();
    }

    internal ISender Sender() => _serviceProvider.GetRequiredService<ISender>();
}