﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Domain.Hypotheses.Interfaces;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.TimeSeries;
using UBA.DataExplorer.UseCases.TimeSeriesView;

namespace UBA.DataExplorer.UseCases.Tests.TimeSeries;
[TestClass]
public class QueryHandlerTests
{
    readonly ConnectionModel _connection = new(new(1, string.Empty, string.Empty, string.Empty, string.Empty, 0));
    readonly CancellationToken _cancellationToken = CancellationToken.None;

    [TestMethod]
    public async Task TimeSeriesByIdListQueryTest()
    {
        // Arrange
        var idList = new List<string>();
        var tsRepo = Mock.Of<ITimeSeriesRepository>();

        Mock.Get(tsRepo)
            .Setup(x => x.Read(idList, _cancellationToken))
            .ReturnsAsync(Result.Success(new List<ITimeSeries>()));

        var sender = new ServiceProviderHelper(c => c.AddTransient(_ => tsRepo))
            .Sender();

        // Act
        await sender.Send(new TimeSeriesByIdListQuery(_connection, idList), _cancellationToken);

        // Assert
        Mock.Get(tsRepo).Verify(x => x.Read(idList, _cancellationToken), Times.Once);
    }

    [TestMethod]
    public async Task TimeSeriesByPrimaryKeysQueryTest()
    {
        // Arrange
        var primaryKeys = new List<int>();
        var tsRepo = Mock.Of<ITimeSeriesRepository>();

        Mock.Get(tsRepo)
            .Setup(x => x.Read(primaryKeys, _cancellationToken))
            .ReturnsAsync(Result.Success(new List<ITimeSeries>()));

        var sender = new ServiceProviderHelper(c => c.AddTransient(_ => tsRepo))
            .Sender();

        // Act
        await sender.Send(new TimeSeriesByPrimaryKeysQuery(_connection, primaryKeys), _cancellationToken);

        // Assert
        Mock.Get(tsRepo).Verify(x => x.Read(primaryKeys, _cancellationToken), Times.Once);
    }

    [TestMethod]
    public async Task TimeSeriesByIdQueryTest()
    {
        // Arrange
        var id = "1";
        var tsRepo = Mock.Of<ITimeSeriesRepository>();
        var ts = Mock.Of<ITimeSeries>();

        Mock.Get(tsRepo)
            .Setup(x => x.Read(id, _cancellationToken))
            .ReturnsAsync(Result.Success(ts));

        var sender = new ServiceProviderHelper(c => c.AddTransient(_ => tsRepo))
            .Sender();

        // Act
        await sender.Send(new TimeSeriesByIdQuery(_connection, id), _cancellationToken);

        // Assert
        Mock.Get(tsRepo).Verify(x => x.Read(id, _cancellationToken), Times.Once);
    }

    [TestMethod]
    public async Task TimeSeriesDataByTimeSeriesListQueryTest()
    {
        // Arrange
        const string hypId = "REF";
        var tsList = new List<ITimeSeries>([Mock.Of<ITimeSeries>()]);
        var tsdRepo = Mock.Of<ITimeSeriesDataRepository>();
        var tsd = Mock.Of<ITimeSeriesData>();
        var hypRepo = Mock.Of<IHypothesisRepository>();
        var hyp = Mock.Of<IHypothesis>();
        var timeAxis = Mock.Of<ITimeAxisAbsolute>();
        var timeAxisService = Mock.Of<ITimeAxisService>();
        const TimeResolution res = TimeResolution.Year;
        var from = new DateTimeOffset(new DateTime(1990, 1, 1), TimeSpan.FromHours(1));
        var to = new DateTimeOffset(new DateTime(2020, 1, 1), TimeSpan.FromHours(1));

        Mock.Get(hypRepo)
            .Setup(x => x.Read(hypId, _cancellationToken))
            .ReturnsAsync(Result.Success(hyp));

        Mock.Get(tsdRepo)
            .Setup(x => x.Read(tsList, hyp,
                timeAxis, res, _cancellationToken))
            .ReturnsAsync(Result.Success(new List<ITimeSeriesData> { tsd }));

        Mock.Get(timeAxisService)
            .Setup(x => x.CreateAbsolute(from, to))
            .Returns(timeAxis);

        var sender = new ServiceProviderHelper(c =>
            {
                c.AddTransient(_ => tsdRepo);
                c.AddTransient(_ => hypRepo);
                c.AddTransient(_ => timeAxisService);
            })
            .Sender();

        // Act
        var result = await sender.Send(new TimeSeriesDataByTimeSeriesListQuery(_connection, tsList, hypId, from, to, res), _cancellationToken);

        // Assert
        Mock.Get(hypRepo).Verify(x => x.Read(hypId, _cancellationToken), Times.Once);
        Mock.Get(tsdRepo).Verify(x => x.Read(tsList, hyp, timeAxis, res, _cancellationToken), Times.Once);
        result.IsSuccess.ShouldBeTrue();
        result.Value.Count.ShouldBe(1);
    }

    [TestMethod]
    public async Task MappedTimeSeriesDataByTimeSeriesListQueryTest()
    {
        // Arrange
        const string hypId = "REF";
        var tsList = new List<ITimeSeries>([Mock.Of<ITimeSeries>()]);
        var tsdRepo = Mock.Of<ITimeSeriesDataRepository>();
        var tsd = Mock.Of<IMappedTimeSeriesData>();
        var hypRepo = Mock.Of<IHypothesisRepository>();
        var hyp = Mock.Of<IHypothesis>();
        var timeAxis = Mock.Of<ITimeAxisAbsolute>();
        var timeAxisService = Mock.Of<ITimeAxisService>();
        const TimeResolution res = TimeResolution.Year;
        var from = new DateTimeOffset(new DateTime(1990, 1, 1), TimeSpan.FromHours(1));
        var to = new DateTimeOffset(new DateTime(2020, 1, 1), TimeSpan.FromHours(1));

        Mock.Get(hypRepo)
            .Setup(x => x.Read(hypId, _cancellationToken))
            .ReturnsAsync(Result.Success(hyp));

        Mock.Get(tsdRepo)
            .Setup(x => x.ReadMapped(tsList, hyp,
                timeAxis, res, _cancellationToken))
            .ReturnsAsync(Result.Success(new List<IMappedTimeSeriesData> { tsd }));

        Mock.Get(timeAxisService)
            .Setup(x => x.CreateAbsolute(from, to))
            .Returns(timeAxis);

        var sender = new ServiceProviderHelper(c =>
            {
                c.AddTransient(_ => tsdRepo);
                c.AddTransient(_ => hypRepo);
                c.AddTransient(_ => timeAxisService);
            })
            .Sender();

        // Act
        var result = await sender.Send(new MappedTimeSeriesDataByTimeSeriesListQuery(_connection, tsList, hypId, from, to, res), _cancellationToken);

        // Assert
        Mock.Get(hypRepo).Verify(x => x.Read(hypId, _cancellationToken), Times.Once);
        Mock.Get(tsdRepo).Verify(x => x.ReadMapped(tsList, hyp, timeAxis, res, _cancellationToken), Times.Once);
        result.IsSuccess.ShouldBeTrue();
        result.Value.Count.ShouldBe(1);
    }

    [TestMethod]
    public async Task TimeSeriesViewTreeQueryTest()
    {
        // Arrange
        var tsvRepo = Mock.Of<ITimeSeriesViewRepository>();
        var tree = Mock.Of<ITimeSeriesViewTree>();

        Mock.Get(tsvRepo)
            .Setup(x => x.ReadTree(_cancellationToken))
            .ReturnsAsync(Result.Success(tree));

        var sender = new ServiceProviderHelper(c => c.AddTransient(_ => tsvRepo))
            .Sender();

        // Act
        var result = await sender.Send(new TimeSeriesViewTreeQuery(_connection), _cancellationToken);

        // Assert
        Mock.Get(tsvRepo).Verify(x => x.ReadTree(_cancellationToken), Times.Once);
        result.IsSuccess.ShouldBeTrue();
        result.Value.ShouldBe(tree);
    }
}