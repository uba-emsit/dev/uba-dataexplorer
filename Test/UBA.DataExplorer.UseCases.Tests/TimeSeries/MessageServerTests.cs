﻿using Microsoft.Extensions.DependencyInjection;
using Moq;
using Shouldly;
using UBA.DataExplorer.Core;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Domain.Shared;
using UBA.DataExplorer.Domain.TimeSeries.Interfaces;
using UBA.DataExplorer.UseCases.MessageServer;
using LockResult = UBA.DataExplorer.Core.LockResult;
using ProcessingResult = UBA.DataExplorer.Core.ProcessingResult;

namespace UBA.DataExplorer.UseCases.Tests.TimeSeries;

[TestClass]
public class MessageServerTests
{
    readonly ConnectionModel _connection = new(new(1, string.Empty, string.Empty, string.Empty, string.Empty, 1));
    readonly CancellationToken _cancellationToken = CancellationToken.None;
    readonly AutoResetEvent _autoResetEvent = new(false);
    List<ITimeSeriesView> _changedViews = [];

    [TestMethod]
    public async Task TestChangesLoader()
    {
        // Arrange
        var tsRepo = Mock.Of<ITimeSeriesRepository>();

        Mock.Get(tsRepo)
            .Setup(x => x.Read(It.IsAny<List<int>>(), _cancellationToken))
            .ReturnsAsync(Result.Success(new List<ITimeSeries>()));

        var tsvRepo = Mock.Of<ITimeSeriesViewRepository>();
        var view = Mock.Of<ITimeSeriesView>();

        Mock.Get(tsvRepo)
            .Setup(x => x.Read(It.IsAny<List<int>>(), _cancellationToken))
            .ReturnsAsync(Result.Success<List<ITimeSeriesView>>([view]));

        var sender = new ServiceProviderHelper(c => c
                .AddTransient(_ => tsRepo)
                .AddTransient(_ => tsvRepo))
            .Sender();

        var messageServer = new MessageServerMock();

        var changesLoader = new ChangesLoader(_connection, messageServer, sender);
        // Act

        var subscription = new ChangesLoader.TimeSeriesViewSubscription(OnChange);
        changesLoader.Subscribe(subscription);
        _autoResetEvent.WaitOne(TimeSpan.FromSeconds(10));

        // Assert
        _changedViews.Count.ShouldBe(1);

        // Continue
        subscription.Dispose();
        _changedViews.Clear();

        await Task.Delay(TimeSpan.FromSeconds(2), _cancellationToken);

        _changedViews.ShouldBeEmpty();
    }

    Task OnChange(ChangesLoader.ObjectChangeReason reason, List<ITimeSeriesView> objects, List<int> primaryKeys)
    {
        _changedViews = objects;
        _autoResetEvent.Set();
        return Task.CompletedTask;
    }

    class MessageServerMock : IMessageServer
    {
        Listener? _listener;

        public int LocksReleased { get; private set; }

        public void RegisterListener(Listener listener)
        {
            _listener = listener;
            Task.Run(async () =>
            {
                for (var i = 0; i < 100; i++)
                {
                    await Task.Delay(TimeSpan.FromSeconds(1));
                    _listener?.OnChanged(ObjectChangeReason.ObjectAddNew, [new(0, 1)]);
                }
            });
        }

        public bool UnregisterListener(Listener listener) => true;

        public LockResult Lock(MessageServerObjectClass objectClass, int primaryKey)
        {
            var userModel = new UserModel("Schulz", "Walter");

            return primaryKey switch
            {
                1 => new LockResult.Success(_ => LocksReleased++),
                2 => new LockResult.Locked(userModel, DateTimeOffset.Now),
                _ => new LockResult.Error(ProcessingResult.Failed, "Failed")
            };
        }
    }

    [TestMethod]
    public void TestObjectLocker()
    {
        // Arrange
        var messageServer = new MessageServerMock();
        var locker = new ObjectLocker(messageServer);

        // Act
        using (var lockResult = locker.Lock(MessageServerObjectClass.TimeSeries, 1))
        {
            // Assert
            lockResult.ShouldBeOfType<MessageServer.LockResult.Success>();
            lockResult.IsSuccess.ShouldBeTrue();
            messageServer.LocksReleased.ShouldBe(0);
        }
        messageServer.LocksReleased.ShouldBe(1);

        // Act
        using (var lockResult = locker.Lock(MessageServerObjectClass.TimeSeries, 2))
        {
            // Assert
            lockResult.ShouldBeOfType<MessageServer.LockResult.Locked>();
            var locked = lockResult as MessageServer.LockResult.Locked;
            locked?.LockedSince.ShouldBeGreaterThan(DateTimeOffset.Now - TimeSpan.FromSeconds(10));
            locked?.LockedSince.ShouldBeLessThan(DateTimeOffset.Now + TimeSpan.FromSeconds(10));
            locked?.OtherUser?.Id.ShouldBe("Walter");
            locked?.OtherUser?.Name.ShouldBe("Schulz");
            lockResult.IsSuccess.ShouldBeFalse();
        }
        messageServer.LocksReleased.ShouldBe(1);

        // Act
        using (var lockResult = locker.Lock(MessageServerObjectClass.TimeSeries, 3))
        {
            // Assert
            lockResult.ShouldBeOfType<MessageServer.LockResult.Error>();
            var error = lockResult as MessageServer.LockResult.Error;
            error?.ProcessingResult.ShouldBe(MessageServer.ProcessingResult.Failed);
            error?.ErrorMessage.ShouldBe("Failed");
            lockResult.IsSuccess.ShouldBeFalse();
        }
        messageServer.LocksReleased.ShouldBe(1);
    }
}