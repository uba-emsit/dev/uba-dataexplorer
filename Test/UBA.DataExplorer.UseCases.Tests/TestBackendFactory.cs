﻿using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.Repositories;
using UBA.DataExplorer.Core.Services;
using UBA.DataExplorer.Domain.Shared;

namespace UBA.DataExplorer.UseCases.Tests;

class TestBackendFactory(IServiceProvider provider) : IRepositoryFactory, IServiceFactory
{
    public ILoginRepository GetLoginRepository()
        => provider.GetRequiredService<ILoginRepository>();

    public IDatabaseRepository GetDatabaseRepository()
        => provider.GetRequiredService<IDatabaseRepository>();

    public T GetRepository<T>(ConnectionModel connection) where T : IRepository
        => provider.GetRequiredService<T>();

    public T GetService<T>(ConnectionModel connection) where T : IService
        => provider.GetRequiredService<T>();
}