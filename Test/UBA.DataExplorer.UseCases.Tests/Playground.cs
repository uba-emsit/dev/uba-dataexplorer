using MediatR;
using Microsoft.Extensions.DependencyInjection;
using UBA.DataExplorer.Core.SQL;
using UBA.DataExplorer.Domain.TimeSeries;
using UBA.DataExplorer.UseCases.Abstractions.Logging;
using UBA.DataExplorer.UseCases.Databases;
using UBA.DataExplorer.UseCases.Login;
using UBA.DataExplorer.UseCases.TimeSeries;
using UBA.DataExplorer.UseCases.TimeSeriesView;

namespace UBA.DataExplorer.UseCases.Tests;

[TestClass]
public class Playground
{
    [TestMethod]
    [Ignore]
    public async Task Playground1()
    {
        var provider = new ServiceCollection()
            .AddLogging()
            .AddMediatR(c =>
                c.RegisterServicesFromAssemblyContaining<LoadAvailableProjectDatabasesQuery>()
                    .AddOpenBehavior(typeof(RequestLoggingPipelineBehavior<,>)))
            .AddBackendFactories()
            .BuildServiceProvider();

        var sender = provider.GetRequiredService<ISender>();

        var login = (await sender.Send(new LoginWithWindowsAccountCommand(), CancellationToken.None)).Value.LoginModel;
        var result = await sender.Send(new LoadAvailableProjectDatabasesQuery(login), CancellationToken.None);

        if (result.IsFailure)
            return;

        var openResult = await sender.Send(new OpenDatabaseConnectionCommand(login, result.Value.First(x => x.Id == "ZSE")),
            CancellationToken.None);


        var tsResult = await sender.Send(new TimeSeriesViewTreeQuery(openResult.Value.ConnectionModel), CancellationToken.None);

        Assert.IsTrue(tsResult.IsSuccess);
    }

    [TestMethod]
    [Ignore]
    public async Task Playground2()
    {
        var ctx = CancellationToken.None;

        var provider = new ServiceCollection()
            .AddLogging()
            .AddMediatR(c =>
                c.RegisterServicesFromAssemblyContaining<LoadAvailableProjectDatabasesQuery>()
                    .AddOpenBehavior(typeof(RequestLoggingPipelineBehavior<,>)))
            .AddBackendFactories()
            .BuildServiceProvider();

        var sender = provider.GetRequiredService<ISender>();

        var login = (await sender.Send(new LoginWithWindowsAccountCommand(), ctx)).Value.LoginModel;
        var result = await sender.Send(new LoadAvailableProjectDatabasesQuery(login), ctx);

        var openResult = await sender.Send(new OpenDatabaseConnectionCommand(login, result.Value.First(x => x.Id == "ZSE")), ctx);

        var conn = openResult.Value.ConnectionModel;

        var tsvResult = await sender.Send(new TimeSeriesViewByIdListQuery(conn, ["Waldbrand"]), ctx);

        var tsResult = await sender.Send(new TimeSeriesByViewQuery(conn, tsvResult.Value.First().Id), ctx);

        var tsdResult = await sender.Send(
            new TimeSeriesDataByTimeSeriesListQuery(conn, tsResult.Value.TimeSeries, "REF",
                DateTime.Now.AddYears(-5), DateTime.Now.AddYears(-4), TimeResolution.Year), ctx);

        var firstTsData = tsdResult.Value.First().Value.First();

        var write = await sender.Send(new TimeSeriesDataWriteCommand(conn, [firstTsData]), ctx);

        Assert.IsTrue(write.IsSuccess);
    }

    [TestMethod]
    [Ignore]
    public async Task Playground3()
    {
        var ctx = CancellationToken.None;

        var provider = new ServiceCollection()
            .AddLogging()
            .AddMediatR(c =>
                c.RegisterServicesFromAssemblyContaining<LoadAvailableProjectDatabasesQuery>()
                    .AddOpenBehavior(typeof(RequestLoggingPipelineBehavior<,>)))
            .AddBackendFactories()
            .BuildServiceProvider();

        var sender = provider.GetRequiredService<ISender>();

        var login = (await sender.Send(new LoginWithWindowsAccountCommand(), ctx)).Value.LoginModel;
        var result = await sender.Send(new LoadAvailableProjectDatabasesQuery(login), ctx);

        var openResult = await sender.Send(new OpenDatabaseConnectionCommand(login, result.Value.First(x => x.Id == "ZSE")), ctx);

        var tsvResult = await sender.Send(new TimeSeriesViewByIdQuery(openResult.Value.ConnectionModel, "Waldbrand"), ctx);

        foreach (var x in tsvResult.Value.TimeSettings.TimeRuleSettings)
        {
            _ = x.GetRangeFromPeriod();
        }

        Assert.IsTrue(tsvResult.IsSuccess);
    }
}