# UBA DataExplorer and UBA EmissionInventories

Environmental data software developed by the German Umweltbundesamt (UBA).

## Data model

Allows for the modelling of environmental data from all domains using:

* Tabular data
* Time series data
* Combined datasets of static and time-variant data in forms

## Core functionality

All data are version controlled and can be extensively documented. Detailed access right settings allow for fine-grained control of data dissemination. In-system data processing and calculation as well as reporting of data in defined formats are fully supported.

## Data storage backends

The following options for backend data storage are supported:

* MS SQL via Seven2one's Mesap Framework
* GraphQL with Hasura
* File (for single user scenarios and testing)

## Architecture

Please refer to the [architecture documentation](Documentation/architecture.md).

## User interfaces

### DataExplorer

Generic environmental data browser, which can be used to explore dataset (such as time series) instantiated from the data model described above.

### EmissionInventories

Domain-specific software based on the same contents but tailored towards the needs of emission inventory experts.

