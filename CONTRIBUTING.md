# Ready to contribute?

We welcome your contribution to the UBA DataExplorer project!

### Guidelines to follow for your code to be accepted into  the `main` branch:

* Format all files using the default Visual Studio profile and the .editorconfig provided
    * The easiest way to achieve this is to enable "format on save" in Visual Studio
* All names, comments and other text elements are in English (incl. generated files)
* All non-trivial code is covered by unit tests
* All relevant types and methods are documented
* Only comment inline where unexpected things happen
* Only commit files that need to be part of the repository and are not user-specific
* Before creating a pull request
    * Fix all errors and lint warnings
    * Run all test successfully
    * Study the "compare to main" output and make sure to only include relevant changes
    * Aim at covering only one topic/feature per pull request

### Additional guidance for users with merge privileges:

* Wait 24h for feedback on your pull request, then feel free to merge
* Write concise yet complete commit messages for merge commits
